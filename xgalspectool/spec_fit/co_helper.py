# -*- coding: utf-8 -*-

import numpy as np
import os
from pathlib import Path
from xgalspectool.fit_algorithm import em_fit_algorithm
from scipy.constants import c as speed_of_light


class COSpecHelper(em_fit_algorithm.EmissionLineFit):

    def __init__(self, co10_spec_path=None, co21_spec_path=None, co_redshift=None, co_data_format=None,
                 antenna_convert_fact=1, **kwargs):
        """
        Class to access CO spectra
        """
        super().__init__(**kwargs)

        self.co10_spec_path = co10_spec_path
        self.co21_spec_path = co21_spec_path

        self.co_redshift = co_redshift

        if co_data_format is None:
            self.co_data_format = 'class_reduced'
        else:
            self.co_data_format = co_data_format

        self.vel_co10 = None
        self.wave_co10 = None
        self.flux_co10 = None

        self.vel_co21 = None
        self.wave_co21 = None
        self.flux_co21 = None

        # if there is an antenna conversion factor to convert the observed spetrum from antenna temperature to Jy
        self.antenna_convert_fact = antenna_convert_fact

    def load_co_spectrum(self):
        if self.co10_spec_path is not None:
            spectrum_co10 = np.genfromtxt(self.co10_spec_path)
            if self.co_data_format == 'class_reduced':
                self.vel_co10 = np.flip(spectrum_co10[:, 0])
                self.flux_co10 = np.flip(spectrum_co10[:, 1]) * 1000
                self.wave_co10 = (self.vel_co10 / (speed_of_light * 1e-3) * self.get_co_obs_wave(co_line='co10') +
                              self.get_co_obs_wave(co_line='co10'))
            elif self.co_data_format == 'xcoldgass':
                self.vel_co10 = np.flip(spectrum_co10[:, 0]) - speed_of_light * 1e-3 * self.co_redshift
                self.flux_co10 = np.flip(spectrum_co10[:, 2])
                self.wave_co10 = (self.vel_co10 / (speed_of_light * 1e-3) * self.get_co_obs_wave(co_line='co10') +
                              self.get_co_obs_wave(co_line='co10'))
            elif self.co_data_format == 'chung_2009':
                frequency_co10 = np.flip(spectrum_co10[:, 0])
                self.wave_co10 = speed_of_light / frequency_co10 * 10
                self.vel_co10 = ((speed_of_light * 1e-3) * (self.wave_co10 - self.get_co_obs_wave(co_line='co10')) /
                                 self.get_co_obs_wave(co_line='co10'))
                self.flux_co10 = np.flip(spectrum_co10[:, 1])
            elif self.co_data_format == 'bauermeister_2013':
                self.vel_co10 = spectrum_co10[:, 0]
                self.flux_co10 = spectrum_co10[:, 1]
                self.wave_co10 = (self.vel_co10 / (speed_of_light * 1e-3) * self.get_co_obs_wave(co_line='co10') +
                              self.get_co_obs_wave(co_line='co10'))
            elif self.co_data_format == 'alma_extract':
                self.vel_co10 = spectrum_co10[:, 0] - speed_of_light * 1e-3 * self.co_redshift
                self.flux_co10 = spectrum_co10[:, 1] * 1000
                self.wave_co10 = (self.vel_co10 / (speed_of_light * 1e-3) * self.get_co_obs_wave(co_line='co10') +
                              self.get_co_obs_wave(co_line='co10'))


            self.flux_co10 *= self.antenna_convert_fact

        if self.co21_spec_path is not None:
            spectrum_co21 = np.genfromtxt(self.co21_spec_path)
            if self.co_data_format == 'class_reduced':
                self.vel_co21 = np.flip(spectrum_co21[:, 0])
                self.flux_co21 = np.flip(spectrum_co21[:, 1]) * 1000
            elif self.co_data_format == 'xcoldgass':
                self.vel_co21 = np.flip(spectrum_co21[:, 0]) - speed_of_light * 1e-3 * self.co_redshift
                self.flux_co21 = np.flip(spectrum_co21[:, 2])
            self.wave_co21 = (self.vel_co21 / (speed_of_light * 1e-3) * self.get_co_obs_wave(co_line='co21') +
                              self.get_co_obs_wave(co_line='co21'))
            self.flux_co21 *= self.antenna_convert_fact

    def get_co_obs_wave(self, co_line='co10'):
        if co_line == 'co10':
            return self.co10_vac_wave * (1 + self.co_redshift)
        elif co_line == 'co21':
            return self.co21_vac_wave * (1 + self.co_redshift)
        else:
            raise KeyError('line must be co10 or co21')

    def rebin_co_spectrum(self, vel_bins=None, vel_bin_width=None, wave_bin_width=None, co_line='co10'):
        if (vel_bins is not None) & (vel_bin_width is None) & (wave_bin_width is None):
            if co_line == 'co10':
                centre_of_new_vel_bins_co10 = (vel_bins[:-1] + vel_bins[1:]) / 2
                new_flux_co10 = np.zeros(len(centre_of_new_vel_bins_co10))
                centre_of_new_wave_bins_co10 = np.zeros(len(centre_of_new_vel_bins_co10))
                print(vel_bins)
                print(len(vel_bins))
                print(len(centre_of_new_vel_bins_co10))
                for bin_index in range(len(centre_of_new_vel_bins_co10)):
                    selected_velocities = ((self.vel_co10 > vel_bins[bin_index]) &
                                           (self.vel_co10 < vel_bins[bin_index + 1]))
                    new_flux_co10[bin_index] = np.nanmean(self.flux_co10[selected_velocities])
                    centre_of_new_wave_bins_co10[bin_index] = np.nanmean(self.wave_co10[selected_velocities])
                self.vel_co10 = centre_of_new_vel_bins_co10
                self.wave_co10 = centre_of_new_wave_bins_co10
                self.flux_co10 = new_flux_co10
            if co_line == 'co21':
                centre_of_new_vel_bins_co21 = (vel_bins[:-1] + vel_bins[1:]) / 2
                new_flux_co21 = np.zeros(len(centre_of_new_vel_bins_co21))
                centre_of_new_wave_bins_co21 = np.zeros(len(centre_of_new_vel_bins_co21))
                for bin_index in range(len(centre_of_new_vel_bins_co21)):
                    selected_velocities = ((self.vel_co21 > vel_bins[bin_index]) &
                                           (self.vel_co21 < vel_bins[bin_index + 1]))
                    new_flux_co21[bin_index] = np.nanmean(self.flux_co21[selected_velocities])
                    centre_of_new_wave_bins_co21[bin_index] = np.nanmean(self.wave_co21[selected_velocities])
                self.vel_co21 = centre_of_new_vel_bins_co21
                self.wave_co21 = centre_of_new_wave_bins_co21
                self.flux_co21 = new_flux_co21
        elif (vel_bin_width is not None) & (wave_bin_width is None) & (vel_bins is None):
            if co_line == 'co10':
                new_vel_bins = np.arange(min(self.vel_co10), max(self.vel_co10), vel_bin_width)
                centre_of_new_vel_bins_co10 = (new_vel_bins[:-1] + new_vel_bins[1:]) / 2
                new_flux_co10 = np.zeros(len(centre_of_new_vel_bins_co10))
                centre_of_new_wave_bins_co10 = np.zeros(len(centre_of_new_vel_bins_co10))
                for bin_index in range(len(centre_of_new_vel_bins_co10)):
                    selected_velocities = ((self.vel_co10 > new_vel_bins[bin_index]) &
                                           (self.vel_co10 < new_vel_bins[bin_index + 1]))
                    new_flux_co10[bin_index] = np.nanmean(self.flux_co10[selected_velocities])
                    centre_of_new_wave_bins_co10[bin_index] = np.nanmean(self.wave_co10[selected_velocities])
                self.vel_co10 = centre_of_new_vel_bins_co10
                self.wave_co10 = centre_of_new_wave_bins_co10
                self.flux_co10 = new_flux_co10
            elif co_line == 'co21':
                new_vel_bins = np.arange(min(self.vel_co21), max(self.vel_co21), vel_bin_width)
                centre_of_new_vel_bins_co21 = (new_vel_bins[:-1] + new_vel_bins[1:]) / 2
                new_flux_co21 = np.zeros(len(centre_of_new_vel_bins_co21))
                centre_of_new_wave_bins_co21 = np.zeros(len(centre_of_new_vel_bins_co21))
                for bin_index in range(len(centre_of_new_vel_bins_co21)):
                    selected_velocities = ((self.vel_co21 > new_vel_bins[bin_index]) &
                                           (self.vel_co21 < new_vel_bins[bin_index + 1]))
                    new_flux_co21[bin_index] = np.nanmean(self.flux_co21[selected_velocities])
                    centre_of_new_wave_bins_co21[bin_index] = np.nanmean(self.wave_co21[selected_velocities])
                self.vel_co21 = centre_of_new_vel_bins_co21
                self.wave_co21 = centre_of_new_wave_bins_co21
                self.flux_co21 = new_flux_co21
            else:
                raise KeyError('co_line not understand must be co10 or co21')
        elif (vel_bin_width is None) & (wave_bin_width is not None) & (vel_bins is None):
            if co_line == 'co10':
                new_wave_bins = np.arange(min(self.wave_co10), max(self.wave_co10), vel_bin_width)
                centre_of_new_wave_bins_co10 = (new_wave_bins[:-1] + new_wave_bins[1:]) / 2
                new_flux_co10 = np.zeros(len(centre_of_new_wave_bins_co10))
                centre_of_new_vel_bins_co10 = np.zeros(len(centre_of_new_wave_bins_co10))
                for bin_index in range(len(centre_of_new_wave_bins_co10)):
                    selected_velocities = ((self.wave_co10 > new_wave_bins[bin_index]) &
                                           (self.wave_co10 < new_wave_bins[bin_index + 1]))
                    new_flux_co10[bin_index] = np.nanmean(self.flux_co10[selected_velocities])
                    centre_of_new_vel_bins_co10[bin_index] = np.nanmean(self.wave_co10[selected_velocities])
                self.vel_co10 = centre_of_new_vel_bins_co10
                self.wave_co10 = centre_of_new_wave_bins_co10
                self.flux_co10 = new_flux_co10
            elif co_line == 'co21':
                new_wave_bins = np.arange(min(self.wave_co21), max(self.wave_co21), vel_bin_width)
                centre_of_new_wave_bins_co21 = (new_wave_bins[:-1] + new_wave_bins[1:]) / 2
                new_flux_co21 = np.zeros(len(centre_of_new_wave_bins_co21))
                centre_of_new_vel_bins_co21 = np.zeros(len(centre_of_new_wave_bins_co21))
                for bin_index in range(len(centre_of_new_wave_bins_co21)):
                    selected_velocities = ((self.wave_co21 > new_wave_bins[bin_index]) &
                                           (self.wave_co21 < new_wave_bins[bin_index + 1]))
                    new_flux_co21[bin_index] = np.nanmean(self.flux_co21[selected_velocities])
                    centre_of_new_vel_bins_co21[bin_index] = np.nanmean(self.wave_co21[selected_velocities])
                self.vel_co21 = centre_of_new_vel_bins_co21
                self.wave_co21 = centre_of_new_wave_bins_co21
                self.flux_co21 = new_flux_co21
            else:
                raise KeyError('co_line not understand must be co10 or co21')
        else:
            raise KeyError('you can only choose one new bin width (vel_bin_width or wave_bin_width) '
                           'the other must be set to None. '
                           'Or you directly give a new bin array and set the other args to None')

    def get_co_mask(self, vel_radius=1000, wave_radius=None, co_line='co10'):
        line_position = self.get_co_obs_wave(co_line=co_line)
        if (vel_radius is not None) & (wave_radius is None):
            return ((getattr(self, 'vel_%s' % co_line) > (- vel_radius)) &
                    (getattr(self, 'vel_%s' % co_line) < (+ vel_radius)))
        elif (vel_radius is None) & (wave_radius is not None):
            return ((getattr(self, 'wave_%s' % co_line) > (line_position - wave_radius)) &
                    (getattr(self, 'wave_%s' % co_line) < (line_position + wave_radius)))
        else:
            raise KeyError('you can only choose one radius (vel_radius or wave_radius) '
                           'the the other must be set to None.')

    def get_co_max_line_flux(self, vel_radius=1000, wave_radius=None, co_line='co10'):
        line_mask = self.get_co_mask(vel_radius=vel_radius, wave_radius=wave_radius, co_line=co_line)
        return np.nanmax(getattr(self, 'flux_%s' % co_line)[line_mask])


