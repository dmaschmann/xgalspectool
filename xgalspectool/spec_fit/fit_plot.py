from xgalspectool.fit_algorithm import em_fit_algorithm
from xgalimgtool.imgplot import ImgPlot
from xgaltool.basic_attributes import PublishingQualityParameters

import numpy as np
from scipy.constants import c as speed_of_light

import matplotlib.pyplot as plt
from astropy.wcs import WCS


class FitPlot(em_fit_algorithm.EmissionLineFit, PublishingQualityParameters):

    def __init__(self, ):

        super().__init__()

    def plot_single_gauss_line_hi(self, ax, ax_residuals, fit_result_dict, vel, flux, sys_vel,
                                         fontsize=13, xlabel=False, ylabel=False, vel_radius=2000, vel_tick_steps=500,
                                         flux_units='mJy'):

        # get parameters
        position_hi = fit_result_dict['mu_hi'] - sys_vel
        sigma_hi = fit_result_dict['sigma_hi']
        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))
        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)
        # calculate all gauss functions
        gauss_hi = self.gaussian(high_res_vel, amp=fit_result_dict['amp_hi'], mu=position_hi,
                                 sigma=sigma_hi)
        # himpute fit value for residuals
        gauss_hi_residual = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_hi'], mu=position_hi,
                                          sigma=sigma_hi)
        residuals_hi = (flux[mask_vel] - gauss_hi_residual)

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')
        # plot fit functions
        ax.plot(high_res_vel, gauss_hi, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_hi, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0], color='g', linewidth=self.line_width)
        ax.text(0.8, 0.85, 'HI', transform=ax.transAxes,  fontsize=fontsize)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_double_gauss_line_hi(self, ax, ax_residuals, fit_result_dict, vel, flux, sys_vel,
                                         fontsize=13, xlabel=False, ylabel=False, vel_radius=2000, vel_tick_steps=500,
                                         flux_units='mJy'):

        # get parameters
        position_hi_1 = fit_result_dict['mu_hi_1'] - sys_vel
        position_hi_2 = fit_result_dict['mu_hi_2'] - sys_vel

        sigma_hi_1 = fit_result_dict['sigma_hi_1']
        sigma_hi_2 = fit_result_dict['sigma_hi_2']

        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))


        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)

        # calculate all gauss functions
        gauss_hi_1 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_hi_1'],
                                     mu=position_hi_1, sigma=sigma_hi_1)
        gauss_hi_2 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_hi_2'],
                                     mu=position_hi_2, sigma=sigma_hi_2)

        # himpute fit value for residuals
        gauss_hi_residual_1 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_hi_1'],
                                              mu=position_hi_1, sigma=sigma_hi_1)

        gauss_hi_residual_2 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_hi_2'],
                                              mu=position_hi_2, sigma=sigma_hi_2)

        residuals_hi = (flux[mask_vel] - (gauss_hi_residual_1 + gauss_hi_residual_2))

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')

        # plot fit functions
        ax.plot(high_res_vel, gauss_hi_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_hi_2, color='r', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_hi_1 + gauss_hi_2, color='g', linewidth=self.line_width)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_hi, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0],
                               color='g', linewidth=self.line_width)

        ax.text(0.8, 0.85, 'HI', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_triple_gauss_line_hi(self, ax, ax_residuals, fit_result_dict, vel, flux, sys_vel,
                                         fontsize=13, xlabel=False, ylabel=False, vel_radius=2000, vel_tick_steps=500,
                                         flux_units='mJy'):

        # get parameters
        position_hi_1 = fit_result_dict['mu_hi_1'] - sys_vel
        position_hi_2 = fit_result_dict['mu_hi_2'] - sys_vel
        position_hi_3 = fit_result_dict['mu_hi_3'] - sys_vel

        sigma_hi_1 = fit_result_dict['sigma_hi_1']
        sigma_hi_2 = fit_result_dict['sigma_hi_2']
        sigma_hi_3 = fit_result_dict['sigma_hi_3']

        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))

        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)

        # calculate all gauss functions
        gauss_hi_1 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_hi_1'],
                                   mu=position_hi_1, sigma=sigma_hi_1)
        gauss_hi_2 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_hi_2'],
                                   mu=position_hi_2, sigma=sigma_hi_2)
        gauss_hi_3 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_hi_3'],
                                   mu=position_hi_3, sigma=sigma_hi_3)

        # himpute fit value for residuals
        gauss_hi_residual_1 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_hi_1'],
                                            mu=position_hi_1, sigma=sigma_hi_1)

        gauss_hi_residual_2 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_hi_2'],
                                            mu=position_hi_2, sigma=sigma_hi_2)

        gauss_hi_residual_3 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_hi_3'],
                                            mu=position_hi_3, sigma=sigma_hi_3)

        residuals_hi = (flux[mask_vel] - (gauss_hi_residual_1 + gauss_hi_residual_2 + gauss_hi_residual_3))

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')

        # plot fit functions
        ax.plot(high_res_vel, gauss_hi_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_hi_2, color='y', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_hi_3, color='r', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_hi_1 + gauss_hi_2 + gauss_hi_3, color='g', linewidth=self.line_width)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_hi, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0],
                               color='g', linewidth=self.line_width)

        ax.text(0.8, 0.85, 'HI', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_single_gauss_fit_results_co(self, ax, ax_residuals, fit_result_dict, line, vel, flux, sys_vel,
                                         fontsize, xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                         flux_units='mJy'):

        # get parameters
        position_co = fit_result_dict['mu_%s' % line] - sys_vel
        sigma_co = fit_result_dict['sigma_%s' % line]
        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))
        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)
        # calculate all gauss functions
        gauss_co = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s' % line], mu=position_co,
                                 sigma=sigma_co)
        # compute fit value for residuals
        gauss_co_residual = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s' % line], mu=position_co,
                                          sigma=sigma_co)
        residuals_co = (flux[mask_vel] - gauss_co_residual)

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')
        # plot fit functions
        ax.plot(high_res_vel, gauss_co, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_co, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0], color='g', linewidth=self.line_width)
        ax.text(0.75, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_double_gauss_fit_results_co(self, ax, ax_residuals, fit_result_dict, line, vel, flux, sys_vel,
                                         fontsize, xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                         flux_units='mJy'):

        # get parameters
        position_co_1 = fit_result_dict['mu_%s_1' % line] - sys_vel
        position_co_2 = fit_result_dict['mu_%s_2' % line] - sys_vel

        sigma_co_1 = fit_result_dict['sigma_%s_1' % line]
        sigma_co_2 = fit_result_dict['sigma_%s_2' % line]

        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))

        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)

        # calculate all gauss functions
        gauss_co_1 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_1' % line],
                                     mu=position_co_1, sigma=sigma_co_1)
        gauss_co_2 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_2' % line],
                                     mu=position_co_2, sigma=sigma_co_2)

        # compute fit value for residuals
        gauss_co_residual_1 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_1' % line],
                                              mu=position_co_1, sigma=sigma_co_1)

        gauss_co_residual_2 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_2' % line],
                                              mu=position_co_2, sigma=sigma_co_2)

        residuals_co = (flux[mask_vel] - (gauss_co_residual_1 + gauss_co_residual_2))

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')

        # plot fit functions
        ax.plot(high_res_vel, gauss_co_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_2, color='r', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_1 + gauss_co_2, color='g', linewidth=self.line_width)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_co, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0],
                               color='g', linewidth=self.line_width)

        ax.text(0.75, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_triple_gauss_fit_results_co(self, ax, ax_residuals, fit_result_dict, line, vel, flux, sys_vel,
                                         fontsize, xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                         flux_units='mJy'):

        # get parameters
        position_co_1 = fit_result_dict['mu_%s_1' % line] - sys_vel
        position_co_2 = fit_result_dict['mu_%s_2' % line] - sys_vel
        position_co_3 = fit_result_dict['mu_%s_3' % line] - sys_vel

        sigma_co_1 = fit_result_dict['sigma_%s_1' % line]
        sigma_co_2 = fit_result_dict['sigma_%s_2' % line]
        sigma_co_3 = fit_result_dict['sigma_%s_3' % line]

        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))

        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)

        # calculate all gauss functions
        gauss_co_1 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_1' % line],
                                   mu=position_co_1, sigma=sigma_co_1)
        gauss_co_2 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_2' % line],
                                   mu=position_co_2, sigma=sigma_co_2)
        gauss_co_3 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_3' % line],
                                   mu=position_co_3, sigma=sigma_co_3)

        # compute fit value for residuals
        gauss_co_residual_1 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_1' % line],
                                            mu=position_co_1, sigma=sigma_co_1)

        gauss_co_residual_2 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_2' % line],
                                            mu=position_co_2, sigma=sigma_co_2)

        gauss_co_residual_3 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_3' % line],
                                            mu=position_co_3, sigma=sigma_co_3)

        residuals_co = (flux[mask_vel] - (gauss_co_residual_1 + gauss_co_residual_2 + gauss_co_residual_3))

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')

        # plot fit functions
        ax.plot(high_res_vel, gauss_co_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_2, color='y', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_3, color='r', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_1 + gauss_co_2 + gauss_co_3, color='g', linewidth=self.line_width)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_co, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0],
                               color='g', linewidth=self.line_width)

        ax.text(0.8, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_single_gauss_fit_results_custom_co(self, ax, ax_residuals, fit_result_dict, line, vel, flux, sys_vel,
                                         fontsize, xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                         flux_units='mJy', large_frame=False):

        # get parameters
        position_co = fit_result_dict['mu_%s_1' % line] - sys_vel
        sigma_co = fit_result_dict['sigma_%s_1' % line]
        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))
        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)
        # calculate all gauss functions
        gauss_co = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_1' % line], mu=position_co,
                                 sigma=sigma_co)
        # compute fit value for residuals
        gauss_co_residual = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_1' % line], mu=position_co,
                                          sigma=sigma_co)
        residuals_co = (flux[mask_vel] - gauss_co_residual)

        # get region where emission line is situated
        smaller_line_limit = fit_result_dict['mu_%s_1' % line] - sys_vel - 3 * fit_result_dict['sigma_%s_1' % line]
        larger_line_limit = fit_result_dict['mu_%s_1' % line] - sys_vel + 3 * fit_result_dict['sigma_%s_1' % line]
        mask_line = (vel > smaller_line_limit) & (vel < larger_line_limit)
        rms = fit_result_dict['rms_%s' % line]

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')
        # plot fit functions
        ax.plot(high_res_vel, gauss_co, color='g', linewidth=self.line_width)
        ax.fill_between(vel, flux, where=mask_line, color='grey', edgecolor=None, step="mid", alpha=0.5)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_co, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0], color='g', linewidth=self.line_width)
        ax_residuals.plot([-1000, 1000], [1 * rms, 1 * rms], color='y')
        ax_residuals.plot([-1000, 1000], [- 1 * rms, - 1 * rms], color='y')

        if large_frame:
            ax.text(0.7, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)
        else:
            ax.text(0.8, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_double_gauss_fit_results_custom_co(self, ax, ax_residuals, fit_result_dict, line, vel, flux, sys_vel,
                                         fontsize, xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                         flux_units='mJy', large_frame=False):

        # get parameters
        position_co_1 = fit_result_dict['mu_%s_1' % line] - sys_vel
        position_co_2 = fit_result_dict['mu_%s_2' % line] - sys_vel

        sigma_co_1 = fit_result_dict['sigma_%s_1' % line]
        sigma_co_2 = fit_result_dict['sigma_%s_2' % line]

        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))

        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)

        # calculate all gauss functions
        gauss_co_1 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_1' % line],
                                     mu=position_co_1, sigma=sigma_co_1)
        gauss_co_2 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_2' % line],
                                     mu=position_co_2, sigma=sigma_co_2)

        # compute fit value for residuals
        gauss_co_residual_1 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_1' % line],
                                              mu=position_co_1, sigma=sigma_co_1)

        gauss_co_residual_2 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_2' % line],
                                              mu=position_co_2, sigma=sigma_co_2)

        residuals_co = (flux[mask_vel] - (gauss_co_residual_1 + gauss_co_residual_2))

        # get region where emission line is situated
        smaller_line_limit = min(fit_result_dict['mu_%s_1' % line] - sys_vel - 3 * fit_result_dict['sigma_%s_1' % line],
                                 fit_result_dict['mu_%s_2' % line] - sys_vel - 3 * fit_result_dict['sigma_%s_2' % line])
        larger_line_limit = max(fit_result_dict['mu_%s_1' % line] - sys_vel + 3 * fit_result_dict['sigma_%s_1' % line],
                                fit_result_dict['mu_%s_2' % line] - sys_vel + 3 * fit_result_dict['sigma_%s_2' % line])
        mask_line = (vel > smaller_line_limit) & (vel < larger_line_limit)
        rms = fit_result_dict['rms_%s' % line]

        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')

        # plot fit functions
        ax.plot(high_res_vel, gauss_co_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_2, color='r', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_1 + gauss_co_2, color='g', linewidth=self.line_width)
        ax.fill_between(vel, flux, where=mask_line, color='grey', edgecolor=None, step="mid", alpha=0.5)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_co, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0],
                               color='g', linewidth=self.line_width)
        ax_residuals.plot([-1000, 1000], [1 * rms, 1 * rms], color='y')
        ax_residuals.plot([-1000, 1000], [- 1 * rms, - 1 * rms], color='y')

        if large_frame:
            ax.text(0.7, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)
        else:
            ax.text(0.8, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)
        if fit_result_dict['combined_fit_%s' % line]:
            ax.text(0.05, 0.85, 'Comb. fit', transform=ax.transAxes, fontsize=fontsize + 2)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_triple_gauss_fit_results_custom_co(self, ax, ax_residuals, fit_result_dict, line, vel, flux, sys_vel,
                                         fontsize, xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=200,
                                         flux_units='mJy', large_frame=False):

        # get parameters
        position_co_1 = fit_result_dict['mu_%s_1' % line] - sys_vel
        position_co_2 = fit_result_dict['mu_%s_2' % line] - sys_vel
        position_co_3 = fit_result_dict['mu_%s_3' % line] - sys_vel

        sigma_co_1 = fit_result_dict['sigma_%s_1' % line]
        sigma_co_2 = fit_result_dict['sigma_%s_2' % line]
        sigma_co_3 = fit_result_dict['sigma_%s_3' % line]

        # wavelength for position
        mask_vel = (vel > (- vel_radius)) & (vel < (+ vel_radius))

        # high resolution wave
        high_res_vel = np.linspace(min(vel[mask_vel]), max(vel[mask_vel]), 1000)

        # calculate all gauss functions
        gauss_co_1 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_1' % line],
                                   mu=position_co_1, sigma=sigma_co_1)
        gauss_co_2 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_2' % line],
                                   mu=position_co_2, sigma=sigma_co_2)
        gauss_co_3 = self.gaussian(high_res_vel, amp=fit_result_dict['amp_%s_3' % line],
                                   mu=position_co_3, sigma=sigma_co_3)

        # compute fit value for residuals
        gauss_co_residual_1 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_1' % line],
                                            mu=position_co_1, sigma=sigma_co_1)

        gauss_co_residual_2 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_2' % line],
                                            mu=position_co_2, sigma=sigma_co_2)

        gauss_co_residual_3 = self.gaussian(vel[mask_vel], amp=fit_result_dict['amp_%s_3' % line],
                                            mu=position_co_3, sigma=sigma_co_3)

        residuals_co = (flux[mask_vel] - (gauss_co_residual_1 + gauss_co_residual_2 + gauss_co_residual_3))

        # get region where emission line is situated
        smaller_line_limit = min(fit_result_dict['mu_%s_1' % line] - sys_vel - 3 * fit_result_dict['sigma_%s_1' % line],
                                 fit_result_dict['mu_%s_2' % line] - sys_vel - 3 * fit_result_dict['sigma_%s_2' % line],
                                 fit_result_dict['mu_%s_3' % line] - sys_vel - 3 * fit_result_dict['sigma_%s_3' % line])
        larger_line_limit = max(fit_result_dict['mu_%s_1' % line] - sys_vel + 3 * fit_result_dict['sigma_%s_1' % line],
                                fit_result_dict['mu_%s_2' % line] - sys_vel + 3 * fit_result_dict['sigma_%s_2' % line],
                                fit_result_dict['mu_%s_3' % line] - sys_vel + 3 * fit_result_dict['sigma_%s_3' % line])
        mask_line = (vel > smaller_line_limit) & (vel < larger_line_limit)
        rms = fit_result_dict['rms_%s' % line]
        # plot
        # data
        ax.step(vel[mask_vel], flux[mask_vel], where='mid', linewidth=2, color='k')

        # plot fit functions
        ax.plot(high_res_vel, gauss_co_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_2, color='orange', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_3, color='r', linewidth=self.line_width)
        ax.plot(high_res_vel, gauss_co_1 + gauss_co_2 + gauss_co_3, color='g', linewidth=self.line_width)
        ax.fill_between(vel, flux, where=mask_line, color='grey', edgecolor=None, step="mid", alpha=0.5)

        # plot residuals
        ax_residuals.step(vel[mask_vel], residuals_co, where='mid', linewidth=2, color='k')
        ax_residuals.plot([min(vel[mask_vel]), max(vel[mask_vel])], [0, 0],
                               color='g', linewidth=self.line_width)
        ax_residuals.plot([-1000, 1000], [1 * rms, 1 * rms], color='y')
        ax_residuals.plot([-1000, 1000], [- 1 * rms, - 1 * rms], color='y')

        if large_frame:
            ax.text(0.7, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)
        else:
            ax.text(0.8, 0.85, line.upper(), transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
        ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 4)

        ax.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)
        ax_residuals.set_xlim(min(vel[mask_vel]) - 1, max(vel[mask_vel]) + 1)

        if xlabel:
            ax_residuals.set_xlabel(r'Velocity, km s $^{-1}$', fontsize=fontsize, labelpad=-4)
        if ylabel:
            ax.set_ylabel(r'T$_{\rm mb}$, %s' % flux_units, labelpad=-2, fontsize=fontsize)

    def plot_single_gauss_isolated_line(self, ax, ax_residuals, line, fit_result_dict, inst_broad_dict, wave,
                                        em_flux, em_flux_err, sys_vel, fontsize, x_format='vel',
                                        xlabel=False, ylabel=False, vel_radius=1000., vel_tick_steps=500,
                                        wave_tick_steps=10):

        if x_format == 'vel':
            position_peak = fit_result_dict['mu_%s' % self.em_wavelength[line]['transition']] - sys_vel
            sigma_peak = np.sqrt(fit_result_dict['sigma_%s' % self.em_wavelength[line]['transition']] ** 2 +
                                 inst_broad_dict[line] ** 2)
            observed_line = self.em_wavelength[line]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            x_data = speed_of_light * 1e-3 * (wave - observed_line) / self.em_wavelength[line]['vac_wave']
            if isinstance(vel_radius, float):
                mask_x_data = (x_data > (position_peak - vel_radius)) & (x_data < (position_peak + vel_radius))
            else:
                mask_x_data = (x_data > (position_peak + vel_radius[0])) & (x_data < (position_peak + vel_radius[1]))
        elif x_format == 'wave':
            position_peak = (fit_result_dict['mu_%s' % self.em_wavelength[line]['transition']] /
                             (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                             self.em_wavelength[line]['vac_wave'])
            sigma_peak = (np.sqrt(fit_result_dict['sigma_%s' % self.em_wavelength[line]['transition']] ** 2 +
                                  inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                          self.em_wavelength[line]['vac_wave'])
            x_data = wave
            mask_x_data = ((wave > (position_peak -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])) &
                           (wave < (position_peak +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_x_data = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)

        # calculate all gauss functions
        gauss_peak = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i' % line], mu=position_peak,
                                   sigma=sigma_peak)

        # compute fit value for residuals
        gauss_peak_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i' % line], mu=position_peak,
                                            sigma=sigma_peak)
        # calculate residuals
        residuals_peak = (em_flux[mask_x_data] - gauss_peak_residual)
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_x_data, gauss_peak, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_peak, yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0], color='g',
                          linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_peak + 1.3 * sigma_peak, fit_result_dict['amp_%i' % line],
        #         self.em_wavelength[line]['plot_name'], fontsize=fontsize)
        ax.text(0.04, 0.85, self.em_wavelength[line]['plot_name'], transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))

        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_isolated_line(self, ax, ax_residuals, line, fit_result_dict, inst_broad_dict, wave,
                                        em_flux, em_flux_err, sys_vel, fontsize, x_format='vel',
                                        xlabel=False, ylabel=False, vel_radius=1000., vel_tick_steps=500,
                                        wave_tick_steps=10):

        if x_format == 'vel':
            position_peak_1 = fit_result_dict['mu_%s_1' % self.em_wavelength[line]['transition']] - sys_vel
            position_peak_2 = fit_result_dict['mu_%s_2' % self.em_wavelength[line]['transition']] - sys_vel
            sigma_peak_1 = np.sqrt(fit_result_dict['sigma_%s_1' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            sigma_peak_2 = np.sqrt(fit_result_dict['sigma_%s_2' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            observed_line = self.em_wavelength[line]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            x_data = speed_of_light * 1e-3 * (wave - observed_line) / self.em_wavelength[line]['vac_wave']
            if isinstance(vel_radius, float):
                mask_x_data = (x_data > (position_peak_1 - vel_radius)) & (x_data < (position_peak_1 + vel_radius))
            else:
                mask_x_data = (x_data > (position_peak_1 + vel_radius[0])) & (x_data < (position_peak_1 + vel_radius[1]))
        elif x_format == 'wave':
            position_peak_1 = (fit_result_dict['mu_%s_1' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            position_peak_2 = (fit_result_dict['mu_%s_2' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            sigma_peak_1 = (np.sqrt(fit_result_dict['sigma_%s_1' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            sigma_peak_2 = (np.sqrt(fit_result_dict['sigma_%s_2' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            x_data = wave
            mask_x_data = ((wave > (position_peak_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])) &
                           (wave < (position_peak_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_x_data = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)

        # calculate all gauss functions
        gauss_peak_1 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_1' % line],
                                     mu=position_peak_1, sigma=sigma_peak_1)
        gauss_peak_2 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_2' % line],
                                     mu=position_peak_2, sigma=sigma_peak_2)

        # compute fit value for residuals
        gauss_peak_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_1' % line],
                                              mu=position_peak_1, sigma=sigma_peak_1)
        gauss_peak_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_2' % line],
                                              mu=position_peak_2, sigma=sigma_peak_2)

        # calculate residuals
        residuals_peak = (em_flux[mask_x_data] - (gauss_peak_residual_1 + gauss_peak_residual_2))

        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_x_data, gauss_peak_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_2, color='r', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_1 + gauss_peak_2, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_peak, yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0], color='g',
                          linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_peak_2 + 1.3 * sigma_peak_2, fit_result_dict['amp_%i_2' % line],
        #         self.em_wavelength[line]['plot_name'], fontsize=fontsize)
        ax.text(0.04, 0.85, self.em_wavelength[line]['plot_name'], transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))

        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_triple_gauss_isolated_line(self, ax, ax_residuals, line, fit_result_dict, inst_broad_dict, wave,
                                        em_flux, em_flux_err, sys_vel, fontsize, x_format='vel',
                                        xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                        wave_tick_steps=10):

        if x_format == 'vel':
            position_peak_1 = fit_result_dict['mu_%s_1' % self.em_wavelength[line]['transition']] - sys_vel
            position_peak_2 = fit_result_dict['mu_%s_2' % self.em_wavelength[line]['transition']] - sys_vel
            position_peak_3 = fit_result_dict['mu_%s_3' % self.em_wavelength[line]['transition']] - sys_vel
            sigma_peak_1 = np.sqrt(fit_result_dict['sigma_%s_1' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            sigma_peak_2 = np.sqrt(fit_result_dict['sigma_%s_2' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            sigma_peak_3 = np.sqrt(fit_result_dict['sigma_%s_3' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            observed_line = self.em_wavelength[line]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            x_data = speed_of_light * 1e-3 * (wave - observed_line) / self.em_wavelength[line]['vac_wave']
            mask_x_data = (x_data > (position_peak_1 - vel_radius)) & (x_data < (position_peak_3 + vel_radius))

        elif x_format == 'wave':
            position_peak_1 = (fit_result_dict['mu_%s_1' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            position_peak_2 = (fit_result_dict['mu_%s_2' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            position_peak_3 = (fit_result_dict['mu_%s_3' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            sigma_peak_1 = (np.sqrt(fit_result_dict['sigma_%s_1' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            sigma_peak_2 = (np.sqrt(fit_result_dict['sigma_%s_2' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            sigma_peak_3 = (np.sqrt(fit_result_dict['sigma_%s_3' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            x_data = wave
            mask_x_data = ((wave > (position_peak_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])) &
                           (wave < (position_peak_3 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_x_data = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)

        # calculate all gauss functions
        gauss_peak_1 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_1' % line],
                                     mu=position_peak_1, sigma=sigma_peak_1)
        gauss_peak_2 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_2' % line],
                                     mu=position_peak_2, sigma=sigma_peak_2)
        gauss_peak_3 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_3' % line],
                                     mu=position_peak_3, sigma=sigma_peak_3)

        # compute fit value for residuals
        gauss_peak_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_1' % line],
                                              mu=position_peak_1, sigma=sigma_peak_1)
        gauss_peak_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_2' % line],
                                              mu=position_peak_2, sigma=sigma_peak_2)
        gauss_peak_residual_3 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_3' % line],
                                              mu=position_peak_3, sigma=sigma_peak_3)

        # calculate residuals
        residuals_peak = (em_flux[mask_x_data] - (gauss_peak_residual_1 + gauss_peak_residual_2 + gauss_peak_residual_3))

        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_x_data, gauss_peak_1, color='y', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_2, color='b', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_3, color='r', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_1 + gauss_peak_2 + gauss_peak_3, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_peak, yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0], color='g',
                          linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_peak_2 + 1.3 * sigma_peak_2, fit_result_dict['amp_%i_2' % line],
        #         self.em_wavelength[line]['plot_name'], fontsize=fontsize)
        ax.text(0.04, 0.85, self.em_wavelength[line]['plot_name'], transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))

        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)


    def plot_single_gauss_single_broad_isolated_line(self, ax, ax_residuals, line, fit_result_dict, inst_broad_dict, wave,
                                                     em_flux, em_flux_err, sys_vel, fontsize, x_format='vel',
                                                     xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                                     wave_tick_steps=10):

        if x_format == 'vel':
            position_peak = fit_result_dict['mu_%s' % self.em_wavelength[line]['transition']] - sys_vel
            position_peak_broad = fit_result_dict['mu_broad'] - sys_vel
            sigma_peak = np.sqrt(fit_result_dict['sigma_%s' % self.em_wavelength[line]['transition']] ** 2 +
                                 inst_broad_dict[line] ** 2)
            sigma_peak_broad = np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[line] ** 2)

            observed_line = self.em_wavelength[line]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            x_data = speed_of_light * 1e-3 * (wave - observed_line) / self.em_wavelength[line]['vac_wave']
            mask_x_data = (x_data > (position_peak - vel_radius)) & (x_data < (position_peak + vel_radius))

        elif x_format == 'wave':
            position_peak = (fit_result_dict['mu_%s' % self.em_wavelength[line]['transition']] /
                             (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                             self.em_wavelength[line]['vac_wave'])
            position_peak_broad = (fit_result_dict['mu_broad'] / (speed_of_light * 1e-3) *
                                   self.em_wavelength[line]['vac_wave'] + self.em_wavelength[line]['vac_wave'])
            sigma_peak = (np.sqrt(fit_result_dict['sigma_%s' % self.em_wavelength[line]['transition']] ** 2 +
                                  inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                          self.em_wavelength[line]['vac_wave'])
            sigma_peak_broad = (np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[line] ** 2) /
                                (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])

            x_data = wave
            mask_x_data = ((wave > (position_peak -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])) &
                           (wave < (position_peak +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_x_data = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)

        # calculate all gauss functions
        gauss_peak = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i' % line], mu=position_peak,
                                   sigma=sigma_peak)
        gauss_peak_broad = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_broad' % line],
                                         mu=position_peak_broad, sigma=sigma_peak_broad)

        # compute fit value for residuals
        gauss_peak_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i' % line], mu=position_peak,
                                            sigma=sigma_peak)
        gauss_peak_residual_broad = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_broad' % line],
                                                  mu=position_peak_broad, sigma=sigma_peak_broad)

        # calculate residuals
        residuals_peak = (em_flux[mask_x_data] - gauss_peak_residual - gauss_peak_residual_broad)
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_x_data, gauss_peak, color='b', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_broad, color='y', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak + gauss_peak_broad, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_peak, yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0], color='g',
                          linewidth=self.line_width)
        # emission_line naming
        ax.text(position_peak + 1.3 * sigma_peak, fit_result_dict['amp_%i' % line],
                self.em_wavelength[line]['plot_name'], fontsize=fontsize)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))

        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_single_broad_isolated_line(self, ax, ax_residuals, line, fit_result_dict, inst_broad_dict,
                                                     wave, em_flux, em_flux_err, sys_vel, fontsize, x_format='vel',
                                                     xlabel=False, ylabel=False, vel_radius=1000, vel_tick_steps=500,
                                                     wave_tick_steps=10):


        if x_format == 'vel':
            position_peak_1 = fit_result_dict['mu_%s_1' % self.em_wavelength[line]['transition']] - sys_vel
            position_peak_2 = fit_result_dict['mu_%s_2' % self.em_wavelength[line]['transition']] - sys_vel
            position_peak_broad = fit_result_dict['mu_broad'] - sys_vel
            sigma_peak_1 = np.sqrt(fit_result_dict['sigma_%s_1' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            sigma_peak_2 = np.sqrt(fit_result_dict['sigma_%s_2' % self.em_wavelength[line]['transition']] ** 2 +
                                   inst_broad_dict[line] ** 2)
            sigma_peak_broad = np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[line] ** 2)

            observed_line = self.em_wavelength[line]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            x_data = speed_of_light * 1e-3 * (wave - observed_line) / self.em_wavelength[line]['vac_wave']
            mask_x_data = (x_data > (position_peak_1 - vel_radius)) & (x_data < (position_peak_2 + vel_radius))

        elif x_format == 'wave':
            position_peak_1 = (fit_result_dict['mu_%s_1' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            position_peak_2 = (fit_result_dict['mu_%s_2' % self.em_wavelength[line]['transition']] /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'] +
                               self.em_wavelength[line]['vac_wave'])
            position_peak_broad = (fit_result_dict['mu_broad'] / (speed_of_light * 1e-3) *
                                   self.em_wavelength[line]['vac_wave'] + self.em_wavelength[line]['vac_wave'])
            sigma_peak_1 = (np.sqrt(fit_result_dict['sigma_%s_1' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            sigma_peak_2 = (np.sqrt(fit_result_dict['sigma_%s_2' % self.em_wavelength[line]['transition']] ** 2 +
                                    inst_broad_dict[line] ** 2) / (speed_of_light * 1e-3) *
                            self.em_wavelength[line]['vac_wave'])
            sigma_peak_broad = (np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[line] ** 2) /
                                (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])

            x_data = wave
            mask_x_data = ((wave > (position_peak_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])) &
                           (wave < (position_peak_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_x_data = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)

        # calculate all gauss functions
        gauss_peak_1 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_1' % line],
                                     mu=position_peak_1, sigma=sigma_peak_1)
        gauss_peak_2 = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_2' % line],
                                     mu=position_peak_2, sigma=sigma_peak_2)
        gauss_peak_broad = self.gaussian(high_res_x_data, amp=fit_result_dict['amp_%i_broad' % line],
                                         mu=position_peak_broad, sigma=sigma_peak_broad)

        # compute fit value for residuals
        gauss_peak_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_1' % line],
                                              mu=position_peak_1, sigma=sigma_peak_1)
        gauss_peak_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_2' % line],
                                              mu=position_peak_2, sigma=sigma_peak_2)
        gauss_peak_residual_broad = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_%i_broad' % line],
                                                  mu=position_peak_broad, sigma=sigma_peak_broad)

        # calculate residuals
        residuals_peak = (em_flux[mask_x_data] - (gauss_peak_residual_1 + gauss_peak_residual_2 +
                                                  gauss_peak_residual_broad))

        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_x_data, gauss_peak_1, color='b', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_2, color='r', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_broad, color='y', linewidth=self.line_width)
        ax.plot(high_res_x_data, gauss_peak_1 + gauss_peak_2 + gauss_peak_broad, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_peak, yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0], color='g',
                          linewidth=self.line_width)
        # emission_line naming
        ax.text(position_peak_2 + 1.3 * sigma_peak_2, fit_result_dict['amp_%i_2' % line],
                self.em_wavelength[line]['plot_name'], fontsize=fontsize)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))

        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_oi_doublet(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                                      em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                                      vel_radius=1000, vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_oi_1 = self.em_wavelength[6302]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_siii = self.em_wavelength[6314]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_si_ii = self.em_wavelength[6349]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_oi_2 = self.em_wavelength[6366]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_oi_1) / self.em_wavelength[6302]['vac_wave']

            vel_dist_siii = (speed_of_light * 1e-3 * (observed_line_oi_1 - observed_line_siii) /
                              self.em_wavelength[6302]['vac_wave'])
            vel_dist_si_ii = (speed_of_light * 1e-3 * (observed_line_oi_1 - observed_line_si_ii) /
                              self.em_wavelength[6302]['vac_wave'])
            vel_dist_oi_2 = (speed_of_light * 1e-3 * (observed_line_oi_1 - observed_line_oi_2) /
                              self.em_wavelength[6302]['vac_wave'])

            position_oi_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel
            position_oi_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel
            position_siii_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_siii
            position_siii_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_siii
            position_si_ii_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_si_ii
            position_si_ii_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_si_ii
            position_oi_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_oi_2
            position_oi_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_oi_2

            sigma_oi_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6302] ** 2)
            sigma_oi_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6302] ** 2)
            sigma_siii_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6314] ** 2)
            sigma_siii_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6314] ** 2)
            sigma_si_ii_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6349] ** 2)
            sigma_si_ii_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6349] ** 2)
            sigma_oi_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6366] ** 2)
            sigma_oi_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6366] ** 2)

            mask_x_data = (x_data > (position_oi_1_1 - vel_radius)) & (x_data < (position_oi_2_2 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_oi_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6302]['vac_wave'] + self.em_wavelength[6302]['vac_wave'])
            position_oi_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6302]['vac_wave'] + self.em_wavelength[6302]['vac_wave'])
            position_siii_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6314]['vac_wave'] + self.em_wavelength[6314]['vac_wave'])
            position_siii_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6314]['vac_wave'] + self.em_wavelength[6314]['vac_wave'])
            position_si_ii_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6349]['vac_wave'] + self.em_wavelength[6349]['vac_wave'])
            position_si_ii_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6349]['vac_wave'] + self.em_wavelength[6349]['vac_wave'])
            position_oi_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6366]['vac_wave'] + self.em_wavelength[6366]['vac_wave'])
            position_oi_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6366]['vac_wave'] + self.em_wavelength[6366]['vac_wave'])

            sigma_oi_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6302] ** 2) /
                            (speed_of_light * 1e-3) * self.em_wavelength[6302]['vac_wave'])
            sigma_oi_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6302] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6302]['vac_wave'])
            sigma_siii_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6314] ** 2) /
                            (speed_of_light * 1e-3) * self.em_wavelength[6314]['vac_wave'])
            sigma_siii_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6314] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6314]['vac_wave'])
            sigma_si_ii_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6349] ** 2) /
                            (speed_of_light * 1e-3) * self.em_wavelength[6349]['vac_wave'])
            sigma_si_ii_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6349] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6349]['vac_wave'])
            sigma_oi_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6366] ** 2) /
                            (speed_of_light * 1e-3) * self.em_wavelength[6366]['vac_wave'])
            sigma_oi_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6366] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6366]['vac_wave'])

            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_oi_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6302]['vac_wave'])) &
                           (wave < (position_oi_2_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6302]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_oi_1_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6302_1'],
                                     mu=position_oi_1_1, sigma=sigma_oi_1_1)
        gauss_oi_1_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6302_2'],
                                     mu=position_oi_1_2, sigma=sigma_oi_1_2)
        gauss_siii_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6314_1'],
                                     mu=position_siii_1, sigma=sigma_siii_1)
        gauss_siii_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6314_2'],
                                     mu=position_siii_2, sigma=sigma_siii_2)
        gauss_si_ii_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6349_1'],
                                     mu=position_si_ii_1, sigma=sigma_si_ii_1)
        gauss_si_ii_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6349_2'],
                                     mu=position_si_ii_2, sigma=sigma_si_ii_2)
        gauss_oi_2_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6366_1'],
                                     mu=position_oi_2_1, sigma=sigma_oi_2_1)
        gauss_oi_2_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6366_2'],
                                     mu=position_oi_2_2, sigma=sigma_oi_2_2)

        # compute fit value for residuals
        gauss_oi_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6302_1'],
                                               mu=position_oi_1_1, sigma=sigma_oi_1_1)
        gauss_oi_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6302_2'],
                                               mu=position_oi_1_2, sigma=sigma_oi_1_2)
        gauss_siii_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6314_1'],
                                               mu=position_siii_1, sigma=sigma_siii_1)
        gauss_siii_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6314_2'],
                                               mu=position_siii_2, sigma=sigma_siii_2)
        gauss_si_ii_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6349_1'],
                                               mu=position_si_ii_1, sigma=sigma_si_ii_1)
        gauss_si_ii_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6349_2'],
                                               mu=position_si_ii_2, sigma=sigma_si_ii_2)
        gauss_oi_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6366_1'],
                                               mu=position_oi_2_1, sigma=sigma_oi_2_1)
        gauss_oi_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6366_2'],
                                               mu=position_oi_2_2, sigma=sigma_oi_2_2)


        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_oi_1_residual_1 + gauss_oi_1_residual_2 +
                                                         gauss_siii_residual_1 + gauss_siii_residual_2 +
                                                         gauss_si_ii_residual_1 + gauss_si_ii_residual_2 +
                                                         gauss_oi_2_residual_1 + gauss_oi_2_residual_2))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_oi_1_1 + gauss_siii_1 + gauss_si_ii_1 + gauss_oi_2_1, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_oi_1_2 + gauss_siii_2 + gauss_si_ii_2 + gauss_oi_2_2, color='r',
                linewidth=self.line_width)

        ax.plot(high_res_wave_h_alpha_nii, gauss_oi_1_1 + gauss_siii_1 + gauss_si_ii_1 + gauss_oi_2_1 +
                gauss_oi_1_2 + gauss_siii_2 + gauss_si_ii_2 + gauss_oi_2_2, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_h_alpha_nii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        ax.text(position_oi_1_1 + 2 * sigma_oi_1_1, fit_result_dict['amp_6302_1'], '[OI] 6302',
                fontsize=self.fontsize_spec)
        ax.text(position_siii_1 + 2 * sigma_siii_1, fit_result_dict['amp_6314_1'], '[SIII] 6314',
                fontsize=self.fontsize_spec)
        ax.text(position_si_ii_1 + 2 * sigma_si_ii_1, fit_result_dict['amp_6349_1'], '[Si II] 6349',
                fontsize=self.fontsize_spec)
        ax.text(position_oi_2_1 + 2 * sigma_oi_2_1, fit_result_dict['amp_6366_1'], '[OI] 6366',
                fontsize=self.fontsize_spec)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_single_gauss_h_alpha_nii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                                      em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                                      vel_radius=1000., vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_nii_1 = self.em_wavelength[6550]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_h_alpha = self.em_wavelength[6565]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_nii_2 = self.em_wavelength[6585]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_h_alpha) / self.em_wavelength[6565]['vac_wave']

            vel_dist_nii_1 = (speed_of_light * 1e-3 * (observed_line_h_alpha - observed_line_nii_1) /
                              self.em_wavelength[6565]['vac_wave'])
            vel_dist_nii_2 = (speed_of_light * 1e-3 * (observed_line_nii_2 - observed_line_h_alpha) /
                              self.em_wavelength[6565]['vac_wave'])

            position_nii_1 = fit_result_dict['mu_forbidden'] - sys_vel - vel_dist_nii_1
            position_h_alpha = fit_result_dict['mu_balmer'] - sys_vel
            position_nii_2 = fit_result_dict['mu_forbidden'] - sys_vel + vel_dist_nii_2

            sigma_nii_1 = np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_h_alpha = np.sqrt(fit_result_dict['sigma_balmer'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_nii_2 = np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6585] ** 2)

            if isinstance(vel_radius, float):
                mask_x_data = (x_data > (position_nii_1 - vel_radius)) & (x_data < (position_nii_2 + vel_radius))
            else:
                mask_x_data = (x_data > (position_h_alpha + vel_radius[0])) & (x_data < (position_h_alpha + vel_radius[1]))

        elif x_format == 'wave':

            # get parameters
            position_nii_1 = (fit_result_dict['mu_forbidden'] / (speed_of_light * 1e-3) *
                              self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_h_alpha = (fit_result_dict['mu_balmer'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_nii_2 = (fit_result_dict['mu_forbidden'] / (speed_of_light * 1e-3) *
                              self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])

            sigma_nii_1 = (np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6550] ** 2) /
                           (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_h_alpha = (np.sqrt(fit_result_dict['sigma_balmer'] ** 2 + inst_broad_dict[6565] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_nii_2 = (np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6585] ** 2) /
                           (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])

            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_nii_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])) &
                           (wave < (position_nii_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_nii_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550'],
                                    mu=position_nii_1, sigma=sigma_nii_1)
        gauss_h_alpha = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565'],
                                      mu=position_h_alpha, sigma=sigma_h_alpha)
        gauss_nii_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585'],
                                    mu=position_nii_2, sigma=sigma_nii_2)
        # compute fit value for residuals
        gauss_nii_1_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550'],
                                             mu=position_nii_1, sigma=sigma_nii_1)
        gauss_h_alpha_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565'],
                                               mu=position_h_alpha, sigma=sigma_h_alpha)
        gauss_nii_2_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585'],
                                             mu=position_nii_2, sigma=sigma_nii_2)

        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_nii_1_residual + gauss_h_alpha_residual +
                                                         gauss_nii_2_residual))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1 + gauss_h_alpha + gauss_nii_2, color='g',
                linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_h_alpha_nii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_nii_1 - 6 * sigma_nii_1, fit_result_dict['amp_6550'], '[NII] 6550',
        #         fontsize=self.fontsize_spec)
        # ax.text(position_h_alpha + 1.3 * sigma_h_alpha, 0.9 * fit_result_dict['amp_6565'],
        #         'H${\\alpha}$ 6565', fontsize=self.fontsize_spec)
        # ax.text(position_nii_2 + 1.3 * sigma_nii_2, fit_result_dict['amp_6585'], '[NII] 6585',
        #         fontsize=self.fontsize_spec)
        ax.text(0.04, 0.85, r'[NII]6550', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.73, r'H${\alpha}$6565', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.61, r'[NII]6585', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_h_alpha_nii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                                      em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                                      vel_radius=1000., vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_nii_1 = self.em_wavelength[6550]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_h_alpha = self.em_wavelength[6565]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_nii_2 = self.em_wavelength[6585]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_h_alpha) / self.em_wavelength[6565]['vac_wave']

            vel_dist_nii_1 = (speed_of_light * 1e-3 * (observed_line_h_alpha - observed_line_nii_1) /
                              self.em_wavelength[6565]['vac_wave'])
            vel_dist_nii_2 = (speed_of_light * 1e-3 * (observed_line_nii_2 - observed_line_h_alpha) /
                              self.em_wavelength[6565]['vac_wave'])

            position_nii_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_nii_1
            position_nii_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_nii_1
            position_h_alpha_1 = fit_result_dict['mu_balmer_1'] - sys_vel
            position_h_alpha_2 = fit_result_dict['mu_balmer_2'] - sys_vel
            position_nii_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel + vel_dist_nii_2
            position_nii_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel + vel_dist_nii_2

            sigma_nii_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_nii_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_h_alpha_1 = np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_h_alpha_2 = np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_nii_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_nii_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2)

            if isinstance(vel_radius, float):
                mask_x_data = (x_data > (position_nii_1_1 - vel_radius)) & (x_data < (position_nii_2_2 + vel_radius))
            else:
                mask_x_data = (x_data > (position_h_alpha_1 + vel_radius[0])) & (x_data < (position_h_alpha_1 + vel_radius[1]))
        elif x_format == 'wave':

            # get parameters
            position_nii_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_nii_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_h_alpha_1 = (fit_result_dict['mu_balmer_1'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_h_alpha_2 = (fit_result_dict['mu_balmer_2'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_nii_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_nii_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])

            sigma_nii_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_nii_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_h_alpha_1 = (np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_h_alpha_2 = (np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_nii_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_nii_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_nii_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])) &
                           (wave < (position_nii_2_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_nii_1_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_1'],
                                      mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_nii_1_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_2'],
                                      mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_1'],
                                        mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_h_alpha_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_2'],
                                        mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_1'],
                                      mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_2_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_2'],
                                      mu=position_nii_2_2, sigma=sigma_nii_2_2)
        # compute fit value for residuals
        gauss_nii_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_1'],
                                               mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_h_alpha_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_1'],
                                                 mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_nii_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_1'],
                                               mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_2'],
                                               mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_2'],
                                                 mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_2'],
                                               mu=position_nii_2_2, sigma=sigma_nii_2_2)
        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_nii_1_residual_1 + gauss_nii_1_residual_2 +
                                                         gauss_h_alpha_residual_1 + gauss_h_alpha_residual_2 +
                                                         gauss_nii_2_residual_1 + gauss_nii_2_residual_2))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_h_alpha_1 + gauss_nii_2_1, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_2 + gauss_h_alpha_2 + gauss_nii_2_2, color='r',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_nii_1_2 + gauss_h_alpha_1 + gauss_h_alpha_2 +
                gauss_nii_2_1 + gauss_nii_2_2, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_h_alpha_nii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # # emission_line naming
        # ax.text(position_nii_1_1 - 6 * sigma_nii_1_1, fit_result_dict['amp_6550_1'], '[NII] 6550',
        #         fontsize=self.fontsize_spec)
        # ax.text(position_h_alpha_2 + 1.3 * sigma_h_alpha_2, 0.9 * fit_result_dict['amp_6565_2'],
        #         'H${\\alpha}$ 6565', fontsize=self.fontsize_spec)
        # ax.text(position_nii_2_2 + 1.3 * sigma_nii_2_2, fit_result_dict['amp_6585_2'], '[NII] 6585',
        #         fontsize=self.fontsize_spec)
        ax.text(0.04, 0.85, r'[NII]6550', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.73, r'H${\alpha}$6565', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.61, r'[NII]6585', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=fontsize, labelpad=-4)

    def plot_triple_gauss_h_alpha_nii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                                      em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                                      vel_radius=1000, vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_nii_1 = self.em_wavelength[6550]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_h_alpha = self.em_wavelength[6565]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_nii_2 = self.em_wavelength[6585]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_h_alpha) / self.em_wavelength[6565]['vac_wave']

            vel_dist_nii_1 = (speed_of_light * 1e-3 * (observed_line_h_alpha - observed_line_nii_1) /
                              self.em_wavelength[6565]['vac_wave'])
            vel_dist_nii_2 = (speed_of_light * 1e-3 * (observed_line_nii_2 - observed_line_h_alpha) /
                              self.em_wavelength[6565]['vac_wave'])

            position_nii_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_nii_1
            position_nii_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_nii_1
            position_nii_1_3 = fit_result_dict['mu_forbidden_3'] - sys_vel - vel_dist_nii_1
            position_h_alpha_1 = fit_result_dict['mu_balmer_1'] - sys_vel
            position_h_alpha_2 = fit_result_dict['mu_balmer_2'] - sys_vel
            position_h_alpha_3 = fit_result_dict['mu_balmer_3'] - sys_vel
            position_nii_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel + vel_dist_nii_2
            position_nii_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel + vel_dist_nii_2
            position_nii_2_3 = fit_result_dict['mu_forbidden_3'] - sys_vel + vel_dist_nii_2

            sigma_nii_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_nii_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_nii_1_3 = np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_h_alpha_1 = np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_h_alpha_2 = np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_h_alpha_3 = np.sqrt(fit_result_dict['sigma_balmer_3'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_nii_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_nii_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_nii_2_3 = np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6585] ** 2)

            mask_x_data = (x_data > (position_nii_1_1 - vel_radius)) & (x_data < (position_nii_2_3 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_nii_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_nii_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_nii_1_3 = (fit_result_dict['mu_forbidden_3'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_h_alpha_1 = (fit_result_dict['mu_balmer_1'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_h_alpha_2 = (fit_result_dict['mu_balmer_2'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_h_alpha_3 = (fit_result_dict['mu_balmer_3'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_nii_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_nii_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_nii_2_3 = (fit_result_dict['mu_forbidden_3'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])

            sigma_nii_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_nii_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_nii_1_3 = (np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_h_alpha_1 = (np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_h_alpha_2 = (np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_h_alpha_3 = (np.sqrt(fit_result_dict['sigma_balmer_3'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_nii_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_nii_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_nii_2_3 = (np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_nii_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])) &
                           (wave < (position_nii_2_3 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_nii_1_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_1'],
                                      mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_nii_1_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_2'],
                                      mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_nii_1_3 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_3'],
                                      mu=position_nii_1_3, sigma=sigma_nii_1_3)
        gauss_h_alpha_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_1'],
                                        mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_h_alpha_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_2'],
                                        mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_h_alpha_3 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_3'],
                                        mu=position_h_alpha_3, sigma=sigma_h_alpha_3)
        gauss_nii_2_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_1'],
                                      mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_2_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_2'],
                                      mu=position_nii_2_2, sigma=sigma_nii_2_2)
        gauss_nii_2_3 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_3'],
                                      mu=position_nii_2_3, sigma=sigma_nii_2_3)
        # compute fit value for residuals
        gauss_nii_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_1'],
                                               mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_h_alpha_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_1'],
                                                 mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_nii_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_1'],
                                               mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_2'],
                                               mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_2'],
                                                 mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_2'],
                                               mu=position_nii_2_2, sigma=sigma_nii_2_2)
        gauss_nii_1_residual_3 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_3'],
                                               mu=position_nii_1_3, sigma=sigma_nii_1_3)
        gauss_h_alpha_residual_3 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_3'],
                                                 mu=position_h_alpha_3, sigma=sigma_h_alpha_3)
        gauss_nii_2_residual_3 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_3'],
                                               mu=position_nii_2_3, sigma=sigma_nii_2_3)
        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_nii_1_residual_1 + gauss_nii_1_residual_2 + gauss_nii_1_residual_3 +
                                                         gauss_h_alpha_residual_1 + gauss_h_alpha_residual_2 + gauss_h_alpha_residual_3 +
                                                         gauss_nii_2_residual_1 + gauss_nii_2_residual_2 + gauss_nii_2_residual_3))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_h_alpha_1 + gauss_nii_2_1, color='y',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_2 + gauss_h_alpha_2 + gauss_nii_2_2, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_3 + gauss_h_alpha_3 + gauss_nii_2_3, color='r',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_nii_1_2 + gauss_nii_1_3 +
                gauss_h_alpha_1 + gauss_h_alpha_2 + gauss_h_alpha_3 + gauss_nii_2_1 + gauss_nii_2_2 + gauss_nii_2_3,
                color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_h_alpha_nii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # # emission_line naming
        # ax.text(position_nii_1_1 - 6 * sigma_nii_1_1, fit_result_dict['amp_6550_1'], '[NII] 6550',
        #         fontsize=self.fontsize_spec)
        # ax.text(position_h_alpha_2 + 1.3 * sigma_h_alpha_2, 0.9 * fit_result_dict['amp_6565_2'],
        #         'H${\\alpha}$ 6565', fontsize=self.fontsize_spec)
        # ax.text(position_nii_2_2 + 1.3 * sigma_nii_2_2, fit_result_dict['amp_6585_2'], '[NII] 6585',
        #         fontsize=self.fontsize_spec)
        ax.text(0.04, 0.85, r'[NII]6550', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.73, r'H${\alpha}$6565', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.61, r'[NII]6585', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=fontsize, labelpad=-4)

    def plot_single_gauss_single_broad_h_alpha_nii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave,
                                                   em_flux, em_flux_err, sys_vel, fontsize,  x_format='vel',
                                                   xlabel=False, ylabel=False, vel_radius=1000., vel_tick_steps=500,
                                                   wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_nii_1 = self.em_wavelength[6550]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_h_alpha = self.em_wavelength[6565]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_nii_2 = self.em_wavelength[6585]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_h_alpha) / self.em_wavelength[6565]['vac_wave']

            vel_dist_nii_1 = (speed_of_light * 1e-3 * (observed_line_h_alpha - observed_line_nii_1) /
                              self.em_wavelength[6565]['vac_wave'])
            vel_dist_nii_2 = (speed_of_light * 1e-3 * (observed_line_nii_2 - observed_line_h_alpha) /
                              self.em_wavelength[6565]['vac_wave'])

            position_nii_1 = fit_result_dict['mu_forbidden'] - sys_vel - vel_dist_nii_1
            position_h_alpha = fit_result_dict['mu_balmer'] - sys_vel
            position_nii_2 = fit_result_dict['mu_forbidden'] - sys_vel + vel_dist_nii_2
            position_h_alpha_broad = fit_result_dict['mu_broad'] - sys_vel

            sigma_nii_1 = np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_h_alpha = np.sqrt(fit_result_dict['sigma_balmer'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_nii_2 = np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_h_alpha_broad = np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[6565] ** 2)

            # mask_x_data = (x_data > (position_nii_1 - vel_radius)) & (x_data < (position_nii_2 + vel_radius))
            if isinstance(vel_radius, float):
                mask_x_data = (x_data > (position_nii_1 - vel_radius)) & (x_data < (position_nii_2 + vel_radius))
            else:
                mask_x_data = (x_data > (position_h_alpha + vel_radius[0])) & (x_data < (position_h_alpha + vel_radius[1]))
        elif x_format == 'wave':

            # get parameters
            position_nii_1 = (fit_result_dict['mu_forbidden'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_h_alpha = (fit_result_dict['mu_balmer'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_nii_2 = (fit_result_dict['mu_forbidden'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_h_alpha_broad = (fit_result_dict['mu_broad'] / (speed_of_light * 1e-3) *
                                      self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])

            sigma_nii_1 = (np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_h_alpha = (np.sqrt(fit_result_dict['sigma_balmer'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_nii_2 = (np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_h_alpha_broad = (np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[6565] ** 2) /
                                   (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])

            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_nii_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])) &
                           (wave < (position_nii_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_nii_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550'],
                                      mu=position_nii_1, sigma=sigma_nii_1)
        gauss_h_alpha = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565'],
                                        mu=position_h_alpha, sigma=sigma_h_alpha)
        gauss_nii_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585'],
                                      mu=position_nii_2, sigma=sigma_nii_2)
        gauss_h_alpha_broad = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_broad'],
                                            mu=position_h_alpha_broad, sigma=sigma_h_alpha_broad)
        # compute fit value for residuals
        gauss_nii_1_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550'],
                                               mu=position_nii_1, sigma=sigma_nii_1)
        gauss_nii_2_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585'],
                                               mu=position_nii_2, sigma=sigma_nii_2)
        gauss_h_alpha_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565'],
                                                 mu=position_h_alpha, sigma=sigma_h_alpha)
        gauss_h_alpha_residual_broad = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_broad'],
                                                     mu=position_h_alpha_broad, sigma=sigma_h_alpha_broad)

        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_nii_1_residual +
                                                         gauss_h_alpha_residual +
                                                         gauss_nii_2_residual +
                                                         gauss_h_alpha_residual_broad))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1 + gauss_h_alpha + gauss_nii_2, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_h_alpha_broad, color='y',  linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1 + gauss_h_alpha +
                gauss_nii_2 + gauss_h_alpha_broad, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_h_alpha_nii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_nii_1 - 6 * sigma_nii_1, fit_result_dict['amp_6550'], '[NII] 6550',
        #         fontsize=self.fontsize_spec)
        # ax.text(position_h_alpha + 1.3 * sigma_h_alpha, 0.9 * fit_result_dict['amp_6565'],
        #         'H${\\alpha}$ 6565', fontsize=self.fontsize_spec)
        # ax.text(position_nii_2 + 1.3 * sigma_nii_2, fit_result_dict['amp_6585'], '[NII] 6585',
        #         fontsize=self.fontsize_spec)
        ax.text(0.04, 0.85, r'[NII]6550', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.73, r'H${\alpha}$6565', transform=ax.transAxes,  fontsize=fontsize)
        ax.text(0.04, 0.61, r'[NII]6585', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_single_broad_h_alpha_nii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave,
                                                   em_flux, em_flux_err, sys_vel, fontsize,  x_format='vel',
                                                   xlabel=False, ylabel=False, vel_radius=3000, vel_tick_steps=500,
                                                   wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_nii_1 = self.em_wavelength[6550]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_h_alpha = self.em_wavelength[6565]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_nii_2 = self.em_wavelength[6585]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_h_alpha) / self.em_wavelength[6565]['vac_wave']

            vel_dist_nii_1 = (speed_of_light * 1e-3 * (observed_line_h_alpha - observed_line_nii_1) /
                              self.em_wavelength[6565]['vac_wave'])
            vel_dist_nii_2 = (speed_of_light * 1e-3 * (observed_line_nii_2 - observed_line_h_alpha) /
                              self.em_wavelength[6565]['vac_wave'])

            position_nii_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_nii_1
            position_nii_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_nii_1
            position_h_alpha_1 = fit_result_dict['mu_balmer_1'] - sys_vel
            position_h_alpha_2 = fit_result_dict['mu_balmer_2'] - sys_vel
            position_nii_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel + vel_dist_nii_2
            position_nii_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel + vel_dist_nii_2
            position_h_alpha_broad = fit_result_dict['mu_broad'] - sys_vel

            sigma_nii_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_nii_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_h_alpha_1 = np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_h_alpha_2 = np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_nii_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_nii_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_h_alpha_broad = np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[6565] ** 2)

            mask_x_data = (x_data > (position_nii_1_1 - vel_radius)) & (x_data < (position_nii_2_2 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_nii_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_nii_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_h_alpha_1 = (fit_result_dict['mu_balmer_1'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_h_alpha_2 = (fit_result_dict['mu_balmer_2'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_nii_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_nii_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_h_alpha_broad = (fit_result_dict['mu_broad'] / (speed_of_light * 1e-3) *
                                      self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])

            sigma_nii_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_nii_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_h_alpha_1 = (np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_h_alpha_2 = (np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_nii_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_nii_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_h_alpha_broad = (np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[6565] ** 2) /
                                   (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])

            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_nii_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])) &
                           (wave < (position_nii_2_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_nii_1_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_1'],
                                      mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_nii_1_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_2'],
                                      mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_1'],
                                        mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_h_alpha_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_2'],
                                        mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_1'],
                                      mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_2_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_2'],
                                      mu=position_nii_2_2, sigma=sigma_nii_2_2)
        gauss_h_alpha_broad = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_broad'],
                                            mu=position_h_alpha_broad, sigma=sigma_h_alpha_broad)
        # compute fit value for residuals
        gauss_nii_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_1'],
                                               mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_h_alpha_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_1'],
                                                 mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_nii_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_1'],
                                               mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_2'],
                                               mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_2'],
                                                 mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_2'],
                                               mu=position_nii_2_2, sigma=sigma_nii_2_2)
        gauss_h_alpha_residual_broad = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_broad'],
                                                     mu=position_h_alpha_broad, sigma=sigma_h_alpha_broad)

        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_nii_1_residual_1 + gauss_nii_1_residual_2 +
                                                         gauss_h_alpha_residual_1 + gauss_h_alpha_residual_2 +
                                                         gauss_nii_2_residual_1 + gauss_nii_2_residual_2 +
                                                         gauss_h_alpha_residual_broad))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_h_alpha_1 + gauss_nii_2_1, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_2 + gauss_h_alpha_2 + gauss_nii_2_2, color='r',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_h_alpha_broad, color='y',  linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_nii_1_2 + gauss_h_alpha_1 + gauss_h_alpha_2 +
                gauss_nii_2_1 + gauss_nii_2_2 + gauss_h_alpha_broad, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_h_alpha_nii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        ax.text(position_nii_1_1 - 6 * sigma_nii_1_1, fit_result_dict['amp_6550_1'], '[NII] 6550',
                fontsize=self.fontsize_spec)
        ax.text(position_h_alpha_2 + 1.3 * sigma_h_alpha_2, 0.9 * fit_result_dict['amp_6565_2'],
                'H${\\alpha}$ 6565', fontsize=self.fontsize_spec)
        ax.text(position_nii_2_2 + 1.3 * sigma_nii_2_2, fit_result_dict['amp_6585_2'], '[NII] 6585',
                fontsize=self.fontsize_spec)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_single_broad_h_alpha_nii_custom(self, ax, fit_result_dict, inst_broad_dict, wave,
                                                   em_flux, em_flux_err, sys_vel, fontsize,  x_format='vel',
                                                   xlabel=False, ylabel=False, vel_radius=1200, vel_tick_steps=1000,
                                                   wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_nii_1 = self.em_wavelength[6550]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_h_alpha = self.em_wavelength[6565]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_nii_2 = self.em_wavelength[6585]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_h_alpha) / self.em_wavelength[6565]['vac_wave']

            vel_dist_nii_1 = (speed_of_light * 1e-3 * (observed_line_h_alpha - observed_line_nii_1) /
                              self.em_wavelength[6565]['vac_wave'])
            vel_dist_nii_2 = (speed_of_light * 1e-3 * (observed_line_nii_2 - observed_line_h_alpha) /
                              self.em_wavelength[6565]['vac_wave'])

            position_nii_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel - vel_dist_nii_1
            position_nii_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel - vel_dist_nii_1
            position_h_alpha_1 = fit_result_dict['mu_balmer_1'] - sys_vel
            position_h_alpha_2 = fit_result_dict['mu_balmer_2'] - sys_vel
            position_nii_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel + vel_dist_nii_2
            position_nii_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel + vel_dist_nii_2
            position_h_alpha_broad = fit_result_dict['mu_broad'] - sys_vel

            sigma_nii_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_nii_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2)
            sigma_h_alpha_1 = np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_h_alpha_2 = np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2)
            sigma_nii_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_nii_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2)
            sigma_h_alpha_broad = np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[6565] ** 2)

            mask_x_data = (x_data > (position_nii_1_1 - vel_radius)) & (x_data < (position_nii_2_2 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_nii_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_nii_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6550]['vac_wave'] + self.em_wavelength[6550]['vac_wave'])
            position_h_alpha_1 = (fit_result_dict['mu_balmer_1'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_h_alpha_2 = (fit_result_dict['mu_balmer_2'] / (speed_of_light * 1e-3) *
                                  self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])
            position_nii_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_nii_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6585]['vac_wave'] + self.em_wavelength[6585]['vac_wave'])
            position_h_alpha_broad = (fit_result_dict['mu_broad'] / (speed_of_light * 1e-3) *
                                      self.em_wavelength[6565]['vac_wave'] + self.em_wavelength[6565]['vac_wave'])

            sigma_nii_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_nii_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6550] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'])
            sigma_h_alpha_1 = (np.sqrt(fit_result_dict['sigma_balmer_1'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_h_alpha_2 = (np.sqrt(fit_result_dict['sigma_balmer_2'] ** 2 + inst_broad_dict[6565] ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])
            sigma_nii_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_nii_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6585] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'])
            sigma_h_alpha_broad = (np.sqrt(fit_result_dict['sigma_broad'] ** 2 + inst_broad_dict[6565] ** 2) /
                                   (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])

            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_nii_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])) &
                           (wave < (position_nii_2_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_h_alpha_nii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_nii_1_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_1'],
                                      mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_nii_1_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6550_2'],
                                      mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_1'],
                                        mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_h_alpha_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_2'],
                                        mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_1 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_1'],
                                      mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_2_2 = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6585_2'],
                                      mu=position_nii_2_2, sigma=sigma_nii_2_2)
        gauss_h_alpha_broad = self.gaussian(high_res_wave_h_alpha_nii, amp=fit_result_dict['amp_6565_broad'],
                                            mu=position_h_alpha_broad, sigma=sigma_h_alpha_broad)
        # compute fit value for residuals
        gauss_nii_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_1'],
                                               mu=position_nii_1_1, sigma=sigma_nii_1_1)
        gauss_h_alpha_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_1'],
                                                 mu=position_h_alpha_1, sigma=sigma_h_alpha_1)
        gauss_nii_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_1'],
                                               mu=position_nii_2_1, sigma=sigma_nii_2_1)
        gauss_nii_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6550_2'],
                                               mu=position_nii_1_2, sigma=sigma_nii_1_2)
        gauss_h_alpha_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_2'],
                                                 mu=position_h_alpha_2, sigma=sigma_h_alpha_2)
        gauss_nii_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6585_2'],
                                               mu=position_nii_2_2, sigma=sigma_nii_2_2)
        gauss_h_alpha_residual_broad = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6565_broad'],
                                                     mu=position_h_alpha_broad, sigma=sigma_h_alpha_broad)

        # calculate residuals
        residuals_h_alpha_nii = (em_flux[mask_x_data] - (gauss_nii_1_residual_1 + gauss_nii_1_residual_2 +
                                                         gauss_h_alpha_residual_1 + gauss_h_alpha_residual_2 +
                                                         gauss_nii_2_residual_1 + gauss_nii_2_residual_2 +
                                                         gauss_h_alpha_residual_broad))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_h_alpha_1 + gauss_nii_2_1, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_2 + gauss_h_alpha_2 + gauss_nii_2_2, color='r',
                linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_h_alpha_broad, color='y',  linewidth=self.line_width)
        ax.plot(high_res_wave_h_alpha_nii, gauss_nii_1_1 + gauss_nii_1_2 + gauss_h_alpha_1 + gauss_h_alpha_2 +
                gauss_nii_2_1 + gauss_nii_2_2 + gauss_h_alpha_broad, color='g', linewidth=self.line_width)

        # emission_line naming
        ax.text(position_nii_1_1 - 3 * sigma_nii_1_1, fit_result_dict['amp_6550_1'] * 2.5, r'[NII]$\lambda$6550',
                fontsize=self.fontsize_spec)
        ax.text(position_h_alpha_1 + 1 * sigma_h_alpha_1, 3.5 * fit_result_dict['amp_6565_2'],
                'H${\\alpha}$', fontsize=self.fontsize_spec)
        ax.text(position_nii_2_2 + 0 * sigma_nii_2_2, 1.5 * fit_result_dict['amp_6585_2'], r'[NII]$\lambda$6585',
                fontsize=self.fontsize_spec)
        # set axis layout
        # ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize-3)
        # ax.set_yticklabels(ax.get)
        ticks = ax.get_yticks()*0.01
        ax.set_yticklabels(ticks)
        # ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        # ax.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, a.u.', labelpad=-2, fontsize=fontsize - 2)
        if xlabel:
            if x_format == 'vel':
                ax.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize - 2, labelpad=-4)
            elif x_format == 'wave':
                ax.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_single_gauss_sii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                              em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                              vel_radius=1000, vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_sii_1 = self.em_wavelength[6718]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_sii_2 = self.em_wavelength[6733]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_sii_1) / self.em_wavelength[6718]['vac_wave']

            vel_dist_sii_2 = (speed_of_light * 1e-3 * (observed_line_sii_2 - observed_line_sii_1) /
                              self.em_wavelength[6718]['vac_wave'])

            position_sii_1 = fit_result_dict['mu_forbidden'] - sys_vel

            position_sii_2 = fit_result_dict['mu_forbidden'] - sys_vel + vel_dist_sii_2

            sigma_sii_1 = np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6718] ** 2)
            sigma_sii_2 = np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6733] ** 2)

            mask_x_data = (x_data > (position_sii_1 - vel_radius)) & (x_data < (position_sii_2 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_sii_1 = (fit_result_dict['mu_forbidden'] / (speed_of_light * 1e-3) *
                              self.em_wavelength[6718]['vac_wave'] + self.em_wavelength[6718]['vac_wave'])
            position_sii_2 = (fit_result_dict['mu_forbidden'] / (speed_of_light * 1e-3) *
                              self.em_wavelength[6733]['vac_wave'] + self.em_wavelength[6733]['vac_wave'])

            sigma_sii_1 = (np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6718] ** 2) /
                           (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])
            sigma_sii_2 = (np.sqrt(fit_result_dict['sigma_forbidden'] ** 2 + inst_broad_dict[6733] ** 2) /
                           (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'])
            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_sii_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])) &
                           (wave < (position_sii_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_sii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_sii_1 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6718'], mu=position_sii_1,
                                    sigma=sigma_sii_1)
        gauss_sii_2 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6733'], mu=position_sii_2,
                                    sigma=sigma_sii_2)

        # compute fit value for residuals
        gauss_sii_1_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718'], mu=position_sii_1,
                                             sigma=sigma_sii_1)
        gauss_sii_2_residual = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718'], mu=position_sii_2,
                                             sigma=sigma_sii_2)
        # calculate residuals
        residuals_sii = (em_flux[mask_x_data] - (gauss_sii_1_residual + gauss_sii_2_residual))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_sii, gauss_sii_1 + gauss_sii_2, color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_sii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        ax.text(position_sii_1 - 6 * sigma_sii_1, fit_result_dict['amp_6718'], '[SII] 6718',
                fontsize=self.fontsize_spec)
        ax.text(position_sii_2 + 1.3 * sigma_sii_2, fit_result_dict['amp_6733'], '[SII] 6733',
                fontsize=self.fontsize_spec)
        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_double_gauss_sii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                              em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                              vel_radius=1000, vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_sii_1 = self.em_wavelength[6718]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_sii_2 = self.em_wavelength[6733]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_sii_1) / self.em_wavelength[6718]['vac_wave']

            vel_dist_sii_2 = (speed_of_light * 1e-3 * (observed_line_sii_2 - observed_line_sii_1) /
                              self.em_wavelength[6718]['vac_wave'])

            position_sii_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel
            position_sii_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel

            position_sii_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel + vel_dist_sii_2
            position_sii_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel + vel_dist_sii_2

            sigma_sii_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6718] ** 2)
            sigma_sii_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6718] ** 2)
            sigma_sii_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6733] ** 2)
            sigma_sii_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6733] ** 2)

            mask_x_data = (x_data > (position_sii_1_1 - vel_radius)) & (x_data < (position_sii_2_2 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_sii_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6718]['vac_wave'] + self.em_wavelength[6718]['vac_wave'])
            position_sii_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6718]['vac_wave'] + self.em_wavelength[6718]['vac_wave'])
            position_sii_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6733]['vac_wave'] + self.em_wavelength[6733]['vac_wave'])
            position_sii_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6733]['vac_wave'] + self.em_wavelength[6733]['vac_wave'])

            sigma_sii_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6718] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])
            sigma_sii_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6718] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])
            sigma_sii_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6733] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'])
            sigma_sii_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6733] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'])
            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_sii_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])) &
                           (wave < (position_sii_2_2 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_sii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_sii_1_1 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6718_1'],
                                      mu=position_sii_1_1, sigma=sigma_sii_1_1)
        gauss_sii_1_2 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6718_2'],
                                      mu=position_sii_1_2, sigma=sigma_sii_1_2)
        gauss_sii_2_1 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6733_1'],
                                      mu=position_sii_2_1, sigma=sigma_sii_2_1)
        gauss_sii_2_2 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6733_2'],
                                      mu=position_sii_2_2, sigma=sigma_sii_2_2)
        # compute fit value for residuals
        gauss_sii_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718_1'],
                                               mu=position_sii_1_1, sigma=sigma_sii_1_1)
        gauss_sii_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6733_1'],
                                               mu=position_sii_2_1, sigma=sigma_sii_2_1)
        gauss_sii_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718_2'],
                                               mu=position_sii_1_2, sigma=sigma_sii_1_2)
        gauss_sii_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6733_2'],
                                               mu=position_sii_2_2, sigma=sigma_sii_2_2)
        # calculate residuals
        residuals_sii = (em_flux[mask_x_data] - (gauss_sii_1_residual_1 + gauss_sii_1_residual_2 +
                                                 gauss_sii_2_residual_1 + gauss_sii_2_residual_2))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_sii, gauss_sii_1_1 + gauss_sii_2_1, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_sii, gauss_sii_1_2 + gauss_sii_2_2, color='r',
                linewidth=self.line_width)
        ax.plot(high_res_wave_sii, gauss_sii_1_1 + gauss_sii_1_2 + gauss_sii_2_1 + gauss_sii_2_2,
                color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_sii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_sii_1_1 - 6 * sigma_sii_1_1, fit_result_dict['amp_6718_1'], '[SII] 6718',
        #         fontsize=self.fontsize_spec)
        # ax.text(position_sii_2_2 + 1.3 * sigma_sii_2_2, fit_result_dict['amp_6733_2'], '[SII] 6733',
        #         fontsize=self.fontsize_spec)
        ax.text(0.6, 0.85, r'[SII] 6718, 6733', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_triple_gauss_sii(self, ax, ax_residuals, fit_result_dict, inst_broad_dict, wave, em_flux,
                              em_flux_err, sys_vel, fontsize,  x_format='vel', xlabel=False, ylabel=False,
                              vel_radius=1000, vel_tick_steps=500, wave_tick_steps=10):

        if x_format == 'vel':
            # wavelength to vel
            observed_line_sii_1 = self.em_wavelength[6718]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))
            observed_line_sii_2 = self.em_wavelength[6733]['vac_wave'] * (1 + sys_vel / (speed_of_light * 1e-3))

            x_data = speed_of_light * 1e-3 * (wave - observed_line_sii_1) / self.em_wavelength[6718]['vac_wave']

            vel_dist_sii_2 = (speed_of_light * 1e-3 * (observed_line_sii_2 - observed_line_sii_1) /
                              self.em_wavelength[6718]['vac_wave'])

            position_sii_1_1 = fit_result_dict['mu_forbidden_1'] - sys_vel
            position_sii_1_2 = fit_result_dict['mu_forbidden_2'] - sys_vel
            position_sii_1_3 = fit_result_dict['mu_forbidden_3'] - sys_vel

            position_sii_2_1 = fit_result_dict['mu_forbidden_1'] - sys_vel + vel_dist_sii_2
            position_sii_2_2 = fit_result_dict['mu_forbidden_2'] - sys_vel + vel_dist_sii_2
            position_sii_2_3 = fit_result_dict['mu_forbidden_3'] - sys_vel + vel_dist_sii_2

            sigma_sii_1_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6718] ** 2)
            sigma_sii_1_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6718] ** 2)
            sigma_sii_1_3 = np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6718] ** 2)
            sigma_sii_2_1 = np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6733] ** 2)
            sigma_sii_2_2 = np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6733] ** 2)
            sigma_sii_2_3 = np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6733] ** 2)

            mask_x_data = (x_data > (position_sii_1_1 - vel_radius)) & (x_data < (position_sii_2_3 + vel_radius))
        elif x_format == 'wave':

            # get parameters
            position_sii_1_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6718]['vac_wave'] + self.em_wavelength[6718]['vac_wave'])
            position_sii_1_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6718]['vac_wave'] + self.em_wavelength[6718]['vac_wave'])
            position_sii_1_3 = (fit_result_dict['mu_forbidden_3'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6718]['vac_wave'] + self.em_wavelength[6718]['vac_wave'])
            position_sii_2_1 = (fit_result_dict['mu_forbidden_1'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6733]['vac_wave'] + self.em_wavelength[6733]['vac_wave'])
            position_sii_2_2 = (fit_result_dict['mu_forbidden_2'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6733]['vac_wave'] + self.em_wavelength[6733]['vac_wave'])
            position_sii_2_3 = (fit_result_dict['mu_forbidden_3'] / (speed_of_light * 1e-3) *
                                self.em_wavelength[6733]['vac_wave'] + self.em_wavelength[6733]['vac_wave'])

            sigma_sii_1_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6718] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])
            sigma_sii_1_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6718] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])
            sigma_sii_1_3 = (np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6718] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])
            sigma_sii_2_1 = (np.sqrt(fit_result_dict['sigma_forbidden_1'] ** 2 + inst_broad_dict[6733] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'])
            sigma_sii_2_2 = (np.sqrt(fit_result_dict['sigma_forbidden_2'] ** 2 + inst_broad_dict[6733] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'])
            sigma_sii_2_3 = (np.sqrt(fit_result_dict['sigma_forbidden_3'] ** 2 + inst_broad_dict[6733] ** 2) /
                             (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'])
            # wavelength for position
            x_data = wave
            mask_x_data = ((wave > (position_sii_1_1 -
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])) &
                           (wave < (position_sii_2_3 +
                                    vel_radius / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'])))
        else:
            raise KeyError('x_data must be vel or wave')

        # high resolution wave
        high_res_wave_sii = np.linspace(min(x_data[mask_x_data]),  max(x_data[mask_x_data]), 1000)
        # calculate all gauss functions
        gauss_sii_1_1 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6718_1'],
                                      mu=position_sii_1_1, sigma=sigma_sii_1_1)
        gauss_sii_1_2 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6718_2'],
                                      mu=position_sii_1_2, sigma=sigma_sii_1_2)
        gauss_sii_1_3 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6718_3'],
                                      mu=position_sii_1_3, sigma=sigma_sii_1_3)
        gauss_sii_2_1 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6733_1'],
                                      mu=position_sii_2_1, sigma=sigma_sii_2_1)
        gauss_sii_2_2 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6733_2'],
                                      mu=position_sii_2_2, sigma=sigma_sii_2_2)
        gauss_sii_2_3 = self.gaussian(high_res_wave_sii, amp=fit_result_dict['amp_6733_3'],
                                      mu=position_sii_2_3, sigma=sigma_sii_2_3)
        # compute fit value for residuals
        gauss_sii_1_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718_1'],
                                               mu=position_sii_1_1, sigma=sigma_sii_1_1)
        gauss_sii_2_residual_1 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6733_1'],
                                               mu=position_sii_2_1, sigma=sigma_sii_2_1)
        gauss_sii_1_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718_2'],
                                               mu=position_sii_1_2, sigma=sigma_sii_1_2)
        gauss_sii_2_residual_2 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6733_2'],
                                               mu=position_sii_2_2, sigma=sigma_sii_2_2)
        gauss_sii_1_residual_3 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6718_3'],
                                               mu=position_sii_1_3, sigma=sigma_sii_1_3)
        gauss_sii_2_residual_3 = self.gaussian(x_data[mask_x_data], amp=fit_result_dict['amp_6733_3'],
                                               mu=position_sii_2_3, sigma=sigma_sii_2_3)
        # calculate residuals
        residuals_sii = (em_flux[mask_x_data] - (gauss_sii_1_residual_1 + gauss_sii_1_residual_2 + gauss_sii_1_residual_3 +
                                                 gauss_sii_2_residual_1 + gauss_sii_2_residual_2 + gauss_sii_2_residual_3))
        # plot
        # data
        ax.errorbar(x_data[mask_x_data], em_flux[mask_x_data], yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        # plot fit functions
        ax.plot(high_res_wave_sii, gauss_sii_1_1 + gauss_sii_2_1, color='b',
                linewidth=self.line_width)
        ax.plot(high_res_wave_sii, gauss_sii_1_2 + gauss_sii_2_2, color='y',
                linewidth=self.line_width)
        ax.plot(high_res_wave_sii, gauss_sii_1_3 + gauss_sii_2_3, color='r',
                linewidth=self.line_width)
        ax.plot(high_res_wave_sii, gauss_sii_1_1 + gauss_sii_1_2 + gauss_sii_1_3 +
                gauss_sii_2_1 + gauss_sii_2_2 + gauss_sii_2_3,
                color='g', linewidth=self.line_width)
        # plot residuals
        ax_residuals.errorbar(x_data[mask_x_data], residuals_sii,
                              yerr=em_flux_err[mask_x_data], fmt='.', color='k')
        ax_residuals.plot([min(x_data[mask_x_data]), max(x_data[mask_x_data])], [0, 0],
                          color='g', linewidth=self.line_width)
        # emission_line naming
        # ax.text(position_sii_1_1 - 6 * sigma_sii_1_1, fit_result_dict['amp_6718_1'], '[SII] 6718',
        #         fontsize=self.fontsize_spec)
        # ax.text(position_sii_2_2 + 1.3 * sigma_sii_2_2, fit_result_dict['amp_6733_2'], '[SII] 6733',
        #         fontsize=self.fontsize_spec)
        ax.text(0.6, 0.85, r'[SII] 6718, 6733', transform=ax.transAxes,  fontsize=fontsize)

        # set axis layout
        ax.set_xticklabels([])
        ax.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)
        ax_residuals.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize, top=True)
        ax_residuals.tick_params(axis='y', labelsize=fontsize - 2)
        if x_format == 'vel':
            ax.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(vel_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(vel_tick_steps / 4))
        elif x_format == 'wave':
            ax.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))
            ax_residuals.xaxis.set_major_locator(plt.MultipleLocator(wave_tick_steps))
            ax_residuals.xaxis.set_minor_locator(plt.MultipleLocator(wave_tick_steps / 4))

        ax.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)
        ax_residuals.set_xlim(min(x_data[mask_x_data]) - 1, max(x_data[mask_x_data]) + 1)

        if ylabel:
            ax.set_ylabel('Flux, 10e-17erg/cm$^2$/s/ $\AA$', labelpad=-2, fontsize=fontsize)
        if xlabel:
            if x_format == 'vel':
                ax_residuals.set_xlabel(r'Velocity, km s$^{-1}$', fontsize=fontsize, labelpad=-4)
            elif x_format == 'wave':
                ax_residuals.set_xlabel(r'Wavelength $\AA$', fontsize=self.fontsize_spec, labelpad=-4)

    def plot_single_gauss_fit_results_hi(self, fit_result_dict, vel_hi, flux_hi, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_hi = fig.add_axes([0.045, 0.3, 0.855, 0.68])
        ax_hi_residuals = fig.add_axes([0.045, 0.1, 0.855, 0.2])

        self.plot_single_gauss_line_hi(ax=ax_hi, ax_residuals=ax_hi_residuals,
                                              fit_result_dict=fit_result_dict,
                                              vel=vel_hi, flux=flux_hi, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_double_gauss_fit_results_hi(self, fit_result_dict, vel_hi, flux_hi, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_hi = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_hi_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_double_gauss_line_hi(ax=ax_hi, ax_residuals=ax_hi_residuals,
                                              fit_result_dict=fit_result_dict,
                                              vel=vel_hi, flux=flux_hi, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_triple_gauss_fit_results_hi(self, fit_result_dict, vel_hi, flux_hi, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_hi = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_hi_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_triple_gauss_line_hi(ax=ax_hi, ax_residuals=ax_hi_residuals,
                                              fit_result_dict=fit_result_dict,
                                              vel=vel_hi, flux=flux_hi, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_single_gauss_fit_results_co10(self, fit_result_dict, vel_co10, flux_co10, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_double_gauss_fit_results_co10(self, fit_result_dict, vel_co10, flux_co10, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_triple_gauss_fit_results_co10(self, fit_result_dict, vel_co10, flux_co10, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_triple_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_best_fit_results_co10(self, best_fit_result_dict, vel_co10, flux_co10, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        # plot co10
        if 'mu_co10_3' in best_fit_result_dict.keys():
            self.plot_triple_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        elif 'mu_co10_2' in best_fit_result_dict.keys():
            self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        else:
            self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        return fig

    def plot_single_gauss_fit_results_co21(self, fit_result_dict, vel_co21, flux_co21, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co21 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_double_gauss_fit_results_co21(self, fit_result_dict, vel_co21, flux_co21, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co21 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_triple_gauss_fit_results_co21(self, fit_result_dict, vel_co21, flux_co21, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co21 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        self.plot_triple_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)

        return fig

    def plot_best_fit_results_co21(self, best_fit_result_dict, vel_co21, flux_co21, sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co21 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])

        # plot co21
        if 'mu_co21_3' in best_fit_result_dict.keys():
            self.plot_triple_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        elif 'mu_co21_2' in best_fit_result_dict.keys():
            self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        else:
            self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        return fig

    def plot_single_gauss_fit_results_co10_co21(self, fit_result_dict, vel_co10, flux_co10, vel_co21, flux_co21,
                                                sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])
        ax_co21 = fig.add_axes([0.535, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.535, 0.1, 0.455, 0.2])

        self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=False)

        return fig

    def plot_double_gauss_fit_results_co10_co21(self, fit_result_dict, vel_co10, flux_co10, vel_co21, flux_co21,
                                                sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])
        ax_co21 = fig.add_axes([0.535, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.535, 0.1, 0.455, 0.2])

        self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=False)

        return fig

    def plot_triple_gauss_fit_results_co10_co21(self, fit_result_dict, vel_co10, flux_co10, vel_co21, flux_co21,
                                                sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])
        ax_co21 = fig.add_axes([0.535, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.535, 0.1, 0.455, 0.2])

        self.plot_triple_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        self.plot_triple_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=False)

        return fig

    def plot_best_fit_results_co10_co21(self, best_fit_result_dict,
                                        vel_co10, flux_co10, vel_co21, flux_co21,
                                                sys_vel):
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))

        ax_co10 = fig.add_axes([0.045, 0.3, 0.455, 0.68])
        ax_co10_residuals = fig.add_axes([0.045, 0.1, 0.455, 0.2])
        ax_co21 = fig.add_axes([0.535, 0.3, 0.455, 0.68])
        ax_co21_residuals = fig.add_axes([0.535, 0.1, 0.455, 0.2])

        # plot co10
        if 'mu_co10_3' in best_fit_result_dict.keys():
            self.plot_triple_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        elif 'mu_co10_2' in best_fit_result_dict.keys():
            self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        else:
            self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        # plot co21
        if 'mu_co21_3' in best_fit_result_dict.keys():
            self.plot_triple_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        elif 'mu_co21_2' in best_fit_result_dict.keys():
            self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        else:
            self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=best_fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)


        return fig

    def plot_single_gauss_fit_results_simple_sdss_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                            inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                            sys_vel, vel_co10, flux_co10, vel_co21, flux_co21):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([0.0, 0.55, 0.37, 0.37], projection=wcs)
        #
        ax_h_beta = fig.add_axes([0.34, 0.64, 0.3, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.34, 0.55, 0.3, 0.09])

        ax_oiii = fig.add_axes([0.69, 0.64, 0.3, 0.29])
        ax_oiii_residuals = fig.add_axes([0.69, 0.55, 0.3, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.45, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.45, 0.09])

        ax_co10 = fig.add_axes([0.55, 0.18, 0.2, 0.29])
        ax_co10_residuals = fig.add_axes([0.55, 0.09, 0.2, 0.09])

        ax_co21 = fig.add_axes([0.79, 0.18, 0.2, 0.29])
        ax_co21_residuals = fig.add_axes([0.79, 0.09, 0.2, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_obs_circle(ax=ax_image, ra=ra, dec=dec)

        # plot h_beta
        self.plot_single_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii
        self.plot_single_gauss_isolated_line(ax=ax_oiii, ax_residuals=ax_oiii_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot co
        self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=False)

        return fig

    def plot_double_gauss_fit_results_simple_sdss_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                            inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                            sys_vel, vel_co10, flux_co10, vel_co21, flux_co21):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([0.0, 0.55, 0.37, 0.37], projection=wcs)
        #
        ax_h_beta = fig.add_axes([0.34, 0.64, 0.3, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.34, 0.55, 0.3, 0.09])

        ax_oiii = fig.add_axes([0.69, 0.64, 0.3, 0.29])
        ax_oiii_residuals = fig.add_axes([0.69, 0.55, 0.3, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.45, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.45, 0.09])

        ax_co10 = fig.add_axes([0.55, 0.18, 0.2, 0.29])
        ax_co10_residuals = fig.add_axes([0.55, 0.09, 0.2, 0.09])

        ax_co21 = fig.add_axes([0.79, 0.18, 0.2, 0.29])
        ax_co21_residuals = fig.add_axes([0.79, 0.09, 0.2, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_obs_circle(ax=ax_image, ra=ra, dec=dec)

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii
        self.plot_double_gauss_isolated_line(ax=ax_oiii, ax_residuals=ax_oiii_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot co
        self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=False)

        return fig


    def plot_single_gauss_fit_results_simple(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.34, 0.64, 0.3, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.34, 0.55, 0.3, 0.09])

        ax_oiii = fig.add_axes([0.69, 0.64, 0.3, 0.29])
        ax_oiii_residuals = fig.add_axes([0.69, 0.55, 0.3, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.45, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.45, 0.09])

        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        if manga_footprint is not None:
            # plot MaNGA FoV
            ImgPlot.plot_manga_fov(manga_im=manga_footprint, ax=ax_image, wcs= wcs, color='magenta', linestyle='-', linewidth=2, alpha=1)
        
        # plot h_beta
        self.plot_single_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii
        self.plot_single_gauss_isolated_line(ax=ax_oiii, ax_residuals=ax_oiii_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)

        return fig

    def plot_single_gauss_fit_results_complex(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.5, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.5, 0.09])

        ax_sii = fig.add_axes([0.58, 0.18, 0.41, 0.29])
        ax_sii_residuals = fig.add_axes([0.58, 0.09, 0.41, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        if manga_footprint is not None:
            # plot MaNGA FoV
            ImgPlot.plot_manga_fov(manga_im=manga_footprint, ax=ax_image, wcs= wcs, color='magenta', linestyle='-', linewidth=2, alpha=1)

        # plot h_beta
        self.plot_single_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_single_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_single_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_single_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)

        return fig

    def plot_double_gauss_fit_results_simple(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.34, 0.64, 0.3, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.34, 0.55, 0.3, 0.09])

        ax_oiii = fig.add_axes([0.69, 0.64, 0.3, 0.29])
        ax_oiii_residuals = fig.add_axes([0.69, 0.55, 0.3, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.45, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.45, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        if manga_footprint is not None:
            # plot MaNGA FoV
            ImgPlot.plot_manga_fov(manga_im=manga_footprint, ax=ax_image, wcs= wcs, color='magenta', linestyle='-', linewidth=2, alpha=1)

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii
        self.plot_double_gauss_isolated_line(ax=ax_oiii, ax_residuals=ax_oiii_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)


        return fig

    def plot_double_gauss_fit_results_complex(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.5, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.5, 0.09])

        ax_sii = fig.add_axes([0.58, 0.18, 0.41, 0.29])
        ax_sii_residuals = fig.add_axes([0.58, 0.09, 0.41, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='c', linestyle='-', linewidth=2, alpha=1.)

        if manga_footprint is not None:
            # plot MaNGA FoV
            ImgPlot.plot_manga_fov(manga_im=manga_footprint, ax=ax_image, wcs=wcs, color='m', linestyle='-', linewidth=2, alpha=1)

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)

        return fig

    def plot_triple_gauss_fit_results_complex(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.5, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.5, 0.09])

        ax_sii = fig.add_axes([0.58, 0.18, 0.41, 0.29])
        ax_sii_residuals = fig.add_axes([0.58, 0.09, 0.41, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        if manga_footprint is not None:
            # plot MaNGA FoV
            ImgPlot.plot_manga_fov(manga_im=manga_footprint, ax=ax_image, wcs= wcs, color='magenta', linestyle='-', linewidth=2, alpha=1)

        # plot h_beta
        self.plot_triple_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_triple_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_triple_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_triple_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_triple_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot sii
        self.plot_triple_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)

        return fig

    def plot_single_gauss_fit_results_complex_sdss_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                             inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                             sys_vel, vel_co10, flux_co10, vel_co21, flux_co21,
                                                             co10_color='g', co21_color='y', co10_radius=23/2):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.3, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.3, 0.09])

        ax_sii = fig.add_axes([0.38, 0.18, 0.24, 0.29])
        ax_sii_residuals = fig.add_axes([0.38, 0.09, 0.24, 0.09])

        ax_co10 = fig.add_axes([0.66, 0.18, 0.15, 0.29])
        ax_co10_residuals = fig.add_axes([0.66, 0.09, 0.15, 0.09])

        ax_co21 = fig.add_axes([0.84, 0.18, 0.15, 0.29])
        ax_co21_residuals = fig.add_axes([0.84, 0.09, 0.15, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=3/2, color='r', linestyle='-', linewidth=3, alpha=1.)
        # co10
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--', linewidth=3, alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=12/2, color=co21_color, linestyle='--', linewidth=3, alpha=1.)


        # plot h_beta
        self.plot_single_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_single_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_single_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_single_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)
        # plot co
        self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                              fit_result_dict=fit_result_dict, line='co10',
                                              vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                              fit_result_dict=fit_result_dict, line='co21',
                                              vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                              fontsize=self.fontsize_spec, xlabel=True, ylabel=False)

        return fig

    def plot_double_gauss_fit_results_complex_sdss_co10(self, fit_result_dict, header, img, ra, dec,
                                                        inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                        sys_vel, vel_co10, flux_co10, n_gauss_co=2, title=None,
                                                        co10_radius=23/2, co10_color='g', beam_data=None,
                                                        co_vel_ticks=500):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.3, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.3, 0.09])

        ax_sii = fig.add_axes([0.38, 0.18, 0.24, 0.29])
        ax_sii_residuals = fig.add_axes([0.38, 0.09, 0.24, 0.09])

        ax_co10 = fig.add_axes([0.66, 0.18, 0.33, 0.29])
        ax_co10_residuals = fig.add_axes([0.66, 0.09, 0.33, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=3,
                                  alpha=1.)
        # co10
        if beam_data is None:
            ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                      linewidth=3, alpha=1.)
        else:
            print(dec + (beam_data[:, 1].T)/3600, ra + (beam_data[:, 0].T)/3600)
            ax_image.scatter(ra + np.flip((beam_data[:, 0].T)/3600), dec + (beam_data[:, 1].T)/3600,
                             transform=ax_image.get_transform('world'), s=3, color='b')

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)
        # plot co
        if n_gauss_co == 2:
            self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True,
                                                  vel_tick_steps=co_vel_ticks)
        elif n_gauss_co == 1:
            self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True,
                                                  vel_tick_steps=co_vel_ticks)
        return fig

    def plot_double_gauss_fit_results_complex_sdss_co21(self, fit_result_dict, header, img, ra, dec,
                                                        inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                        sys_vel, vel_co21, flux_co21, n_gauss_co=2, title=None,
                                                         co21_color='y', co21_radius=12/2):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.3, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.3, 0.09])

        ax_sii = fig.add_axes([0.38, 0.18, 0.24, 0.29])
        ax_sii_residuals = fig.add_axes([0.38, 0.09, 0.24, 0.09])

        ax_co21 = fig.add_axes([0.66, 0.18, 0.33, 0.29])
        ax_co21_residuals = fig.add_axes([0.66, 0.09, 0.33, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=3,
                                  alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co21_radius, color=co21_color, linestyle='--',
                                  linewidth=3, alpha=1.)

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)
        # plot co
        if n_gauss_co == 2:
            self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        elif n_gauss_co == 1:
            self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
        return fig

    def plot_double_gauss_fit_results_complex_sdss_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                             inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                             sys_vel, vel_co10, flux_co10, vel_co21, flux_co21,
                                                             n_gauss_co=2, title=None, co10_color='g', co21_color='y',
                                                             co10_radius=23/2, co21_radius=12/2):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.3, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.3, 0.09])

        ax_sii = fig.add_axes([0.38, 0.18, 0.24, 0.29])
        ax_sii_residuals = fig.add_axes([0.38, 0.09, 0.24, 0.09])

        ax_co10 = fig.add_axes([0.66, 0.18, 0.15, 0.29])
        ax_co10_residuals = fig.add_axes([0.66, 0.09, 0.15, 0.09])

        ax_co21 = fig.add_axes([0.84, 0.18, 0.15, 0.29])
        ax_co21_residuals = fig.add_axes([0.84, 0.09, 0.15, 0.09])


        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=3,
                                  alpha=1.)
        # co10
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                  linewidth=3, alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co21_radius, color=co21_color, linestyle='--',
                                  linewidth=3, alpha=1.)

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)
        # plot co
        if n_gauss_co == 1:
            self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
            self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=False)
        elif n_gauss_co == 2:
            self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
            self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=False)
        elif n_gauss_co == 3:
            self.plot_triple_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=True)
            self.plot_triple_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_spec, xlabel=True, ylabel=False)
        return fig

    def plot_double_gauss_fit_results_h_apha_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                       inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                       sys_vel, vel_co10, flux_co10, vel_co21, flux_co21, n_gauss_co=2,
                                                       title=None, co10_color='g', co21_color='y',
                                                       co10_radius=23/2, co21_radius=12/2):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, 3))
        # snapshot
        ax_image = fig.add_axes([-0.24, 0.15, 0.73, 0.73], projection=wcs)

        ax_h_alpha_nii = fig.add_axes([0.23, 0.33, 0.29, 0.55])
        ax_h_alpha_nii_residuals = fig.add_axes([0.23, 0.13, 0.29, 0.2])

        ax_co10 = fig.add_axes([0.56, 0.33, 0.2, 0.55])
        ax_co10_residuals = fig.add_axes([0.56, 0.13, 0.2, 0.2])

        ax_co21 = fig.add_axes([0.79, 0.33, 0.2, 0.55])
        ax_co21_residuals = fig.add_axes([0.79, 0.13, 0.2, 0.2])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec,
                                 x_label_rot=0, y_label_rot=20, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=3,
                                  alpha=1.)
        # co10
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                  linewidth=3, alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co21_radius, color=co21_color, linestyle='--',
                                  linewidth=3, alpha=1.)

        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot co
        if n_gauss_co == 2:
            self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True)
            self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=False)
        elif n_gauss_co == 1:
            self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True)
            self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=False)
        return fig

    def plot_double_gauss_fit_results_h_apha_co10(self, fit_result_dict, header, img, ra, dec,
                                                  inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                  sys_vel, vel_co10, flux_co10, n_gauss_co=2, title=None,
                                                  co10_color='g',  co10_radius=23/2, beam_data=None, co_vel_ticks=500,
                                                  co_line_name_pos=400):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, 3))
        # snapshot
        ax_image = fig.add_axes([-0.24, 0.15, 0.73, 0.73], projection=wcs)

        ax_h_alpha_nii = fig.add_axes([0.23, 0.33, 0.29, 0.55])
        ax_h_alpha_nii_residuals = fig.add_axes([0.23, 0.13, 0.29, 0.2])

        ax_co10 = fig.add_axes([0.56, 0.33, 0.2, 0.55])
        ax_co10_residuals = fig.add_axes([0.56, 0.13, 0.2, 0.2])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec,
                                 x_label_rot=0, y_label_rot=20, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=3,
                                  alpha=1.)
        # co10
        if beam_data is None:
            ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                      linewidth=3, alpha=1.)
        else:
            print(dec + (beam_data[:, 1].T)/3600, ra + (beam_data[:, 0].T)/3600)
            ax_image.scatter(ra + np.flip((beam_data[:, 0].T)/3600), dec + (beam_data[:, 1].T)/3600,
                             transform=ax_image.get_transform('world'), s=3, color='b')
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot co
        if n_gauss_co == 2:
            self.plot_double_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec,
                                                  xlabel=True, ylabel=True, vel_tick_steps=co_vel_ticks,
                                                  line_name_pos=co_line_name_pos)
        elif n_gauss_co == 1:
            self.plot_single_gauss_fit_results_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec,
                                                  xlabel=True, ylabel=True, vel_tick_steps=co_vel_ticks,
                                                  line_name_pos=co_line_name_pos)

        return fig

    def plot_double_gauss_fit_results_h_apha_co21(self, fit_result_dict, header, img, ra, dec,
                                                  inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                  sys_vel, vel_co21, flux_co21, n_gauss_co=2, title=None,
                                                  co21_radius=12/2, co_vel_ticks=500, co_line_name_pos=400):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, 3))
        # snapshot
        ax_image = fig.add_axes([-0.24, 0.15, 0.73, 0.73], projection=wcs)

        ax_h_alpha_nii = fig.add_axes([0.23, 0.33, 0.29, 0.55])
        ax_h_alpha_nii_residuals = fig.add_axes([0.23, 0.13, 0.29, 0.2])

        ax_co21 = fig.add_axes([0.79, 0.33, 0.2, 0.55])
        ax_co21_residuals = fig.add_axes([0.79, 0.13, 0.2, 0.2])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec,
                                 x_label_rot=0, y_label_rot=20, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=3,
                                  alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co21_radius, color='y', linestyle='--',
                                  linewidth=3, alpha=1.)

        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot co
        if n_gauss_co == 2:
            self.plot_double_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec,
                                                  xlabel=True, ylabel=True, line_name_pos=co_line_name_pos)
        elif n_gauss_co == 1:
            self.plot_single_gauss_fit_results_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec,
                                                  xlabel=True, ylabel=True, line_name_pos=co_line_name_pos)

        return fig

    def plot_double_gauss_fit_results_h_apha_best_fit_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                             inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                             sys_vel, vel_co10, flux_co10, vel_co21, flux_co21,
                                                             title=None, co10_color='g', co21_color='k',
                                                             co10_radius=23/2, co21_radius=12/2, co_vel_ticks=500):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, 3))
        # snapshot
        ax_image = fig.add_axes([-0.24, 0.15, 0.73, 0.73], projection=wcs)

        ax_h_alpha_nii = fig.add_axes([0.23, 0.33, 0.29, 0.55])
        ax_h_alpha_nii_residuals = fig.add_axes([0.23, 0.13, 0.29, 0.2])

        ax_co10 = fig.add_axes([0.56, 0.33, 0.2, 0.55])
        ax_co10_residuals = fig.add_axes([0.56, 0.13, 0.2, 0.2])

        ax_co21 = fig.add_axes([0.79, 0.33, 0.2, 0.55])
        ax_co21_residuals = fig.add_axes([0.79, 0.13, 0.2, 0.2])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec,
                                 x_label_rot=0, y_label_rot=20, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=2,
                                  alpha=1.)
        # co10
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                  linewidth=3, alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co21_radius, color=co21_color, linestyle='--',
                                  linewidth=3, alpha=1.)

        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot co10
        if ~np.isnan(fit_result_dict['mu_co10_3']):
            self.plot_triple_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=True, vel_tick_steps=co_vel_ticks)
        elif ~np.isnan(fit_result_dict['mu_co10_2']):
            self.plot_double_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=True, vel_tick_steps=co_vel_ticks)
        else:
            self.plot_single_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=True, vel_tick_steps=co_vel_ticks)

        # plot co21
        if ~np.isnan(fit_result_dict['mu_co21_3']):
            self.plot_triple_gauss_fit_results_custom_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=False, vel_tick_steps=co_vel_ticks)
        elif ~np.isnan(fit_result_dict['mu_co21_2']):
            self.plot_double_gauss_fit_results_custom_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=False, vel_tick_steps=co_vel_ticks)
        else:
            self.plot_single_gauss_fit_results_custom_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=False, vel_tick_steps=co_vel_ticks)

        return fig

    def plot_double_gauss_fit_results_h_apha_best_fit_co10(self, fit_result_dict, header, img, ra, dec,
                                                  inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                  sys_vel, vel_co10, flux_co10, title=None,
                                                  co10_color='g',  co10_radius=23/2, beam_data=None, co_vel_ticks=500,
                                                  co_line_name_pos=400):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, 3))
        # snapshot
        ax_image = fig.add_axes([-0.24, 0.15, 0.73, 0.73], projection=wcs)

        ax_h_alpha_nii = fig.add_axes([0.23, 0.33, 0.29, 0.55])
        ax_h_alpha_nii_residuals = fig.add_axes([0.23, 0.13, 0.29, 0.2])

        ax_co10 = fig.add_axes([0.56, 0.33, 0.2, 0.55])
        ax_co10_residuals = fig.add_axes([0.56, 0.13, 0.2, 0.2])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec,
                                 x_label_rot=0, y_label_rot=20, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=2,
                                  alpha=1.)

        # co10
        if beam_data is None:
            ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                      linewidth=3, alpha=1.)
        else:
            print(dec + (beam_data[:, 1].T)/3600, ra + (beam_data[:, 0].T)/3600)
            ax_image.scatter(ra + np.flip((beam_data[:, 0].T)/3600), dec + (beam_data[:, 1].T)/3600,
                             transform=ax_image.get_transform('world'), s=3, color='b')

        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot co10
        if ~np.isnan(fit_result_dict['mu_co10_3']):
            self.plot_triple_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True,
                                                         vel_tick_steps=co_vel_ticks)
        elif ~np.isnan(fit_result_dict['mu_co10_2']):
            self.plot_double_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True,
                                                         vel_tick_steps=co_vel_ticks)
        else:
            self.plot_single_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True,
                                                         vel_tick_steps=co_vel_ticks)

        return fig

    def plot_double_gauss_fit_results_complex_sdss_best_fit_co10_co21(self, fit_result_dict, header, img, ra, dec,
                                                             inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                             sys_vel, vel_co10, flux_co10, vel_co21, flux_co21,
                                                             title=None, co10_color='g', co21_color='k',
                                                             co10_radius=23/2, co21_radius=12/2, co_vel_ticks=500):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.3, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.3, 0.09])

        ax_sii = fig.add_axes([0.38, 0.18, 0.24, 0.29])
        ax_sii_residuals = fig.add_axes([0.38, 0.09, 0.24, 0.09])

        ax_co10 = fig.add_axes([0.66, 0.18, 0.15, 0.29])
        ax_co10_residuals = fig.add_axes([0.66, 0.09, 0.15, 0.09])

        ax_co21 = fig.add_axes([0.84, 0.18, 0.15, 0.29])
        ax_co21_residuals = fig.add_axes([0.84, 0.09, 0.15, 0.09])


        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=2,
                                  alpha=1.)
        # co10
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                  linewidth=3, alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co21_radius, color=co21_color, linestyle='--',
                                  linewidth=3, alpha=1.)

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot sii
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_narrow_full_page_spec)
        # plot co10
        if ~np.isnan(fit_result_dict['mu_co10_3']):
            self.plot_triple_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=True, large_frame=True, vel_tick_steps=co_vel_ticks)
        elif ~np.isnan(fit_result_dict['mu_co10_2']):
            self.plot_double_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=True, large_frame=True, vel_tick_steps=co_vel_ticks)
        else:
            self.plot_single_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=True, large_frame=True, vel_tick_steps=co_vel_ticks)

        # plot co21
        if ~np.isnan(fit_result_dict['mu_co21_3']):
            self.plot_triple_gauss_fit_results_custom_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=False, large_frame=True, vel_tick_steps=co_vel_ticks)
        elif ~np.isnan(fit_result_dict['mu_co21_2']):
            self.plot_double_gauss_fit_results_custom_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=False, large_frame=True, vel_tick_steps=co_vel_ticks)
        else:
            self.plot_single_gauss_fit_results_custom_co(ax=ax_co21, ax_residuals=ax_co21_residuals,
                                                  fit_result_dict=fit_result_dict, line='co21',
                                                  vel=vel_co21, flux=flux_co21, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True,
                                                         ylabel=False, large_frame=True, vel_tick_steps=co_vel_ticks)


        return fig

    def plot_double_gauss_fit_results_complex_sdss_best_fit_co10(self, fit_result_dict, header, img, ra, dec,
                                                  inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                                  sys_vel, vel_co10, flux_co10, title=None,
                                                  co10_color='g',  co10_radius=23/2, beam_data=None, co_vel_ticks=500,
                                                  co_line_name_pos=400):

       # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.3, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.3, 0.09])

        ax_sii = fig.add_axes([0.38, 0.18, 0.24, 0.29])
        ax_sii_residuals = fig.add_axes([0.38, 0.09, 0.24, 0.09])

        ax_co10 = fig.add_axes([0.66, 0.18, 0.15, 0.29])
        ax_co10_residuals = fig.add_axes([0.66, 0.09, 0.15, 0.09])


        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_narrow_full_page_spec,
                                 x_label_rot=0, y_label_rot=20, title=title)
        # sdss
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=1.5, color='r', linestyle='-', linewidth=2,
                                  alpha=1.)
        # co10
        if beam_data is None:
            ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=co10_radius, color=co10_color, linestyle='--',
                                      linewidth=3, alpha=1.)
        else:
            print(dec + (beam_data[:, 1].T)/3600, ra + (beam_data[:, 0].T)/3600)
            ax_image.scatter(ra + np.flip((beam_data[:, 0].T)/3600), dec + (beam_data[:, 1].T)/3600,
                             transform=ax_image.get_transform('world'), s=3, color='b')

        # plot h_beta
        self.plot_double_gauss_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_narrow_full_page_spec)
        # plot h_alpha
        self.plot_double_gauss_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_narrow_full_page_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_narrow_full_page_spec)
        # plot co10
        if ~np.isnan(fit_result_dict['mu_co10_3']):
            self.plot_triple_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True, large_frame=True,
                                                         vel_tick_steps=co_vel_ticks)
        elif ~np.isnan(fit_result_dict['mu_co10_2']):
            self.plot_double_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec,
                                                         xlabel=True, ylabel=True, large_frame=True,
                                                         vel_tick_steps=co_vel_ticks)
        else:
            self.plot_single_gauss_fit_results_custom_co(ax=ax_co10, ax_residuals=ax_co10_residuals,
                                                  fit_result_dict=fit_result_dict, line='co10',
                                                  vel=vel_co10, flux=flux_co10, sys_vel=sys_vel,
                                                  fontsize=self.fontsize_narrow_full_page_spec, xlabel=True, ylabel=True, large_frame=True,
                                                         vel_tick_steps=co_vel_ticks)

        return fig

    def plot_double_gauss_fit_results_muse_weak(self, fit_result_dict,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel):

        # get wsc projection
        # wcs = WCS(header)
        # # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        # ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        # ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        # ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        # ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        # ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])
        #
        # ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        # ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])
        #
        # ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        # ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])


        ax_h_beta = fig.add_axes([0.05, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.05, 0.55, 0.15, 0.09])


        ax_oiii_1 = fig.add_axes([0.23, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.23, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.41, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.41, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.59, 0.64, 0.4, 0.29])
        ax_oi_residuals = fig.add_axes([0.59, 0.55, 0.4, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.5, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.5, 0.09])

        ax_sii = fig.add_axes([0.58, 0.18, 0.41, 0.29])
        ax_sii_residuals = fig.add_axes([0.58, 0.09, 0.41, 0.09])

        # # plot coord image
        # ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        # ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        # plot h_beta
        self.plot_double_gauss_single_broad_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_double_gauss_oi_doublet(ax=ax_oi, ax_residuals=ax_oi_residuals,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_single_broad_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)

        return fig


    def plot_single_gauss_single_broad_fit_results_complex(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.5, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.5, 0.09])

        ax_sii = fig.add_axes([0.58, 0.18, 0.41, 0.29])
        ax_sii_residuals = fig.add_axes([0.58, 0.09, 0.41, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        # plot h_beta
        self.plot_single_gauss_single_broad_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_single_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_single_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_single_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_single_broad_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_single_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)

        return fig

    def plot_double_gauss_single_broad_fit_results_complex(self, fit_result_dict, header, img, ra, dec,
                                              inst_broad_dict, wave_sdss, em_flux_sdss, em_flux_err_sdss,
                                              sys_vel, fiber_radius=1.5, manga_footprint=None):

        # get wsc projection
        wcs = WCS(header)
        # create figure
        fig = plt.figure(figsize=(self.fig_size_x_axis_spec_plot, self.fig_size_y_axis_spec_plot))
        # snapshot
        ax_image = fig.add_axes([-0.04, 0.55, 0.35, 0.35], projection=wcs)

        ax_h_beta = fig.add_axes([0.29, 0.64, 0.15, 0.29])
        ax_h_beta_residuals = fig.add_axes([0.29, 0.55, 0.15, 0.09])

        ax_oiii_1 = fig.add_axes([0.47, 0.64, 0.15, 0.29])
        ax_oiii_1_residuals = fig.add_axes([0.47, 0.55, 0.15, 0.09])

        ax_oiii_2 = fig.add_axes([0.66, 0.64, 0.15, 0.29])
        ax_oiii_2_residuals = fig.add_axes([0.66, 0.55, 0.15, 0.09])

        ax_oi = fig.add_axes([0.84, 0.64, 0.15, 0.29])
        ax_oi_residuals = fig.add_axes([0.84, 0.55, 0.15, 0.09])

        ax_h_alpha_nii = fig.add_axes([0.05, 0.18, 0.5, 0.29])
        ax_h_alpha_nii_residuals = fig.add_axes([0.05, 0.09, 0.5, 0.09])

        ax_sii = fig.add_axes([0.58, 0.18, 0.41, 0.29])
        ax_sii_residuals = fig.add_axes([0.58, 0.09, 0.41, 0.09])

        # plot coord image
        ImgPlot.plot_coord_image(ax=ax_image, header=header, img=img, fontsize=self.fontsize_spec)
        ImgPlot.plot_coord_circle(ax=ax_image, ra=ra, dec=dec, radius=fiber_radius, color='r', linestyle='-', linewidth=3, alpha=1.)

        # plot h_beta
        self.plot_double_gauss_single_broad_isolated_line(ax=ax_h_beta, ax_residuals=ax_h_beta_residuals, line=4863,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=True,
                                             fontsize=self.fontsize_spec)
        # plot oiii 1
        self.plot_double_gauss_isolated_line(ax=ax_oiii_1, ax_residuals=ax_oiii_1_residuals, line=4960,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oiii 2
        self.plot_double_gauss_isolated_line(ax=ax_oiii_2, ax_residuals=ax_oiii_2_residuals, line=5008,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot oi
        self.plot_double_gauss_isolated_line(ax=ax_oi, ax_residuals=ax_oi_residuals, line=6302,
                                             fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                             wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                             sys_vel=sys_vel, x_format='vel', xlabel=False, ylabel=False,
                                             fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_single_broad_h_alpha_nii(ax=ax_h_alpha_nii, ax_residuals=ax_h_alpha_nii_residuals,
                                           fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                           wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                           sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=True,
                                           fontsize=self.fontsize_spec)
        # plot h_alpha
        self.plot_double_gauss_sii(ax=ax_sii, ax_residuals=ax_sii_residuals,
                                   fit_result_dict=fit_result_dict, inst_broad_dict=inst_broad_dict,
                                   wave=wave_sdss, em_flux=em_flux_sdss, em_flux_err=em_flux_err_sdss,
                                   sys_vel=sys_vel, x_format='vel', xlabel=True, ylabel=False,
                                   fontsize=self.fontsize_spec)

        return fig