# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import os
from pathlib import Path

from scipy.constants import c as speed_of_light
from astropy.io import fits
from re import findall

from xgalspectool.fit_algorithm import em_fit_algorithm
from xgalspectool.spec_fit import helper_tools
from xgaltool.basic_attributes import download_file


class RCSEDSpecHelper(em_fit_algorithm.EmissionLineFit):

    def __init__(self, sdss_mjd=None, sdss_plate=None, sdss_fiberid=None, rcsed_version=1, data_path=None, **kwargs):
        """
        Class to access SDSS emission line spectra. We use processed files from the RCSED catalog.
        Parameters
        ---------
        sdss_mjd, sdss_plate, sdss_fiberid: int
          mjd, plate and fiberid as SDSS spectral observation identifier.
        data_path : str
          data path to RCSED .fits files / folder structure
        """
        super().__init__(**kwargs)
        # check if data_path is given and correct
        if data_path is None:  # use short cut
            data_path = Path.home() / 'data'
        if os.path.isdir(Path(data_path)):
            self.data_path = Path(data_path)
        else:
            raise OSError('The data path %s is not existent' % data_path)

        if (sdss_mjd is not None) & (sdss_plate is not None) & (sdss_fiberid is not None):
            self.sdss_mjd = int(sdss_mjd)
            self.sdss_plate = int(sdss_plate)
            self.sdss_fiberid = int(sdss_fiberid)
        else:
            self.sdss_mjd = None
            self.sdss_plate = None
            self.sdss_fiberid = None

        # load rcsed version into attributes
        self.rcsed_version = rcsed_version

        # load the spectrum into attributes
        self.spec_dict = None

    def check_rcsed_identifier(self):
        """ Function to check weather the RCSED identifier are set"""
        if (self.sdss_mjd is None) & (self.sdss_plate is None) & (self.sdss_fiberid is None):
            raise KeyError('To access RCSED/SDSS spectra you need to specify the concrete identifier namely '
                           'self.sdss_mjd, self.sdss_plate, self.sdss_fiberid')
        else:
            return True

    def get_rcsed_object_file_path(self):
        """ function to get file_path of sdss spectral objects """
        # check if identifier is correct
        self.check_rcsed_identifier()
        # merge filepath
        file_path = (self.data_path / 'galaxies' / 'sdss_spectra' /
                     str(self.sdss_mjd) / str(self.sdss_plate) / str(self.sdss_fiberid))
        # check if filepath exists and create it if necessary
        if not os.path.isdir(file_path):
            os.makedirs(file_path)
        return file_path

    def get_rcsed_spec_identifier(self, nburst_fit='standard'):
        """ get file_name of sdss spectra """
        # check if identifier is correct
        self.check_rcsed_identifier()
        if nburst_fit == 'standard':
            if self.rcsed_version == 1:
                # get identifier as strings (mjd 5 digits, plate with and fiberid with 3)
                file_name = ('sdss_spec_contfit_nburst_rcsed1'
                             + '_' + f'{self.sdss_mjd}'
                                     '_' + f'{self.sdss_plate:04d}'
                                           '_' + f'{self.sdss_fiberid:03d}' + '.fits')
                url = ('http://gal-02.sai.msu.ru/data1/fit_MILES_x/'
                       + f'{self.sdss_plate:04d}'
                       + '/spSpec'
                       + f'{self.sdss_mjd}' + '-'
                       + f'{self.sdss_plate:04d}' + '-'
                       + f'{self.sdss_fiberid:03d}'
                       + '_results.fits')
                return file_name, url
            elif self.rcsed_version == 2:
                # get identifier as strings (mjd, plate and fiber id will have 5 digits !!!! )
                file_name = ('sdss_spec_nburst_MILES_MGFE_EMIS1_rcsed2'
                             + '_' + f'{self.sdss_mjd}'
                                     '_' + f'{self.sdss_plate:05d}'
                                           '_' + f'{self.sdss_fiberid:05d}' + '.fits')
                # example url This was found in the backend at: https://api.dev-rcsed2.voxastro.org/spec/755200/
                # file URL = "https://gal-02.voxastro.org/rcsed2/sdss/MILES_MGFE_EMIS1/
                # 01825/nbursts_sdss_01825_53504_00471.fits.gz",
                url = ('https://gal-02.voxastro.org/rcsed2/sdss/MILES_MGFE_EMIS1/'
                       + f'{self.sdss_plate:05d}'
                       + '/nbursts_sdss_'
                       + f'{self.sdss_plate:05d}' + '_'
                       + f'{self.sdss_mjd}' + '_'
                       + f'{self.sdss_fiberid:05d}'
                       + '.fits.gz')
                return file_name, url
            else:
                raise KeyError('The RCSED version hasto be 1 or 2 !')
        elif nburst_fit == 'manual':
            # get identifier as strings (mjd 5 digits, plate with and fiberid with 3)
            file_name = ('spSpec'
                         + f'{self.sdss_mjd}' 
                           '-' + f'{self.sdss_plate:04d}'
                                 '-' + f'{self.sdss_fiberid:03d}' + '_results' + '.fits')
            return file_name, None
        else:
            raise KeyError('burst_fit must be standard or manual')

    def download_rcsed_spec_fits_file(self, reload=False):
        """download an sdss object spectrum processed by rcsed"""

        # get filepath where to store data
        file_path = self.get_rcsed_object_file_path()
        # get file name of rcsed spectrum
        spec_file_name, url = self.get_rcsed_spec_identifier()
        # the URL file is .gz for the RCSED version 2
        if self.rcsed_version == 2:
            unpack_flag = True
        else:
            unpack_flag = False
        download_file(file_path=file_path, file_name=spec_file_name, unpack=unpack_flag, url=url, reload=reload)

    def get_rcsed_spec_dict(self, nburst_fit='standard', stellar_continuum=True, gaussian_fit=True, nonpar_fit=True, **kwargs):
        """load SDSS spectrum from the RCSED fit and passes it into a dictionary"""

        # check if spec dict is already in attributes
        if self.spec_dict is not None:
            return self.spec_dict

        # if not yet loaded, get an empty dictionary
        spec_dict = {}

        # get data access
        file_path = self.get_rcsed_object_file_path()
        spec_file_name, url = self.get_rcsed_spec_identifier(nburst_fit=nburst_fit)

        # download the file
        if self.rcsed_version == 2:
            unpack_flag = True
        else:
            unpack_flag = False
        download_file(file_path=file_path, file_name=spec_file_name, unpack=unpack_flag, url=url)

        if self.rcsed_version == 1:
            # access file
            try:
                # try to open. This can cause trouble due to corrupted files
                hdu = fits.open(file_path / spec_file_name)
            except OSError:
                # delete spec file and reload
                print('cannot open', file_path / spec_file_name)
                print('--->reload file')
                # delete file and download again
                download_file(file_path=file_path, file_name=spec_file_name, unpack=unpack_flag, url=url, reload=True)
                hdu = fits.open(file_path / spec_file_name)
            # RCSED Version 1
            # hdu 0 original observed spectrum
            # hdu 1 continuum fit of spectrum
            # hdu 2 gaussian emission line fit of spectrum
            # hdu 3 non-parametric emission line fit of spectrum

            # access dta
            if stellar_continuum:
                # get information from first hdu
                # stellar line of sight velocity
                spec_dict.update({'los_v_star': hdu[1].data['V'][0]})
                # stellar line of sight velocity error
                spec_dict.update({'los_v_star_err': hdu[1].data['E_V'][0]})
                # stellar standard deviation of the line of sight velocity
                spec_dict.update({'los_v_star_sig': hdu[1].data['SIG'][0]})
                # err of the stellar standard deviation of the line of sight velocity
                spec_dict.update({'los_v_star_sig_err': hdu[1].data['E_SIG'][0]})
                # observed spectrum
                spec_dict.update({'total_flux': hdu[1].data['FLUX'][0]})
                # uncertainties
                spec_dict.update({'total_flux_err': hdu[1].data['ERROR'][0]})
                # best continuum fit
                spec_dict.update({'fit_contin': hdu[1].data['FIT'][0]})
                # wavelengths
                spec_dict.update({'wave_total_flux': hdu[1].data['WAVE'][0]})
                # instrumental broadening
                spec_dict.update({'inst_broad': hdu[1].data['LSF_SIG'][0]})
                # all pixels where a emission line is present
                spec_dict.update({'good_pixels': hdu[1].data['GOODPIXELS'][0]})
            # check new data format for fit results of RCSED version 2
            if gaussian_fit | nonpar_fit:
                # information of second hdu (gaussian fit)
                # emission line names and wavelength this is the same for hdu 3. We also extract the line identifier for
                spec_dict.update({'rcsed_em_name': hdu[2].data['LINE_NAME'][0]})
                em_identifier = [round(float(findall('\d+\.\d+', spec_dict['rcsed_em_name'][x])[0]))
                                 for x in range(len(spec_dict['rcsed_em_name']))]
                spec_dict.update({'em_identifier': em_identifier})

            if gaussian_fit:
                # spectral information (gaussian fit)
                # wavelength gaussian fit
                spec_dict.update({'wave_gauss': hdu[2].data['SPEC_WAVE'][0]})
                # gaussian fit
                spec_dict.update({'em_fit_gauss': hdu[2].data['SPEC_FIT'][0]})
                # the pure emission line spectrum as same shape of wavelength same data as total_flux - fit_contin
                spec_dict.update({'em_spec_gauss': hdu[2].data['SPEC_EMIS'][0]})
                # gaussian line flux, err and reduced chi2
                spec_dict.update({'em_flux_gauss': hdu[2].data['LINE_FLUX'][0]})
                spec_dict.update({'em_flux_err_gauss': hdu[2].data['LINE_FLUX_ERR'][0]})
                spec_dict.update({'em_chi2dof_gauss': hdu[2].data['SPEC_CHI2DOF'][0]})

                # get fit and flux and fit reprojected to continuum wavelength
                em_spec_gauss_reprojected = np.zeros(len(spec_dict['wave_total_flux']))
                em_fit_gauss_reprojected = np.zeros(len(spec_dict['wave_total_flux']))
                for index in range(len(spec_dict['wave_total_flux'])):
                    if spec_dict['wave_total_flux'][index] in spec_dict['wave_gauss']:
                        em_spec_gauss_reprojected[index] = \
                            spec_dict['em_spec_gauss'][spec_dict['wave_gauss'] == spec_dict['wave_total_flux'][index]]
                        em_fit_gauss_reprojected[index] = \
                            spec_dict['em_fit_gauss'][spec_dict['wave_gauss'] == spec_dict['wave_total_flux'][index]]
                spec_dict.update({'em_spec_gauss_reprojected': em_spec_gauss_reprojected})
                spec_dict.update({'em_fit_gauss_reprojected': em_fit_gauss_reprojected})

                # get line of sight velocities
                # there are two numbers first is the velocity second is the gaussian dispersion (sigma)
                spec_dict.update({'los_v_forbid_gauss': hdu[2].data['LOSVD_FORBIDDEN'][0][0]})
                spec_dict.update({'los_v_forbid_gauss_sig': hdu[2].data['LOSVD_FORBIDDEN'][0][1]})
                spec_dict.update({'los_v_forbid_gauss_err': hdu[2].data['LOSVD_FORBIDDEN_ERR'][0][0]})
                spec_dict.update({'los_v_forbid_gauss_sig_err': hdu[2].data['LOSVD_FORBIDDEN_ERR'][0][1]})
                spec_dict.update({'los_v_balmer_gauss': hdu[2].data['LOSVD_ALLOWED'][0][0]})
                spec_dict.update({'los_v_balmer_gauss_sig': hdu[2].data['LOSVD_ALLOWED'][0][1]})
                spec_dict.update({'los_v_balmer_gauss_err': hdu[2].data['LOSVD_ALLOWED_ERR'][0][0]})
                spec_dict.update({'los_v_balmer_gauss_sig_err': hdu[2].data['LOSVD_ALLOWED_ERR'][0][1]})

            if nonpar_fit:
                # information of third hdu (non-parametric fit)
                # wavelength non parametric fit
                spec_dict.update({'wave_non_par': hdu[3].data['SPEC_WAVE'][0]})
                # non parametric fit
                spec_dict.update({'em_fit_non_par': hdu[3].data['SPEC_FIT'][0]})
                # the pure emission line spectrum as same shape of wavelength same data as total_flux - fit_contin
                spec_dict.update({'em_spec_non_par': hdu[3].data['SPEC_EMIS'][0]})
                # line flux err and reduced chi2
                spec_dict.update({'em_flux_non_par': hdu[3].data['LINE_FLUX'][0]})
                spec_dict.update({'em_flux_err_non_par': hdu[3].data['LINE_FLUX_ERR'][0]})
                spec_dict.update({'em_chi2dof_non_par': hdu[3].data['SPEC_CHI2DOF'][0]})

                # get fit and flux and fit reprojected to continuum wavelength
                em_spec_non_par_reprojected = np.zeros(len(spec_dict['wave_total_flux']))
                em_fit_non_par_reprojected = np.zeros(len(spec_dict['wave_total_flux']))
                for index in range(len(spec_dict['wave_total_flux'])):
                    if spec_dict['wave_total_flux'][index] in spec_dict['wave_non_par']:
                        em_spec_non_par_reprojected[index] = \
                            spec_dict['em_spec_non_par'][spec_dict['wave_non_par'] == spec_dict['wave_total_flux'][index]]
                        em_fit_non_par_reprojected[index] = \
                            spec_dict['em_fit_non_par'][spec_dict['wave_non_par'] == spec_dict['wave_total_flux'][index]]
                spec_dict.update({'em_spec_non_par_reprojected': em_spec_non_par_reprojected})
                spec_dict.update({'em_fit_non_par_reprojected': em_fit_non_par_reprojected})

                # get emission line velocity distributions
                spec_dict.update({'los_vel_balmer_non_par': hdu[3].data['LOSVD_VBIN_ALLOWED'][0]})
                spec_dict.update({'los_v_dist_balmer_non_par': hdu[3].data['LOSVD_ALLOWED'][0]})
                spec_dict.update({'los_vel_forbid_non_par': hdu[3].data['LOSVD_VBIN_FORBIDDEN'][0]})
                spec_dict.update({'los_v_dist_forbid_non_par': hdu[3].data['LOSVD_FORBIDDEN'][0]})
                # get distribution percentiles from commulative func
                if 'non_par_los_vel_dist' in kwargs:
                    if kwargs.get('non_par_los_vel_dist'):
                        p16, median, p84 = helper_tools.get_function_percentiles(spec_dict['los_vel_balmer_non_par'],
                                                                                 spec_dict['los_v_dist_balmer_non_par'])
                        spec_dict.update({'los_v_p16_balmer_non_par': p16})
                        spec_dict.update({'los_v_median_balmer_non_par': median})
                        spec_dict.update({'los_v_p84_balmer_non_par': p84})

                        p16, median, p84 = helper_tools.get_function_percentiles(spec_dict['los_vel_forbid_non_par'],
                                                                                 spec_dict['los_v_dist_forbid_non_par'])
                        spec_dict.update({'los_v_p16_forbid_non_par': p16})
                        spec_dict.update({'los_v_median_forbid_non_par': median})
                        spec_dict.update({'los_v_p84_forbid_non_par': p84})

            # close hdu
            hdu.close()

        elif self.rcsed_version == 2:

            # hdu = fits.open(file_path / spec_file_name)
            # print(hdu.info())

            # access data
            spec_data = fits.getdata(file_path / spec_file_name, "SPECTRUM")
            em_data = fits.getdata(file_path / spec_file_name, "EM_LINES")

            # get information from first hdu
            # stellar line of sight velocity
            spec_dict.update({'los_v_star': spec_data[0]['V'][0][0]})
            # stellar line of sight velocity error
            spec_dict.update({'los_v_star_err': spec_data[0]['E_V'][0][0]})
            # stellar standard deviation of the line of sight velocity
            spec_dict.update({'los_v_star_sig': spec_data[0]['SIG'][0][0]})
            # err of the stellar standard deviation of the line of sight velocity
            spec_dict.update({'los_v_star_sig_err': spec_data[0]['E_SIG'][0][0]})

            # get line of sight velocities
            # there are two numbers first is the velocity second is the gaussian dispersion (sigma)
            spec_dict.update({'los_v_forbid_gauss': spec_data[0]['V'][1][0]})
            spec_dict.update({'los_v_forbid_gauss_sig': spec_data[0]['E_V'][1][0]})
            spec_dict.update({'los_v_forbid_gauss_err': spec_data[0]['SIG'][1][0]})
            spec_dict.update({'los_v_forbid_gauss_sig_err': spec_data[0]['E_SIG'][1][0]})
            spec_dict.update({'los_v_balmer_gauss': spec_data[0]['V'][1][0]})
            spec_dict.update({'los_v_balmer_gauss_sig': spec_data[0]['E_V'][1][0]})
            spec_dict.update({'los_v_balmer_gauss_err': spec_data[0]['SIG'][1][0]})
            spec_dict.update({'los_v_balmer_gauss_sig_err': spec_data[0]['E_SIG'][1][0]})

            # observed spectrum
            spec_dict.update({'total_flux': spec_data[0]["FLUX"]})
            # uncertainties
            spec_dict.update({'total_flux_err': spec_data[0]["ERROR"]})
            # best continuum fit
            spec_dict.update({'fit_contin': spec_data[0]["FIT_COMP"][0, :]})
            # wavelengths
            spec_dict.update({'wave_total_flux': spec_data[0]["WAVE"]})
            # instrumental broadening
            spec_dict.update({'inst_broad': spec_data[0]['LSF_SIG']})
            # all pixels where a emission line is present
            spec_dict.update({'good_pixels': spec_data[0]['GOODPIXELS']})


            # information of second hdu (gaussian fit)
            # emission line names and wavelength this is the same for hdu 3. We also extract the line identifier for
            spec_dict.update({'rcsed_em_name': em_data[0]['LINE_ID']})
            em_identifier = [self.new_nbursts_line_names[spec_dict['rcsed_em_name'][x]]
                             for x in range(len(spec_dict['rcsed_em_name']))]
            spec_dict.update({'em_identifier': em_identifier})

            # gaussian line flux, err and reduced chi2
            spec_dict.update({'em_flux_gauss': em_data[0]['FLUX']})
            spec_dict.update({'em_flux_err_gauss': em_data[0]['FLUX_ERR']})

            # spectral information (gaussian fit)
            # wavelength gaussian fit
            spec_dict.update({'wave_gauss': spec_data[0]["WAVE"]})
            # gaussian fit
            spec_dict.update({'em_fit_gauss': spec_data[0]["FIT_COMP"][1, :]})
            # the pure emission line spectrum as same shape of wavelength same data as total_flux - fit_contin
            spec_dict.update({'em_spec_gauss': spec_dict['total_flux'] - spec_dict['fit_contin']})


        # load dict into attributes
        self.spec_dict = spec_dict

        return spec_dict

    def get_rcsed_em_spec(self):
        """access pure emission line spectrum by subtracting the best continuum fit.
        Returns
        -------
        wavelength, emission_line_flux, emission_line_flux_err
        """
        spec_dict = self.get_rcsed_spec_dict()
        emission_line_flux = spec_dict['total_flux'] - spec_dict['fit_contin']
        return spec_dict['wave_total_flux'], emission_line_flux, spec_dict['total_flux_err']

    def get_rcsed_spec_continuum(self):
        """access nburst continuum fit"""
        spec_dict = self.get_rcsed_spec_dict()
        return spec_dict['wave_total_flux'], spec_dict['fit_contin']

    def get_rcsed_spec_redshift(self, vel_measure='balmer_gauss'):
        """gets the line of sight velocity of the spectral observation

        Parameters
        ----------

        vel_measure : str (default 'balmer_gauss' ) for example balmer_gauss for allowed emission lines
          of the gaussian fit. This works also for forbidden emission line, nonparametric (non_par) fit
          or the stellar continuum ('stellar')

        Returns
        -------
        rcsed_spec_redshift : float
          Redshift measured by the spectral observation

        Notes
        ------
         None
        """
        spec_dict = self.get_rcsed_spec_dict()
        emission_line_velocity = spec_dict['los_v_%s' % vel_measure]
        return emission_line_velocity / (speed_of_light * 1e-3)

    def get_rcsed_sys_vel(self, redshift=None, vel_measure='balmer_gauss'):
        """ function to get the systematic velocity of the observed spectra

        Parameters
        ----------

        redshift : float default None
          redshift to estimate line position
        vel_measure : str
          Same as for `~self.get_rcsed_spec_redshift`. Will be only used if redshift is None

        Returns
        -------
        obs_line_position : float
          systematic velocity in km / s

        """
        if redshift is None:
            return self.get_rcsed_spec_dict()['los_v_%s' % vel_measure]
        else:
            return speed_of_light * 1e-3 * redshift

    def get_rcsed_obs_line_position(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ function to get the observed wavelength

        Parameters
        ----------

        line : int
          must be identifier key from `~self.em_wavelength`
        redshift : float default None
          redshift to estimate line position
        vel_measure : str
          Same as for `~self.get_rcsed_spec_redshift`. Will be only used if redshift is None

        Returns
        -------
        obs_line_position : float
          Position of the observed emission line in :math: `\\AA`

        """
        if redshift is None:
            redshift = self.get_rcsed_spec_redshift(vel_measure=vel_measure)

        return self.em_wavelength[line]['vac_wave'] * (1 + redshift)

    def get_rcsed_line_mask(self, line, redshift=None, vel_measure='balmer_gauss', blue_limit=30., red_limit=30.):
        """ returns a boolean mask of selected wavelength with the observed line position with a specific range

        Parameters
        ----------

        line : int
          Same as for `~self.get_rcsed_spec_redshift`
        redshift : float (default=None)
          same as `~self.get_rcsed_obs_line_position`
        vel_measure : str
          Same as for `~self.get_rcsed_spec_redshift`
        blue_limit, red_limit : float
          limit of selected wavelength at the blue shifted and redshifted border in :math: `\\AA`

        Returns
        -------
        line_mask : ndarray
          mask of selected wavelength at the observed line position

        Notes
        -----
        Some lines are doublets or inseparable from their neighbors. Thus they will get a special treatment
        """

        if (line == 3727) | (line == 3730):  # OII doublet
            boarder_1 = (self.get_rcsed_obs_line_position(line=3727, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_rcsed_obs_line_position(line=3730, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)
        elif (line == 6550) | (line == 6565) | (line == 6585):  # H-alpha and NII doublet
            boarder_1 = (self.get_rcsed_obs_line_position(line=6550, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_rcsed_obs_line_position(line=6585, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)
        elif (line == 6718) | (line == 6733):  # SII doublet
            boarder_1 = (self.get_rcsed_obs_line_position(line=6718, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_rcsed_obs_line_position(line=6733, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)
        else:  # isolated emission lines
            boarder_1 = (self.get_rcsed_obs_line_position(line=line, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_rcsed_obs_line_position(line=line, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)

        wavelength = self.get_rcsed_spec_dict()['wave_total_flux']

        nan_values = np.isnan(self.get_rcsed_spec_dict()['total_flux'])

        # return boolean array
        return (wavelength > boarder_1) & (wavelength < boarder_2) & ~nan_values

    def get_rcsed_multiple_line_masks(self, line_list=None, redshift=None, vel_measure='balmer_gauss',
                                      blue_limit=30., red_limit=30.):
        """ returns a combined boolean array with all emission line position

        Parameters
        ----------

        line_list: list
          must be list of identifier key from `~self.em_wavelength`
        redshift : float (default=None)
          same as `~self.get_rcsed_obs_line_position`
        vel_measure : str
          Same as for `~self.get_rcsed_spec_redshift`
        blue_limit, red_limit : float
          Same as for `~self.get_rcsed_line_mask`

        Returns
        -------
        line_mask : ndarray
          mask of multiple selected wavelength at the observed line position

        Notes
        -----
        Some lines are doublets or inseparable from their neighbors. Thus they will get a special treatment

        """
        if line_list is None:
            line_list = [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]

        wave_mask = None
        for line in line_list:
            if wave_mask is None:
                wave_mask = self.get_rcsed_line_mask(line=line, redshift=redshift, vel_measure=vel_measure,
                                                     blue_limit=blue_limit, red_limit=red_limit)
            else:
                wave_mask += self.get_rcsed_line_mask(line=line, redshift=redshift, vel_measure=vel_measure,
                                                      blue_limit=blue_limit,  red_limit=red_limit)

        return wave_mask

    def get_rcsed_wave_bin_width(self, line=6565, unit='vel'):
        spec_dict = self.get_rcsed_spec_dict()
        mask_line = self.get_rcsed_line_mask(line=line)
        if unit == 'wave':
            bin_width = np.nanmean(spec_dict['wave_total_flux'][mask_line][1:] -
                                   spec_dict['wave_total_flux'][mask_line][:-1])
        elif unit == 'vel':
            bin_width = (np.nanmean(spec_dict['wave_total_flux'][mask_line][1:] -
                                    spec_dict['wave_total_flux'][mask_line][:-1]) *
                         (speed_of_light * 1e-3) / self.em_wavelength[line]['vac_wave'])
        else:
            raise KeyError('unit must be wave or vel')
        return bin_width

    def get_rcsed_line_inst_broad(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ returns instrumental broadening of a specific emission line

        Parameters
        ----------

        line : int
          Same as for `~self.get_rcsed_spec_redshift`
        redshift : float (default=None)
          same as `~self.get_rcsed_obs_line_position`
        vel_measure : str
          Same as for `~self.get_rcsed_spec_redshift`

        Returns
        -------
        line_inst_broad : float
          instrumental broadening

        """
        instrumental_broadening = self.get_rcsed_spec_dict()['inst_broad']
        line_mask = self.get_rcsed_line_mask(line=line, redshift=redshift, vel_measure=vel_measure)
        return np.nanmean(instrumental_broadening[line_mask])

    def get_line_velocity(self, line, redshift=None, vel_measure='balmer_gauss'):
        r"""
        convert the wavelength into velocity with respect to a specific emission line position

        Parameters
        ----------

        line : int
          Same as for `~self.get_rcsed_spec_redshift`
        redshift : float (default=None)
          same as `~self.get_rcsed_obs_line_position`
        vel_measure : str
          Same as for `~self.get_rcsed_spec_redshift`

        Returns
        -------
        velocity : ndarray
          velocity array
        """

        # get observed emission line
        observed_line = self.get_rcsed_obs_line_position(line=line, redshift=redshift, vel_measure=vel_measure)

        wavelength = self.get_rcsed_spec_dict()['wave_total_flux']

        return speed_of_light * 1e-3 * (wavelength - observed_line) / observed_line

    def get_rcsed_line_flux(self, line, rcsed_fit='gauss'):
        """ access emission line fit of the rcsed spec

        Parameters
        ----------

        line: int
          must be identifier key from `~self.em_wavelength`
        rcsed_fit: str (default: 'gauss')
          specifies the emission line fit from the RCSED can be gauss or non_par

        Returns
        -------
        line_flux: float
          flux of the measured emission line
        """
        mask_selected_line = np.where(np.array(self.get_rcsed_spec_dict()['em_identifier']) == line)
        return self.get_rcsed_spec_dict()['em_flux_%s' % rcsed_fit][mask_selected_line][0]

    def get_rcsed_single_gauss_obs_sigma(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ calculates gaussian observed sigma for rcsed fitted emission line

        Parameters
        ----------

        line: int
          must be identifier key from `~self.em_wavelength`
        redshift : float (default=None)
          same as `~self.get_rcsed_obs_line_position`
        vel_measure: str (default: 'balmer_gauss')
          Same as for `~self.get_rcsed_spec_redshift`

        Returns
        -------
        line_sigma_angstrom: float
          gaussian sigma in units of :math: ´\\sigma`
        """
        line_broadening = self.get_rcsed_line_inst_broad(line=line, redshift=redshift, vel_measure=vel_measure)
        gaussian_sigma = self.get_rcsed_spec_dict()['los_v_%s_sig' % vel_measure]
        line_sigma_angstrom = (np.sqrt(gaussian_sigma ** 2 + line_broadening ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])
        return line_sigma_angstrom

    def get_rcsed_single_gauss_amp(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ calculates gaussian amplitude for rcsed fitted emission line

        Parameters
        ----------

        line: int
          must be identifier key from `~self.em_wavelength`
        redshift : float (default=None)
          same as `~self.get_rcsed_obs_line_position`
        vel_measure: str (default: 'balmer_gauss')
          Same as for `~self.get_rcsed_spec_redshift`

        Returns
        -------
        line_amp: float
          gaussian amplitude of the measured emission line
        """
        line_flux = self.get_rcsed_line_flux(line=line)
        return line_flux / (np.sqrt(2 * np.pi) *
                            self.get_rcsed_single_gauss_obs_sigma(line=line, redshift=redshift,
                                                                  vel_measure=vel_measure))
