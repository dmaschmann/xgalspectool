import os
from scipy.interpolate import interp1d
from scipy.optimize import newton
from numpy import nancumsum, nanmean


# it would be better to include this into the core code of xgalspectol. Or even source this out to xgaltool


def get_function_percentiles(x_data, y_data, negative_flag=True):

    if negative_flag:
        inter_cum_dist = interp1d(x_data, 1 - nancumsum(y_data), kind='quadratic')
    else:
        inter_cum_dist = interp1d(x_data, nancumsum(y_data), kind='quadratic')

    interp_fn_p16 = lambda x: inter_cum_dist(x) - 0.16
    interp_fn_median = lambda x: inter_cum_dist(x) - 0.5
    interp_fn_p84 = lambda x: inter_cum_dist(x) - 0.84

    return (newton(interp_fn_p16, nanmean(x_data)), newton(interp_fn_median, nanmean(x_data)),
            newton(interp_fn_p84, nanmean(x_data)))
