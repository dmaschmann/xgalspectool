# -*- coding: utf-8 -*-

import numpy as np
from xgalspectool.fit_algorithm import em_fit_algorithm
from scipy.constants import c as speed_of_light


class HISpecHelper(em_fit_algorithm.EmissionLineFit):

    def __init__(self, hi_spec_path=None, hi_redshift=None, antenna_convert_fact=1, **kwargs):
        """
        Class to access HI spectra
        """
        super().__init__(**kwargs)

        self.hi_spec_path = hi_spec_path

        self.hi_redshift = hi_redshift

        self.vel_hi = None
        self.wave_hi = None
        self.flux_hi = None

        # if there is an antenna conversion factor to convert the observed spetrum from antenna temperature to Jy
        self.antenna_convert_fact = antenna_convert_fact

    def load_hi_spectrum(self):
        if self.hi_spec_path is not None:
            spectrum_hi = np.genfromtxt(self.hi_spec_path)
            self.vel_hi = spectrum_hi[:, 0] # - speed_of_light * 1e-3 * self.hi_redshift / (1 + self.hi_redshift)
            self.flux_hi = spectrum_hi[:, 1]
            # self.wave_hi = (self.vel_hi / (speed_of_light * 1e-3) * self.get_hi_obs_wave() + self.get_hi_obs_wave())
            self.wave_hi = self.get_hi_obs_wave() / (1 - speed_of_light * 1e-3 * self.vel_hi)


            print('v_opt ', speed_of_light * 1e-3 * self.hi_redshift )
            print('v_radio ', speed_of_light * 1e-3 * self.hi_redshift / (1 + self.hi_redshift))
            import matplotlib.pyplot as plt

            #plt.scatter(self.vel_hi, self.flux_hi)
            #plt.show()



    def get_hi_obs_wave(self):
        return self.hi_vac_wave * (1 + self.hi_redshift)

    def rebin_hi_spectrum(self, vel_bins=None, vel_bin_width=None, wave_bin_width=None):
        if (vel_bins is not None) & (vel_bin_width is None) & (wave_bin_width is None):
            centre_of_new_vel_bins_hi = (vel_bins[:-1] + vel_bins[1:]) / 2
            new_flux_hi = np.zeros(len(centre_of_new_vel_bins_hi))
            centre_of_new_wave_bins_hi = np.zeros(len(centre_of_new_vel_bins_hi))
            for bin_index in range(len(centre_of_new_vel_bins_hi)):
                selected_velocities = ((self.vel_hi > vel_bins[bin_index]) &
                                       (self.vel_hi < vel_bins[bin_index + 1]))
                new_flux_hi[bin_index] = np.nanmean(self.flux_hi[selected_velocities])
                centre_of_new_wave_bins_hi[bin_index] = np.nanmean(self.wave_hi[selected_velocities])
            self.vel_hi = centre_of_new_vel_bins_hi
            self.wave_hi = centre_of_new_wave_bins_hi
            self.flux_hi = new_flux_hi
        elif (vel_bin_width is not None) & (wave_bin_width is None) & (vel_bins is None):
            new_vel_bins = np.arange(min(self.vel_hi), max(self.vel_hi), vel_bin_width)
            centre_of_new_vel_bins_hi = (new_vel_bins[:-1] + new_vel_bins[1:]) / 2
            new_flux_hi = np.zeros(len(centre_of_new_vel_bins_hi))
            centre_of_new_wave_bins_hi = np.zeros(len(centre_of_new_vel_bins_hi))
            for bin_index in range(len(centre_of_new_vel_bins_hi)):
                selected_velocities = ((self.vel_hi > new_vel_bins[bin_index]) &
                                       (self.vel_hi < new_vel_bins[bin_index + 1]))
                new_flux_hi[bin_index] = np.nanmean(self.flux_hi[selected_velocities])
                centre_of_new_wave_bins_hi[bin_index] = np.nanmean(self.wave_hi[selected_velocities])
            self.vel_hi = centre_of_new_vel_bins_hi
            self.wave_hi = centre_of_new_wave_bins_hi
            self.flux_hi = new_flux_hi
        elif (vel_bin_width is None) & (wave_bin_width is not None) & (vel_bins is None):
            new_wave_bins = np.arange(min(self.wave_hi), max(self.wave_hi), vel_bin_width)
            centre_of_new_wave_bins_hi = (new_wave_bins[:-1] + new_wave_bins[1:]) / 2
            new_flux_hi = np.zeros(len(centre_of_new_wave_bins_hi))
            centre_of_new_vel_bins_hi = np.zeros(len(centre_of_new_wave_bins_hi))
            for bin_index in range(len(centre_of_new_wave_bins_hi)):
                selected_velocities = ((self.wave_hi > new_wave_bins[bin_index]) &
                                       (self.wave_hi < new_wave_bins[bin_index + 1]))
                new_flux_hi[bin_index] = np.nanmean(self.flux_hi[selected_velocities])
                centre_of_new_vel_bins_hi[bin_index] = np.nanmean(self.wave_hi[selected_velocities])
            self.vel_hi = centre_of_new_vel_bins_hi
            self.wave_hi = centre_of_new_wave_bins_hi
            self.flux_hi = new_flux_hi
        else:
            raise KeyError('you can only choose one new bin width (vel_bin_width or wave_bin_width) '
                           'the other must be set to None. '
                           'Or you directly give a new bin array and set the other args to None')

    def get_hi_mask(self, vel_radius=1000, wave_radius=None):
        line_position = self.get_hi_obs_wave()
        if (vel_radius is not None) & (wave_radius is None):
            return (self.vel_hi > (- vel_radius)) & (self.vel_hi < (+ vel_radius))
        elif (vel_radius is None) & (wave_radius is not None):
            return (self.wave_hi > (line_position - wave_radius)) & (self.wave_hi < (line_position + wave_radius))
        else:
            raise KeyError('you can only choose one radius (vel_radius or wave_radius) '
                           'the the other must be set to None.')

    def get_hi_max_line_flux(self, vel_radius=1000, wave_radius=None):
        line_mask = self.get_hi_mask(vel_radius=vel_radius, wave_radius=wave_radius)
        return np.nanmax(self.flux_hi[line_mask])

