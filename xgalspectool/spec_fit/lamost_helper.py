# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import os
from pathlib import Path

from scipy.constants import c as speed_of_light
from astropy.io import fits
from re import findall

from xgalspectool.fit_algorithm import em_fit_algorithm
from xgalspectool.spec_fit import helper_tools
from xgaltool.basic_attributes import download_file


class LAMOSTSpecHelper(em_fit_algorithm.EmissionLineFit):

    def __init__(self, lamost_fiberid=None, lamost_lmjd=None, lamost_obsdate=None, lamost_planid=None, lamost_spid=None,
                 data_path=None, **kwargs):
        """

        """
        super().__init__(**kwargs)
        # check if data_path is given and correct
        if data_path is None:  # use short cut
            data_path = Path.home() / 'data'
        if os.path.isdir(Path(data_path)):
            self.data_path = Path(data_path)
        else:
            raise OSError('The data path %s is not existent' % data_path)

        if ((lamost_fiberid is not None) & (lamost_lmjd is not None) & (lamost_obsdate is not None) &
                (lamost_planid is not None) & (lamost_spid is not None)):
            self.lamost_fiberid = int(lamost_fiberid)
            self.lamost_lmjd = int(lamost_lmjd)
            self.lamost_obsdate = str(lamost_obsdate)
            self.lamost_planid = str(lamost_planid)
            self.lamost_spid = int(lamost_spid)
        else:
            self.lamost_fiberid = None
            self.lamost_lmjd = None
            self.lamost_obsdate = None
            self.lamost_planid = None
            self.lamost_spid = None

        # load the spectrum into attributes
        self.spec_dict = None

    def check_lamost_identifier(self):
        """ Function to check weather the lamost identifier are set"""
        if ((self.lamost_fiberid is None) & (self.lamost_lmjd is None) & (self.lamost_obsdate is None) &
                (self.lamost_planid is None) & (self.lamost_spid is None)):
            raise KeyError('To access lamost spectra you need to specify the concrete identifier namely '
                           'lamost_fiberid, lamost_lmjd, lamost_obsdate, lamost_planid, lamost_spid')
        else:
            return True

    def get_lamost_object_file_path(self):
        """ function to get file_path of lamost spectral objects """
        # check if identifier is correct
        self.check_lamost_identifier()
        # merge filepath
        file_path = (self.data_path / 'galaxies' / 'lamost_spectra' /
                     str(self.lamost_fiberid) / str(self.lamost_lmjd) / str(self.lamost_obsdate) /
                     str(self.lamost_planid) / str(self.lamost_spid))
        # check if filepath exists and create it if necessary
        if not os.path.isdir(file_path):
            os.makedirs(file_path)
        return file_path

    def get_lamost_spec_identifier(self):
        """ get file_name of lamost spectra """
        # check if identifier is correct
        self.check_lamost_identifier()

        # get identifier as strings (mjd, plate and fiber id will have 5 digits !!!! )
        file_name = ('lamost_spec_nburst_MILES_MGFE_EMIS1_rcsed2'
                     + '_' + f'{self.lamost_fiberid}'
                             '_' + f'{self.lamost_lmjd}'
                                   '_' + self.lamost_obsdate +
                     '_' + self.lamost_planid +
                     '_' + f'{self.lamost_spid:02d}' + '.fits')
        # example url This was found in the backend at:https://api.dev-rcsed2.voxastro.org/spec/4856464/
        # file URL = "https://gal-02.voxastro.org/rcsed2/lamost/MILES_MGFE_EMIS1/
        # 20120218/F5597602/nbursts_lamost_55976-F5597602_sp05-072.fits.gz",
        # here the following keywords were used
        # lamost_fiberid = 72
        # lamost_lmjd = 55976
        # lamost_obsdate = 2012-02-18
        # lamost_planid = F5597602
        # lamost_spid = 5
        url = ('https://gal-02.voxastro.org/rcsed2/lamost/MILES_MGFE_EMIS1/'
               + self.lamost_obsdate.replace('-', '') + '/'
               + self.lamost_planid
               + '/nbursts_lamost_'
               + f'{self.lamost_lmjd}' + '-'
               + self.lamost_planid + '_sp'
               + f'{self.lamost_spid:02d}' + '-'
               + f'{self.lamost_fiberid:03d}'
               + '.fits.gz')
        return file_name, url

    def download_lamost_spec_fits_file(self, reload=False):
        """download an lamost object spectrum processed by lamost"""

        # get filepath where to store data
        file_path = self.get_lamost_object_file_path()
        # get file name of lamost spectrum
        spec_file_name, url = self.get_lamost_spec_identifier()

        download_file(file_path=file_path, file_name=spec_file_name, unpack=True, url=url, reload=reload)

    def get_lamost_spec_dict(self):
        """load lamost spectrum from the lamost fit and passes it into a dictionary"""

        # check if spec dict is already in attributes
        if self.spec_dict is not None:
            return self.spec_dict

        # if not yet loaded, get an empty dictionary
        spec_dict = {}

        # get data access
        file_path = self.get_lamost_object_file_path()
        spec_file_name, url = self.get_lamost_spec_identifier()

        # download the file
        download_file(file_path=file_path, file_name=spec_file_name, unpack=True, url=url)

        # access data
        spec_data = fits.getdata(file_path / spec_file_name, "SPECTRUM")
        em_data = fits.getdata(file_path / spec_file_name, "EM_LINES")

        # get information from first hdu
        # stellar line of sight velocity
        spec_dict.update({'los_v_star': spec_data[0]['V'][0][0]})
        # stellar line of sight velocity error
        spec_dict.update({'los_v_star_err': spec_data[0]['E_V'][0][0]})
        # stellar standard deviation of the line of sight velocity
        spec_dict.update({'los_v_star_sig': spec_data[0]['SIG'][0][0]})
        # err of the stellar standard deviation of the line of sight velocity
        spec_dict.update({'los_v_star_sig_err': spec_data[0]['E_SIG'][0][0]})

        # get line of sight velocities
        # there are two numbers first is the velocity second is the gaussian dispersion (sigma)
        spec_dict.update({'los_v_forbid_gauss': spec_data[0]['V'][1][0]})
        spec_dict.update({'los_v_forbid_gauss_sig': spec_data[0]['E_V'][1][0]})
        spec_dict.update({'los_v_forbid_gauss_err': spec_data[0]['SIG'][1][0]})
        spec_dict.update({'los_v_forbid_gauss_sig_err': spec_data[0]['E_SIG'][1][0]})
        spec_dict.update({'los_v_balmer_gauss': spec_data[0]['V'][1][0]})
        spec_dict.update({'los_v_balmer_gauss_sig': spec_data[0]['E_V'][1][0]})
        spec_dict.update({'los_v_balmer_gauss_err': spec_data[0]['SIG'][1][0]})
        spec_dict.update({'los_v_balmer_gauss_sig_err': spec_data[0]['E_SIG'][1][0]})

        # observed spectrum
        spec_dict.update({'total_flux': spec_data[0]["FLUX"]})
        # uncertainties
        spec_dict.update({'total_flux_err': spec_data[0]["ERROR"]})
        # best continuum fit
        spec_dict.update({'fit_contin': spec_data[0]["FIT_COMP"][0, :]})
        # wavelengths
        spec_dict.update({'wave_total_flux': spec_data[0]["WAVE"]})
        # instrumental broadening
        spec_dict.update({'inst_broad': spec_data[0]['LSF_SIG']})
        # all pixels where a emission line is present
        spec_dict.update({'good_pixels': spec_data[0]['GOODPIXELS']})


        # information of second hdu (gaussian fit)
        # emission line names and wavelength this is the same for hdu 3. We also extract the line identifier for
        spec_dict.update({'rcsed_em_name': em_data[0]['LINE_ID']})
        em_identifier = [self.new_nbursts_line_names[spec_dict['rcsed_em_name'][x]]
                         for x in range(len(spec_dict['rcsed_em_name']))]
        spec_dict.update({'em_identifier': em_identifier})

        # gaussian line flux, err and reduced chi2
        spec_dict.update({'em_flux_gauss': em_data[0]['FLUX']})
        spec_dict.update({'em_flux_err_gauss': em_data[0]['FLUX_ERR']})

        # spectral information (gaussian fit)
        # wavelength gaussian fit
        spec_dict.update({'wave_gauss': spec_data[0]["WAVE"]})
        # gaussian fit
        spec_dict.update({'em_fit_gauss': spec_data[0]["FIT_COMP"][1, :]})
        # the pure emission line spectrum as same shape of wavelength same data as total_flux - fit_contin
        spec_dict.update({'em_spec_gauss': spec_dict['total_flux'] - spec_dict['fit_contin']})

        # load dict into attributes
        self.spec_dict = spec_dict

        return spec_dict

    def get_lamost_em_spec(self):
        """access pure emission line spectrum by subtracting the best continuum fit.
        Returns
        -------
        wavelength, emission_line_flux, emission_line_flux_err
        """
        spec_dict = self.get_lamost_spec_dict()
        emission_line_flux = spec_dict['total_flux'] - spec_dict['fit_contin']
        return spec_dict['wave_total_flux'], emission_line_flux, spec_dict['total_flux_err']

    def get_lamost_spec_continuum(self):
        """access nburst continuum fit"""
        spec_dict = self.get_lamost_spec_dict()
        return spec_dict['wave_total_flux'], spec_dict['fit_contin']

    def get_lamost_spec_redshift(self, vel_measure='balmer_gauss'):
        """gets the line of sight velocity of the spectral observation

        Parameters
        ----------

        vel_measure : str (default 'balmer_gauss' ) for example balmer_gauss for allowed emission lines
          of the gaussian fit. This works also for forbidden emission line, nonparametric (non_par) fit
          or the stellar continuum ('stellar')

        Returns
        -------
        lamost_spec_redshift : float
          Redshift measured by the spectral observation

        Notes
        ------
         None
        """
        spec_dict = self.get_lamost_spec_dict()
        emission_line_velocity = spec_dict['los_v_%s' % vel_measure]
        return emission_line_velocity / (speed_of_light * 1e-3)

    def get_lamost_sys_vel(self, redshift=None, vel_measure='balmer_gauss'):
        """ function to get the systematic velocity of the observed spectra

        Parameters
        ----------

        redshift : float default None
          redshift to estimate line position
        vel_measure : str
          Same as for `~self.get_lamost_spec_redshift`. Will be only used if redshift is None

        Returns
        -------
        obs_line_position : float
          systematic velocity in km / s

        """
        if redshift is None:
            return self.get_lamost_spec_dict()['los_v_%s' % vel_measure]
        else:
            return speed_of_light * 1e-3 * redshift

    def get_lamost_obs_line_position(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ function to get the observed wavelength

        Parameters
        ----------

        line : int
          must be identifier key from `~self.em_wavelength`
        redshift : float default None
          redshift to estimate line position
        vel_measure : str
          Same as for `~self.get_lamost_spec_redshift`. Will be only used if redshift is None

        Returns
        -------
        obs_line_position : float
          Position of the observed emission line in :math: `\\AA`

        """
        if redshift is None:
            redshift = self.get_lamost_spec_redshift(vel_measure=vel_measure)

        return self.em_wavelength[line]['vac_wave'] * (1 + redshift)

    def get_lamost_line_mask(self, line, redshift=None, vel_measure='balmer_gauss', blue_limit=30., red_limit=30.):
        """ returns a boolean mask of selected wavelength with the observed line position with a specific range

        Parameters
        ----------

        line : int
          Same as for `~self.get_lamost_spec_redshift`
        redshift : float (default=None)
          same as `~self.get_lamost_obs_line_position`
        vel_measure : str
          Same as for `~self.get_lamost_spec_redshift`
        blue_limit, red_limit : float
          limit of selected wavelength at the blue shifted and redshifted border in :math: `\\AA`

        Returns
        -------
        line_mask : ndarray
          mask of selected wavelength at the observed line position

        Notes
        -----
        Some lines are doublets or inseparable from their neighbors. Thus they will get a special treatment
        """

        if (line == 3727) | (line == 3730):  # OII doublet
            boarder_1 = (self.get_lamost_obs_line_position(line=3727, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_lamost_obs_line_position(line=3730, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)
        elif (line == 6550) | (line == 6565) | (line == 6585):  # H-alpha and NII doublet
            boarder_1 = (self.get_lamost_obs_line_position(line=6550, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_lamost_obs_line_position(line=6585, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)
        elif (line == 6718) | (line == 6733):  # SII doublet
            boarder_1 = (self.get_lamost_obs_line_position(line=6718, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_lamost_obs_line_position(line=6733, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)
        else:  # isolated emission lines
            boarder_1 = (self.get_lamost_obs_line_position(line=line, redshift=redshift, vel_measure=vel_measure)
                         - blue_limit)
            boarder_2 = (self.get_lamost_obs_line_position(line=line, redshift=redshift, vel_measure=vel_measure)
                         + red_limit)

        wavelength = self.get_lamost_spec_dict()['wave_total_flux']

        # return boolean array
        return (wavelength > boarder_1) & (wavelength < boarder_2)

    def get_lamost_multiple_line_masks(self, line_list=None, redshift=None, vel_measure='balmer_gauss',
                                      blue_limit=30., red_limit=30.):
        """ returns a combined boolean array with all emission line position

        Parameters
        ----------

        line_list: list
          must be list of identifier key from `~self.em_wavelength`
        redshift : float (default=None)
          same as `~self.get_lamost_obs_line_position`
        vel_measure : str
          Same as for `~self.get_lamost_spec_redshift`
        blue_limit, red_limit : float
          Same as for `~self.get_lamost_line_mask`

        Returns
        -------
        line_mask : ndarray
          mask of multiple selected wavelength at the observed line position

        Notes
        -----
        Some lines are doublets or inseparable from their neighbors. Thus they will get a special treatment

        """
        if line_list is None:
            line_list = [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]

        wave_mask = None
        for line in line_list:
            if wave_mask is None:
                wave_mask = self.get_lamost_line_mask(line=line, redshift=redshift, vel_measure=vel_measure,
                                                     blue_limit=blue_limit, red_limit=red_limit)
            else:
                wave_mask += self.get_lamost_line_mask(line=line, redshift=redshift, vel_measure=vel_measure,
                                                      blue_limit=blue_limit,  red_limit=red_limit)

        return wave_mask

    def get_lamost_wave_bin_width(self, line=6565, unit='vel'):
        spec_dict = self.get_lamost_spec_dict()
        mask_line = self.get_lamost_line_mask(line=line)
        if unit == 'wave':
            bin_width = np.nanmean(spec_dict['wave_total_flux'][mask_line][1:] -
                                   spec_dict['wave_total_flux'][mask_line][:-1])
        elif unit == 'vel':
            bin_width = (np.nanmean(spec_dict['wave_total_flux'][mask_line][1:] -
                                    spec_dict['wave_total_flux'][mask_line][:-1]) *
                         (speed_of_light * 1e-3) / self.em_wavelength[line]['vac_wave'])
        else:
            raise KeyError('unit must be wave or vel')
        return bin_width

    def get_lamost_line_inst_broad(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ returns instrumental broadening of a specific emission line

        Parameters
        ----------

        line : int
          Same as for `~self.get_lamost_spec_redshift`
        redshift : float (default=None)
          same as `~self.get_lamost_obs_line_position`
        vel_measure : str
          Same as for `~self.get_lamost_spec_redshift`

        Returns
        -------
        line_inst_broad : float
          instrumental broadening

        """
        instrumental_broadening = self.get_lamost_spec_dict()['inst_broad']
        line_mask = self.get_lamost_line_mask(line=line, redshift=redshift, vel_measure=vel_measure)
        return np.nanmean(instrumental_broadening[line_mask])

    def get_line_velocity(self, line, redshift=None, vel_measure='balmer_gauss'):
        r"""
        convert the wavelength into velocity with respect to a specific emission line position

        Parameters
        ----------

        line : int
          Same as for `~self.get_lamost_spec_redshift`
        redshift : float (default=None)
          same as `~self.get_lamost_obs_line_position`
        vel_measure : str
          Same as for `~self.get_lamost_spec_redshift`

        Returns
        -------
        velocity : ndarray
          velocity array
        """

        # get observed emission line
        observed_line = self.get_lamost_obs_line_position(line=line, redshift=redshift, vel_measure=vel_measure)

        wavelength = self.get_lamost_spec_dict()['wave_total_flux']

        return speed_of_light * 1e-3 * (wavelength - observed_line) / observed_line

    def get_lamost_line_flux(self, line, lamost_fit='gauss'):
        """ access emission line fit of the lamost spec

        Parameters
        ----------

        line: int
          must be identifier key from `~self.em_wavelength`
        lamost_fit: str (default: 'gauss')
          specifies the emission line fit from the RCSED can be gauss or non_par

        Returns
        -------
        line_flux: float
          flux of the measured emission line
        """
        mask_selected_line = np.where(np.array(self.get_lamost_spec_dict()['em_identifier']) == line)
        return self.get_lamost_spec_dict()['em_flux_%s' % lamost_fit][mask_selected_line][0]

    def get_lamost_single_gauss_obs_sigma(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ calculates gaussian observed sigma for lamost fitted emission line

        Parameters
        ----------

        line: int
          must be identifier key from `~self.em_wavelength`
        redshift : float (default=None)
          same as `~self.get_lamost_obs_line_position`
        vel_measure: str (default: 'balmer_gauss')
          Same as for `~self.get_lamost_spec_redshift`

        Returns
        -------
        line_sigma_angstrom: float
          gaussian sigma in units of :math: ´\\sigma`
        """
        line_broadening = self.get_lamost_line_inst_broad(line=line, redshift=redshift, vel_measure=vel_measure)
        gaussian_sigma = self.get_lamost_spec_dict()['los_v_%s_sig' % vel_measure]
        line_sigma_angstrom = (np.sqrt(gaussian_sigma ** 2 + line_broadening ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])
        return line_sigma_angstrom

    def get_lamost_single_gauss_amp(self, line, redshift=None, vel_measure='balmer_gauss'):
        """ calculates gaussian amplitude for lamost fitted emission line

        Parameters
        ----------

        line: int
          must be identifier key from `~self.em_wavelength`
        redshift : float (default=None)
          same as `~self.get_lamost_obs_line_position`
        vel_measure: str (default: 'balmer_gauss')
          Same as for `~self.get_lamost_spec_redshift`

        Returns
        -------
        line_amp: float
          gaussian amplitude of the measured emission line
        """
        line_flux = self.get_lamost_line_flux(line=line)
        return line_flux / (np.sqrt(2 * np.pi) *
                            self.get_lamost_single_gauss_obs_sigma(line=line, redshift=redshift,
                                                                  vel_measure=vel_measure))
