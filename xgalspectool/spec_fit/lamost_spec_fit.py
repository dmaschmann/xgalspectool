# -*- coding: utf-8 -*-

from xgalspectool.spec_fit import lamost_helper


class LAMOSTSpecFit(lamost_helper.LAMOSTSpecHelper):

    def __init__(self, **kwargs):
        """
        Class to prepare fitting procedure of emission lines

        """
        super().__init__(**kwargs)

    def get_lamost_em_fit_init_mu_sigma_guess(self, param_dict=None, init_mu_offset=100, init_sigma=100,
                                             init_broad_sigma=800, redshift=None, vel_measure='balmer_gauss'):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get systematic velocity
        sys_vel = self.get_lamost_sys_vel(redshift=redshift, vel_measure=vel_measure)

        # get initial mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                param_dict.update({'mu_balmer': sys_vel})
                param_dict.update({'mu_forbidden': sys_vel})
                param_dict.update({'sigma_balmer': init_sigma})
                param_dict.update({'sigma_forbidden': init_sigma})
            elif self.n_gauss == 2:
                param_dict.update({'mu_balmer_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_balmer_2': sys_vel + init_mu_offset})
                param_dict.update({'mu_forbidden_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_forbidden_2': sys_vel + init_mu_offset})
                param_dict.update({'sigma_balmer_1': init_sigma})
                param_dict.update({'sigma_balmer_2': init_sigma})
                param_dict.update({'sigma_forbidden_1': init_sigma})
                param_dict.update({'sigma_forbidden_2': init_sigma})
            elif self.n_gauss == 3:
                param_dict.update({'mu_balmer_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_balmer_2': sys_vel})
                param_dict.update({'mu_balmer_3': sys_vel + init_mu_offset})
                param_dict.update({'mu_forbidden_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_forbidden_2': sys_vel})
                param_dict.update({'mu_forbidden_3': sys_vel + init_mu_offset})
                param_dict.update({'sigma_balmer_1': init_sigma})
                param_dict.update({'sigma_balmer_2': init_sigma})
                param_dict.update({'sigma_balmer_3': init_sigma})
                param_dict.update({'sigma_forbidden_1': init_sigma})
                param_dict.update({'sigma_forbidden_2': init_sigma})
                param_dict.update({'sigma_forbidden_3': init_sigma})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
        else:
            if self.n_gauss == 1:
                param_dict.update({'mu': sys_vel})
                param_dict.update({'sigma': init_sigma})

            elif self.n_gauss == 2:
                param_dict.update({'mu_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_2': sys_vel + init_mu_offset})
                param_dict.update({'sigma_1': init_sigma})
                param_dict.update({'sigma_2': init_sigma})
            elif self.n_gauss == 3:
                param_dict.update({'mu_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_2': sys_vel})
                param_dict.update({'mu_3': sys_vel + init_mu_offset})
                param_dict.update({'sigma_1': init_sigma})
                param_dict.update({'sigma_2': init_sigma})
                param_dict.update({'sigma_3': init_sigma})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')

        if self.n_broad == 1:
            param_dict.update({'mu_broad': sys_vel})
            param_dict.update({'sigma_broad': init_broad_sigma})

        return param_dict

    def get_lamost_em_fit_init_amp_guess(self, param_dict=None, init_amp_frac=1., init_broad_amp_frac=0.2, redshift=None,
                                        vel_measure='balmer_gauss'):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'amp_%i' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_amp_frac})
            elif self.n_gauss == 2:
                param_dict.update({'amp_%i_1' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_amp_frac})
                param_dict.update({'amp_%i_2' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_amp_frac})
            elif self.n_gauss == 3:
                param_dict.update({'amp_%i_1' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_amp_frac})
                param_dict.update({'amp_%i_2' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_amp_frac})
                param_dict.update({'amp_%i_3' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_amp_frac})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')

            if (self.n_broad == 1) & (self.em_wavelength[line]['transition'] == 'balmer'):
                param_dict.update({'amp_%i_broad' % line:
                                       self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                                       vel_measure=vel_measure) * init_broad_amp_frac})

        return param_dict

    def get_lamost_em_fit_mu_sigma_lim(self, param_dict=None, mu_lim=None, sigma_lim=None, sigma_broad_lim=None,
                                      redshift=None, vel_measure='balmer_gauss'):

        # get systematic velocity
        sys_vel = self.get_lamost_sys_vel(redshift=redshift, vel_measure=vel_measure)

        if param_dict is None:
            param_dict = {}
        if mu_lim is None:
            mu_lim = (sys_vel - 1000., sys_vel + 1000.)
        if sigma_lim is None:
            sigma_lim = (60., 600.)
        if sigma_broad_lim is None:
            sigma_broad_lim = (500., 5000.)

        # get initial mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                param_dict.update({'limit_mu_balmer': mu_lim})
                param_dict.update({'limit_mu_forbidden': mu_lim})
                param_dict.update({'limit_sigma_balmer': sigma_lim})
                param_dict.update({'limit_sigma_forbidden': sigma_lim})

            elif self.n_gauss == 2:
                param_dict.update({'limit_mu_balmer_1': mu_lim})
                param_dict.update({'limit_mu_balmer_2': mu_lim})
                param_dict.update({'limit_mu_forbidden_1': mu_lim})
                param_dict.update({'limit_mu_forbidden_2': mu_lim})
                param_dict.update({'limit_sigma_balmer_1': sigma_lim})
                param_dict.update({'limit_sigma_balmer_2': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_1': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_2': sigma_lim})
            elif self.n_gauss == 3:
                param_dict.update({'limit_mu_balmer_1': mu_lim})
                param_dict.update({'limit_mu_balmer_2': mu_lim})
                param_dict.update({'limit_mu_balmer_3': mu_lim})
                param_dict.update({'limit_mu_forbidden_1': mu_lim})
                param_dict.update({'limit_mu_forbidden_2': mu_lim})
                param_dict.update({'limit_mu_forbidden_3': mu_lim})
                param_dict.update({'limit_sigma_balmer_1': sigma_lim})
                param_dict.update({'limit_sigma_balmer_2': sigma_lim})
                param_dict.update({'limit_sigma_balmer_3': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_1': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_2': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_3': sigma_lim})

            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
        else:
            if self.n_gauss == 1:
                param_dict.update({'limit_mu': mu_lim})
                param_dict.update({'limit_sigma': sigma_lim})

            elif self.n_gauss == 2:
                param_dict.update({'limit_mu_1': mu_lim})
                param_dict.update({'limit_mu_2': mu_lim})
                param_dict.update({'limit_sigma_1': sigma_lim})
                param_dict.update({'limit_sigma_2': sigma_lim})
            elif self.n_gauss == 3:
                param_dict.update({'limit_mu_1': mu_lim})
                param_dict.update({'limit_mu_2': mu_lim})
                param_dict.update({'limit_mu_3': mu_lim})
                param_dict.update({'limit_sigma_1': sigma_lim})
                param_dict.update({'limit_sigma_2': sigma_lim})
                param_dict.update({'limit_sigma_3': sigma_lim})

            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
        if self.n_broad == 1:
            param_dict.update({'limit_mu_broad': (sys_vel - 200., sys_vel + 200.)})
            param_dict.update({'limit_sigma_broad': sigma_broad_lim})



        return param_dict

    def get_lamost_em_fit_amp_lim(self, param_dict=None, amp_lim=None, redshift=None, vel_measure='balmer_gauss'):

        if param_dict is None:
            param_dict = {}
        if amp_lim is None:
            amp_lim = (0., 1.5)

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({
                    'limit_amp_%i' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})
            elif self.n_gauss == 2:
                param_dict.update({
                    'limit_amp_%i_1' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})
                param_dict.update({
                    'limit_amp_%i_2' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})
            elif self.n_gauss == 3:
                param_dict.update({
                    'limit_amp_%i_1' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})
                param_dict.update({
                    'limit_amp_%i_2' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})
                param_dict.update({
                    'limit_amp_%i_3' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
            if (self.n_broad == 1) & (self.em_wavelength[line]['transition'] == 'balmer'):
                param_dict.update({
                    'limit_amp_%i_broad' % line:
                        tuple(self.get_lamost_single_gauss_amp(line=line, redshift=redshift,
                                                              vel_measure=vel_measure) * x for x in amp_lim)})

        return param_dict

    def get_lamost_em_fit_param_init_step(self, param_dict=None, amp_init_step_frac=0.66, mu_init_step=100,
                                         sigma_init_step=100):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get initial mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                param_dict.update({'error_mu_balmer': mu_init_step})
                param_dict.update({'error_mu_forbidden': mu_init_step})
                param_dict.update({'error_sigma_balmer': sigma_init_step})
                param_dict.update({'error_sigma_forbidden': sigma_init_step})
            elif self.n_gauss == 2:
                param_dict.update({'error_mu_balmer_1': mu_init_step})
                param_dict.update({'error_mu_balmer_2': mu_init_step})
                param_dict.update({'error_mu_forbidden_1': mu_init_step})
                param_dict.update({'error_mu_forbidden_2': mu_init_step})
                param_dict.update({'error_sigma_balmer_1': sigma_init_step})
                param_dict.update({'error_sigma_balmer_2': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_1': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_2': sigma_init_step})
            elif self.n_gauss == 3:
                param_dict.update({'error_mu_balmer_1': mu_init_step})
                param_dict.update({'error_mu_balmer_2': mu_init_step})
                param_dict.update({'error_mu_balmer_3': mu_init_step})
                param_dict.update({'error_mu_forbidden_1': mu_init_step})
                param_dict.update({'error_mu_forbidden_2': mu_init_step})
                param_dict.update({'error_mu_forbidden_3': mu_init_step})
                param_dict.update({'error_sigma_balmer_1': sigma_init_step})
                param_dict.update({'error_sigma_balmer_2': sigma_init_step})
                param_dict.update({'error_sigma_balmer_3': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_1': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_2': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_3': sigma_init_step})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
        else:
            if self.n_gauss == 1:
                param_dict.update({'error_mu': mu_init_step})
                param_dict.update({'error_sigma': sigma_init_step})
            elif self.n_gauss == 2:
                param_dict.update({'error_mu_1': mu_init_step})
                param_dict.update({'error_mu_2': mu_init_step})
                param_dict.update({'error_sigma_1': sigma_init_step})
                param_dict.update({'error_sigma_2': sigma_init_step})
            elif self.n_gauss == 3:
                param_dict.update({'error_mu_1': mu_init_step})
                param_dict.update({'error_mu_2': mu_init_step})
                param_dict.update({'error_mu_3': mu_init_step})
                param_dict.update({'error_sigma_1': sigma_init_step})
                param_dict.update({'error_sigma_2': sigma_init_step})
                param_dict.update({'error_sigma_3': sigma_init_step})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
        if self.n_broad == 1:
            param_dict.update({'error_mu_broad': mu_init_step})
            param_dict.update({'error_sigma_broad': sigma_init_step})

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'error_amp_%i' % line: (amp_init_step_frac *
                                                           self.get_lamost_single_gauss_amp(line=line))})
            elif self.n_gauss == 2:
                param_dict.update({'error_amp_%i_1' % line: (amp_init_step_frac *
                                                             self.get_lamost_single_gauss_amp(line=line))})
                param_dict.update({'error_amp_%i_2' % line: (amp_init_step_frac *
                                                             self.get_lamost_single_gauss_amp(line=line))})
            elif self.n_gauss == 3:
                param_dict.update({'error_amp_%i_1' % line: (amp_init_step_frac *
                                                             self.get_lamost_single_gauss_amp(line=line))})
                param_dict.update({'error_amp_%i_2' % line: (amp_init_step_frac *
                                                             self.get_lamost_single_gauss_amp(line=line))})
                param_dict.update({'error_amp_%i_3' % line: (amp_init_step_frac *
                                                             self.get_lamost_single_gauss_amp(line=line))})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')
            if (self.n_broad == 1) & (self.em_wavelength[line]['transition'] == 'balmer'):
                param_dict.update({'error_amp_%i_broad' % line: (amp_init_step_frac *
                                                                 self.get_lamost_single_gauss_amp(line=line))})

        return param_dict

    def run_lamost_em_fit(self, n_gauss=1, n_broad=0, fit_mode='simple', two_vel=False, doublet_ratio=True, **kwargs):

        """ run fit for an lamost spectrum. per default it will perform a single gaussian fit to the spectrum

        Parameters
        ----------

        n_gauss: int (default: 1)
          Same as for `~self.select_fit_model`
        fit_mode : str (default: 'simple')
          Same as for `~self.select_fit_model`
        two_vel : bool (default: False)
          Same as for `~self.select_fit_model`
        doublet_ratio: bool (default: True)
          Same as for `~self.select_fit_model`

        Returns
        -------
        dict

        """
        # set up emission line fit
        self.setup_fit_configuration(n_gauss=n_gauss, n_broad=n_broad, fit_mode=fit_mode, two_vel=two_vel,
                                     doublet_ratio=doublet_ratio)
        # set up model parameters
        # self.set_up_emission_line_positions(redshift=self.get_lamost_spec_redshift(**kwargs))
        for line in self.get_full_line_list():
            self.set_up_inst_broad(inst_broad=self.get_lamost_line_inst_broad(line, **kwargs), line=line)

        # get fit model
        model = self.select_fit_model()
        # get fit parameter list
        fit_parameter_list = self.get_fit_parameter_list()
        # get data
        wave, em_flux, em_flux_err = self.get_lamost_em_spec()
        # get emission line_mask
        line_mask = self.get_lamost_multiple_line_masks(line_list=self.get_full_line_list(), **kwargs)

        # get initial parameter guess

        # init_fit_param_dict = self.get_lamost_em_fit_init_mu_sigma_guess(**kwargs)
        # init_fit_param_dict = self.get_lamost_em_fit_init_amp_guess(param_dict=init_fit_param_dict, **kwargs)
        # init_fit_param_dict = self.get_lamost_em_fit_mu_sigma_lim(param_dict=init_fit_param_dict, **kwargs)
        # init_fit_param_dict = self.get_lamost_em_fit_amp_lim(param_dict=init_fit_param_dict, **kwargs)
        # init_fit_param_dict = self.get_lamost_em_fit_param_init_step(param_dict=init_fit_param_dict, **kwargs)
        #
        # self.fit_model2data(model=model, param_dict=init_fit_param_dict,
        #                     x_data=wave[line_mask], y_data=em_flux[line_mask], y_data_err=em_flux_err[line_mask])

        # get initial parameter guess

        init_fit_param_dict = self.get_rcsed_em_fit_init_mu_sigma_guess(**kwargs)
        init_fit_param_dict = self.get_rcsed_em_fit_init_amp_guess(param_dict=init_fit_param_dict, **kwargs)


        lim_fit_param_dict = self.get_rcsed_em_fit_mu_sigma_lim(**kwargs)
        lim_fit_param_dict = self.get_rcsed_em_fit_amp_lim(param_dict=lim_fit_param_dict, **kwargs)
        lim_fit_param_dict = self.get_rcsed_em_fit_param_init_step(param_dict=lim_fit_param_dict, **kwargs)

        # print(init_fit_param_dict)

        self.fit_model2data(model=model, init_param_dict=init_fit_param_dict, lim_param_dict=lim_fit_param_dict,
                            x_data=wave[line_mask], y_data=em_flux[line_mask], y_data_err=em_flux_err[line_mask])

