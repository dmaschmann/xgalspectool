# -*- coding: utf-8 -*-

from spec_fit import muse_helper
import numpy as np
import os


class MUSESpecFit(muse_helper.MUSESpecHelper):

    def __init__(self, **kwargs):
        r"""
        class to access Manga observation
        """
        super().__init__(**kwargs)

    def get_muse_em_fit_init_mu_sigma_guess(self, bin_id, param_dict=None, init_mu_offset=200, init_sigma=100,
                                            init_broad_sigma=700):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get systematic velocity
        sys_vel = self.get_muse_em_vel_map(6565)[bin_id]

        # get initial mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                param_dict.update({'mu_balmer': sys_vel})
                param_dict.update({'mu_forbidden': sys_vel})
                param_dict.update({'sigma_balmer': init_sigma})
                param_dict.update({'sigma_forbidden': init_sigma})
            elif self.n_gauss == 2:
                param_dict.update({'mu_balmer_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_balmer_2': sys_vel + init_mu_offset})
                param_dict.update({'mu_forbidden_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_forbidden_2': sys_vel + init_mu_offset})
                param_dict.update({'sigma_balmer_1': init_sigma})
                param_dict.update({'sigma_balmer_2': init_sigma})
                param_dict.update({'sigma_forbidden_1': init_sigma})
                param_dict.update({'sigma_forbidden_2': init_sigma})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
        else:
            if self.n_gauss == 1:
                param_dict.update({'mu': sys_vel})
                param_dict.update({'sigma': init_sigma})

            elif self.n_gauss == 2:
                param_dict.update({'mu_1': sys_vel - init_mu_offset})
                param_dict.update({'mu_2': sys_vel + init_mu_offset})
                param_dict.update({'sigma_1': init_sigma})
                param_dict.update({'sigma_2': init_sigma})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
        if self.n_broad == 1:
            param_dict.update({'mu_broad': sys_vel})
            param_dict.update({'sigma_broad': init_broad_sigma})

        return param_dict

    def get_muse_em_fit_init_amp_guess(self, bin_id, param_dict=None, init_amp_frac=1., init_broad_amp_frac=0.2):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'amp_%i' % line:
                                   self.get_muse_line_max_value(bin_id=bin_id, line=line) * init_amp_frac})
            elif self.n_gauss == 2:
                param_dict.update({'amp_%i_1' % line:
                                   self.get_muse_line_max_value(bin_id=bin_id, line=line) * init_amp_frac})
                param_dict.update({'amp_%i_2' % line:
                                   self.get_muse_line_max_value(bin_id=bin_id, line=line) * init_amp_frac})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')

            if (self.n_broad == 1) & (self.em_wavelength[line]['transition'] == 'balmer'):
                param_dict.update({'amp_%i_broad' % line:
                                   self.get_muse_line_max_value(bin_id=bin_id, line=line) * init_broad_amp_frac})

        return param_dict

    def get_muse_em_fit_mu_sigma_lim(self, bin_id, param_dict=None, mu_lim=None, sigma_lim=None, sigma_broad_lim=None):

        # get systematic velocity
        sys_vel = self.get_muse_em_vel_map(6565)[bin_id]

        if param_dict is None:
            param_dict = {}
        if mu_lim is None:
            mu_lim = (sys_vel - 2000., sys_vel + 2000.)
        if sigma_lim is None:
            sigma_lim = (0., 2000.)
        if sigma_broad_lim is None:
            sigma_broad_lim = (0., 3000.)

        # get initial mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                param_dict.update({'limit_mu_balmer': mu_lim})
                param_dict.update({'limit_mu_forbidden': mu_lim})
                param_dict.update({'limit_sigma_balmer': sigma_lim})
                param_dict.update({'limit_sigma_forbidden': sigma_lim})
            elif self.n_gauss == 2:
                param_dict.update({'limit_mu_balmer_1': mu_lim})
                param_dict.update({'limit_mu_balmer_2': mu_lim})
                param_dict.update({'limit_mu_forbidden_1': mu_lim})
                param_dict.update({'limit_mu_forbidden_2': mu_lim})
                param_dict.update({'limit_sigma_balmer_1': sigma_lim})
                param_dict.update({'limit_sigma_balmer_2': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_1': sigma_lim})
                param_dict.update({'limit_sigma_forbidden_2': sigma_lim})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
        else:
            if self.n_gauss == 1:
                param_dict.update({'limit_mu': mu_lim})
                param_dict.update({'limit_sigma': sigma_lim})
            elif self.n_gauss == 2:
                param_dict.update({'limit_mu_1': mu_lim})
                param_dict.update({'limit_mu_2': mu_lim})
                param_dict.update({'limit_sigma_1': sigma_lim})
                param_dict.update({'limit_sigma_2': sigma_lim})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
        if self.n_broad == 1:
            param_dict.update({'limit_mu_broad': mu_lim})
            param_dict.update({'limit_sigma_broad': sigma_broad_lim})

        return param_dict

    def get_muse_em_fit_amp_lim(self, bin_id, param_dict=None, amp_lim=None):

        if param_dict is None:
            param_dict = {}
        if amp_lim is None:
            amp_lim = (0., 1.5)

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({
                    'limit_amp_%i' % line:
                        tuple(self.get_muse_line_max_value(bin_id=bin_id, line=line) * x for x in amp_lim)})
            elif self.n_gauss == 2:
                param_dict.update({
                    'limit_amp_%i_1' % line:
                        tuple(self.get_muse_line_max_value(bin_id=bin_id, line=line) * x for x in amp_lim)})
                param_dict.update({
                    'limit_amp_%i_2' % line:
                        tuple(self.get_muse_line_max_value(bin_id=bin_id, line=line) * x for x in amp_lim)})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
            if (self.n_broad == 1) & (self.em_wavelength[line]['transition'] == 'balmer'):
                param_dict.update({
                    'limit_amp_%i_broad' % line:
                        tuple(self.get_muse_line_max_value(bin_id=bin_id, line=line) * x for x in amp_lim)})
        return param_dict

    def get_muse_em_fit_param_init_step(self, bin_id, param_dict=None, amp_init_step_frac=0.66, mu_init_step=100,
                                         sigma_init_step=100):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get initial mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                param_dict.update({'error_mu_balmer': mu_init_step})
                param_dict.update({'error_mu_forbidden': mu_init_step})
                param_dict.update({'error_sigma_balmer': sigma_init_step})
                param_dict.update({'error_sigma_forbidden': sigma_init_step})
            elif self.n_gauss == 2:
                param_dict.update({'error_mu_balmer_1': mu_init_step})
                param_dict.update({'error_mu_balmer_2': mu_init_step})
                param_dict.update({'error_mu_forbidden_1': mu_init_step})
                param_dict.update({'error_mu_forbidden_2': mu_init_step})
                param_dict.update({'error_sigma_balmer_1': sigma_init_step})
                param_dict.update({'error_sigma_balmer_2': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_1': sigma_init_step})
                param_dict.update({'error_sigma_forbidden_2': sigma_init_step})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
        else:
            if self.n_gauss == 1:
                param_dict.update({'error_mu': mu_init_step})
                param_dict.update({'error_sigma': sigma_init_step})
            elif self.n_gauss == 2:
                param_dict.update({'error_mu_1': mu_init_step})
                param_dict.update({'error_mu_2': mu_init_step})
                param_dict.update({'error_sigma_1': sigma_init_step})
                param_dict.update({'error_sigma_2': sigma_init_step})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
        if self.n_broad == 1:
            param_dict.update({'error_mu_broad': mu_init_step})
            param_dict.update({'error_sigma_broad': sigma_init_step})

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'error_amp_%i' % line: (amp_init_step_frac *
                                                           self.get_muse_line_max_value(bin_id=bin_id, line=line))})
            elif self.n_gauss == 2:
                param_dict.update({'error_amp_%i_1' % line: (amp_init_step_frac *
                                                             self.get_muse_line_max_value(bin_id=bin_id, line=line))})
                param_dict.update({'error_amp_%i_2' % line: (amp_init_step_frac *
                                                             self.get_muse_line_max_value(bin_id=bin_id, line=line))})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')
            if (self.n_broad == 1) & (self.em_wavelength[line]['transition'] == 'balmer'):
                param_dict.update({'error_amp_%i_broad' % line: (amp_init_step_frac *
                                   self.get_muse_line_max_value(bin_id=bin_id, line=line))})

        return param_dict

    def run_muse_individual_spax_em_fit(self, bin_id, n_gauss=2, n_broad=0, fit_mode='muse_weak', two_vel=False,
                                        doublet_ratio=True,
                                        init_params=None, param_limits=None, param_step=None, sort_peaks=False,
                                        blue_limit=50, red_limit=50):

        """ run fit for an rcsed spectrum. per default it will perform a single gaussian fit to the spectrum

        Parameters
        ----------

        i, j : int
          coordinates of spaxel
        n_gauss: int (default: 1)
          Same as for `~self.select_fit_model`
        fit_mode : str (default: 'simple')
          Same as for `~self.select_fit_model`
        two_vel : bool (default: False)
          Same as for `~self.select_fit_model`
        doublet_ratio: bool (default: True)
          Same as for `~self.select_fit_model`

        Returns
        -------
        dict

        """
        # set up emission line fit
        self.setup_fit_configuration(n_gauss=n_gauss, n_broad=n_broad, fit_mode=fit_mode, two_vel=two_vel,
                                     doublet_ratio=doublet_ratio)
        # # set up model parameters
        # for line in self.get_full_line_list():
        #     self.set_up_inst_broad(inst_broad=self.get_muse_inst_broad_map(line=line)[i, j])

        # get fit model
        model = self.select_fit_model()
        # get fit parameter list
        fit_parameter_list = self.get_fit_parameter_list()
        print(fit_parameter_list)

        # get data
        muse_cube_dict = self.get_muse_cube_dict()

        wave = muse_cube_dict['wave'][bin_id]
        em_flux = muse_cube_dict['flux'][bin_id] - muse_cube_dict['total_fit_contin'][bin_id]
        em_flux_err = muse_cube_dict['flux_err'][bin_id]



        # get emission line_mask
        line_mask = self.get_muse_bin_multiple_em_mask(bin_id=bin_id, line_list=self.get_full_line_list(),
                                                       blue_limit=blue_limit, red_limit=red_limit)

        # get initial parameter guess
        init_fit_param_dict = {}
        if init_params is not None:
            init_fit_param_dict.update(init_params)
        else:
            init_fit_param_dict = self.get_muse_em_fit_init_mu_sigma_guess(bin_id, param_dict=init_fit_param_dict)
            init_fit_param_dict = self.get_muse_em_fit_init_amp_guess(bin_id, param_dict=init_fit_param_dict)
        if param_limits is not None:
            init_fit_param_dict.update(param_limits)
        else:
            init_fit_param_dict = self.get_muse_em_fit_mu_sigma_lim(bin_id, param_dict=init_fit_param_dict)
            init_fit_param_dict = self.get_muse_em_fit_amp_lim(bin_id, param_dict=init_fit_param_dict)
        if param_step is not None:
            init_fit_param_dict.update(param_step)
        else:
            init_fit_param_dict = self.get_muse_em_fit_param_init_step(bin_id, param_dict=init_fit_param_dict)

        print(init_fit_param_dict)

        self.fit_model2data(model=model, param_dict=init_fit_param_dict,
                            x_data=wave[line_mask], y_data=em_flux[line_mask], y_data_err=em_flux_err[line_mask],
                            sort_peaks=sort_peaks)
        print(self.fit_param_dict)

    def get_muse_evolution_fit_param_average(self, list_bin_ids):

        mean_parameter_dict = {}
        for bin_id in list_bin_ids:
            fit_results_folder = self.get_muse_object_file_path() / self.get_muse_fit_param_folder_name()
            fit_results = np.load(str(fit_results_folder / str(int(bin_id))) + '.npy', allow_pickle=True).item()

            if self.two_vel:
                if 'mu_balmer' not in mean_parameter_dict:
                    for param in self.get_fit_parameter_list():
                        mean_parameter_dict.update({param: fit_results[param]})
                else:
                    for param in self.get_fit_parameter_list():
                        mean_parameter_dict.update({param: np.mean((fit_results[param], mean_parameter_dict[param]))})
            else:
                if self.n_gauss == 1:
                    if 'mu' not in mean_parameter_dict:
                        mean_parameter_dict.update({'mu': fit_results['mu_balmer']})
                        mean_parameter_dict.update({'sigma': fit_results['sigma_balmer']})
                        for line in self.get_indep_line_list():
                            mean_parameter_dict.update({'amp_%i' % line: fit_results['amp_%i' % line]})
                    else:
                        mean_parameter_dict.update({'mu': np.mean((fit_results['mu'],
                                                                   mean_parameter_dict['mu_balmer']))})
                        mean_parameter_dict.update({'sigma': np.mean((fit_results['sigma'],
                                                                      mean_parameter_dict['sigma_balmer']))})
                        for line in self.get_indep_line_list():
                            mean_parameter_dict.update({'amp_%i' % line:
                                                            np.mean((fit_results['amp_%i' % line],
                                                                     mean_parameter_dict['amp_%i' % line]))})
                elif self.n_gauss == 2:
                    if 'mu_1' not in mean_parameter_dict:
                        mean_parameter_dict.update({'mu_1': fit_results['mu_balmer_1']})
                        mean_parameter_dict.update({'mu_2': fit_results['mu_balmer_2']})
                        mean_parameter_dict.update({'sigma_1': fit_results['sigma_balmer_1']})
                        mean_parameter_dict.update({'sigma_2': fit_results['sigma_balmer_2']})
                        for line in self.get_indep_line_list():
                            mean_parameter_dict.update({'amp_%i_1' % line: fit_results['amp_%i_1' % line]})
                            mean_parameter_dict.update({'amp_%i_2' % line: fit_results['amp_%i_2' % line]})
                    else:
                        mean_parameter_dict.update({'mu_1': np.mean((fit_results['mu_balmer_1'],
                                                                     mean_parameter_dict['mu_1']))})
                        mean_parameter_dict.update({'mu_2': np.mean((fit_results['mu_balmer_2'],
                                                                     mean_parameter_dict['mu_2']))})
                        mean_parameter_dict.update({'sigma_1': np.mean((fit_results['sigma_balmer_1'],
                                                                        mean_parameter_dict['sigma_1']))})
                        mean_parameter_dict.update({'sigma_2': np.mean((fit_results['sigma_balmer_2'],
                                                                        mean_parameter_dict['sigma_2']))})
                        for line in self.get_indep_line_list():
                            mean_parameter_dict.update({'amp_%i_1' % line:
                                                            np.mean((fit_results['amp_%i_1' % line],
                                                                     mean_parameter_dict['amp_%i_1' % line]))})
                            mean_parameter_dict.update({'amp_%i_2' % line:
                                                            np.mean((fit_results['amp_%i_2' % line],
                                                                     mean_parameter_dict['amp_%i_2' % line]))})
                else:
                    raise KeyError('number of n_gauss must be 1 or 2')
        return mean_parameter_dict

    def get_muse_evolution_fit_param_limit(self, mean_parameter_dict, dist_factor):
        limit_parameter_dict = {}

        if self.n_gauss == 1:
            if self.two_vel:
                limit_parameter_dict.update({'limit_mu_balmer': (mean_parameter_dict['mu_balmer'] -
                                                                 self.muse_mu_evolve * dist_factor,
                                                                 mean_parameter_dict['mu_balmer'] +
                                                                 self.muse_mu_evolve * dist_factor)})
                if mean_parameter_dict['sigma_balmer'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_balmer = 0
                else:
                    min_sigma_balmer = mean_parameter_dict['sigma_balmer'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_balmer': (min_sigma_balmer,
                                                                    mean_parameter_dict['sigma_balmer'] +
                                                                    self.muse_sigma_evolve * dist_factor)})

                limit_parameter_dict.update({'limit_mu_forbidden': (mean_parameter_dict['mu_forbidden'] -
                                                                    self.muse_mu_evolve * dist_factor,
                                                                    mean_parameter_dict['mu_forbidden'] +
                                                                    self.muse_mu_evolve * dist_factor)})
                if mean_parameter_dict['sigma_forbidden'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_forbidden = 0
                else:
                    min_sigma_forbidden = mean_parameter_dict['sigma_forbidden'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_forbidden': (min_sigma_forbidden,
                                                                       mean_parameter_dict['sigma_forbidden'] +
                                                                       self.muse_sigma_evolve * dist_factor)})
            else:
                limit_parameter_dict.update({'limit_mu': (mean_parameter_dict['mu'] - self.muse_mu_evolve * dist_factor,
                                                          mean_parameter_dict['mu'] + self.muse_mu_evolve * dist_factor)})
                if mean_parameter_dict['sigma'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma = 0
                else:
                    min_sigma = mean_parameter_dict['sigma'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma': (min_sigma, mean_parameter_dict['sigma'] +
                                                             self.muse_sigma_evolve * dist_factor)})

            for line in self.get_indep_line_list():
                limit_parameter_dict.update({'limit_amp_%i' % line: (0, mean_parameter_dict['amp_%i' % line] *
                                                                     self.muse_amp_evolve * dist_factor)})
        elif self.n_gauss == 2:
            if self.two_vel:
                limit_parameter_dict.update({'limit_mu_balmer_1': (mean_parameter_dict['mu_balmer_1'] -
                                                                   self.muse_mu_evolve * dist_factor,
                                                                   mean_parameter_dict['mu_balmer_1'] +
                                                                   self.muse_mu_evolve * dist_factor)})
                limit_parameter_dict.update({'limit_mu_balmer_2': (mean_parameter_dict['mu_balmer_2'] -
                                                                   self.muse_mu_evolve * dist_factor,
                                                                   mean_parameter_dict['mu_balmer_2'] +
                                                                   self.muse_mu_evolve * dist_factor)})

                if mean_parameter_dict['sigma_balmer_1'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_balmer_1 = 0
                else:
                    min_sigma_balmer_1 = mean_parameter_dict['sigma_balmer_1'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_balmer_1': (min_sigma_balmer_1,
                                                                      mean_parameter_dict['sigma_balmer_1'] +
                                                                      self.muse_sigma_evolve * dist_factor)})
                if mean_parameter_dict['sigma_balmer_2'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_balmer_2 = 0
                else:
                    min_sigma_balmer_2 = mean_parameter_dict['sigma_balmer_2'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_balmer_2': (min_sigma_balmer_2,
                                                                      mean_parameter_dict['sigma_balmer_2'] +
                                                                      self.muse_sigma_evolve * dist_factor)})

                limit_parameter_dict.update({'limit_mu_forbidden_1': (mean_parameter_dict['mu_forbidden_1'] -
                                                                      self.muse_mu_evolve * dist_factor,
                                                                      mean_parameter_dict['mu_forbidden_1'] +
                                                                      self.muse_mu_evolve * dist_factor)})
                limit_parameter_dict.update({'limit_mu_forbidden_2': (mean_parameter_dict['mu_forbidden_2'] -
                                                                      self.muse_mu_evolve * dist_factor,
                                                                      mean_parameter_dict['mu_forbidden_2'] +
                                                                      self.muse_mu_evolve * dist_factor)})

                if mean_parameter_dict['sigma_forbidden_1'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_forbidden_1 = 0
                else:
                    min_sigma_forbidden_1 = mean_parameter_dict['sigma_forbidden_1'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_forbidden_1': (min_sigma_forbidden_1,
                                                                         mean_parameter_dict['sigma_forbidden_1'] +
                                                                         self.muse_sigma_evolve * dist_factor)})
                if mean_parameter_dict['sigma_forbidden_2'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_forbidden_2 = 0
                else:
                    min_sigma_forbidden_2 = mean_parameter_dict['sigma_forbidden_2'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_forbidden_2': (min_sigma_forbidden_2,
                                                                         mean_parameter_dict['sigma_forbidden_2'] +
                                                                         self.muse_sigma_evolve * dist_factor)})

            else:
                limit_parameter_dict.update({'limit_mu_1': (mean_parameter_dict['mu_1'] - self.muse_mu_evolve * dist_factor,
                                                                 mean_parameter_dict['mu_1'] + self.muse_mu_evolve * dist_factor)})
                limit_parameter_dict.update({'limit_mu_2': (mean_parameter_dict['mu_2'] - self.muse_mu_evolve * dist_factor,
                                                                 mean_parameter_dict['mu_2'] + self.muse_mu_evolve * dist_factor)})

                if mean_parameter_dict['sigma_1'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_1 = 0
                else:
                    min_sigma_1 = mean_parameter_dict['sigma_1'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_1': (min_sigma_1,
                                                                    mean_parameter_dict['sigma_1'] +
                                                                    self.muse_sigma_evolve * dist_factor)})
                if mean_parameter_dict['sigma_2'] - self.muse_sigma_evolve * dist_factor < 0:
                    min_sigma_2 = 0
                else:
                    min_sigma_2 = mean_parameter_dict['sigma_2'] - self.muse_sigma_evolve * dist_factor
                limit_parameter_dict.update({'limit_sigma_2': (min_sigma_2,
                                                                    mean_parameter_dict['sigma_2'] +
                                                                    self.muse_sigma_evolve * dist_factor)})

            for line in self.get_indep_line_list():
                limit_parameter_dict.update({'limit_amp_%i_1' % line: (0, mean_parameter_dict['amp_%i_1' % line] *
                                                                 self.muse_amp_evolve * dist_factor)})
                limit_parameter_dict.update({'limit_amp_%i_2' % line: (0, mean_parameter_dict['amp_%i_2' % line] *
                                                                 self.muse_amp_evolve * dist_factor)})

        return limit_parameter_dict

    def _run_muse_evolution_fit(self, fit_bin_id, closest_bin_ids=None, distance_to_closest=None, plot=True):

        fit_results_folder = self.get_muse_object_file_path() / self.get_muse_fit_param_folder_name()
        # check if folder exists
        if not os.path.isdir(fit_results_folder):
            os.makedirs(fit_results_folder)
        # check if fit already exists
        if os.path.isfile(str(fit_results_folder / str(int(fit_bin_id))) + '.npy'):
            return None

        # # the selected spaxel was not fitted yet
        # objects = np.where(np.array(binid_map) == fit_bin_id)
        # i_list = objects[0]
        # j_list = objects[1]
        # i, j = i_list[0], j_list[0]
        # print('coords to fit: i=', i, 'j=', j)

        if closest_bin_ids is None:
            # fit first central bin
            self.run_muse_individual_spax_em_fit(bin_id=fit_bin_id, n_gauss=self.n_gauss, n_broad=self.n_broad,
                                                 fit_mode=self.fit_mode, two_vel=self.two_vel,
                                                 doublet_ratio=self.doublet_ratio, sort_peaks=False)

        else:
            # get fit parameter from closest spaxels
            mean_parameter_dict = self.get_muse_evolution_fit_param_average(list_bin_ids=closest_bin_ids)
            # compute the limits of fit_parameters
            limit_parameter_dict = self.get_muse_evolution_fit_param_limit(mean_parameter_dict=mean_parameter_dict,
                                                                           dist_factor=distance_to_closest)
            # fit nth bin
            self.run_muse_individual_spax_em_fit(bin_id=fit_bin_id, n_gauss=self.n_gauss, n_broad=self.n_broad,
                                                 fit_mode=self.fit_mode, two_vel=self.two_vel,
                                                 doublet_ratio=self.doublet_ratio,  init_params=mean_parameter_dict,
                                                 param_limits=limit_parameter_dict, sort_peaks=False)

        # plot results
        # get path for plot output
        if plot:
            from spec_fit import fit_plot
            from xgalimgtool import imgtool

            ra_centre, dec_centre = self.get_muse_bin_ra_dec(bin_id=0)
            spax_ra, spax_dec = self.get_muse_bin_ra_dec(bin_id=fit_bin_id)
            image_access = imgtool.ImgTool(ra=ra_centre, dec=dec_centre)
            g_header, img_rgb = image_access.get_legacy_survey_coord_img()

            plotting_class = fit_plot.FitPlot()
            muse_cube_dict = self.get_muse_cube_dict()
            wave = muse_cube_dict['wave'][fit_bin_id]
            em_flux = muse_cube_dict['flux'][fit_bin_id] - muse_cube_dict['total_fit_contin'][fit_bin_id]
            em_flux_err = muse_cube_dict['flux_err'][fit_bin_id]

            sys_vel = self.get_muse_sys_vel()
            fig = plotting_class.plot_double_gauss_fit_results_complex(fit_result_dict=self.fit_param_dict,
                                                                       header=g_header, img=img_rgb,
                                                                       ra=spax_ra, dec=spax_dec,
                                                                       inst_broad_dict=self.inst_broad,
                                                                       wave_sdss=wave, em_flux_sdss=em_flux,
                                                                       em_flux_err_sdss=em_flux_err,
                                                                       sys_vel=sys_vel, fiber_radius=0.5)

            plot_folder = ('plot_output/' +
                           'muse_fit_' + str(self.project_name) + '_' +
                           self.get_muse_fit_param_folder_name())
            if not os.path.isdir(plot_folder):
                os.makedirs(plot_folder)
            fig.savefig(plot_folder + '/bin_%i.png' % (fit_bin_id))
            fig.clf()

        # save fit results
        np.save(str(fit_results_folder / str(int(fit_bin_id))) + '.npy', self.fit_param_dict)
        print(self.fit_param_dict)
        # reset the fit parameter dict
        self.fit_param_dict = None

    def set_up_muse_spec_fit(self, n_gauss, n_broad, fit_mode, two_vel, doublet_ratio, muse_mu_evolve, muse_sigma_evolve, muse_amp_evolve):
        self.n_gauss = n_gauss
        self.n_broad = n_broad
        self.fit_mode = fit_mode
        self.two_vel = two_vel
        self.doublet_ratio = doublet_ratio
        self.muse_mu_evolve = muse_mu_evolve
        self.muse_sigma_evolve = muse_sigma_evolve
        self.muse_amp_evolve = muse_amp_evolve

    def muse_evolution_fit(self, n_gauss=2, n_broad=0, fit_mode='complex', two_vel=False, doublet_ratio=True,
                           muse_mu_evolve=100, muse_sigma_evolve=40, muse_amp_evolve=1, plot=True):

        # set up the fit parameters
        self.set_up_muse_spec_fit(n_gauss=n_gauss, n_broad=n_broad, fit_mode=fit_mode, two_vel=two_vel,
                                  doublet_ratio=doublet_ratio,
                                  muse_mu_evolve=muse_mu_evolve, muse_sigma_evolve=muse_sigma_evolve,
                                  muse_amp_evolve=muse_amp_evolve)

        # load the Manga bin id map
        binid_map = np.array(self.get_muse_bin_map())
        list_bin_ids = np.array(np.unique(binid_map), dtype=int)

        # remove -1 values
        # list_bin_ids = list_bin_ids[list_bin_ids != -1]

        # get the centre of the muse map
        muse_cube_dict = self.get_muse_cube_dict()

        x_pos = muse_cube_dict['x_pos']
        y_pos = muse_cube_dict['y_pos']
        i_0 = x_pos[0]
        j_0 = y_pos[0]
        print('the centre is at', i_0, j_0)

        # get the distances to the centre
        distance_list = np.sqrt((i_0 - x_pos) ** 2 + (j_0 - y_pos) ** 2)

        # distance_list = np.zeros(len(list_bin_ids))
        # distance_matrix = np.zeros(binid_map.shape)
        # for index in list_bin_ids:
        #     i = x_pos[index]
        #     j = y_pos[index]
        #     i_centre, j_centre = self.calculate_spax_center(i, j)
        #     distance_list[int(index)] = self.calculate_bin_distance(i_0, j_0, i_centre, j_centre)
        #     distance_matrix[i, j] = distance_list[int(index)]

        # get a unique distance list
        unique_distance_list = np.sort(np.unique(distance_list))

        # this is a dummy list that represent which spaxel has been fitted
        dummy_fit_list = np.zeros(len(list_bin_ids))

        for distance in unique_distance_list:

            print('distance from centre', distance)
            if distance == 0:
                # fit the central bin
                print('fitting Bin_id % i' % 0)
                self._run_muse_evolution_fit(fit_bin_id=0, plot=plot)

                dummy_fit_list[0] += 1
            else:
                # fit other bins
                # get positions of all spaxels with the distance
                # already_fitted_binids = list_bin_ids[np.array(dummy_fit_list, dtype=bool)]

                bin_ids_to_fit = list_bin_ids[np.where(distance_list == distance)]

                # fit each spaxel
                for fit_bin_id in bin_ids_to_fit:

                    # get positions of the bins which will be fitted
                    i_to_fit = x_pos[np.where(list_bin_ids == fit_bin_id)]
                    j_to_fit = y_pos[np.where(list_bin_ids == fit_bin_id)]
                    # find_closest_already_fitted_bins
                    # get distance to all other bins
                    distance_to_other_bins = np.sqrt((i_to_fit - x_pos) ** 2 + (j_to_fit - y_pos) ** 2)
                    # only the distance of all already fitted bins
                    closest_distance = np.min(distance_to_other_bins[np.array(dummy_fit_list, dtype=bool)])
                    index_closest_bin_ids = (distance_to_other_bins == closest_distance) * np.array(dummy_fit_list, dtype=bool)
                    closest_bin_ids = list_bin_ids[index_closest_bin_ids]

                    # i_to_fit, j_to_fit = np.where(binid_map == fit_bin_id)
                    # # i_to_fit_centre, j_to_fit_centre = self.calculate_spax_center(i_to_fit, j_to_fit)
                    # i_to_fit_centre = x_pos[fit_bin_id]
                    # j_to_fit_centre = y_pos[fit_bin_id]
                    # i_closest_list, j_closest_list, distance_to_closest = self.find_closest_already_fitted_spaxel(
                    #     binid_map, i_to_fit_centre, j_to_fit_centre, already_fitted_binids)

                    # closest_bin_ids = np.unique(binid_map[i_closest_list, j_closest_list])
                    # get the parameter of the closest fitted spaxels
                    # fit the spaxel
                    print('fitting Bin_id % i' % int(fit_bin_id), 'distance to closest bin', closest_distance)
                    self._run_muse_evolution_fit(fit_bin_id=fit_bin_id, closest_bin_ids=closest_bin_ids,
                                            distance_to_closest=closest_distance, plot=plot)
                    dummy_fit_list[fit_bin_id] += 1



