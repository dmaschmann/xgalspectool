# -*- coding: utf-8 -*-

from xgalspectool.spec_fit import rcsed_spec_fit, lamost_spec_fit, co_spec_fit, hi_spec_fit, sdss_co_spec_fit, manga_spec_fit


class EmissionLineFit(rcsed_spec_fit.RCSEDSpecFit, lamost_spec_fit.LAMOSTSpecFit, co_spec_fit.COSpecFit,
                      hi_spec_fit.HISpecFit, sdss_co_spec_fit.SDSSCOSpecFit, manga_spec_fit.MaNGASpecFit):

    def __init__(self, **kwargs):
        """
        Class to execute fitting procedure of emission lines

        """
        super().__init__(**kwargs)
