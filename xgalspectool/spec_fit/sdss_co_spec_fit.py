# -*- coding: utf-8 -*-

from xgalspectool.spec_fit import rcsed_helper, co_helper
from scipy.constants import c as speed_of_light
import numpy as np


class SDSSCOSpecFit(rcsed_helper.RCSEDSpecHelper, co_helper.COSpecHelper):

    def __init__(self, **kwargs):
        """
        Class to prepare fitting procedure of emission lines

        """
        super().__init__(**kwargs)

    def get_sdss_co_em_fit_init_mu_sigma_guess(self, param_dict=None, init_mu_offset=300, init_sigma=100,
                                               sigma_frac=0.66):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get systematic velocity
        sys_vel = speed_of_light * 1e-3 * self.co_redshift

        # get initial mu and sigma
        if self.n_gauss == 1:
            param_dict.update({'mu': sys_vel})
            param_dict.update({'sigma': init_sigma})
        elif self.n_gauss == 2:
            param_dict.update({'mu_1': sys_vel - init_mu_offset})
            param_dict.update({'mu_2': sys_vel + init_mu_offset})
            param_dict.update({'sigma_1': init_sigma * sigma_frac})
            param_dict.update({'sigma_2': init_sigma * sigma_frac})
        else:
            raise KeyError('self.n_gauss must be 1 or 2')

        return param_dict

    def get_sdss_co_em_fit_init_amp_guess(self, param_dict=None, init_amp_frac=1.):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                if isinstance(line, int):
                    param_dict.update({'amp_%i' % line:
                                           self.get_rcsed_single_gauss_amp(line=line, redshift=self.co_redshift) *
                                           init_amp_frac})
                else:
                    param_dict.update({'amp_%s' % line: self.get_co_max_line_flux(co_line=line) * init_amp_frac})
            elif self.n_gauss == 2:
                if isinstance(line, int):

                    param_dict.update({'amp_%i_1' % line:
                                           self.get_rcsed_single_gauss_amp(line=line, redshift=self.co_redshift) *
                                           init_amp_frac})
                    param_dict.update({'amp_%i_2' % line:
                                           self.get_rcsed_single_gauss_amp(line=line, redshift=self.co_redshift) *
                                           init_amp_frac})
                else:
                    param_dict.update({'amp_%s_1' % line: self.get_co_max_line_flux(co_line=line) * init_amp_frac})
                    param_dict.update({'amp_%s_2' % line: self.get_co_max_line_flux(co_line=line) * init_amp_frac})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')

        return param_dict

    def get_sdss_co_em_fit_mu_sigma_lim(self, param_dict=None, mu_lim=None, sigma_lim=None):

        # get systematic velocity
        sys_vel = speed_of_light * 1e-3 * self.co_redshift

        if param_dict is None:
            param_dict = {}
        if mu_lim is None:
            mu_lim = (sys_vel - 1000., sys_vel + 1000.)
        if sigma_lim is None:
            sigma_lim = (0., 1500.)
        # get initial mu and sigma
        if self.n_gauss == 1:
            param_dict.update({'limit_mu': mu_lim})
            param_dict.update({'limit_sigma': sigma_lim})
        elif self.n_gauss == 2:
            param_dict.update({'limit_mu_1': mu_lim})
            param_dict.update({'limit_mu_2': mu_lim})
            param_dict.update({'limit_sigma_1': sigma_lim})
            param_dict.update({'limit_sigma_2': sigma_lim})
        else:
            raise KeyError('self.n_gauss must be 1 or 2')

        return param_dict

    def get_sdss_co_em_fit_amp_lim(self, param_dict=None, amp_lim=None, max_am_frac=3.):

        if param_dict is None:
            param_dict = {}
        if amp_lim is None:
            amp_lim = (0., 1.5)

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                if isinstance(line, int):
                    param_dict.update({
                        'limit_amp_%i' % line:
                            tuple(self.get_rcsed_single_gauss_amp(line=line, redshift=self.co_redshift) * x for x in amp_lim)})
                else:
                    param_dict.update({'limit_amp_%s' % line: (0, self.get_co_max_line_flux(co_line=line) * max_am_frac)})
            elif self.n_gauss == 2:
                if isinstance(line, int):
                    param_dict.update({
                        'limit_amp_%i_1' % line:
                            tuple(self.get_rcsed_single_gauss_amp(line=line, redshift=self.co_redshift) * x for x in amp_lim)})
                    param_dict.update({
                        'limit_amp_%i_2' % line:
                            tuple(self.get_rcsed_single_gauss_amp(line=line, redshift=self.co_redshift) * x for x in amp_lim)})
                else:
                    param_dict.update({'limit_amp_%s_1' % line: (0, self.get_co_max_line_flux(co_line=line) * max_am_frac)})
                    param_dict.update({'limit_amp_%s_2' % line: (0, self.get_co_max_line_flux(co_line=line) * max_am_frac)})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')

        return param_dict

    def get_sdss_co_em_fit_param_init_step(self, param_dict=None, amp_init_step_frac=0.66, mu_init_step=100,
                                         sigma_init_step_frac=0.66):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        sys_vel = speed_of_light * 1e-3 * self.co_redshift

        if self.n_gauss == 1:
            param_dict.update({'error_mu': mu_init_step})
            param_dict.update({'error_sigma': sys_vel * sigma_init_step_frac})
        elif self.n_gauss == 2:
            param_dict.update({'error_mu_1': mu_init_step})
            param_dict.update({'error_mu_2': mu_init_step})
            param_dict.update({'error_sigma_1': sys_vel * sigma_init_step_frac})
            param_dict.update({'error_sigma_2': sys_vel * sigma_init_step_frac})
        else:
            raise KeyError('self.n_gauss must be 1 or 2')

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                if isinstance(line, int):
                    param_dict.update({'error_amp_%i' % line: amp_init_step_frac * self.get_rcsed_single_gauss_amp(line=line)})
                else:
                    param_dict.update({'error_amp_%s' % line: amp_init_step_frac * self.get_co_max_line_flux(co_line=line)})
            elif self.n_gauss == 2:
                if isinstance(line, int):
                    param_dict.update({'error_amp_%i_1' % line: amp_init_step_frac * self.get_rcsed_single_gauss_amp(line=line)})
                    param_dict.update({'error_amp_%i_2' % line: amp_init_step_frac * self.get_rcsed_single_gauss_amp(line=line)})
                else:
                    param_dict.update({'error_amp_%s_1' % line: amp_init_step_frac * self.get_co_max_line_flux(co_line=line)})
                    param_dict.update({'error_amp_%s_2' % line: amp_init_step_frac * self.get_co_max_line_flux(co_line=line)})
            else:
                raise KeyError('self.n_gauss must be 1 or 2')

        return param_dict

    def run_sdss_co_fit(self, n_gauss=1, fit_mode='simple_co10_co21', doublet_ratio=True, co_weights=1.,
                        vel_bin_width_co10=60, vel_bin_width_co21=60, **kwargs):

        """ run fit for an rcsed spectrum. per default it will perform a single gaussian fit to the spectrum

        Parameters
        ----------

        n_gauss: int (default: 1)
          Same as for `~self.select_fit_model`
        fit_mode : str (default: 'simple')
          Same as for `~self.select_fit_model`
        doublet_ratio: bool (default: True)
          Same as for `~self.select_fit_model`
        co_weights: float (default: 1.)
          weight given to the co spectra

        Returns
        -------
        dict

        """
        # set up emission line fit
        self.setup_fit_configuration(n_gauss=n_gauss, fit_mode=fit_mode, doublet_ratio=doublet_ratio)
        # set up model parameters
        # self.set_up_emission_line_positions(redshift=self.get_rcsed_spec_redshift(**kwargs))
        for line in self.get_full_line_list():
            if isinstance(line, int):
                self.set_up_inst_broad(inst_broad=self.get_rcsed_line_inst_broad(line, **kwargs), line=line)

        # get fit model
        model = self.select_fit_model()

        # get fit parameter list
        fit_parameter_list = self.get_fit_parameter_list()

        # get data
        wave_sdss, em_flux_sdss, em_flux_err_sdss = self.get_rcsed_em_spec()
        # get emission line_mask
        line_mask_sdss = self.get_rcsed_multiple_line_masks(line_list=[x for x in self.get_full_line_list()
                                                                       if isinstance(x, int)], **kwargs)

        # load data
        self.load_co_spectrum()

        # combine data
        wave = wave_sdss
        em_flux = em_flux_sdss
        line_mask = line_mask_sdss
        em_flux_err = em_flux_err_sdss
        if 'co10' in fit_mode:
            if vel_bin_width_co10 is not None:
                self.rebin_co_spectrum(vel_bin_width=vel_bin_width_co10, co_line='co10')
            line_mask_co10 = self.get_co_mask(vel_radius=1000, wave_radius=None, co_line='co10')
            wave = np.concatenate([wave, self.wave_co10])
            em_flux = np.concatenate([em_flux, self.flux_co10])
            line_mask = np.concatenate([line_mask, line_mask_co10])
            em_flux_err = np.concatenate([em_flux_err, np.ones(len(self.flux_co10)) * (1 / co_weights)])
        if 'co21' in fit_mode:
            if vel_bin_width_co21 is not None:
                self.rebin_co_spectrum(vel_bin_width=vel_bin_width_co21, co_line='co21')
            line_mask_co21 = self.get_co_mask(vel_radius=1000, wave_radius=None, co_line='co21')
            wave = np.concatenate([wave, self.wave_co21])
            em_flux = np.concatenate([em_flux, self.flux_co21])
            line_mask = np.concatenate([line_mask, line_mask_co21])
            em_flux_err = np.concatenate([em_flux_err, np.ones(len(self.flux_co21)) * (1 / co_weights)])


        # wave = np.concatenate([wave_sdss, self.wave_co10, self.wave_co21])
        # em_flux = np.concatenate([em_flux_sdss, self.flux_co10, self.flux_co21])
        # line_mask = np.concatenate([line_mask_sdss, line_mask_co10, line_mask_co21])
        # em_flux_err = np.concatenate([em_flux_err_sdss, np.ones(len(self.flux_co10)) * (1 / co_weights), np.ones(len(self.flux_co21)) * co_weights])
        #

        # get initial parameter guess
        init_fit_param_dict = self.get_sdss_co_em_fit_init_mu_sigma_guess(**kwargs)
        init_fit_param_dict = self.get_sdss_co_em_fit_init_amp_guess(param_dict=init_fit_param_dict, **kwargs)
        init_fit_param_dict = self.get_sdss_co_em_fit_mu_sigma_lim(param_dict=init_fit_param_dict, **kwargs)
        init_fit_param_dict = self.get_sdss_co_em_fit_amp_lim(param_dict=init_fit_param_dict, **kwargs)
        init_fit_param_dict = self.get_sdss_co_em_fit_param_init_step(param_dict=init_fit_param_dict, **kwargs)

        self.fit_model2data(model=model, param_dict=init_fit_param_dict,
                            x_data=wave[line_mask], y_data=em_flux[line_mask], y_data_err=em_flux_err[line_mask])



