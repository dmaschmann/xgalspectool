# -*- coding: utf-8 -*-
import numpy as np

from xgalspectool.spec_fit import hi_helper
from scipy.constants import c as speed_of_light


class HISpecFit(hi_helper.HISpecHelper):

    def __init__(self, **kwargs):
        """
        Class to prepare fitting procedure of emission lines
        """
        super().__init__(**kwargs)

    def get_hi_em_fit_init_mu_sigma_guess(self, param_dict=None, sigma_frac=1.):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get systematic velocity
        sys_vel = speed_of_light * 1e-3 * self.hi_redshift

        # get initial mu and sigma
        if self.n_gauss == 1:
            param_dict.update({'mu': sys_vel})
            param_dict.update({'sigma': 200})
        elif self.n_gauss == 2:
            param_dict.update({'mu_1': sys_vel - 200})
            param_dict.update({'mu_2': sys_vel + 200})
            param_dict.update({'sigma_1': 200 * sigma_frac})
            param_dict.update({'sigma_2': 200 * sigma_frac})
        elif self.n_gauss == 3:
            param_dict.update({'mu_1': sys_vel - 200})
            param_dict.update({'mu_2': sys_vel})
            param_dict.update({'mu_3': sys_vel + 200})
            param_dict.update({'sigma_1': 200 * sigma_frac})
            param_dict.update({'sigma_2': 200 * sigma_frac})
            param_dict.update({'sigma_3': 200 * sigma_frac})
        else:
            raise KeyError('self.n_gauss must be 1, 2 or 3')

        return param_dict

    def get_hi_em_fit_init_amp_guess(self, param_dict=None, init_amp_frac=0.5):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'amp_%s' % line: self.get_hi_max_line_flux() * init_amp_frac})
            elif self.n_gauss == 2:
                param_dict.update({'amp_%s_1' % line: self.get_hi_max_line_flux() * init_amp_frac})
                param_dict.update({'amp_%s_2' % line: self.get_hi_max_line_flux() * init_amp_frac})
            elif self.n_gauss == 3:
                param_dict.update({'amp_%s_1' % line: self.get_hi_max_line_flux() * init_amp_frac})
                param_dict.update({'amp_%s_2' % line: self.get_hi_max_line_flux() * init_amp_frac})
                param_dict.update({'amp_%s_3' % line: self.get_hi_max_line_flux() * init_amp_frac})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')

        return param_dict

    def get_hi_em_fit_mu_sigma_lim(self, param_dict=None, mu_lim=None, sigma_lim=None):

        # get systematic velocity
        sys_vel = speed_of_light * 1e-3 * self.hi_redshift

        if param_dict is None:
            param_dict = {}
        if mu_lim is None:
            mu_lim = (sys_vel - 1000., sys_vel + 1000.)
        if sigma_lim is None:
            sigma_lim = (0., 1500.)
        # get initial mu and sigma
        if self.n_gauss == 1:
            param_dict.update({'limit_mu': mu_lim})
            param_dict.update({'limit_sigma': sigma_lim})
        elif self.n_gauss == 2:
            param_dict.update({'limit_mu_1': mu_lim})
            param_dict.update({'limit_mu_2': mu_lim})
            param_dict.update({'limit_sigma_1': sigma_lim})
            param_dict.update({'limit_sigma_2': sigma_lim})
        elif self.n_gauss == 3:
            param_dict.update({'limit_mu_1': mu_lim})
            param_dict.update({'limit_mu_2': mu_lim})
            param_dict.update({'limit_mu_3': mu_lim})
            param_dict.update({'limit_sigma_1': sigma_lim})
            param_dict.update({'limit_sigma_2': sigma_lim})
            param_dict.update({'limit_sigma_3': sigma_lim})
        else:
            raise KeyError('self.n_gauss must be 1, 2 or 3')

        return param_dict

    def get_hi_em_fit_amp_lim(self, param_dict=None, max_am_frac=3.):

        if param_dict is None:
            param_dict = {}

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'limit_amp_%s' % line: (0, self.get_hi_max_line_flux() * max_am_frac)})
            elif self.n_gauss == 2:
                param_dict.update({'limit_amp_%s_1' % line: (0, self.get_hi_max_line_flux() * max_am_frac)})
                param_dict.update({'limit_amp_%s_2' % line: (0, self.get_hi_max_line_flux() * max_am_frac)})
            elif self.n_gauss == 3:
                param_dict.update({'limit_amp_%s_1' % line: (0, self.get_hi_max_line_flux() * max_am_frac)})
                param_dict.update({'limit_amp_%s_2' % line: (0, self.get_hi_max_line_flux() * max_am_frac)})
                param_dict.update({'limit_amp_%s_3' % line: (0, self.get_hi_max_line_flux() * max_am_frac)})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')

        return param_dict

    def get_hi_em_fit_param_init_step(self, param_dict=None, amp_init_step_frac=0.66, mu_init_step=100,
                                      sigma_init_step_frac=0.66):
        # use initial amplitude
        if param_dict is None:
            param_dict = {}

        sys_vel = speed_of_light * 1e-3 * self.hi_redshift

        if self.n_gauss == 1:
            param_dict.update({'error_mu': mu_init_step})
            param_dict.update({'error_sigma': sys_vel * sigma_init_step_frac})
        elif self.n_gauss == 2:
            param_dict.update({'error_mu_1': mu_init_step})
            param_dict.update({'error_mu_2': mu_init_step})
            param_dict.update({'error_sigma_1': sys_vel * sigma_init_step_frac})
            param_dict.update({'error_sigma_2': sys_vel * sigma_init_step_frac})
        elif self.n_gauss == 3:
            param_dict.update({'error_mu_1': mu_init_step})
            param_dict.update({'error_mu_2': mu_init_step})
            param_dict.update({'error_mu_3': mu_init_step})
            param_dict.update({'error_sigma_1': sys_vel * sigma_init_step_frac})
            param_dict.update({'error_sigma_2': sys_vel * sigma_init_step_frac})
            param_dict.update({'error_sigma_3': sys_vel * sigma_init_step_frac})
        else:
            raise KeyError('self.n_gauss must be 1, 2 or 3')

        # get emission_line amplitudes
        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                param_dict.update({'error_amp_%s' % line: amp_init_step_frac * self.get_hi_max_line_flux()})
            elif self.n_gauss == 2:
                param_dict.update({'error_amp_%s_1' % line: amp_init_step_frac * self.get_hi_max_line_flux()})
                param_dict.update({'error_amp_%s_2' % line: amp_init_step_frac * self.get_hi_max_line_flux()})
            elif self.n_gauss == 3:
                param_dict.update({'error_amp_%s_1' % line: amp_init_step_frac * self.get_hi_max_line_flux()})
                param_dict.update({'error_amp_%s_2' % line: amp_init_step_frac * self.get_hi_max_line_flux()})
                param_dict.update({'error_amp_%s_3' % line: amp_init_step_frac * self.get_hi_max_line_flux()})
            else:
                raise KeyError('self.n_gauss must be 1, 2 or 3')

        return param_dict

    def run_hi_fit(self, n_gauss=1, fit_mode='hi', vel_bin_width_hi=10,
                   init_params=None, param_limits=None, param_step=None):

        """ run fit for an rcsed spectrum. per default it will perform a single gaussian fit to the spectrum

        Parameters
        ----------

        n_gauss: int (default: 1)
          Same as for `~self.select_fit_model`
        fit_mode : str (default: 'simple')
          Same as for `~self.select_fit_model`

        Returns
        -------
        dict

        """
        # set up emission line fit
        self.setup_fit_configuration(n_gauss=n_gauss, fit_mode=fit_mode, two_vel=None, doublet_ratio=None)
        # set up model parameters
        # self.set_up_emission_line_positions(redshift=self.hi_redshift)

        # get fit model
        model = self.select_fit_model()
        # get fit parameter list
        fit_parameter_list = self.get_fit_parameter_list()

        # wave, em_flux, em_flux_err = self.get_rcsed_em_spec()

        # load data
        self.load_hi_spectrum()
        # rebin data
        if vel_bin_width_hi is not None:
            self.rebin_hi_spectrum(vel_bin_width=vel_bin_width_hi)

        line_mask_hi = self.get_hi_mask(vel_radius=2000)

        wave = self.wave_hi
        em_flux = self.flux_hi
        line_mask = line_mask_hi


        # # get initial parameter guess
        # init_fit_param_dict = self.get_hi_em_fit_init_mu_sigma_guess(**kwargs)
        # init_fit_param_dict = self.get_hi_em_fit_init_amp_guess(param_dict=init_fit_param_dict, **kwargs)
        # init_fit_param_dict = self.get_hi_em_fit_mu_sigma_lim(param_dict=init_fit_param_dict, **kwargs)
        # init_fit_param_dict = self.get_hi_em_fit_amp_lim(param_dict=init_fit_param_dict, **kwargs)
        # init_fit_param_dict = self.get_hi_em_fit_param_init_step(param_dict=init_fit_param_dict, **kwargs)
        # get initial parameter guess
        init_fit_param_dict = {}
        if init_params is not None:
            init_fit_param_dict.update(init_params)
        else:
            init_fit_param_dict = self.get_hi_em_fit_init_mu_sigma_guess(param_dict=init_fit_param_dict)
            init_fit_param_dict = self.get_hi_em_fit_init_amp_guess(param_dict=init_fit_param_dict)
        if param_limits is not None:
            init_fit_param_dict.update(param_limits)
        else:
            init_fit_param_dict = self.get_hi_em_fit_mu_sigma_lim(param_dict=init_fit_param_dict)
            init_fit_param_dict = self.get_hi_em_fit_amp_lim(param_dict=init_fit_param_dict)
        if param_step is not None:
            init_fit_param_dict.update(param_step)
        else:
            init_fit_param_dict = self.get_hi_em_fit_param_init_step(param_dict=init_fit_param_dict)

        self.fit_model2data(model=model, param_dict=init_fit_param_dict,
                            x_data=wave[line_mask], y_data=em_flux[line_mask])

