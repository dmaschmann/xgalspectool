# -*- coding: utf-8 -*-

import numpy as np
import os
from pathlib import Path
from astropy.io import fits

from scipy.constants import c as speed_of_light

from fit_algorithm import em_fit_algorithm


class MUSESpecHelper(em_fit_algorithm.EmissionLineFit):

    def __init__(self, project_name, muse_ra, muse_dec, muse_cube_path=None, data_path=None,
                 x_muse_bin_size=None, y_muse_bin_size=None, muse_redshift=None,
                 nbursts_n_stellar_comp=1, nbursts_n_narrow_lines=1, nbursts_n_broad_lines=0, **kwargs):
        """

        """
        super().__init__(**kwargs)

        # get project name and coordinates as attributes
        self.project_name = project_name
        self.muse_ra = muse_ra
        self.muse_dec = muse_dec

        # check if cube path is given and correct
        if not os.path.isdir(muse_cube_path):
            raise KeyError('Data path for muse cubes does not exist')
        self.muse_cube_path = muse_cube_path

        # check if data_path is given and correct
        if data_path is None:  # use short cut
            data_path = Path.home() / 'data'
        if os.path.isdir(Path(data_path)):
            self.data_path = Path(data_path)
        else:
            raise OSError('The data path %s is not existent' % data_path)

        # get more specifications about the data format
        self.x_muse_bin_size = x_muse_bin_size
        self.y_muse_bin_size = y_muse_bin_size

        # specifications on nbursts fit
        self.nbursts_n_stellar_comp = nbursts_n_stellar_comp
        self.nbursts_n_narrow_lines = nbursts_n_narrow_lines
        self.nbursts_n_broad_lines = nbursts_n_broad_lines

        # specify the object redshift
        self.muse_redshift = muse_redshift

        # MUSE data cubes
        self.muse_bin_id_map = None
        self.muse_cube_dict = None

        # parameter variation for evolution fit parameters
        self.muse_mu_evolve = None
        self.muse_sigma_evolve = None
        self.muse_amp_evolve = None

    def get_muse_bin_map(self):

        if self.muse_bin_id_map is not None:
            return self.muse_bin_id_map

        hdu_binnum = fits.open(self.muse_cube_path + 'binnum.fits')
        bin_map = np.array(hdu_binnum[0].data, dtype=float).reshape(self.x_muse_bin_size, self.y_muse_bin_size)

        # get a 2 array with the right bin ids
        # over write also nan value bins

        for i in range(self.x_muse_bin_size):
            for j in range(self.y_muse_bin_size):
                if bin_map[i, j] == -999:
                    # get list of neighboring pixel
                    bin_list = []
                    if i < (self.x_muse_bin_size - 1):
                        bin_list.append(bin_map[i + 1, j])
                    if j < (self.y_muse_bin_size - 1):
                        bin_list.append(bin_map[i, j + 1])
                    if (i < (self.x_muse_bin_size - 1)) & (j < (self.y_muse_bin_size - 1)):
                        bin_list.append(bin_map[i + 1, j + 1])

                    if i > 0:
                        bin_list.append(bin_map[i - 1, j])
                    if j > 0:
                        bin_list.append(bin_map[i, j - 1])
                    if (i > 0) & (j > 0):
                        bin_list.append(bin_map[i - 1, j - 1])

                    if (i < (self.x_muse_bin_size - 1)) & (j > 0):
                        bin_list.append(bin_map[i + 1, j - 1])
                    if (i > 0) & (j < (self.y_muse_bin_size - 1)):
                        bin_list.append(bin_map[i - 1, j + 1])

                    # remove all -999 values
                    bin_list = list(filter((-999.0).__ne__, bin_list))

                    # get the most frequent bin id:
                    most_frequent_bin = max(set(bin_list), key=bin_list.count)

                    bin_map[i, j] = most_frequent_bin

        self.muse_bin_id_map = bin_map

        # close hdu
        hdu_binnum.close()

        return self.muse_bin_id_map

    def get_muse_cube_dict(self, eml_list=None):
        if self.muse_cube_dict is not None:
            return self.muse_cube_dict

        if eml_list is None:
            eml_list = [3727, 3730, 4103, 4342, 4863, 4960, 5008, 6302, 6314, 6349, 6366, 6550, 6565, 6585, 6718, 6733]

        # open hdu of nburst fit
        hdu_ppxf_results = fits.open(self.muse_cube_path + 'ppxf_results.fits')

        # get an empty dictionary
        muse_cube_dict = {}

        # get wavelength, flux, error and total best fit
        muse_cube_dict.update({'wave': hdu_ppxf_results[1].data['WAVE']})
        muse_cube_dict.update({'flux': hdu_ppxf_results[1].data['FLUX']})
        muse_cube_dict.update({'flux_err': hdu_ppxf_results[1].data['ERROR']})
        muse_cube_dict.update({'total_fit': hdu_ppxf_results[1].data['FIT']})
        # get stellar continuum
        for index in range(0, self.nbursts_n_stellar_comp):
            # this is only meant for a single stellar component so far.
            # adapt in future version
            muse_cube_dict.update({'fit_contin_%i' % (index + 1): hdu_ppxf_results[1].data['FIT_COMP'][:, index]})
            # fitting parameters
            muse_cube_dict.update({'vel_contin_%i' % (index + 1): hdu_ppxf_results[1].data['V'][:, index].T[0]})
            muse_cube_dict.update({'vel_contin_%i_err' % (index + 1): hdu_ppxf_results[1].data['E_V'][:, index].T[0]})
            muse_cube_dict.update({'sig_contin_%i' % (index + 1): hdu_ppxf_results[1].data['SIG'][:, index].T[0]})
            muse_cube_dict.update({'sig_contin_%i_err' % (index + 1): hdu_ppxf_results[1].data['E_SIG'][:, index].T[0]})
            muse_cube_dict.update({'h3_contin_%i' % (index + 1): hdu_ppxf_results[1].data['H3'][:, index].T[0]})
            muse_cube_dict.update({'h3_contin_%i_err' % (index + 1): hdu_ppxf_results[1].data['E_H3'][:, index].T[0]})
            muse_cube_dict.update({'h4_contin_%i' % (index + 1): hdu_ppxf_results[1].data['H4'][:, index].T[0]})
            muse_cube_dict.update({'h4_contin_%i_err' % (index + 1): hdu_ppxf_results[1].data['E_H4'][:, index].T[0]})
            # ages and metallicity
            muse_cube_dict.update({'stellar_age': hdu_ppxf_results[1].data['AGE']})
            muse_cube_dict.update({'stellar_age_err': hdu_ppxf_results[1].data['E_AGE']})
            muse_cube_dict.update({'stellar_met': hdu_ppxf_results[1].data['MET']})
            muse_cube_dict.update({'stellar_met_err': hdu_ppxf_results[1].data['E_MET']})

        # get a complete fir continuum (for later correct continuum subtraction)
        muse_cube_dict.update({'total_fit_contin': np.zeros(muse_cube_dict['fit_contin_1'].shape)})
        for index in range(0, self.nbursts_n_stellar_comp):
            muse_cube_dict['total_fit_contin'] += muse_cube_dict['fit_contin_%i' % (index + 1)]

        # get narrow emission lines
        for index in range(self.nbursts_n_stellar_comp, self.nbursts_n_stellar_comp + self.nbursts_n_narrow_lines):
            # the fitted lines
            muse_cube_dict.update({'fit_narrow_el_%i' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['FIT_COMP'][:, index]})
            # fitting parameters
            muse_cube_dict.update({'vel_narrow_el_%i' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['V'][:, index].T[0]})
            muse_cube_dict.update({'vel_narrow_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['E_V'][:, index].T[0]})
            muse_cube_dict.update({'sig_narrow_el_%i' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['SIG'][:, index].T[0]})
            muse_cube_dict.update({'sig_narrow_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['E_SIG'][:, index].T[0]})
            muse_cube_dict.update({'h3_narrow_el_%i' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['H3'][:, index].T[0]})
            muse_cube_dict.update({'h3_narrow_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['E_H3'][:, index].T[0]})
            muse_cube_dict.update({'h4_narrow_el_%i' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['H4'][:, index].T[0]})
            muse_cube_dict.update({'h4_narrow_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp):
                                   hdu_ppxf_results[1].data['E_H4'][:, index].T[0]})
            # emission line fluxes
            for line in eml_list:
                # find line position in the fits file
                line_index_position = np.where((hdu_ppxf_results[2].data['LINE_ID'] ==
                                                self.em_wavelength[line]['nbursts_name']) &
                                               (hdu_ppxf_results[2].data['LOSVD_ID'] == (index + 1 - self.nbursts_n_stellar_comp)))
                # save flux
                muse_cube_dict.update({'narrow_el_%i_flux_%i' % (line, (index + 1 - self.nbursts_n_stellar_comp)):
                                       hdu_ppxf_results[2].data['FLUX'][line_index_position]})
                muse_cube_dict.update({'narrow_el_%i_flux_%i_err' % (line, (index + 1 - self.nbursts_n_stellar_comp)):
                                       hdu_ppxf_results[2].data['FLUX_ERR'][line_index_position]})
                # continuum level
                muse_cube_dict.update({'narrow_el_%i_cont_%i' % (line, (index + 1 - self.nbursts_n_stellar_comp)):
                                       hdu_ppxf_results[2].data['CONT'][line_index_position]})
                muse_cube_dict.update({'narrow_el_%i_cont_%i_err' % (line, (index + 1 - self.nbursts_n_stellar_comp)):
                                       hdu_ppxf_results[2].data['CONT_ERR'][line_index_position]})
                # equivalent width
                muse_cube_dict.update({'narrow_el_%i_ew_%i' % (line, (index + 1 - self.nbursts_n_stellar_comp)):
                                       hdu_ppxf_results[2].data['EW'][line_index_position]})
                muse_cube_dict.update({'narrow_el_%i_ew_%i_err' % (line, (index + 1 - self.nbursts_n_stellar_comp)):
                                       hdu_ppxf_results[2].data['EW_ERR'][line_index_position]})

        # get broad emission lines
        for index in range(self.nbursts_n_stellar_comp + self.nbursts_n_narrow_lines, self.nbursts_n_stellar_comp + self.nbursts_n_narrow_lines + self.nbursts_n_broad_lines):
            # the fitted lines
            muse_cube_dict.update({'fit_broad_el_%i' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['FIT_COMP'][:, index]})
            # fitting parameters
            muse_cube_dict.update({'vel_broad_el_%i' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['V'][:, index].T[0]})
            muse_cube_dict.update({'vel_broad_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['E_V'][:, index].T[0]})
            muse_cube_dict.update({'sig_broad_el_%i' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['SIG'][:, index].T[0]})
            muse_cube_dict.update({'sig_broad_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['E_SIG'][:, index].T[0]})
            muse_cube_dict.update({'h3_broad_el_%i' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['H3'][:, index].T[0]})
            muse_cube_dict.update({'h3_broad_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['E_H3'][:, index].T[0]})
            muse_cube_dict.update({'h4_broad_el_%i' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['H4'][:, index].T[0]})
            muse_cube_dict.update({'h4_broad_el_%i_err' % (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines):
                                   hdu_ppxf_results[1].data['E_H4'][:, index].T[0]})

            # emission line fluxes
            for line in eml_list:
                # use only balmer transitions
                if self.em_wavelength[line]['transition'] == 'forbidden':
                    continue
                # find line position in the fits file
                line_index_position = np.where((hdu_ppxf_results[2].data['LINE_ID'] ==
                                                self.em_wavelength[line]['nbursts_name']) &
                                               (hdu_ppxf_results[2].data['LOSVD_ID'] ==
                                                (index + 1 - self.nbursts_n_stellar_comp)))
                # save flux
                muse_cube_dict.update({'broad_el_%i_flux_%i' % (line, (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines)):
                                       hdu_ppxf_results[2].data['FLUX'][line_index_position]})
                muse_cube_dict.update({'broad_el_%i_flux_%i_err' % (line, (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines)):
                                       hdu_ppxf_results[2].data['FLUX_ERR'][line_index_position]})
                # continuum level
                muse_cube_dict.update({'broad_el_%i_cont_%i' % (line, (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines)):
                                       hdu_ppxf_results[2].data['CONT'][line_index_position]})
                muse_cube_dict.update({'broad_el_%i_cont_%i_err' % (line, (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines)):
                                       hdu_ppxf_results[2].data['CONT_ERR'][line_index_position]})
                # equivalent width
                muse_cube_dict.update({'broad_el_%i_ew_%i' % (line, (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines)):
                                       hdu_ppxf_results[2].data['EW'][line_index_position]})
                muse_cube_dict.update({'broad_el_%i_ew_%i_err' % (line, (index + 1 - self.nbursts_n_stellar_comp - self.nbursts_n_narrow_lines)):
                                       hdu_ppxf_results[2].data['EW_ERR'][line_index_position]})

        # get further information from nburst binning and fit

        muse_cube_dict.update({'chi2': hdu_ppxf_results[1].data['CHI2']})
        muse_cube_dict.update({'x_pos': hdu_ppxf_results[1].data['XPOS']})
        muse_cube_dict.update({'y_pos': hdu_ppxf_results[1].data['YPOS']})
        muse_cube_dict.update({'flux_sn': hdu_ppxf_results[1].data['FLUXSN']})

        # get wsc
        from astropy.wcs import WCS

        # for name in list(hdu_ppxf_results[0].header.keys()):
        #     print(name, ' ', hdu_ppxf_results[0].header[name])


        wcs_helix_list = WCS(naxis=2)
        wcs_helix_list.wcs.crpix = [1, 1]
        # wcs_helix_list.wcs.crval = [hdu_ppxf_results[0].header['CRVAL1G'], hdu_ppxf_results[0].header['CRVAL2G']]
        wcs_helix_list.wcs.crval = [253.24515793 + 0.0013, 2.40060455 - 0.0006]
        wcs_helix_list.wcs.cunit = [hdu_ppxf_results[0].header['CUNIT1G'], hdu_ppxf_results[0].header['CUNIT2G']]
        wcs_helix_list.wcs.ctype = [hdu_ppxf_results[0].header['CTYPE1G'], hdu_ppxf_results[0].header['CTYPE2G']]
        wcs_helix_list.wcs.cdelt = [hdu_ppxf_results[0].header['CD1_1G'], hdu_ppxf_results[0].header['CD2_2G']]
        wcs_helix_list.array_shape = [self.x_muse_bin_size, self.y_muse_bin_size]

        # print(wcs_helix_list)
        #
        # exit()

        muse_cube_dict.update({'wcs_helix_list': wcs_helix_list})

        self.muse_cube_dict = muse_cube_dict

        # close hdu
        hdu_ppxf_results.close()

        return self.muse_cube_dict

    def get_muse_bin_ra_dec(self, bin_id):
        muse_cube_dict = self.get_muse_cube_dict()
        bin_x_pos = muse_cube_dict['x_pos'][bin_id]
        bin_y_pos = muse_cube_dict['y_pos'][bin_id]

        return muse_cube_dict['wcs_helix_list'].pixel_to_world_values(bin_x_pos, bin_y_pos)

    def get_muse_object_file_path(self):

        file_path = (self.data_path / 'galaxies' / 'muse_cubes' / str(self.project_name))
        # check if filepath exists and create it if necessary
        if not os.path.isdir(file_path):
            os.makedirs(file_path)
        return file_path

    def get_muse_fit_param_folder_name(self):
        r"""
        Function to get folder name of fit results
        """
        # name of fit file
        if (self.muse_mu_evolve is None) & (self.muse_sigma_evolve is None) & (self.muse_amp_evolve is None):
            return ('fit_results_individual_fit_n_gauss_%i_n_broad_%i_fit_mode_%s_two_vel_%i_doublet_ratio_%i' %
                    (self.n_gauss, self.n_broad, self.fit_mode, int(self.two_vel), int(self.doublet_ratio)))
        else:
            return ('fit_results_evolution_fit_n_gauss_%i_n_broad_%i_fit_mode_%s_two_vel_%i_doublet_ratio_%i_'
                    'mu_evolve_%i_sigma_evolve_%i_amp_evolve_%i' %
                    (self.n_gauss, self.n_broad, self.fit_mode, int(self.two_vel), int(self.doublet_ratio),
                     self.muse_mu_evolve, self.muse_sigma_evolve, self.muse_amp_evolve*100))

    def get_muse_sys_vel(self):
        return speed_of_light * 1e-3 * self.muse_redshift

    def get_muse_em_vel_map(self, line):
        muse_cube_dict = self.get_muse_cube_dict()

        if self.em_wavelength[line]['transition'] == 'balmer':
            number_of_fit_components = self.nbursts_n_narrow_lines + self.nbursts_n_broad_lines
        else:
            number_of_fit_components = self.nbursts_n_narrow_lines

        mean_vel_map = np.zeros((number_of_fit_components, len(muse_cube_dict['vel_narrow_el_1'])))
        line_flux_map = np.zeros((number_of_fit_components, len(muse_cube_dict['vel_narrow_el_1'])))

        for index in range(0, self.nbursts_n_narrow_lines):
            mean_vel_map[index] = muse_cube_dict['vel_narrow_el_%i' % (index + 1)]
            line_flux_map[index] = muse_cube_dict['narrow_el_%i_flux_%i' % (line, index + 1)]
        if self.em_wavelength[line]['transition'] == 'balmer':
            for index in range(0, self.nbursts_n_broad_lines):
                mean_vel_map[index + self.nbursts_n_narrow_lines] = muse_cube_dict['vel_broad_el_%i' % (index + 1)]
                line_flux_map[index + self.nbursts_n_narrow_lines] = muse_cube_dict['broad_el_%i_flux_%i' % (line, index + 1)]

        # identify bins where all fluxes sum up to 0
        zero_sums = np.where(np.nansum(line_flux_map, axis=0) == 0)
        line_flux_map[:, zero_sums] = 1

        vel_map = np.average(mean_vel_map, axis=0, weights=line_flux_map)
        vel_map[np.isnan(vel_map)] = self.get_muse_sys_vel()

        return vel_map

    def get_muse_line_pos(self, line):
        rest_line = self.em_wavelength[line]['vac_wave']
        line_vel_map = self.get_muse_em_vel_map(line=line)
        return rest_line * (1 + line_vel_map / (speed_of_light * 1e-3))

    def get_muse_bin_em_mask(self, bin_id, line, blue_limit=30., red_limit=30.):

        muse_cube_dict = self.get_muse_cube_dict()
        wave = muse_cube_dict['wave'][bin_id]
        if line in (6550, 6565, 6585):
            nii_6550_observed_line = self.get_muse_line_pos(6550)[bin_id]
            nii_6585_observed_line = self.get_muse_line_pos(6585)[bin_id]
            return (wave > (nii_6550_observed_line - blue_limit)) & (wave < nii_6585_observed_line + red_limit)
        elif line in (6718, 6733):
            sii_6718_observed_line = self.get_muse_line_pos(6718)[bin_id]
            sii_6733_observed_line = self.get_muse_line_pos(6733)[bin_id]
            return (wave > (sii_6718_observed_line - blue_limit)) & (wave < sii_6733_observed_line + red_limit)
        else:
            obs_line = self.get_muse_line_pos(line)[bin_id]
            return (wave > (obs_line - blue_limit)) & (wave < obs_line + red_limit)

    def get_muse_bin_multiple_em_mask(self, bin_id, line_list, blue_limit=30., red_limit=30.):

        muse_cube_dict = self.get_muse_cube_dict()
        wave = muse_cube_dict['wave'][bin_id]

        multi_em_mask = np.zeros(len(wave), dtype=bool)
        if line_list is None:
            line_list = [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]

        for line in line_list:
            multi_em_mask += self.get_muse_bin_em_mask(bin_id=bin_id, line=line, blue_limit=blue_limit,
                                                       red_limit=red_limit)

        return multi_em_mask

    def get_muse_line_max_value(self, line, bin_id):
        muse_cube_dict = self.get_muse_cube_dict()
        flux = muse_cube_dict['flux'][bin_id]
        mask_line = self.get_muse_bin_em_mask(bin_id=bin_id, line=line)
        return np.nanmax(flux[mask_line])

    def create_muse_fit_parameter_dict(self):

        # load the Manga bin id map
        binid_map = np.array(self.get_muse_bin_map())
        list_bin_ids = np.unique(binid_map)

        if self.n_gauss == 1:

            #map_chi2 = np.zeros(len(list_bin_ids))
            #map_ndf = np.zeros(len(list_bin_ids))

            map_mu_balmer = np.zeros(len(list_bin_ids))
            map_mu_balmer_err = np.zeros(len(list_bin_ids))
            map_mu_forbidden = np.zeros(len(list_bin_ids))
            map_mu_forbidden_err = np.zeros(len(list_bin_ids))

            map_sigma_balmer = np.zeros(len(list_bin_ids))
            map_sigma_balmer_err = np.zeros(len(list_bin_ids))
            map_sigma_forbidden = np.zeros(len(list_bin_ids))
            map_sigma_forbidden_err = np.zeros(len(list_bin_ids))

            # we create an array of higher dimensions for the line amplitudes and fluxes so that we can loop
            # over all lines. The 1st dim denotes the emission line, 2nd amp, amp_err, flux, flux_err amd SNR
            # the 3rd and 4th represents the muse map
            map_emission_lines = np.zeros((len(self.get_full_line_list()), 2, len(list_bin_ids)))

            line_list = self.get_full_line_list()

            for bin_id in list_bin_ids:
                # get coordinates
                bin_index = np.where(list_bin_ids == bin_id)
                # load fit_parameters
                fit_results_folder = self.get_muse_object_file_path() / self.get_muse_fit_param_folder_name()
                fit_results = np.load(str(fit_results_folder / str(int(bin_id))) + '.npy', allow_pickle=True).item()
                self.fit_param_dict = fit_results
                self.get_em_flux()

                # get general data
                #map_chi2[bin_index] = fit_results['chi2']
                #map_ndf[bin_index] = fit_results['ndf']
                map_mu_balmer[bin_index] = fit_results['mu_balmer']
                map_mu_balmer_err[bin_index] = fit_results['mu_balmer_err']
                map_mu_forbidden[bin_index] = fit_results['mu_forbidden']
                map_mu_forbidden_err[bin_index] = fit_results['mu_forbidden_err']
                map_sigma_balmer[bin_index] = fit_results['sigma_balmer']
                map_sigma_balmer_err[bin_index] = fit_results['sigma_balmer_err']
                map_sigma_forbidden[bin_index] = fit_results['sigma_forbidden']
                map_sigma_forbidden_err[bin_index] = fit_results['sigma_forbidden_err']


                # get line _specific data
                for line, line_index in zip(line_list,
                                            range(0, len(line_list))):
                    map_emission_lines[line_index, 0, bin_index] = self.line_flux_dict['flux_%i' % line]
                    map_emission_lines[line_index, 1, bin_index] = self.line_flux_dict['flux_%i_err' % line]

            map_dict = {
                #'chi2': map_chi2,
                #'ndf': map_ndf,
                'mu_balmer': map_mu_balmer,
                'mu_balmer_err': map_mu_balmer_err,
                'mu_forbidden': map_mu_forbidden,
                'mu_forbidden_err': map_mu_forbidden_err,
                'sigma_balmer': map_sigma_balmer,
                'sigma_balmer_err': map_sigma_balmer_err,
                'sigma_forbidden': map_sigma_forbidden,
                'sigma_forbidden_err': map_sigma_forbidden_err
            }

            for line, line_index in zip(line_list,
                                        range(0, len(line_list))):
                map_dict.update({'flux_%i' % line: map_emission_lines[line_index, 0]})
                map_dict.update({'flux_%i_err' % line: map_emission_lines[line_index, 1]})

        elif self.n_gauss == 2:

            #map_chi2 = np.zeros(len(list_bin_ids))
            #map_ndf = np.zeros(len(list_bin_ids))

            map_mu_balmer_1 = np.zeros(len(list_bin_ids))
            map_mu_balmer_1_err = np.zeros(len(list_bin_ids))
            map_mu_balmer_2 = np.zeros(len(list_bin_ids))
            map_mu_balmer_2_err = np.zeros(len(list_bin_ids))
            map_mu_forbidden_1 = np.zeros(len(list_bin_ids))
            map_mu_forbidden_1_err = np.zeros(len(list_bin_ids))
            map_mu_forbidden_2 = np.zeros(len(list_bin_ids))
            map_mu_forbidden_2_err = np.zeros(len(list_bin_ids))

            map_sigma_balmer_1 = np.zeros(len(list_bin_ids))
            map_sigma_balmer_1_err = np.zeros(len(list_bin_ids))
            map_sigma_balmer_2 = np.zeros(len(list_bin_ids))
            map_sigma_balmer_2_err = np.zeros(len(list_bin_ids))
            map_sigma_forbidden_1 = np.zeros(len(list_bin_ids))
            map_sigma_forbidden_1_err = np.zeros(len(list_bin_ids))
            map_sigma_forbidden_2 = np.zeros(len(list_bin_ids))
            map_sigma_forbidden_2_err = np.zeros(len(list_bin_ids))

            map_delta_v_balmer = np.zeros(len(list_bin_ids))
            map_delta_v_balmer_err = np.zeros(len(list_bin_ids))
            map_delta_v_forbidden = np.zeros(len(list_bin_ids))
            map_delta_v_forbidden_err = np.zeros(len(list_bin_ids))

            # we create an array of higher dimensions for the line amplitudes and fluxes so that we can loop
            # over all lines. The 1st dim denotes the emission line, 2nd amp_i, amp_err_i
            # the 3rd and 4th represent the MaNGA map dimensions
            map_emission_lines = np.zeros((len(self.get_full_line_list()), 5, len(list_bin_ids)))

            line_list = self.get_full_line_list()

            for bin_id in list_bin_ids:
                # get coordinates
                bin_index = np.where(list_bin_ids == bin_id)

                # load fit_parameters
                fit_results_folder = self.get_muse_object_file_path() / self.get_muse_fit_param_folder_name()
                fit_results = np.load(str(fit_results_folder / str(int(bin_id))) + '.npy', allow_pickle=True).item()
                self.fit_param_dict = fit_results
                self.get_em_flux()

                # get general data
                map_mu_balmer_1[bin_index] = fit_results['mu_balmer_1']
                map_mu_balmer_1_err[bin_index] = fit_results['mu_balmer_1_err']
                map_mu_balmer_2[bin_index] = fit_results['mu_balmer_2']
                map_mu_balmer_2_err[bin_index] = fit_results['mu_balmer_2_err']
                map_mu_forbidden_1[bin_index] = fit_results['mu_forbidden_1']
                map_mu_forbidden_1_err[bin_index] = fit_results['mu_forbidden_1_err']
                map_mu_forbidden_2[bin_index] = fit_results['mu_forbidden_2']
                map_mu_forbidden_2_err[bin_index] = fit_results['mu_forbidden_2_err']

                map_sigma_balmer_1[bin_index] = fit_results['sigma_balmer_1']
                map_sigma_balmer_1_err[bin_index] = fit_results['sigma_balmer_1_err']
                map_sigma_balmer_2[bin_index] = fit_results['sigma_balmer_2']
                map_sigma_balmer_2_err[bin_index] = fit_results['sigma_balmer_2_err']
                map_sigma_forbidden_1[bin_index] = fit_results['sigma_forbidden_1']
                map_sigma_forbidden_1_err[bin_index] = fit_results['sigma_forbidden_1_err']
                map_sigma_forbidden_2[bin_index] = fit_results['sigma_forbidden_1']
                map_sigma_forbidden_2_err[bin_index] = fit_results['sigma_forbidden_2_err']

                map_delta_v_balmer[bin_index] = map_mu_balmer_2[bin_index] - map_mu_balmer_1[bin_index]
                map_delta_v_balmer_err[bin_index] = np.sqrt(map_mu_balmer_1_err[bin_index] ** 2 + map_mu_balmer_2_err[bin_index] ** 2)
                map_delta_v_forbidden[bin_index] = map_mu_forbidden_2[bin_index] - map_mu_forbidden_1[bin_index]
                map_delta_v_forbidden_err[bin_index] = np.sqrt(map_mu_forbidden_1_err[bin_index] ** 2 +
                                                           map_mu_forbidden_2_err[bin_index] ** 2)

                # get line _specific data
                for line, line_index in zip(line_list, range(0, len(line_list))):
                    map_emission_lines[line_index, 0, bin_index] = self.line_flux_dict['flux_%i_1' % line]
                    map_emission_lines[line_index, 1, bin_index] = self.line_flux_dict['flux_%i_1_err' % line]
                    map_emission_lines[line_index, 2, bin_index] = self.line_flux_dict['flux_%i_2' % line]
                    map_emission_lines[line_index, 3, bin_index] = self.line_flux_dict['flux_%i_2_err' % line]

            map_dict = {
                'mu_balmer_1': map_mu_balmer_1,
                'mu_balmer_1_err': map_mu_balmer_1_err,
                'mu_balmer_2': map_mu_balmer_2,
                'mu_balmer_2_err': map_mu_balmer_2_err,
                'mu_forbidden_1': map_mu_forbidden_1,
                'mu_forbidden_1_err': map_mu_forbidden_1_err,
                'mu_forbidden_2': map_mu_forbidden_2,
                'mu_forbidden_2_err': map_mu_forbidden_2_err,
                'sigma_balmer_1': map_sigma_balmer_1,
                'sigma_balmer_1_err': map_sigma_balmer_1_err,
                'sigma_balmer_2': map_sigma_balmer_2,
                'sigma_balmer_2_err': map_sigma_balmer_2_err,
                'sigma_forbidden_1': map_sigma_forbidden_1,
                'sigma_forbidden_1_err': map_sigma_forbidden_1_err,
                'sigma_forbidden_2': map_sigma_forbidden_2,
                'sigma_forbidden_2_err': map_sigma_forbidden_2_err,
                'delta_v_balmer': map_delta_v_balmer,
                'delta_v_balmer_err': map_delta_v_balmer_err,
                'delta_v_forbidden': map_delta_v_forbidden,
                'delta_v_forbidden_err': map_delta_v_forbidden_err}

            for line, line_index in zip(line_list,
                                        range(0, len(line_list))):
                map_dict.update({'flux_%i_1' % line: map_emission_lines[line_index, 0]})
                map_dict.update({'flux_%i_1_err' % line: map_emission_lines[line_index, 1]})
                map_dict.update({'flux_%i_2' % line: map_emission_lines[line_index, 2]})
                map_dict.update({'flux_%i_2_err' % line: map_emission_lines[line_index, 3]})

        else:
            raise KeyError('n_gauss attribute should be 1 or 2')

        return map_dict
