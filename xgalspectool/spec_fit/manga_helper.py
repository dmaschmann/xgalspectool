# -*- coding: utf-8 -*-

import numpy as np
import os
from pathlib import Path

from scipy.constants import c as speed_of_light

from xgalspectool.fit_algorithm import em_fit_algorithm
from xgaltool.basic_attributes import download_file

from marvin.tools.maps import Maps
from marvin.tools.modelcube import ModelCube

import astropy.io.fits as fits
class MaNGASpecHelper(em_fit_algorithm.EmissionLineFit):

    def __init__(self, manga_plate=None, manga_ifudsgn=None, data_path=None, **kwargs):
        """
        Class to access SDSS emission line spectra. We use processed files from the RCSED catalog.
        Parameters
        ---------
        manga_plate: int
          manga_plate as MaNGA SDSS spectral observation identifier.
        manga_ifudsgn: int
          manga_ifudsgn as MaNGA SDSS spectral observation identifier.
        data_path : str
          data path to RCSED .fits files / folder structure
        """
        super().__init__(**kwargs)
        # check if data_path is given and correct
        if data_path is None:  # use short cut
            data_path = Path.home() / 'data'
        if os.path.isdir(Path(data_path)):
            self.data_path = Path(data_path)
        else:
            raise OSError('The data path %s is not existent' % data_path)

        if (manga_plate is not None) & (manga_ifudsgn is not None):
            self.manga_plate = int(manga_plate)
            self.manga_ifudsgn = int(manga_ifudsgn)
        else:
            self.manga_plate = None
            self.manga_ifudsgn = None

        self.manga_cube = None
        self.manga_map = None
        self.pipe3d_cube = None

        # parameter variation for parameters
        self.mu_evolve = None
        self.sigma_evolve = None
        self.amp_evolve = None

    def check_manga_identifier(self):
        """ Function to check weather the RCSED identifier are set"""
        if (self.manga_plate is None) & (self.manga_ifudsgn is None):
            raise KeyError('To access MaNGA/SDSS cubes you need to specify the concrete identifier namely '
                           'self.manga_plate and self.manga_ifudsgn')
        else:
            return True

    def get_manga_object_file_path(self):
            """ function to get file_path of sdss spectral objects """
            # check if identifier is correct
            self.check_manga_identifier()
            # merge filepath
            file_path = (self.data_path / 'galaxies' / 'manga_spectra' /
                        str(self.manga_plate) / str(self.manga_ifudsgn))
            # check if filepath exists and create it if necessary
            if not os.path.isdir(file_path):
                os.makedirs(file_path)
            return file_path

    def get_manga_cube_file_name(self, cube_format='voronoy_logcube', unpack=True):
        r"""
        compose cube name see: https://www.sdss.org/dr16/manga/manga-data/data-access/

        Parameters
        ---------
        cube_format: str
          Kew word to specify which cube format to access. "reduced" for the reduced data cube,
          "hybrid_logcube" for derived quantities with a hybrid and  "voronoy_logcube" for derived quantities with a
          Voronoy binning model
        unpack: bool (default = True)
          specify tu get .fits or .fits.gz ending
        """
        if cube_format == 'reduced':
            # DRP 3D Output Files (Reduced spectra)
            cube_file_name = 'manga-%i-%i-LOGCUBE' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'hybrid_logcube':
            # hybrid derived quantities
            # DAP Output Files (Derived Quantities) with an hybrid model
            cube_file_name = 'manga-%i-%i-LOGCUBE-HYB10-GAU-MILESHC' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'voronoy_logcube':
            # pure Voronoy binned spaxels providing derived quantities
            # DAP Output Files (Derived Quantities) for pure voronoy binned model
            cube_file_name = 'manga-%i-%i-LOGCUBE-VOR10-GAU-MILESHC' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'voronoi_logcube_dr17':
            cube_file_name = 'manga-%i-%i-LOGCUBE-VOR10-MILESHC-MASTARSSP' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'hybrid_logcube_dr17':
            cube_file_name = 'manga-%i-%i-LOGCUBE-HYB10-MILESHC-MASTARSSP' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'pipe3d_cube' or 'pipe3d_cube_dr17':
            cube_file_name = 'manga-%i-%i.Pipe3D.cube' % (self.manga_plate, self.manga_ifudsgn)
        else:
            raise KeyError('cube format not understand')
        if unpack:
            cube_file_name = Path(cube_file_name).with_suffix(Path(cube_file_name).suffix + '.fits')
        else:
            cube_file_name = Path(cube_file_name).with_suffix(Path(cube_file_name).suffix + '.fits.gz')
        return cube_file_name

    def get_manga_map_file_name(self, map_format='voronoy_maps', unpack=True):
        r"""
        compose Map name see: https://www.sdss.org/dr16/manga/manga-data/data-access/

        Parameters
        ---------
        map_format: str
          Kew word to specify which map fomat to access. "hybrid_maps" for derived quantities with a hybrid and
          "voronoy_maps" for derived quantities with a Voronoy binning model
        unpack: bool (default = True)
          specify tu get .fits or .fits.gz ending
        """
        if map_format == 'hybrid_maps': # DR15
            # hybrid derived quantities
            # DAP Output Files (Derived Quantities) with an hybrid model
            map_file_name = 'manga-%i-%i-MAPS-HYB10-GAU-MILESHC' % (self.manga_plate, self.manga_ifudsgn)
        elif map_format == 'voronoy_maps':
            # pure Voronoy binned spaxels providing derived quantities
            # DAP Output Files (Derived Quantities) for pure voronoy binned model
            map_file_name = 'manga-%i-%i-MAPS-VOR10-GAU-MILESHC' % (self.manga_plate, self.manga_ifudsgn)
        elif map_format == 'voronoi_maps_dr17':
            map_file_name = 'manga-%i-%i-MAPS-VOR10-MILESHC-MASTARSSP' % (self.manga_plate, self.manga_ifudsgn)
        elif map_format == 'hybrid_maps_dr17':
            map_file_name = 'manga-%i-%i-MAPS-HYB10-MILESHC-MASTARSSP' % (self.manga_plate, self.manga_ifudsgn)
        else:
            raise KeyError('cube format not understand')
        if unpack:
            map_file_name = Path(map_file_name).with_suffix('.fits')
        else:
            map_file_name = Path(map_file_name).with_suffix('.fits.gz')
        return map_file_name

    def get_manga_cube_url(self, cube_format='voronoy_logcube'):

        url = 'https://data.sdss.org/sas/dr16/manga/spectro/'
        if cube_format == 'reduced':
            # DRP 3D Output Files (Reduced spectra)
            url += 'redux/v2_4_3/%i/stack/' % self.manga_plate
        elif cube_format == 'hybrid_logcube':
            # hybrid derived quantities
            # DAP Output Files (Derived Quantities) with an hybrid model
            url += 'analysis/v2_4_3/2.2.1/HYB10-GAU-MILESHC/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'voronoy_logcube':
            # pure Voronoi-binned spaxels providing derived quantities
            # DAP Output Files (Derived Quantities) for pure voronoy binned model
            url += 'analysis/v2_4_3/2.2.1/VOR10-GAU-MILESHC/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'voronoi_logcube_dr17':
            url = 'https://data.sdss.org/sas/dr17/manga/spectro/'
            url += 'analysis/v3_1_1/3.1.0/VOR10-MILESHC-MASTARSSP/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'hybrid_logcube_dr17':
            url = 'https://data.sdss.org/sas/dr17/manga/spectro/'
            url += 'analysis/v3_1_1/3.1.0/HYB10-MILESHC-MASTARSSP/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif cube_format == 'pipe3d_cube':
            url+= 'pipe3d/v2_4_3/2.4.3/%i/' % (self.manga_plate)
        elif cube_format == 'pipe3d_cube_dr17':
            url = 'https://data.sdss.org/sas/dr17/manga/spectro/'
            url+= 'pipe3d/v3_1_1/3.1.1/%i/' % (self.manga_plate)
        else:
            raise KeyError('cube format not understand')
        url += str(self.get_manga_cube_file_name(cube_format=cube_format, unpack=False))
        return url

    def get_manga_map_url(self, map_format='voronoy_maps'):

        url = 'https://data.sdss.org/sas/dr16/manga/spectro/'
        if map_format == 'hybrid_maps':
            # hybrid derived quantities
            # DAP Output Files (Derived Quantities) with an hybrid model
            url += 'analysis/v2_4_3/2.2.1/HYB10-GAU-MILESHC/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif map_format == 'voronoy_maps':
            # pure Voronoy binned spaxels providing derived quantities
            # DAP Output Files (Derived Quantities) for pure voronoy binned model
            url += 'analysis/v2_4_3/2.2.1/VOR10-GAU-MILESHC/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif map_format == 'voronoi_maps_dr17':
            url = 'https://data.sdss.org/sas/dr17/manga/spectro/'
            url += 'analysis/v3_1_1/3.1.0/VOR10-MILESHC-MASTARSSP/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        elif map_format == 'hybrid_maps_dr17':
            url = 'https://data.sdss.org/sas/dr17/manga/spectro/'
            url += 'analysis/v3_1_1/3.1.0/HYB10-MILESHC-MASTARSSP/%i/%i/' % (self.manga_plate, self.manga_ifudsgn)
        else:
            raise KeyError('map format not understand')
        url += str(self.get_manga_map_file_name(map_format=map_format, unpack=False))
        return url

    def download_manga_cube(self, cube_format='voronoy_logcube'):

        cube_file_path = self.get_manga_object_file_path()
        # check if path already exists
        if os.path.isfile(cube_file_path / self.get_manga_cube_file_name(cube_format=cube_format, unpack=False)):
            return None

        if not os.path.isdir(cube_file_path):
            os.makedirs(cube_file_path)

        cube_file_name = self.get_manga_cube_file_name(cube_format=cube_format, unpack=False)
        cube_url = self.get_manga_cube_url(cube_format=cube_format)
        download_file(file_path=cube_file_path, file_name=cube_file_name, url=cube_url, unpack=False)

    def download_manga_map(self, map_format='voronoy_maps'):

        map_file_path = self.get_manga_object_file_path()
        # check if path already exists
        if os.path.isfile(map_file_path / self.get_manga_map_file_name(map_format=map_format, unpack=True)):
            return None

        if not os.path.isdir(map_file_path):
            os.makedirs(map_file_path)

        map_file_name = self.get_manga_map_file_name(map_format=map_format, unpack=True)
        map_url = self.get_manga_map_url(map_format=map_format)
        download_file(file_path=map_file_path, file_name=map_file_name, url=map_url, unpack=True)

    def load_manga_cube(self, cube_format='voronoy_logcube', nsa_source='drpall'):

        # self.manga_maps = Maps(filename=maps_file_path, drpall=drpall_file_path)
        # self.manga_cube = ModelCube(filename=cube_file_path, drpall=drpall_file_path)
        self.download_manga_cube(cube_format=cube_format)

        file_path = self.get_manga_object_file_path()
        cube_file_name = self.get_manga_cube_file_name(cube_format=cube_format, unpack=False)

        if cube_format == 'voronoy_logcube' or 'hybrid_logcube' or 'voronoi_logcube_dr17' or 'hybrid_logcube_dr17':
            self.manga_cube = ModelCube(filename=str(file_path / cube_file_name))
        if cube_format=='pipe3d_cube' or 'pipe3d_cube_dr17':
            self.pipe3d_cube = fits.open(str(file_path / cube_file_name))

    def load_manga_map(self, map_format='voronoy_maps', nsa_source='drpall'):

        self.download_manga_map(map_format=map_format)

        file_path = self.get_manga_object_file_path()
        map_file_name = self.get_manga_map_file_name(map_format=map_format, unpack=True)

        self.manga_map = Maps(filename=str(file_path / map_file_name))

    def get_manga_fit_param_folder_name(self):
        r"""
        Function to get folder name of fit results
        """
        # name of fit file
        if (self.mu_evolve is None) & (self.sigma_evolve is None) & (self.amp_evolve is None):
            return ('fit_results_individual_fit_n_gauss_%i_fit_mode_%s_two_vel_%i_doublet_ratio_%i' %
                    (self.n_gauss, self.fit_mode, int(self.two_vel), int(self.doublet_ratio)))
        else:
            return ('fit_results_evolution_fit_n_gauss_%i_fit_mode_%s_two_vel_%i_doublet_ratio_%i_'
                    'mu_evolve_%i_sigma_evolve_%i_amp_evolve_%i' %
                    (self.n_gauss, self.fit_mode, int(self.two_vel), int(self.doublet_ratio),
                     self.mu_evolve, self.sigma_evolve, self.amp_evolve*100))

    def get_manga_redshift(self):
        return self.manga_map.nsa['z']

    def get_manga_iau_name(self):
        return self.manga_map.nsa['iauname']

    def get_manga_central_spax(self):
        return self.manga_cube.getSpaxel(x=0, y=0).x, self.manga_cube.getSpaxel(x=0, y=0).y

    @staticmethod
    def calculate_spax_center(i_list, j_list):
        # find the centre of a Voronoy bin
        if len(i_list) == 1:
            return i_list, j_list
        else:
            return sum(i_list) / len(i_list), sum(j_list) / len(j_list)

    @staticmethod
    def calculate_bin_distance(i_0, j_0, i, j):
        # calculate the distance of each bin
        return np.sqrt((i_0 - i) ** 2 + (j_0 - j) ** 2)

    def find_closest_already_fitted_spaxel(self, binid_map, i_to_fit, j_to_fit, already_fitted_binids):
        local_distance_matrix = np.zeros(binid_map.shape)
        for index in already_fitted_binids:
            # print('index', index)
            i_fitted, j_fitted = np.where(binid_map == index)
            # print('i_fitted, j_fitted =', i_fitted, j_fitted)
            local_distance_matrix[i_fitted, j_fitted] = self.calculate_bin_distance(i_to_fit, j_to_fit,
                                                                                    i_fitted, j_fitted)
            # print(local_distance_matrix[i_fitted, j_fitted])

        local_distance_matrix[local_distance_matrix == 0] = 999999
        # print(np.min(local_distance_matrix))
        # print(np.where(local_distance_matrix == np.min(local_distance_matrix)))
        i_closest_list, j_closest_list = np.where(local_distance_matrix == np.min(local_distance_matrix))
        distance_to_closest = np.sqrt((i_closest_list[0] - i_to_fit) ** 2 + (j_closest_list[0] - j_to_fit) ** 2)
        return i_closest_list, j_closest_list, distance_to_closest

    def get_manga_spax_ra_dec(self, i, j):
        # get relative i and j
        x_centre, y_centre = self.get_manga_central_spax()
        return (self.manga_cube.getSpaxel(y=i - x_centre, x=j - y_centre).ra,
                self.manga_cube.getSpaxel(y=i - x_centre, x=j - y_centre).dec)

    def get_manga_sys_vel(self):
        return self.get_manga_redshift() * speed_of_light * 1e-3

    def get_manga_eff_radius(self):
        return self.manga_map.header['reff']

    def get_manga_bin_id_map(self):
        return self.manga_map.get_binid()

    def get_manga_spax_wave(self, i, j):
        return self.manga_cube[i, j].full_fit.wavelength.value

    def get_manga_spax_flux(self, i, j):
        return self.manga_cube[i, j].binned_flux

    def get_manga_spax_flux_ivar(self, i, j):
        return self.get_manga_spax_flux(i, j).ivar

    def get_manga_spax_flux_err(self, i, j):
        return 1 / np.sqrt(self.get_manga_spax_flux_ivar(i, j))

    def get_manga_spax_dap_full_fit(self, i, j):
        return self.manga_cube[i, j].full_fit.value

    def get_manga_spax_dap_em_gauss_fit(self, i, j):
        return self.manga_cube[i, j].emline_fit.value

    def get_manga_spax_continuum_fit(self, i, j):
        return self.get_manga_spax_dap_full_fit(i, j) - self.get_manga_spax_dap_em_gauss_fit(i, j)

    def get_manga_spax_em_spec(self, i, j):
        return np.array(self.get_manga_spax_flux(i, j).value) - self.get_manga_spax_continuum_fit(i, j)

    def get_corrected_manga_emission_line_spectrum(self, i, j):
        wavelength = self.manga_cube[i, j].full_fit.wavelength
        flux = np.array(self.get_manga_spax_flux(i, j))
        continuum = self.get_manga_spax_continuum_fit(i, j)

        from scipy.interpolate import interp1d
        extrapolated_continuum = interp1d(wavelength, continuum, kind='quadratic')
        fixed_continuum = extrapolated_continuum(wavelength)

        return flux - fixed_continuum

    def get_manga_dap_em_flux_map(self, line, fit_type='gauss'):
        if fit_type == 'gauss':
            return self.manga_map.__getattr__('emline_%sflux_%s' %
                                              ('g', self.em_wavelength[line]['manga_name'])).value
        elif fit_type == 'nonpar':
            return self.manga_map.__getattr__('emline_%sflux_%s' %
                                              ('s', self.em_wavelength[line]['manga_name'])).value
        else:
            raise ValueError('fit_type not understand it must be gauss or nonpar')

    def get_manga_em_snr_map(self, line, fit_type='gauss'):
        return self.get_manga_dap_em_flux_map(line=line, fit_type=fit_type).snr

    def get_manga_dap_em_gauss_vel_map(self, line):
        return self.manga_map.__getattr__('emline_gvel_%s' % (self.em_wavelength[line]['manga_name'])).value

    def get_manga_dap_em_gauss_sigma_map(self, line):
        return self.manga_map.__getattr__('emline_gsigma_%s' % (self.em_wavelength[line]['manga_name'])).value

    def get_manga_inst_broad_map(self, line):
        # of lsf of the model cube ?
        return self.manga_map.__getattr__('emline_instsigma_%s' % (self.em_wavelength[line]['manga_name'])).value

    def get_manga_dap_sys_vel_map(self, line):
        return self.get_manga_sys_vel() + self.get_manga_dap_em_gauss_vel_map(line=line)

    def get_manga_dap_line_pos(self, line):
        rest_line = self.em_wavelength[line]['vac_wave']
        line_vel_map = self.get_manga_dap_sys_vel_map(line=line)
        return rest_line * (1 + line_vel_map / (speed_of_light * 1e-3))

    def get_manga_spax_dap_em_mask(self, i, j, line, blue_limit=30., red_limit=30.):
        if line in (6550, 6565, 6585):
            nii_6550_observed_line = self.get_manga_dap_line_pos(6550)[i, j]
            nii_6585_observed_line = self.get_manga_dap_line_pos(6585)[i, j]
            return (self.get_manga_spax_wave(i=i, j=j) > (nii_6550_observed_line - blue_limit)) & \
                   (self.get_manga_spax_wave(i=i, j=j) < nii_6585_observed_line + red_limit)
        elif line in (6718, 6733):
            sii_6718_observed_line = self.get_manga_dap_line_pos(6718)[i, j]
            sii_6733_observed_line = self.get_manga_dap_line_pos(6733)[i, j]
            return (self.get_manga_spax_wave(i=i, j=j) > (sii_6718_observed_line - blue_limit)) & \
                   (self.get_manga_spax_wave(i=i, j=j) < sii_6733_observed_line + red_limit)
        else:
            obs_line = self.get_manga_dap_line_pos(line)[i, j]
            return (self.get_manga_spax_wave(i=i, j=j) > (obs_line - blue_limit)) & \
                   (self.get_manga_spax_wave(i=i, j=j) < obs_line + red_limit)

    def get_manga_spax_dap_multiple_em_mask(self, i, j, line_list, blue_limit=30., red_limit=30.):

        multi_em_mask = np.zeros(len(self.get_manga_spax_wave(i=i, j=j)), dtype=bool)
        if line_list is None:
            line_list = [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]

        for line in line_list:
            multi_em_mask += self.get_manga_spax_dap_em_mask(i, j, line=line, blue_limit=blue_limit,
                                                             red_limit=red_limit)

        return multi_em_mask

    def get_manga_dap_gauss_obs_sigma_map(self, line):
        line_broadening = self.get_manga_inst_broad_map(line=line)
        gauss_sigma_map = self.get_manga_dap_em_gauss_sigma_map(line=line)

        line_sigma_angstrom = (np.sqrt(gauss_sigma_map ** 2 + line_broadening ** 2) /
                               (speed_of_light * 1e-3) * self.em_wavelength[line]['vac_wave'])
        return line_sigma_angstrom

    def get_manga_dap_gauss_amp_map(self, line):

        line_flux = self.get_manga_dap_em_flux_map(line=line, fit_type='gauss')
        obs_sigma_map = self.get_manga_dap_gauss_obs_sigma_map(line=line)
        return line_flux / (np.sqrt(2 * np.pi) * obs_sigma_map)

    def get_manga_fit_parameter_file_path(self, bin_id=None, index=0):
        r"""
        Function to get file path of fit parameters of different bins (fitted with iminuit)
        """
        if bin_id is not None:
            # path to the fit files
            fit_file_path = self.data_path / 'galaxies' / 'manga_spectra' / str(int(self.manga_plate)) / str(int(self.manga_ifudsgn)) / Path('fit_results_evolution_fit_n_gauss_%i_fit_mode_%s_two_vel_%i_doublet_ratio_%i_mu_evolve_%i_sigma_evolve_%i_amp_evolve_%i'%(self.n_gauss, self.fit_mode,
                             self.two_vel, self.doublet_ratio, self.mu_evolve, self.sigma_evolve, self.amp_evolve*100)) 

            file_name = Path(str(int(bin_id)) ).with_suffix('.npy')

            return fit_file_path, file_name
        else:
            # path to the fit files
            fit_file_path = (self.data_path / 'galaxies' / 'manga_spectra' /    
                             str(int(self.manga_plate)) / str(int(self.manga_ifudsgn)) /
                             'maps')

            file_name = Path('fit_results_evolution_fit_n_gauss_%i_fit_mode_%s_two_vel_%i_doublet_ratio_%i_mu_evolve_%i_sigma_evolve_%i_amp_evolve_%i' %
                                 (self.n_gauss,
                                  self.fit_mode, self.two_vel,
                                  self.doublet_ratio, self.mu_evolve,
                                  self.sigma_evolve, self.amp_evolve*100)).with_suffix('.npy')

            return fit_file_path, file_name

    def create_manga_fit_parameter_dict(self):

        # load the Manga bin id map
        binid_map = np.array(self.get_manga_bin_id_map())
        list_bin_ids = np.unique(binid_map)
        # get map of nan values
        nan_map = binid_map == -1

        # remove -1 values
        list_bin_ids = list_bin_ids[list_bin_ids != -1]

        if self.n_gauss == 1:

            map_chi2 = np.zeros(binid_map.shape)
            map_ndf = np.zeros(binid_map.shape)

            map_mu_balmer = np.zeros(binid_map.shape)
            map_mu_balmer_err = np.zeros(binid_map.shape)
            map_mu_forbidden = np.zeros(binid_map.shape)
            map_mu_forbidden_err = np.zeros(binid_map.shape)

            map_sigma_balmer = np.zeros(binid_map.shape)
            map_sigma_balmer_err = np.zeros(binid_map.shape)
            map_sigma_forbidden = np.zeros(binid_map.shape)
            map_sigma_forbidden_err = np.zeros(binid_map.shape)

            # we create an array of higher dimensions for the line amplitudes and fluxes so that we can loop
            # over all lines. The 1st dim denotes the emission line, 2nd amp, amp_err, flux, flux_err amd SNR
            # the 3rd and 4th represents the manga map
            map_emission_lines = np.zeros((len(self.get_full_line_list()), 2,
                                           binid_map.shape[0], binid_map.shape[1]))

            # set values to nan where we do not have a spaxel:
            map_chi2[nan_map] = np.nan
            map_ndf[nan_map] = np.nan
            map_mu_balmer[nan_map] = np.nan
            map_mu_balmer_err[nan_map] = np.nan
            map_mu_forbidden[nan_map] = np.nan
            map_mu_forbidden_err[nan_map] = np.nan
            map_sigma_balmer[nan_map] = np.nan
            map_sigma_balmer_err[nan_map] = np.nan
            map_sigma_forbidden[nan_map] = np.nan
            map_sigma_forbidden_err[nan_map] = np.nan
            map_emission_lines[:, :, nan_map] = np.nan

            line_list = self.get_full_line_list()

            for bin_id in list_bin_ids:
                # get coordinates
                i, j = np.where(binid_map == bin_id)

                # load fit_parameters
                fit_file_path, file_name = self.get_manga_fit_parameter_file_path(bin_id=bin_id)

                fit_results = np.load(fit_file_path / file_name, allow_pickle=True).item()

                print(fit_results.keys())

                # get general data
                map_chi2[i, j] = fit_results['chi2_complex']
                map_ndf[i, j] = fit_results['ndf_complex']
                map_mu_balmer[i, j] = fit_results['mu_balmer']
                map_mu_balmer_err[i, j] = fit_results['mu_balmer_err']
                map_mu_forbidden[i, j] = fit_results['mu_forbidden']
                map_mu_forbidden_err[i, j] = fit_results['mu_forbidden_err']
                map_sigma_balmer[i, j] = fit_results['sigma_balmer']
                map_sigma_balmer_err[i, j] = fit_results['sigma_balmer_err']
                map_sigma_forbidden[i, j] = fit_results['sigma_forbidden']
                map_sigma_forbidden_err[i, j] = fit_results['sigma_forbidden_err']


                # get line _specific data
                for line, line_index in zip(line_list,
                                            range(0, len(line_list))):
                    map_emission_lines[line_index, 0, i, j] = fit_results['amp_%i' % line]
                    map_emission_lines[line_index, 1, i, j] = fit_results['amp_%i_err' % line]

            map_dict = {
                'chi2': map_chi2,
                'ndf': map_ndf,
                'mu_balmer': map_mu_balmer,
                'mu_balmer_err': map_mu_balmer_err,
                'mu_forbidden': map_mu_forbidden,
                'mu_forbidden_err': map_mu_forbidden_err,
                'sigma_balmer': map_sigma_balmer,
                'sigma_balmer_err': map_sigma_balmer_err,
                'sigma_forbidden': map_sigma_forbidden,
                'sigma_forbidden_err': map_sigma_forbidden_err
            }

            for line, line_index in zip(line_list,
                                        range(0, len(line_list))):
                map_dict.update({'amp_%i' % line: map_emission_lines[line_index, 0]})
                map_dict.update({'amp_%i_err' % line: map_emission_lines[line_index, 1]})

        elif self.n_gauss == 2:

            map_chi2 = np.zeros(binid_map.shape)
            map_ndf = np.zeros(binid_map.shape)

            map_mu_balmer_1 = np.zeros(binid_map.shape)
            map_mu_balmer_1_err = np.zeros(binid_map.shape)
            map_mu_balmer_2 = np.zeros(binid_map.shape)
            map_mu_balmer_2_err = np.zeros(binid_map.shape)
            map_mu_forbidden_1 = np.zeros(binid_map.shape)
            map_mu_forbidden_1_err = np.zeros(binid_map.shape)
            map_mu_forbidden_2 = np.zeros(binid_map.shape)
            map_mu_forbidden_2_err = np.zeros(binid_map.shape)

            map_sigma_balmer_1 = np.zeros(binid_map.shape)
            map_sigma_balmer_1_err = np.zeros(binid_map.shape)
            map_sigma_balmer_2 = np.zeros(binid_map.shape)
            map_sigma_balmer_2_err = np.zeros(binid_map.shape)
            map_sigma_forbidden_1 = np.zeros(binid_map.shape)
            map_sigma_forbidden_1_err = np.zeros(binid_map.shape)
            map_sigma_forbidden_2 = np.zeros(binid_map.shape)
            map_sigma_forbidden_2_err = np.zeros(binid_map.shape)

            map_delta_v_balmer = np.zeros(binid_map.shape)
            map_delta_v_balmer_err = np.zeros(binid_map.shape)
            map_delta_v_forbidden = np.zeros(binid_map.shape)
            map_delta_v_forbidden_err = np.zeros(binid_map.shape)

            # we create an array of higher dimensions for the line amplitudes and fluxes so that we can loop
            # over all lines. The 1st dim denotes the emission line, 2nd amp_i, amp_err_i
            # the 3rd and 4th represent the MaNGA map dimensions
            map_emission_lines = np.zeros((len(self.get_full_line_list()), 5,
                                           binid_map.shape[0], binid_map.shape[1]))

            # set values to nan where we do not have a spaxel with coverage:
            map_chi2[nan_map] = np.nan
            map_ndf[nan_map] = np.nan
            map_mu_balmer_1[nan_map] = np.nan
            map_mu_balmer_1_err[nan_map] = np.nan
            map_mu_balmer_2[nan_map] = np.nan
            map_mu_balmer_2_err[nan_map] = np.nan
            map_mu_forbidden_1[nan_map] = np.nan
            map_mu_forbidden_1_err[nan_map] = np.nan
            map_mu_forbidden_2[nan_map] = np.nan
            map_mu_forbidden_2_err[nan_map] = np.nan
            map_sigma_balmer_1[nan_map] = np.nan
            map_sigma_balmer_1_err[nan_map] = np.nan
            map_sigma_balmer_2[nan_map] = np.nan
            map_sigma_balmer_2_err[nan_map] = np.nan
            map_sigma_forbidden_1[nan_map] = np.nan
            map_sigma_forbidden_1_err[nan_map] = np.nan
            map_sigma_forbidden_2[nan_map] = np.nan
            map_sigma_forbidden_2_err[nan_map] = np.nan

            map_delta_v_balmer[nan_map] = np.nan
            map_delta_v_balmer_err[nan_map] = np.nan
            map_delta_v_forbidden[nan_map] = np.nan
            map_delta_v_forbidden_err[nan_map] = np.nan

            map_emission_lines[:, :, nan_map] = np.nan

            line_list = self.get_full_line_list()

            for bin_id in list_bin_ids:
                # get coordinates
                i, j = np.where(binid_map == bin_id)

                # load fit_parameters
                fit_file_path, file_name = self.get_manga_fit_parameter_file_path(bin_id=bin_id)

                fit_results = np.load(fit_file_path / file_name, allow_pickle=True).item()

                map_chi2[i, j] = fit_results['chi2_complex']
                map_ndf[i, j] = fit_results['ndf_complex']
                # get general data
                map_mu_balmer_1[i, j] = fit_results['mu_balmer_1']
                map_mu_balmer_1_err[i, j] = fit_results['mu_balmer_1_err']
                map_mu_balmer_2[i, j] = fit_results['mu_balmer_2']
                map_mu_balmer_2_err[i, j] = fit_results['mu_balmer_2_err']
                map_mu_forbidden_1[i, j] = fit_results['mu_forbidden_1']
                map_mu_forbidden_1_err[i, j] = fit_results['mu_forbidden_1_err']
                map_mu_forbidden_2[i, j] = fit_results['mu_forbidden_2']
                map_mu_forbidden_2_err[i, j] = fit_results['mu_forbidden_2_err']

                map_sigma_balmer_1[i, j] = fit_results['sigma_balmer_1']
                map_sigma_balmer_1_err[i, j] = fit_results['sigma_balmer_1_err']
                map_sigma_balmer_2[i, j] = fit_results['sigma_balmer_2']
                map_sigma_balmer_2_err[i, j] = fit_results['sigma_balmer_2_err']
                map_sigma_forbidden_1[i, j] = fit_results['sigma_forbidden_1']
                map_sigma_forbidden_1_err[i, j] = fit_results['sigma_forbidden_1_err']
                map_sigma_forbidden_2[i, j] = fit_results['sigma_forbidden_1']
                map_sigma_forbidden_2_err[i, j] = fit_results['sigma_forbidden_2_err']

                map_delta_v_balmer[i, j] = map_mu_balmer_2[i, j] - map_mu_balmer_1[i, j]
                map_delta_v_balmer_err[i, j] = np.sqrt(map_mu_balmer_1_err[i, j] ** 2 + map_mu_balmer_2_err[i, j] ** 2)
                map_delta_v_forbidden[i, j] = map_mu_forbidden_2[i, j] - map_mu_forbidden_1[i, j]
                map_delta_v_forbidden_err[i, j] =  np.sqrt(map_mu_forbidden_1_err[i, j] ** 2 +
                                                           map_mu_forbidden_2_err[i, j] ** 2)

                # get line _specific data
                for line, line_index in zip(line_list, range(0, len(line_list))):
                    map_emission_lines[line_index, 0, i, j] = fit_results['amp_%i_1' % line]
                    map_emission_lines[line_index, 1, i, j] = fit_results['amp_%i_1_err' % line]
                    map_emission_lines[line_index, 2, i, j] = fit_results['amp_%i_2' % line]
                    map_emission_lines[line_index, 3, i, j] = fit_results['amp_%i_2_err' % line]

            map_dict = {
                'chi2': map_chi2,
                'ndf': map_ndf,
                'mu_balmer_1': map_mu_balmer_1,
                'mu_balmer_1_err': map_mu_balmer_1_err,
                'mu_balmer_2': map_mu_balmer_2,
                'mu_balmer_2_err': map_mu_balmer_2_err,
                'mu_forbidden_1': map_mu_forbidden_1,
                'mu_forbidden_1_err': map_mu_forbidden_1_err,
                'mu_forbidden_2': map_mu_forbidden_2,
                'mu_forbidden_2_err': map_mu_forbidden_2_err,
                'sigma_balmer_1': map_sigma_balmer_1,
                'sigma_balmer_1_err': map_sigma_balmer_1_err,
                'sigma_balmer_2': map_sigma_balmer_2,
                'sigma_balmer_2_err': map_sigma_balmer_2_err,
                'sigma_forbidden_1': map_sigma_forbidden_1,
                'sigma_forbidden_1_err': map_sigma_forbidden_1_err,
                'sigma_forbidden_2': map_sigma_forbidden_2,
                'sigma_forbidden_2_err': map_sigma_forbidden_2_err,
                'delta_v_balmer': map_delta_v_balmer,
                'delta_v_balmer_err': map_delta_v_balmer_err,
                'delta_v_forbidden': map_delta_v_forbidden,
                'delta_v_forbidden_err': map_delta_v_forbidden_err}

            for line, line_index in zip(line_list,
                                        range(0, len(line_list))):
                map_dict.update({'amp_%i_1' % line: map_emission_lines[line_index, 0]})
                map_dict.update({'amp_%i_1_err' % line: map_emission_lines[line_index, 1]})
                map_dict.update({'amp_%i_2' % line: map_emission_lines[line_index, 2]})
                map_dict.update({'amp_%i_2_err' % line: map_emission_lines[line_index, 3]})
                
        else:
            raise KeyError('n_gauss attribute should be 1 or 2')

        return map_dict
