#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 09:41:21 2021

@author: ppiovesanalm

Plot SITELLE spectra
"""

#load_ext autoreload
#autoreload 2
import os.path

import numpy as np
import matplotlib.pyplot as plt

import orcs.process
import orcs.fit
import orcs.utils
import orcs.core

import orb

import pandas as pd
# import sitelle

from orb.core import Lines

#from sitelle.constants import FITS_DIR
#from sitelle.process import SpectralCubePatch as SpectralCube

from orb.utils import io
#from sitelle.constants import SN2_LINES, SN3_LINES
#from sitelle.fit import fit_SN2
# from sitelle.plot import plot_map, plot_scatter, plot_spectra
#from sitelle.source import extract_point_source, get_sources

from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from mpl_toolkits.axes_grid.inset_locator import InsetPosition


def update_ax2(ax1):
   x1, x2 = ax1.get_xlim()
   v1 = 1.e8/x1
   v2 = 1.e8/x2
   ax2.set_ylim(v1,v2)
   ax2.figure.canvas.draw()

def wavenumber2wavelength(wavenumber):
   return 1e8 / wavenumber

def wavelength2wavenumber(lbda):
   return 1e8 / lbda

def search_index(Lambda,Spectrum):
   num=0
   for i in range(np.size(Spectrum)-1):
      if ((Lambda > Spectrum[i]) & (Lambda < Spectrum[i+1])):
         num = i
         break
   return num

#def search_jump(sigma,mean)
#     if sigma[500:600]:
#   return num1,num2

INSET = 'NO'

#def tick_function(X):
#    V = 1.e8/X
#return ["%.3f" % z for z in V]

#  ============ =======
#    Em. Line     Air
#  ============ =======
#  [OII]3726    372.603
#  [OII]3729    372.882
#  Hepsilon     397.007
#  Hdelta       410.176
#  Hgamma       434.047
#  [OIII]4363   436.321
#  Hbeta        486.133
#  [OIII]4959   495.892
#  [Oiii]5007   500.684
#  [NII]6548    654.803
#  Halpha       656.279
#  [NII]6583    658.341
#  [SII]6716    671.647
#  [SII]6731    673.085

OIIIa_A = Lines().get_line_nm('[OIII]5007')*10
OIIIb_A = Lines().get_line_nm('[OIII]4959')*10
hbeta_A = Lines().get_line_nm('Hbeta')*10
halpha_A = Lines().get_line_nm('Halpha')*10
NII_A = Lines().get_line_nm('[NII]6583')*10
SIIa_A = Lines().get_line_nm('[SII]6716')*10
SIIb_A = Lines().get_line_nm('[SII]6731')*10

OIIIa_cm1 = Lines().get_line_cm1('[OIII]5007')
OIIIb_cm1 = Lines().get_line_cm1('[OIII]4959')
hbeta_cm1 = Lines().get_line_cm1('Hbeta')
NII_cm1 = Lines().get_line_cm1('[NII]6583')
SIIa_cm1 = Lines().get_line_cm1('[SII]6716')
SIIb_cm1 = Lines().get_line_cm1('[SII]6731')

##### Petit programme d'exemple pour ouvrir des données sitelle

##### En premier lieu, il faut installer ORCS et ORB qui sont sur le github de Thomas Martin
##### La procédure d'installation est expliquée sur github, et normalement sur linux ça devrait marcher du premier coup

# Pour ouvrir le cube dans la classe associée
#cubeSN2=orcs.process.SpectralCube('/home/ppiovesan/stage/Data/M31_SN2.merged.cm1.1.0.hdf5.1')

if os.path.isdir('/home/benutzer/data/m31/sitelle'):
   cubeSN2 = orcs.process.SpectralCube('/home/benutzer/data/m31/sitelle/M31_SN2.merged.cm1.1.0.hdf5.1')

else:
   cubeSN2=orcs.process.SpectralCube('/Users/melchior/Research/M31/SITELLE1/DataCube/M31_SN2.merged.cm1.1.0.hdf5.1')


# La shape du cube est de la forme (2048,2064,556) pour SN2 et (2048,2064,840) pour SN3

# Des coordonnées où il y a une raie OIII
x=1651
y=1110
# Centre
x0=1075
y0=1035

x=x0
y=y0

# Pour accéder à la data, il y a deux façons:
# Pour un seul spectre:
spectrum=np.real(cubeSN2.extract_spectrum(x,y)) # array de shape (2,556) avec spectrum[0] les coordonnées de l'axespectral et spectrum[1] la densité spectrale

nx = np.size(spectrum[0])

my_std  = np.zeros(nx)             
my_med  = np.zeros(nx)             
my_mean = np.zeros(nx)             

npt=10
for ik in np.arange(npt,nx-npt,dtype='int'):
   my_mean[ik] = np.mean(spectrum[1][ik-npt:ik+npt-1])
   my_med[ik]  = np.median(spectrum[1][ik-npt:ik+npt-1])
   my_std[ik]  = np.std(spectrum[1][ik-npt:ik+npt-1])


fig = plt.figure(figsize=(12, 9))
fontsize = 15
ax1 = fig.add_subplot(211)
ax2 = ax1.twiny()

ax3 = fig.add_subplot(212)
ax4 = ax3.twiny()

ax1.plot(spectrum[0], spectrum[1]*1.e16, color='blue', label=('Pixel %d,%d' % (x, y)))
ax1.plot(spectrum[0], my_mean*1.e16, label='Mean')
ax1.plot(spectrum[0], my_med*1.e16, label='Median')
ax1.plot(spectrum[0], my_std*1.e16, label='Sigma')
ax1.grid(axis='x')
# ax1.set_xlabel(r"Wave number (cm$^{-1}$)", fontsize=fontsize)
ax1.set_xticklabels([])
ax1.set_ylabel(r"Flux (${\rm 10^{16} erg  \,\,cm^{-2} \,\, s^{-1} \,\, A^{-1}}$)", fontsize=fontsize)
ax1.legend(loc=4, fontsize=fontsize)
ax1.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)

new_tick_locations = ax1.get_xticks()[1:-1]

ax2.set_xlim(ax1.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(["%.1f" % i for i in wavenumber2wavelength(new_tick_locations)])
ax2.set_xlabel(r'Wavelength $(\AA)$', fontsize=fontsize)
ax2.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)


ax3.plot(spectrum[0], spectrum[1]*1.e16, color='blue', label=('Pixel %d,%d' % (x, y)))
ax3.plot(spectrum[0], my_mean*1.e16, label='Mean')
ax3.plot(spectrum[0], my_med*1.e16, label='Median')
ax3.plot(spectrum[0], my_std*1.e16, label='Sigma')
ax3.grid(axis='x')
ax3.set_xlabel(r"Wave number (cm$^{-1}$)", fontsize=fontsize)
ax3.set_ylabel(r"Flux (${\rm 10^{16} erg  \,\,cm^{-2} \,\, s^{-1} \,\, A^{-1}}$)", fontsize=fontsize)
ax3.legend(loc=4, fontsize=fontsize)
ax3.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)

new_tick_locations = ax3.get_xticks()[1:-1]

ax4.set_xlim(ax3.get_xlim())
ax4.set_xticks(new_tick_locations)
# ax4.set_xticklabels(["%.1f" % i for i in wavenumber2wavelength(new_tick_locations)])
ax4.set_xticklabels([])
# ax4.set_xlabel(r'Wavelength $(\AA)$', fontsize=fontsize)
ax4.tick_params(axis='both', which='both', width=2, direction='in', labelsize=fontsize)

plt.tight_layout()
plt.subplots_adjust(hspace=0)
plt.show()
plt.savefig("Jump.png")





Lambda1= 4878.
Lambda2= 5250.
nmax =  search_index(wavelength2wavenumber(Lambda1),spectrum[0])
nmin =  search_index(wavelength2wavenumber(Lambda2),spectrum[0])
print(nmin,nmax)

print(spectrum[0][nmax],wavelength2wavenumber(Lambda1),wavenumber2wavelength(wavelength2wavenumber(Lambda1)))
print(spectrum[0][nmin],wavelength2wavenumber(Lambda2),wavenumber2wavelength(wavelength2wavenumber(Lambda2)))

