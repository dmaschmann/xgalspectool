#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 09:41:21 2021

@author: ppiovesan
"""

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import os

import orcs.process
import orcs.fit
import orcs.utils
import orcs.core

import orb

##### Petit programme d'exemple pour ouvrir des données sitelle

##### En premier lieu, il faut installer ORCS et ORB qui sont sur le github de Thomas Martin
##### La procédure d'installation est expliquée sur github, et normalement sur linux ça devrait marcher du premier coup

# Pour ouvrir le cube dans la classe associée


if os.path.isdir(Path.home() / 'data' / 'm31' / 'sitelle'):
    data_path = Path.home() / 'data' / 'm31' / 'sitelle'
elif os.path.isdir(Path.home() / 'stage' / 'Data'):
    data_path = Path.home() / 'stage' / 'Data'
else:
    raise LookupError('please specify a data path to the SITELLE spectra')




file_name_sn2 = 'M31_SN2.merged.cm1.1.0.hdf5.1'
file_name_sn3 = 'M31_SN3.merged.cm1.1.0.hdf5.1'


cubeSN2 = orcs.process.SpectralCube(data_path / file_name_sn2)
cubeSN3 = orcs.process.SpectralCube(data_path / file_name_sn3)

# print(cubeSN2.extract_spectrum(1651, 1110, 4))
# exit()

spec_sn2 = cubeSN2.extract_spectrum(1651, 1110, 4)
# # spec_sn3 = cubeSN3.extract_spectrum_bin(x=1651, y=1110, b=4)
#
# filter_1 = orb.core.Cm1Vector1d(np.array(spec_sn2[1]))
# print(filter_1)
# #
#
# cubeSN2.integrate(spec_sn2[1])
# exit()


plt.plot(1 / spec_sn2[0] * 1e10, spec_sn2[1], color='b', label='exemple de spectre')
# plt.plot(spec_sn3[0], spec_sn3[1], color='r', label='exemple de spectre')
#
plt.show()


# deep = cubeSN3.get_deep_frame() # this step should take a second or less. If it takes a lot of time, see above.
# deep.imshow(perc=95, wcs=True)
# plt.grid()
# cb = plt.colorbar(shrink=0.8, format='%.1e')
# cb.ax.set_title('counts')
# # deep.to_fits('M31_SN3.deep_frame.fits')
# plt.show()

exit()


# La shape du cube est de la forme (2048,2064,556) pour SN2 et (2048,2064,840) pour SN3

# Des coordonnées où il y a une raie OIII
x=1651
y=1110

# Pour accéder à la data, il y a deux façons:
# Pour un seul specte:
spectrum=np.real(cubeSN2.extract_spectrum(x,y)) # array de shape (2,556) avec spectrum[0] les coordonnées de l'axe spectral et spectrum[1] la densité spectrale

plt.figure()
plt.plot(spectrum[0],spectrum[1],color='blue',label='exemple de spectre')
plt.legend()
plt.xlabel("nombre d'onde en $cm^{-1}$")
plt.ylabel("densité spectrale en $erg.cm^{-2}.s^{-1}.A^{-1}$")
plt.show()

# Pour une region entiere:
dataregion=cubeSN2.get_data(x-15,x+15,y-15,y+15,0,556) # ce serait cool de pouvoir extraire la data d'un coup mais j'ai pas assez de RAM

plt.figure()
plt.plot(spectrum[0],dataregion[15,15,:],color='blue',label='le même spectre')
plt.legend()
plt.xlabel("nombre d'onde en $cm^{-1}$")
plt.ylabel("densité spectrale en $erg.cm^{-2}.s^{-1}.A^{-1}$")
plt.show()


# Une autre façon d'obtenir les coordonnées de l'axis spectral (en cm**(-1))
baseaxis=cubeSN2.get_base_axis()
baseaxisnp=np.array(baseaxis.get_gvar()) # sous forme de numpy array


# Pour retirer le background en faisant, pour chaque channel du spectre, la médiane des pixels à coté, il ya une fonction de thomas martin
# cette fonction extrait dans un anneau circulaire, sinon tu peux bien sûr faire à la main avec des numpy array à l'aide de data region
background=cubeSN2.get_spectrum_in_annulus(x,y,2.5,15,median=True, mean_flux=True) # la classe de orb associée
backgroundnp=background.data # le numy array de shape (556)


plt.figure()
plt.plot(spectrum[0],spectrum[1],color='blue',label='spectre')
plt.plot(spectrum[0],backgroundnp,color='orange',label='background')
plt.legend()
plt.xlabel("nombre d'onde en $cm^{-1}$")
plt.ylabel("densité spectrale en $erg.cm^{-2}.s^{-1}.A^{-1}$")
plt.show()

# Et le spectre sans le background
spectrumsoustra=spectrum[1]-np.real(background.data)

plt.figure()
plt.plot(spectrum[0],spectrumsoustra,color='blue',label='spectre sans background')
plt.legend()
plt.xlabel("nombre d'onde en $cm^{-1}$")
plt.ylabel("densité spectrale en $erg.cm^{-2}.s^{-1}.A^{-1}$")
plt.show()



# Il y a plein de fonctions/methodes intégrées à ORCS donc n'heiste pas a me demander ce dont tu as besoin ((et c'est bien documenté aussi et ça c'est cool))
# Mais typiquement, les classes dans ORCS ont souvent un .data pour accéder au numpy array et pouvoir bosser dessus et des .params pour des informations