from xgalspectool.spec_fit import emission_line_fit
import marvin

# galaxy_object = emission_line_fit.EmissionLineFit(sdss_mjd=54526, sdss_plate=2884, sdss_fiberid=198,
#                                                   manga_plate=8562, manga_ifudsgn=6101)

#galaxy_object = emission_line_fit.EmissionLineFit(sdss_mjd=53054, sdss_plate=1438, sdss_fiberid=269,
#                                                  manga_plate=8993, manga_ifudsgn=6103)

# galaxy_object = emission_line_fit.EmissionLineFit(sdss_mjd=54526, sdss_plate=2884, sdss_fiberid=235,
#                                                   manga_plate=8482, manga_ifudsgn=12705)

galaxy_object = emission_line_fit.EmissionLineFit(sdss_mjd=53330, sdss_plate=1936, sdss_fiberid=597,
                                                  manga_plate=11748, manga_ifudsgn=12705)

galaxy_object.load_manga_cube(nsa_source='drpall', cube_format='voronoi_logcube_dr17')
galaxy_object.load_manga_map(nsa_source='drpall', map_format='voronoi_maps_dr17')

galaxy_object.manga_evolution_fit(n_gauss=2, fit_mode='complex', two_vel=False, doublet_ratio=True,
                            mu_evolve=100, sigma_evolve=40, amp_evolve=1, plot=False)


# galaxy_object.set_up_manga_spec_fit(n_gauss=2, fit_mode='complex', two_vel=False, doublet_ratio=True,
#                                     mu_evolve=100, sigma_evolve=40, amp_evolve=1)
#
# fit_results_folder = galaxy_object.get_manga_object_file_path() / galaxy_object.get_manga_fit_param_folder_name()
#
# fit_results = np.load(str(fit_results_folder / '0') + '.npy', allow_pickle=True).item()
# print(fit_results)
# wave = galaxy_object.get_manga_spax_wave(i=i, j=j)
# em_flux = galaxy_object.get_manga_spax_em_spec(i=i, j=j)
# em_flux_err = galaxy_object.get_manga_spax_flux_err(i=i, j=j)
#
# redshift = galaxy_object.get_manga_redshift()
# sys_vel = galaxy_object.get_manga_sys_vel()
#
# spax_ra, spax_dec = galaxy_object.get_manga_spax_ra_dec(i=i, j=j)
#
# inst_broad_dict = {}
# for line in galaxy_object.get_full_line_list():
#     inst_broad_dict.update({line: galaxy_object.get_manga_inst_broad_map(line=line)[i, j]})
#
#
# image_access = imgtool.ImgTool(ra=ra, dec=dec)
# g_header, img_rgb = image_access.get_legacy_survey_coord_img()
#
# plotting_class = fit_plot.FitPlot()
#
# fig = plotting_class.plot_double_gauss_fit_results_complex(fit_result_dict=fit_results,
#                                                            header=g_header, img=img_rgb, ra=spax_ra, dec=spax_dec,
#                                                            inst_broad_dict=inst_broad_dict,
#                                                            wave_sdss=wave, em_flux_sdss=em_flux,
#                                                            em_flux_err_sdss=em_flux_err,
#                                                            sys_vel=sys_vel)
#
# plt.show()
#
# exit()
#
#
#
# # galaxy_object.run_manga_spaxel_em_fit(i=i, j=j, n_gauss=2, fit_mode='complex', two_vel=False, doublet_ratio=True)
#
#
# print(galaxy_object.fit_param_dict)
#
#
# wave = galaxy_object.get_manga_spax_wave(i=i, j=j)
# em_flux = galaxy_object.get_manga_spax_em_spec(i=i, j=j)
# em_flux_err = galaxy_object.get_manga_spax_flux_err(i=i, j=j)
#
# redshift = galaxy_object.get_manga_redshift()
# sys_vel = galaxy_object.get_manga_sys_vel()
#
# spax_ra, spax_dec = galaxy_object.get_manga_spax_ra_dec(i=i, j=j)
#
# image_access = imgtool.ImgTool(ra=ra, dec=dec)
# g_header, img_rgb = image_access.get_legacy_survey_coord_img()
#
# plotting_class = fit_plot.FitPlot()
#
# fig = plotting_class.plot_double_gauss_fit_results_complex(fit_result_dict=galaxy_object.fit_param_dict,
#                                                            header=g_header, img=img_rgb, ra=spax_ra, dec=spax_dec,
#                                                            inst_broad_dict=inst_broad_dict,
#                                                            wave_sdss=wave, em_flux_sdss=em_flux,
#                                                            em_flux_err_sdss=em_flux_err,
#                                                            sys_vel=sys_vel)
#
# plt.savefig('plot_output/test_manga_fit.png')
#
#
