from spec_fit import emission_line_fit, fit_plot
import matplotlib.pyplot as plt
from xgaltool import analysis_tools
from scipy.constants import c as speed_of_light

# hi_spec_path = 'spectrum_hi_MNG-DP-3.dat'
# hi_redshift = 0.03129

hi_spec_path = '/Users/barbara/Documents/PhD/dp_manga/observation/nrt/data_reduction/6_plot_factory/spectrum/spectrum_hi_MNG-DP-1.dat'
hi_redshift_mng_dp_6 = 0.02615
hi_redshift_mng_dp_7 = 0.02497
hi_redshift_mng_dp_8 = 0.02530
hi_redshift_mng_dp_1 = 0.05795


galaxy_object = emission_line_fit.EmissionLineFit(hi_spec_path=hi_spec_path, hi_redshift=hi_redshift_mng_dp_1)

# galaxy_object.load_hi_spectrum()

# plt.scatter(galaxy_object.vel_hi, galaxy_object.flux_hi)
# plt.show()

galaxy_object.run_hi_fit(n_gauss=1, fit_mode='hi')

exit()
print(galaxy_object.fit_param_dict)


vel_hi = galaxy_object.vel_hi
flux_hi = galaxy_object.flux_hi
sys_vel = speed_of_light * 1e-3 * hi_redshift


print('BLABLABLA')
plotting_class = fit_plot.FitPlot()
fig = plotting_class.plot_single_gauss_fit_results_hi(fit_result_dict=galaxy_object.fit_param_dict,
                                                      vel_hi=vel_hi, flux_hi=flux_hi, sys_vel=sys_vel)

plt.show()
exit()




