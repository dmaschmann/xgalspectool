from spec_fit import emission_line_fit, fit_plot
from xgalimgtool import imgtool
import matplotlib.pyplot as plt
import numpy as np

mjd = 53795
plate = 1812
fiberid = 187
ra = 217.82492187
dec = 7.94463399
redshift = 0.0269095

co10_path = ('/home/benutzer/Documents/projects/double_peak_origin_analysis/observation/iram_30m/run_166_20/'
             'data_reduction/6_plot_factory/spectrum/spectrum_co10_DP-7.dat')

co21_path = ('/home/benutzer/Documents/projects/double_peak_origin_analysis/observation/iram_30m/run_166_20/'
             'data_reduction/6_plot_factory/spectrum/spectrum_co21_DP-7.dat')


rcsed_object = emission_line_fit.EmissionLineFit(sdss_mjd=mjd, sdss_plate=plate, sdss_fiberid=fiberid,
                                                 co10_spec_path=co10_path, co21_spec_path=co21_path,
                                                 co_redshift=redshift)

# run SDSS fit
# rcsed_object.run_rcsed_em_fit(n_gauss=2, fit_mode='complex', two_vel=False, doublet_ratio=True)

# inst_broad_dict = {}
# for line in rcsed_object.get_full_line_list():
#     if isinstance(line, int):
#         inst_broad_dict.update({line: rcsed_object.get_rcsed_line_inst_broad(line=line)})


print('SDSS fit results')
print(rcsed_object.fit_param_dict)

# run CO fit
vel_bin_width_co10 = 30
vel_bin_width_co21 = 30
sys_vel = rcsed_object.get_rcsed_sys_vel(redshift=redshift)

# fix fir parameters
# param_limits = {'limit_mu_1': (rcsed_object.fit_param_dict['mu_balmer_1'], rcsed_object.fit_param_dict['mu_balmer_1']),
#                 'limit_mu_2': (rcsed_object.fit_param_dict['mu_balmer_2'], rcsed_object.fit_param_dict['mu_balmer_2']),
#                 'limit_mu_3': (rcsed_object.fit_param_dict['mu_balmer_2'] - 1000, rcsed_object.fit_param_dict['mu_balmer_2'] + 1000),
#                 'limit_sigma_1': (rcsed_object.fit_param_dict['sigma_balmer_1'], rcsed_object.fit_param_dict['sigma_balmer_1']),
#                 'limit_sigma_2': (rcsed_object.fit_param_dict['sigma_balmer_2'], rcsed_object.fit_param_dict['sigma_balmer_2']),
#                 'limit_sigma_3': (40, 1000),
#                 'limit_amp_co10_1': (10, 50),
#                 'limit_amp_co10_2': (10, 50),
#                 'limit_amp_co10_3': (10, 100),
#                 'limit_amp_co21_1': (20, 100),
#                 'limit_amp_co21_2': (20, 100),
#                 'limit_amp_co21_3': (20, 100)}
# param_limits = {'limit_mu_1': (rcsed_object.fit_param_dict['mu_balmer_1'], rcsed_object.fit_param_dict['mu_balmer_1']),
#                 'limit_mu_2': (rcsed_object.fit_param_dict['mu_balmer_2'], rcsed_object.fit_param_dict['mu_balmer_2']),
#                 'limit_sigma_1': (rcsed_object.fit_param_dict['sigma_balmer_1'], rcsed_object.fit_param_dict['sigma_balmer_1']),
#                 'limit_sigma_2': (rcsed_object.fit_param_dict['sigma_balmer_2'], rcsed_object.fit_param_dict['sigma_balmer_2'])}
#
# rcsed_object.run_co_fit(n_gauss=2, fit_mode='co10_co21',
#                         vel_bin_width_co10=vel_bin_width_co10, vel_bin_width_co21=vel_bin_width_co21,
#                         init_params=None, param_limits=param_limits, param_step=None)
#
# print('SDSS and CO fit results')
# print(rcsed_object.fit_param_dict)


wave, em_flux, em_flux_err = rcsed_object.get_rcsed_em_spec()

redshift = rcsed_object.get_rcsed_spec_redshift()

image_access = imgtool.ImgTool(ra=ra, dec=dec)
g_header, img_rgb = image_access.get_legacy_survey_coord_img()



vel_bins = np.arange(-2000, 2000, 70)

rcsed_object.load_co_spectrum()
if vel_bin_width_co10 is not None:
    rcsed_object.rebin_co_spectrum(vel_bins=vel_bins, co_line='co10')
if vel_bin_width_co21 is not None:
    rcsed_object.rebin_co_spectrum(vel_bins=vel_bins, co_line='co21')

vel_co10 = rcsed_object.vel_co10
flux_co10 = rcsed_object.flux_co10
vel_co21 = rcsed_object.vel_co21
flux_co21 = rcsed_object.flux_co21

fig, ax = plt.subplots(ncols=1, nrows=3, sharex='all', figsize=(12, 6))

ax[0].scatter(vel_co10, flux_co10)
ax[1].scatter(vel_co21, flux_co21)
ax[2].scatter(vel_co21, flux_co10 / flux_co21)
ax[2].plot([-1000, 1000], [0.77, 0.77])
ax[2].set_ylim(0, 2)
plt.show()

exit()

plotting_class = fit_plot.FitPlot()

fig = plotting_class.plot_double_gauss_fit_results_complex_sdss_co10_co21(fit_result_dict=rcsed_object.fit_param_dict,
                                                                          header=g_header, img=img_rgb,
                                                                          ra=ra,
                                                                          dec=dec,
                                                                          inst_broad_dict=inst_broad_dict,
                                                                          wave_sdss=wave, em_flux_sdss=em_flux,
                                                                          em_flux_err_sdss=em_flux_err,
                                                                          sys_vel=sys_vel,
                                                                          vel_co10=vel_co10, flux_co10=flux_co10,
                                                                          vel_co21=vel_co21, flux_co21=flux_co21,
                                                                          n_gauss_co=2,
                                                                          title='test')


plt.show()


