from spec_fit import emission_line_fit, fit_plot
import matplotlib.pyplot as plt
from xgaltool import analysis_tools
from xgalimgtool import imgtool, imgplot

from scipy.constants import c as speed_of_light






sf_radio_co_access = analysis_tools.AnalysisTools(object_type='sf_radio_co', table_cross_match='rcsed_co_observation')
proposal_identifier_sf_radio_co = sf_radio_co_access.table['proposal_identifier']
mjd = sf_radio_co_access.get_mjd()
plate = sf_radio_co_access.get_plate()
fiberid = sf_radio_co_access.get_fiberid()
ra = sf_radio_co_access.get_ra()
dec = sf_radio_co_access.get_dec()

redshift = sf_radio_co_access.get_redshift()
sys_vel = speed_of_light * 1e-3 * redshift


# galaxy_of_interest = ((mjd == 52703) & (plate == 1161) & (fiberid == 595))
galaxy_of_interest = ((mjd == 54234) & (plate == 2748) & (fiberid == 440))


image_access = imgtool.ImgTool(ra=ra[galaxy_of_interest], dec=dec[galaxy_of_interest])
g_header, img_rgb = image_access.get_legacy_survey_coord_img()


plotting_class = fit_plot.FitPlot()
fig = plotting_class.plot_double_gauss_fit_results_complex_sdss_co10_co21(fit_result_dict=None,
                                                                          header=g_header, img=img_rgb,
                                                                          ra=ra[galaxy_of_interest],
                                                                          dec=dec[galaxy_of_interest],
                                                                          inst_broad_dict=None,
                                                                          wave_sdss=None, em_flux_sdss=None,
                                                                          em_flux_err_sdss=None,
                                                                          sys_vel=None,
                                                                          vel_co10=None, flux_co10=None,
                                                                          vel_co21=None, flux_co21=None)

exit()






# spec_path = '/home/benutzer/Documents/projects/double_peak_origin_analysis/observation/iram_30m/run_198_19/data_reduction/6_plot_factory/spectrum/'
spec_path = '/home/benutzer/Documents/projects/double_peak_origin_analysis/observation/iram_30m/run_166_20/data_reduction/6_plot_factory/spectrum/'

co10_path = spec_path + 'spectrum_co10_%s.dat' % proposal_identifier_sf_radio_co[galaxy_of_interest][0]
co21_path = spec_path + 'spectrum_co21_%s.dat' % proposal_identifier_sf_radio_co[galaxy_of_interest][0]


galaxy_object = emission_line_fit.EmissionLineFit(sdss_mjd=mjd[galaxy_of_interest],
                                                  sdss_plate=plate[galaxy_of_interest],
                                                  sdss_fiberid=fiberid[galaxy_of_interest],
                                                  co10_spec_path=co10_path, co21_spec_path=co21_path,
                                                  co_redshift=redshift[galaxy_of_interest])
#
galaxy_object.run_sdss_co_fit(n_gauss=2, fit_mode='complex_co10_co21')


image_access = imgtool.ImgTool(ra=ra[galaxy_of_interest], dec=dec[galaxy_of_interest])
g_header, img_rgb = image_access.get_legacy_survey_coord_img()

inst_broad_dict = {}
for line in galaxy_object.get_full_line_list():
    if isinstance(line, int):
        inst_broad_dict.update({line: galaxy_object.get_rcsed_line_inst_broad(line=line)})

wave, em_flux, em_flux_err = galaxy_object.get_rcsed_em_spec()

vel_co10 = galaxy_object.vel_co10
flux_co10 = galaxy_object.flux_co10
vel_co21 = galaxy_object.vel_co21
flux_co21 = galaxy_object.flux_co21

plotting_class = fit_plot.FitPlot()
fig = plotting_class.plot_double_gauss_fit_results_complex_sdss_co10_co21(fit_result_dict=galaxy_object.fit_param_dict,
                                                                          header=g_header, img=img_rgb,
                                                                          ra=ra[galaxy_of_interest],
                                                                           dec=dec[galaxy_of_interest],
                                                                          inst_broad_dict=inst_broad_dict,
                                                                          wave_sdss=wave, em_flux_sdss=em_flux,
                                                                          em_flux_err_sdss=em_flux_err,
                                                                          sys_vel=sys_vel[galaxy_of_interest],
                                                                          vel_co10=vel_co10, flux_co10=flux_co10,
                                                                          vel_co21=vel_co21, flux_co21=flux_co21)

# fig = plotting_class.plot_single_gauss_fit_results_complex_sdss_co10_co21(fit_result_dict=galaxy_object.fit_param_dict,
#                                                                           header=g_header, img=img_rgb,
#                                                                           ra=ra[galaxy_of_interest],
#                                                                           dec=dec[galaxy_of_interest],
#                                                                           inst_broad_dict=inst_broad_dict,
#                                                                           wave_sdss=wave, em_flux_sdss=em_flux,
#                                                                           em_flux_err_sdss=em_flux_err,
#                                                                           sys_vel=sys_vel[galaxy_of_interest],
#                                                                           vel_co10=vel_co10, flux_co10=flux_co10,
#                                                                           vel_co21=vel_co21, flux_co21=flux_co21)


fig.savefig('plot_output/test.png')
exit()



galaxy_object.run_sdss_co_fit(n_gauss=2, fit_mode='simple_co10_co21')

print(galaxy_object.fit_param_dict)





inst_broad_dict = {}
for line in galaxy_object.get_full_line_list():
    if isinstance(line, int):
        inst_broad_dict.update({line: galaxy_object.get_rcsed_line_inst_broad(line=line)})

wave, em_flux, em_flux_err = galaxy_object.get_rcsed_em_spec()

redshift = galaxy_object.get_rcsed_spec_redshift()
plotting_class = fit_plot.FitPlot()
fig0 = plotting_class.plot_double_gauss_fit_results_simple(fit_result_dict=galaxy_object.fit_param_dict,
                                                          inst_broad_dict=inst_broad_dict, wave=wave, em_flux=em_flux, em_flux_err=em_flux_err)

plt.show()
plt.clf()
plt.close()

fig1 = plotting_class.plot_double_gauss_fit_results_co10_co21(fit_result_dict=galaxy_object.fit_param_dict,
                                                             vel_co10=vel_co10, flux_co10=flux_co10,
                                                             vel_co21=vel_co21, flux_co21=flux_co21,
                                                             sys_vel=sys_vel)


plt.show()

exit()




exit()



g_header, img_rgb = sf_radio_co_access.get_legacy_coord_image(size=200, pixel_scale=29, index=index)



wcs = WCS(g_header)

fig = plt.figure(figsize=(17, 6))

ax_image = fig.add_axes([0.013, 0.085, 0.4, 0.85], projection=wcs)
if co_10_flag:
    ax_spec_co10 = fig.add_axes([0.42, 0.1, 0.275, 0.85])
if co_21_flag:
    ax_spec_co21 = fig.add_axes([0.72, 0.1, 0.275, 0.85])

ax_image.set_title(sdss_designation, fontsize=fontsize)
ax_image.imshow(np.flipud(img_rgb), origin='lower')
# We need to set the ticklabel position explicitly
ax_image.coords['ra'].set_ticklabel_position('b')
ax_image.coords['dec'].set_ticklabel_position('l')

# rotate the dec ax_imageis
ax_image.coords['dec'].set_ticklabel(rotation=60)
# grids
ax_image.coords.grid(color='grey', ls=':', linewidth=2)

# Still refers to the image x axis
ax_image.coords['ra'].set_axislabel('R.A. (2000.0)', minpad=0.3, fontsize=fontsize)
ax_image.coords['dec'].set_axislabel('DEC. (2000.0)', minpad=-0.4, fontsize=fontsize)
ax_image.tick_params(axis='both', which='both', width=2, color='white', labelsize=fontsize)

circle_co10 = SphericalCircle((m_19_ra[index] * u.deg, m_19_dec[index] * u.deg), 20 * u.arcsec,
                              edgecolor='y', facecolor='none', linewidth=3, linestyle='--', alpha=1,
                              transform=ax_image.get_transform('icrs'))

circle_co21 = SphericalCircle((m_19_ra[index] * u.deg, m_19_dec[index] * u.deg), 10 * u.arcsec,
                              edgecolor='g', facecolor='none', linewidth=3, linestyle='--', alpha=1,
                              transform=ax_image.get_transform('icrs'))

ax_image.add_patch(circle_co10)
ax_image.add_patch(circle_co21)




exit()



# galaxy_object.run_sdss_co_fit(n_gauss=2,  fit_mode='complex_co10_co21', co_weights=1)
#
# print(galaxy_object.fit_param_dict)
#
#
# exit()

vel_co10 = galaxy_object.vel_co10
flux_co10 = galaxy_object.flux_co10
vel_co21 = galaxy_object.vel_co21
flux_co21 = galaxy_object.flux_co21
sys_vel = speed_of_light * 1e-3 * redshift[galaxy_of_interest]

inst_broad_dict = {}
for line in galaxy_object.get_full_line_list():
    if isinstance(line, int):
        inst_broad_dict.update({line: galaxy_object.get_rcsed_line_inst_broad(line=line)})

wave_sdss, em_flux_sdss, em_flux_err_sdss = galaxy_object.get_rcsed_em_spec()


plotting_class = fit_plot.FitPlot()
fig = plotting_class.plot_double_gauss_fit_results_simple_sdss_co10_co21(fit_result_dict=galaxy_object.fit_param_dict,
                                                                         vel_co10=vel_co10, flux_co10=flux_co10,
                                                                         vel_co21=vel_co21, flux_co21=flux_co21,
                                                                         inst_broad_dict=inst_broad_dict,
                                                                         wave_sdss=wave_sdss, em_flux_sdss=em_flux_sdss,
                                                                         em_flux_err_sdss=em_flux_err_sdss,
                                                                         sys_vel=sys_vel)


plt.show()


fig = plotting_class.plot_double_gauss_fit_results_simple(fit_result_dict=galaxy_object.fit_param_dict,
                                                          inst_broad_dict=inst_broad_dict, wave=wave, em_flux=em_flux, em_flux_err=em_flux_err)
plt.show()

