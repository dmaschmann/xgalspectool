from spec_fit import emission_line_fit, fit_plot
from xgalimgtool import imgtool
import matplotlib.pyplot as plt

# rcsed_object = emission_line_fit.EmissionLineFit(sdss_mjd=53795, sdss_plate=1812, sdss_fiberid=187)
rcsed_object = emission_line_fit.EmissionLineFit(sdss_mjd=54539, sdss_plate=2154, sdss_fiberid=36)



# ra = 217.82492187
# dec = 7.94463399

ra = 228.665810
dec = 26.594198



rcsed_object.get_rcsed_spec_dict()


rcsed_object.run_rcsed_em_fit(n_gauss=2, fit_mode='complex', two_vel=False, doublet_ratio=True)

print(rcsed_object.fit_param_dict)

inst_broad_dict = {}
for line in rcsed_object.get_full_line_list():
    inst_broad_dict.update({line: rcsed_object.get_rcsed_line_inst_broad(line=line)})

wave, em_flux, em_flux_err = rcsed_object.get_rcsed_em_spec()

redshift = rcsed_object.get_rcsed_spec_redshift()
sys_vel = rcsed_object.get_rcsed_sys_vel(redshift=redshift)


image_access = imgtool.ImgTool(ra=ra, dec=dec)
g_header, img_rgb = image_access.get_legacy_survey_coord_img()


plotting_class = fit_plot.FitPlot()
fig = plotting_class.plot_double_gauss_fit_results_complex(fit_result_dict=rcsed_object.fit_param_dict,
                                                           header=g_header, img=img_rgb, ra=ra, dec=dec,
                                                           inst_broad_dict=inst_broad_dict,
                                                           wave_sdss=wave, em_flux_sdss=em_flux,
                                                           em_flux_err_sdss=em_flux_err, sys_vel=sys_vel)


plt.show()


