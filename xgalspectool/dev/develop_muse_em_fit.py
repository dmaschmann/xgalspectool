from spec_fit import muse_spec_fit, fit_plot
from xgalimgtool import imgtool
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt


cube_path = '/home/benutzer/Documents/projects/ngc6240/nbursts_script/ngc6240/ngc6240/results/sn006_3el_comp/'


redshift_ngc6240 = 0.0243
ra, dec = 253.245295, 2.400926


muse_object = muse_spec_fit.MUSESpecFit(project_name='ngc6240', muse_ra=ra, muse_dec=dec, muse_cube_path=cube_path,
                                        x_muse_bin_size=250, y_muse_bin_size=250, muse_redshift=redshift_ngc6240,
                                        nbursts_n_stellar_comp=1, nbursts_n_narrow_lines=2, nbursts_n_broad_lines=1)

muse_cube_dict = muse_object.get_muse_cube_dict()

# muse_object.muse_evolution_fit(muse_mu_evolve=20, muse_sigma_evolve=10, muse_amp_evolve=1)

muse_object.set_up_muse_spec_fit(n_gauss=2, n_broad=0, fit_mode='complex', two_vel=False, doublet_ratio=True,
                                 muse_mu_evolve=10, muse_sigma_evolve=5, muse_amp_evolve=1)

dict_fit_results = muse_object.create_muse_fit_parameter_dict()
print(dict_fit_results)

binid_map = np.array(muse_object.get_muse_bin_map())
list_bin_ids = np.unique(binid_map)

map_mu_1 = np.zeros(binid_map.shape)
map_mu_2 = np.zeros(binid_map.shape)

map_flux_h_alpha_1 = np.zeros(binid_map.shape)
map_flux_h_alpha_2 = np.zeros(binid_map.shape)

map_flux_err_h_alpha_1 = np.zeros(binid_map.shape)
map_flux_err_h_alpha_2 = np.zeros(binid_map.shape)

map_contin_vel = np.zeros(binid_map.shape)

for bin_id in list_bin_ids:
    map_index = np.where(binid_map == bin_id)
    bin_id_index = np.where(list_bin_ids == bin_id)
    map_mu_1[map_index] = dict_fit_results['mu_balmer_1'][bin_id_index] - muse_object.get_muse_sys_vel()
    map_mu_2[map_index] = dict_fit_results['mu_balmer_2'][bin_id_index] - muse_object.get_muse_sys_vel()
    map_flux_h_alpha_1[map_index] = dict_fit_results['flux_6565_1'][bin_id_index]
    map_flux_h_alpha_2[map_index] = dict_fit_results['flux_6565_2'][bin_id_index]
    map_flux_err_h_alpha_1[map_index] = dict_fit_results['flux_6565_1_err'][bin_id_index]
    map_flux_err_h_alpha_2[map_index] = dict_fit_results['flux_6565_2_err'][bin_id_index]
    map_contin_vel[map_index] = muse_cube_dict['vel_contin_1'][bin_id_index] - muse_object.get_muse_sys_vel()

# cut_1 = ((map_flux_h_alpha_1 / map_flux_err_h_alpha_1) < 3) | (map_flux_h_alpha_1 < 50)
cut_1 = (map_flux_h_alpha_1 < 6000)
map_mu_1[cut_1] = np.nan
map_flux_h_alpha_1[cut_1] = np.nan
#
# cut_2 = ((map_flux_h_alpha_2 / map_flux_err_h_alpha_2) < 3) | (map_flux_h_alpha_2 < 50)
cut_2 = (map_flux_h_alpha_2 < 6000)
map_mu_2[cut_2] = np.nan
map_flux_h_alpha_2[cut_2] = np.nan


fig, ax = plt.subplots(nrows=2, ncols=3)
ax[0, 0].imshow(binid_map, origin='lower')
ax[0, 1].imshow(map_mu_1, origin='lower', vmin=-500, vmax=500)

ax[1, 0].imshow(map_contin_vel, origin='lower', vmin=-500, vmax=500)
ax[1, 1].imshow(map_flux_h_alpha_1, origin='lower')
ax[1, 2].imshow(map_flux_h_alpha_2, origin='lower')

im = ax[0, 2].imshow(map_mu_2, origin='lower', vmin=-500, vmax=500)

fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax)
plt.show()
exit()

plt.imshow(map_mu_1, origin='lower')

plt.imshow(map_mu_2, origin='lower')
plt.show()


print(dict_fit_results)


exit()


flux_h_alpha_broad = muse_cube_dict['broad_el_6565_flux_1']
print(np.where(flux_h_alpha_broad == np.nanmax(flux_h_alpha_broad)))

# exit()
bin_id = 539

muse_object.run_muse_individual_spax_em_fit(bin_id=bin_id, n_gauss=2, n_broad=0, fit_mode='complex')
np.save('fit_param_dict.npy', muse_object.fit_param_dict)
muse_object.fit_param_dict = np.load('fit_param_dict.npy', allow_pickle=True).item()

print(muse_object.fit_param_dict)



wave = muse_cube_dict['wave'][bin_id]
em_flux = muse_cube_dict['flux'][bin_id] - muse_cube_dict['total_fit_contin'][bin_id]
em_flux_err = muse_cube_dict['flux_err'][bin_id]


inst_broad_dict = muse_object.inst_broad

sys_vel = muse_object.get_muse_sys_vel()


plotting_class = fit_plot.FitPlot()


image_access = imgtool.ImgTool(ra=ra, dec=dec)
g_header, img_rgb = image_access.get_legacy_survey_coord_img()


fig = plotting_class.plot_double_gauss_fit_results_complex(fit_result_dict=muse_object.fit_param_dict,
                                                           header=g_header, img=img_rgb,
                                                           ra=ra, dec=dec,
                                                           inst_broad_dict=inst_broad_dict,
                                                           wave_sdss=wave, em_flux_sdss=em_flux,
                                                           em_flux_err_sdss=em_flux_err,
                                                           sys_vel=sys_vel,)

# fig = plotting_class.plot_double_gauss_fit_results_muse_weak(fit_result_dict=muse_object.fit_param_dict,
#                                                              inst_broad_dict=inst_broad_dict,
#                                                              wave_sdss=wave, em_flux_sdss=em_flux,
#                                                              em_flux_err_sdss=em_flux_err, sys_vel=sys_vel)

plt.show()

exit()

muse_spec_dict = muse_object.get_muse_cube_dict()

bin_id = 400
plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['flux'][bin_id], color='k')
plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['total_fit'][bin_id])
plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_contin_1'][bin_id])
plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_narrow_el_1'][bin_id])
plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_narrow_el_2'][bin_id])
plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_broad_el_1'][bin_id])

for line in muse_object.em_wavelength.keys():
    plt.plot([muse_object.em_wavelength[line]['vac_wave'] * (1 + redshift_ngc6240),
              muse_object.em_wavelength[line]['vac_wave'] * (1 + redshift_ngc6240)], [0, 4000], linestyle='--', color='k')


plt.show()

exit()







bin_id_map = muse_object.get_muse_bin_map()

v_1_list = muse_spec_dict['vel_narrow_el_1']
v_2_list = muse_spec_dict['vel_narrow_el_2']

# mask_h_alpha_1 = muse_spec_dict['narrow_el_6565_1_flux'] / muse_spec_dict['narrow_el_6565_1_flux_err'] > 3
# mask_h_alpha_2 = muse_spec_dict['narrow_el_6565_2_flux'] / muse_spec_dict['narrow_el_6565_2_flux_err'] > 3

mask_h_alpha_1 = muse_spec_dict['narrow_el_6565_1_flux'] > 300
mask_h_alpha_2 = muse_spec_dict['narrow_el_6565_2_flux'] > 300



# get h_alpha snr mask


bin_id_list = np.unique(bin_id_map)

print(v_1_list.shape)
print(v_2_list.shape)
print(bin_id_list.shape)

v_1_map = np.zeros((muse_object.x_muse_bin_size, muse_object.y_muse_bin_size), dtype=float)
flux_h_alpha_1_map = np.zeros((muse_object.x_muse_bin_size, muse_object.y_muse_bin_size), dtype=float)
v_2_map = np.zeros((muse_object.x_muse_bin_size, muse_object.y_muse_bin_size), dtype=float)
flux_h_alpha_2_map = np.zeros((muse_object.x_muse_bin_size, muse_object.y_muse_bin_size), dtype=float)

for bin_id, bin_index in zip(bin_id_list, range(len(bin_id_list))):
    bin_loc = np.where(bin_id_map == bin_id)
    if mask_h_alpha_1[bin_index]:
        v_1_map[bin_loc] = v_1_list[bin_index] - muse_object.get_muse_sys_vel()
        flux_h_alpha_1_map[bin_loc] = muse_spec_dict['narrow_el_6565_1_flux'][bin_index]
    else:
        v_1_map[bin_loc] = np.nan
        flux_h_alpha_1_map[bin_loc] = np.nan

    if mask_h_alpha_2[bin_index]:
        v_2_map[bin_loc] = v_2_list[bin_index] - muse_object.get_muse_sys_vel()
        flux_h_alpha_2_map[bin_loc] = muse_spec_dict['narrow_el_6565_2_flux'][bin_index]
    else:
        v_2_map[bin_loc] = np.nan
        flux_h_alpha_2_map[bin_loc] = np.nan



plt.imshow(v_1_map)
plt.show()

plt.imshow(v_2_map)
plt.show()




# bin_id = 1
#
#
# # plt.scatter(muse_spec_dict['x_pos'], muse_spec_dict['y_pos'])
# # plt.scatter(muse_spec_dict['x_pos'][bin_id], muse_spec_dict['y_pos'][bin_id])
# # plt.show()
# # exit()
# # plt.scatter(np.arange(0, len(muse_spec_dict['wave'][bin_id])), muse_spec_dict['flux'][bin_id], color='k')
#
#
# plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['flux'][bin_id], color='k')
# plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['total_fit'][bin_id])
# plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_contin_1'][bin_id])
# plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_narrow_el_1'][bin_id])
# plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_narrow_el_2'][bin_id])
# plt.scatter(muse_spec_dict['wave'][bin_id], muse_spec_dict['fit_broad_el_1'][bin_id])
#
#
# plt.show()


exit()





data_path = '/home/benutzer/data/galaxies/muse_cubes/ngc6240/ngc6240.fits'

hdu = fits.open(data_path)

print(hdu.info())

header = hdu[1].header
data = hdu[1].data

print(list(header.keys()))

print(data.shape)

print(np.sum(data, axis=0))


image = np.nansum(data, axis=0)

plt.plot(np.arange(len(data[:,225,225])), data[:,225,225])
plt.show()
exit()

plt.imshow(image, vmin=500, vmax=50000)
plt.show()

hdu.close()


