from spec_fit import emission_line_fit, fit_plot
import matplotlib.pyplot as plt
from xgaltool import analysis_tools
from scipy.constants import c as speed_of_light

sf_radio_co_access = analysis_tools.AnalysisTools(object_type='sf_radio_co', table_cross_match='rcsed_co_observation')
proposal_identifier_sf_radio_co = sf_radio_co_access.table['proposal_identifier']
mjd = sf_radio_co_access.get_mjd()
plate = sf_radio_co_access.get_plate()
fiberid = sf_radio_co_access.get_fiberid()
redshift = sf_radio_co_access.get_redshift()
galaxy_of_interest = ((mjd == 52703) & (plate == 1161) & (fiberid == 595))

spec_path = '/home/benutzer/Documents/projects/double_peak_origin_analysis/observation/iram_30m/run_198_19/data_reduction/6_plot_factory/spectrum/'
co10_path = spec_path + 'spectrum_co10_%s.dat' % proposal_identifier_sf_radio_co[galaxy_of_interest][0]
co21_path = spec_path + 'spectrum_co21_%s.dat' % proposal_identifier_sf_radio_co[galaxy_of_interest][0]

galaxy_object = emission_line_fit.EmissionLineFit(sdss_mjd=mjd[galaxy_of_interest],
                                                 sdss_plate=plate[galaxy_of_interest],
                                                 sdss_fiberid=fiberid[galaxy_of_interest],
                                                 co10_spec_path=co10_path, co21_spec_path=co21_path,
                                                 co_redshift=redshift[galaxy_of_interest])

galaxy_object.run_co_fit(n_gauss=2, fit_mode='co10_co21')

print(galaxy_object.fit_param_dict)

vel_co10 = galaxy_object.vel_co10
flux_co10 = galaxy_object.flux_co10
vel_co21 = galaxy_object.vel_co21
flux_co21 = galaxy_object.flux_co21
sys_vel = speed_of_light * 1e-3 * redshift[galaxy_of_interest]


plotting_class = fit_plot.FitPlot()
fig = plotting_class.plot_double_gauss_fit_results_co10_co21(fit_result_dict=galaxy_object.fit_param_dict,
                                                             vel_co10=vel_co10, flux_co10=flux_co10,
                                                             vel_co21=vel_co21, flux_co21=flux_co21,
                                                             sys_vel=sys_vel)

import matplotlib.pyplot as plt

plt.show()



