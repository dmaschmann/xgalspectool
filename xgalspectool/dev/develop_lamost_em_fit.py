from spec_fit import emission_line_fit, fit_plot

lamost_fiberid = 72
lamost_lmjd = 55976
lamost_obsdate = '2012-02-18'
lamost_planid = 'F5597602'
lamost_spid = 5

lamost_helper = emission_line_fit.EmissionLineFit(lamost_fiberid=lamost_fiberid, lamost_lmjd=lamost_lmjd,
                                                  lamost_obsdate=lamost_obsdate, lamost_planid=lamost_planid,
                                                  lamost_spid=lamost_spid)

