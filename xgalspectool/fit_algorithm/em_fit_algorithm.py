"""
The Classes in this file build up basic concepts properties and Physical constants for the galtool project
"""

from xgaltool.basic_attributes import PhysicalConstants

import numpy as np
from scipy.constants import c as speed_of_light
from scipy.stats import f as fisher

from probfit import Chi2Regression
from iminuit import Minuit, describe


class EmissionLineFit(PhysicalConstants):

    def __init__(self):

        super().__init__()

        self.inst_broad = {4863: 0, 4960: 0, 5008: 0, 6302: 0, 6314: 0, 6349: 0,
                           6366: 0, 6550: 0, 6565: 0, 6585: 0, 6718: 0, 6733: 0}

        self.n_gauss = None
        self.n_broad = None
        self.fit_mode = None
        self.two_vel = None
        self.doublet_ratio = None
        self.fit_param_dict = None
        self.line_flux_dict = None

    def set_up_inst_broad(self, inst_broad, line=4863):
        self.inst_broad[line] = inst_broad

    @staticmethod
    def chi2(y_values, y_err_values, function):
        return sum((y_values - function) ** 2 / (y_err_values ** 2))

    @staticmethod
    def compute_f_test(chi2_advanced_model, chi2_simple_model, ndf_advanced_model, ndf_simple_model):
        r"""
        return a bool or a bool array expresing if the advanced_model seems better than the simple_model fit
        following Mendenhall & Sincich (2011)
        This is also explained in detail in Maschmann et al. (2020) or the wikipedia article on f-test
        """
        f_ratio = (((chi2_simple_model - chi2_advanced_model) / chi2_advanced_model) /
                   ((ndf_simple_model - ndf_advanced_model) / ndf_advanced_model))
        p_value = 1 - fisher.cdf(f_ratio, dfn=ndf_simple_model - ndf_advanced_model, dfd=ndf_advanced_model)
        result_ftest = p_value < 0.05
        return result_ftest

    @staticmethod
    def gaussian(x_values, amp, mu, sigma):
        return amp * np.exp(-(x_values - mu) ** 2 / (2 * sigma ** 2))

    def double_gaussian(self, x_values, amp_1, mu_1, sigma_1, amp_2, mu_2, sigma_2):
        return self.gaussian(x_values, amp_1, mu_1, sigma_1) + self.gaussian(x_values, amp_2, mu_2, sigma_2)

    def triple_gaussian(self, x_values, amp_1, mu_1, sigma_1, amp_2, mu_2, sigma_2, amp_3, mu_3, sigma_3):
        return (self.gaussian(x_values, amp_1, mu_1, sigma_1) + self.gaussian(x_values, amp_2, mu_2, sigma_2) +
                self.gaussian(x_values, amp_3, mu_3, sigma_3))

    def gaussian_hi_line(self, x_values, mu, sigma, amp_hi):

        return (self.gaussian(x_values=x_values, amp=amp_hi,
                              mu=(mu / (speed_of_light * 1e-3) * self.hi_vac_wave + self.hi_vac_wave),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[4863] ** 2) / (speed_of_light * 1e-3) *
                                     self.hi_vac_wave)))

    def gaussian_co10_line(self, x_values, mu, sigma, amp_co10):

        return (self.gaussian(x_values=x_values, amp=amp_co10,
                              mu=(mu / (speed_of_light * 1e-3) * self.co10_vac_wave + self.co10_vac_wave),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[4863] ** 2) / (speed_of_light * 1e-3) *
                                     self.co10_vac_wave)))

    def gaussian_co21_line(self, x_values, mu, sigma, amp_co21):

        return (self.gaussian(x_values=x_values, amp=amp_co21,
                              mu=(mu / (speed_of_light * 1e-3) * self.co21_vac_wave + self.co21_vac_wave),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[4863] ** 2) / (speed_of_light * 1e-3) *
                                     self.co21_vac_wave)))

    def gaussian_balmer_lines(self, x_values, mu, sigma, amp_4863, amp_6565):

        return (self.gaussian(x_values=x_values, amp=amp_4863,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[4863]['vac_wave'] +
                                  self.em_wavelength[4863]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[4863] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[4863]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6565,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6565]['vac_wave'] +
                                  self.em_wavelength[6565]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6565] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6565]['vac_wave'])))

    def gaussian_forbidden_lines_simple(self, x_values, mu, sigma, amp_5008, amp_6550, amp_6585):

        return (self.gaussian(x_values=x_values, amp=amp_5008,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[5008]['vac_wave'] +
                                  self.em_wavelength[5008]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[5008] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[5008]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6550,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'] +
                                  self.em_wavelength[6550]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6550] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6550]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6585,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'] +
                                  self.em_wavelength[6585]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6585] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6585]['vac_wave'])))

    def gaussian_forbidden_lines_complex(self, x_values, mu, sigma, amp_4960, amp_5008, amp_6302, amp_6550, amp_6585,
                                         amp_6718, amp_6733):

        return (self.gaussian(x_values=x_values, amp=amp_4960,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[4960]['vac_wave'] +
                                  self.em_wavelength[4960]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[4960] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[4960]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_5008,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[5008]['vac_wave'] +
                                  self.em_wavelength[5008]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[5008] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[5008]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6302,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6302]['vac_wave'] +
                                  self.em_wavelength[6302]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6302] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6302]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6550,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'] +
                                  self.em_wavelength[6550]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6550] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6550]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6585,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'] +
                                  self.em_wavelength[6585]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6585] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6585]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6718,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'] +
                                  self.em_wavelength[6718]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6718] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6718]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6733,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'] +
                                  self.em_wavelength[6733]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6733] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6733]['vac_wave'])))

    def gaussian_forbidden_lines_muse_weak(self, x_values, mu, sigma, amp_4960, amp_5008, amp_6302, amp_6314, amp_6349,
                                           amp_6366, amp_6550, amp_6585, amp_6718, amp_6733):

        return (self.gaussian(x_values=x_values, amp=amp_4960,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[4960]['vac_wave'] +
                                  self.em_wavelength[4960]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[4960] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[4960]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_5008,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[5008]['vac_wave'] +
                                  self.em_wavelength[5008]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[5008] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[5008]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6302,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6302]['vac_wave'] +
                                  self.em_wavelength[6302]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6302] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6302]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6314,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6314]['vac_wave'] +
                                  self.em_wavelength[6314]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6314] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6314]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6349,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6349]['vac_wave'] +
                                  self.em_wavelength[6349]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6349] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6349]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6366,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6366]['vac_wave'] +
                                  self.em_wavelength[6366]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6366] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6366]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6550,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6550]['vac_wave'] +
                                  self.em_wavelength[6550]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6550] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6550]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6585,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6585]['vac_wave'] +
                                  self.em_wavelength[6585]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6585] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6585]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6718,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6718]['vac_wave'] +
                                  self.em_wavelength[6718]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6718] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6718]['vac_wave'])) +
                self.gaussian(x_values=x_values, amp=amp_6733,
                              mu=(mu / (speed_of_light * 1e-3) * self.em_wavelength[6733]['vac_wave'] +
                                  self.em_wavelength[6733]['vac_wave']),
                              sigma=(np.sqrt(sigma ** 2 + self.inst_broad[6733] ** 2) / (speed_of_light * 1e-3) *
                                     self.em_wavelength[6733]['vac_wave'])))

    # single gaussian models
    def single_gauss_simple_one_vel(self, x_values, mu, sigma, amp_4863, amp_5008, amp_6550, amp_6565, amp_6585):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu, sigma=sigma,
                                                     amp_5008=amp_5008, amp_6550=amp_6550, amp_6585=amp_6585))

    def single_gauss_complex_one_vel(self, x_values, mu, sigma, amp_4863, amp_4960, amp_5008, amp_6302, amp_6550,
                                     amp_6565, amp_6585, amp_6718, amp_6733):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu, sigma=sigma,
                                                      amp_4960=amp_4960, amp_5008=amp_5008, amp_6302=amp_6302,
                                                      amp_6550=amp_6550, amp_6585=amp_6585, amp_6718=amp_6718,
                                                      amp_6733=amp_6733))

    def single_gauss_simple_one_vel_doublet_ratio(self, x_values, mu, sigma, amp_4863, amp_5008, amp_6565, amp_6585):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu, sigma=sigma,
                                                     amp_5008=amp_5008, amp_6550=amp_6585 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585))

    def single_gauss_complex_one_vel_doublet_ratio(self, x_values, mu, sigma, amp_4863, amp_5008, amp_6302, amp_6565,
                                                   amp_6585, amp_6718, amp_6733):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu, sigma=sigma,
                                                      amp_4960=amp_5008 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008, amp_6302=amp_6302,
                                                      amp_6550=amp_6585 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585, amp_6718=amp_6718,
                                                      amp_6733=amp_6733))

    def single_gauss_single_broad_complex_one_vel_doublet_ratio(self, x_values, mu, sigma, mu_broad, sigma_broad,
                                                                amp_4863, amp_4863_broad, amp_5008, amp_6302, amp_6565,
                                                                 amp_6565_broad, amp_6585, amp_6718, amp_6733):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_broad, sigma=sigma_broad,
                                           amp_4863=amp_4863_broad, amp_6565=amp_6565_broad) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu, sigma=sigma,
                                                      amp_4960=amp_5008 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008, amp_6302=amp_6302,
                                                      amp_6550=amp_6585 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585, amp_6718=amp_6718,
                                                      amp_6733=amp_6733))

    def single_gauss_simple_two_vel(self, x_values, mu_balmer, sigma_balmer, mu_forbidden, sigma_forbidden,
                                    amp_4863, amp_5008, amp_6550, amp_6565, amp_6585):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer, sigma=sigma_balmer,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_forbidden, sigma=sigma_forbidden,
                                                     amp_5008=amp_5008, amp_6550=amp_6550, amp_6585=amp_6585))

    def single_gauss_complex_two_vel(self, x_values, mu_balmer, sigma_balmer, mu_forbidden, sigma_forbidden,
                                     amp_4863, amp_4960, amp_5008, amp_6302, amp_6550,
                                     amp_6565, amp_6585, amp_6718, amp_6733):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer, sigma=sigma_balmer,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden, sigma=sigma_forbidden,
                                                      amp_4960=amp_4960, amp_5008=amp_5008, amp_6302=amp_6302,
                                                      amp_6550=amp_6550, amp_6585=amp_6585, amp_6718=amp_6718,
                                                      amp_6733=amp_6733))

    def single_gauss_simple_two_vel_doublet_ratio(self, x_values, mu_balmer, sigma_balmer, mu_forbidden,
                                                  sigma_forbidden, amp_4863, amp_5008, amp_6565, amp_6585):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer, sigma=sigma_balmer,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_forbidden, sigma=sigma_forbidden,
                                                     amp_5008=amp_5008, amp_6550=amp_6585 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585))

    def single_gauss_complex_two_vel_doublet_ratio(self, x_values, mu_balmer, sigma_balmer,
                                                   mu_forbidden, sigma_forbidden,
                                                   amp_4863, amp_5008, amp_6302, amp_6565, amp_6585,
                                                   amp_6718, amp_6733):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer, sigma=sigma_balmer,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden, sigma=sigma_forbidden,
                                                      amp_4960=amp_5008 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008, amp_6302=amp_6302,
                                                      amp_6550=amp_6585 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585, amp_6718=amp_6718, amp_6733=amp_6733))

    def single_gauss_co10(self, x_values, mu, sigma, amp_co10):
        return self.gaussian_co10_line(x_values=x_values, mu=mu, sigma=sigma, amp_co10=amp_co10)

    def single_gauss_co21(self, x_values, mu, sigma, amp_co21):
        return self.gaussian_co21_line(x_values=x_values, mu=mu, sigma=sigma, amp_co21=amp_co21)

    def single_gauss_co10_co21(self, x_values, mu, sigma, amp_co10, amp_co21):
        return(self.gaussian_co10_line(x_values=x_values, mu=mu, sigma=sigma, amp_co10=amp_co10) +
               self.gaussian_co21_line(x_values=x_values, mu=mu, sigma=sigma, amp_co21=amp_co21))

    def single_gauss_simple_co10_co21_one_vel_doublet_ratio(self, x_values, mu, sigma, amp_4863, amp_5008,
                                                            amp_6565, amp_6585, amp_co10, amp_co21):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu, sigma=sigma,
                                                     amp_5008=amp_5008, amp_6550=amp_6585 / 3.,
                                                     amp_6585=amp_6585) +
                self.gaussian_co10_line(x_values=x_values, mu=mu, sigma=sigma, amp_co10=amp_co10) +
                self.gaussian_co21_line(x_values=x_values, mu=mu, sigma=sigma, amp_co21=amp_co21))

    def single_gauss_complex_co10_co21_one_vel_doublet_ratio(self, x_values, mu, sigma, amp_4863, amp_5008, amp_6302,
                                                             amp_6565, amp_6585, amp_6718, amp_6733, amp_co10,
                                                             amp_co21):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu, sigma=sigma,
                                           amp_4863=amp_4863, amp_6565=amp_6565) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu, sigma=sigma,
                                                      amp_4960=amp_5008 / 3., amp_5008=amp_5008,
                                                      amp_6302=amp_6302, amp_6550=amp_6585 / 3.,
                                                      amp_6585=amp_6585, amp_6718=amp_6718, amp_6733=amp_6733) +
                self.gaussian_co10_line(x_values=x_values, mu=mu, sigma=sigma, amp_co10=amp_co10) +
                self.gaussian_co21_line(x_values=x_values, mu=mu, sigma=sigma, amp_co21=amp_co21))

    # double gaussian models
    def double_gauss_simple_one_vel(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                    amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                    amp_6550_1, amp_6550_2, amp_6565_1, amp_6565_2, amp_6585_1, amp_6585_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                     amp_5008=amp_5008_1, amp_6550=amp_6550_1, amp_6585=amp_6585_1) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                     amp_5008=amp_5008_2, amp_6550=amp_6550_2, amp_6585=amp_6585_2))

    def double_gauss_complex_one_vel(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                     amp_4863_1, amp_4863_2, amp_4960_1, amp_4960_2, amp_5008_1, amp_5008_2,
                                     amp_6302_1, amp_6302_2, amp_6550_1, amp_6550_2, amp_6565_1, amp_6565_2,
                                     amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2, amp_6733_1, amp_6733_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                      amp_4960=amp_4960_1, amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6550_1, amp_6585=amp_6585_1, amp_6718=amp_6718_1,
                                                      amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                      amp_4960=amp_4960_2, amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6550_2, amp_6585=amp_6585_2, amp_6718=amp_6718_2,
                                                      amp_6733=amp_6733_2))

    def double_gauss_simple_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                  amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                  amp_6565_1, amp_6565_2, amp_6585_1, amp_6585_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_5008=amp_5008_1,
                                                     amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585_1) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_5008=amp_5008_2,
                                                     amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585_2))

    def double_gauss_complex_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                   amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                   amp_6302_1, amp_6302_2, amp_6565_1, amp_6565_2,
                                                   amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2,
                                                   amp_6733_1, amp_6733_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2))

    def triple_gauss_complex_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2, mu_3, sigma_3,
                                                   amp_4863_1, amp_4863_2, amp_4863_3,
                                                   amp_5008_1, amp_5008_2, amp_5008_3,
                                                   amp_6302_1, amp_6302_2, amp_6302_3,
                                                   amp_6565_1, amp_6565_2, amp_6565_3,
                                                   amp_6585_1, amp_6585_2, amp_6585_3,
                                                   amp_6718_1, amp_6718_2, amp_6718_3,
                                                   amp_6733_1, amp_6733_2, amp_6733_3):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_3, sigma=sigma_3,
                                           amp_4863=amp_4863_3, amp_6565=amp_6565_3) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_3, sigma=sigma_3,
                                                      amp_4960=amp_5008_3 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_3, amp_6302=amp_6302_3,
                                                      amp_6550=amp_6585_3 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_3, amp_6718=amp_6718_3, amp_6733=amp_6733_3))

    def double_gauss_single_broad_complex_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                                mu_broad, sigma_broad,
                                                                amp_4863_1, amp_4863_2, amp_4863_broad,
                                                                amp_5008_1, amp_5008_2, amp_6302_1, amp_6302_2,
                                                                amp_6565_1, amp_6565_2, amp_6565_broad,
                                                                amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2,
                                                                amp_6733_1, amp_6733_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_broad, sigma=sigma_broad,
                                           amp_4863=amp_4863_broad, amp_6565=amp_6565_broad) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2))

    def double_gauss_simple_two_vel(self, x_values, mu_balmer_1, sigma_balmer_1, mu_balmer_2, sigma_balmer_2,
                                    mu_forbidden_1, sigma_forbidden_1, mu_forbidden_2, sigma_forbidden_2,
                                    amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2, amp_6550_1, amp_6550_2,
                                    amp_6565_1, amp_6565_2, amp_6585_1, amp_6585_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_1, sigma=sigma_balmer_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_2, sigma=sigma_balmer_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_forbidden_1, sigma=sigma_forbidden_1,
                                                     amp_5008=amp_5008_1, amp_6550=amp_6550_1, amp_6585=amp_6585_1) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_forbidden_2, sigma=sigma_forbidden_2,
                                                     amp_5008=amp_5008_2, amp_6550=amp_6550_2, amp_6585=amp_6585_2))

    def double_gauss_complex_two_vel(self, x_values, mu_balmer_1, sigma_balmer_1, mu_balmer_2, sigma_balmer_2,
                                     mu_forbidden_1, sigma_forbidden_1, mu_forbidden_2, sigma_forbidden_2,
                                     amp_4863_1, amp_4863_2, amp_4960_1, amp_4960_2, amp_5008_1, amp_5008_2,
                                     amp_6302_1, amp_6302_2, amp_6550_1, amp_6550_2, amp_6565_1, amp_6565_2,
                                     amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2, amp_6733_1, amp_6733_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_1, sigma=sigma_balmer_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_2, sigma=sigma_balmer_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_1, sigma=sigma_forbidden_1,
                                                      amp_4960=amp_4960_1, amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6550_1, amp_6585=amp_6585_1, amp_6718=amp_6718_1,
                                                      amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_2, sigma=sigma_forbidden_2,
                                                      amp_4960=amp_4960_2, amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6550_2, amp_6585=amp_6585_2, amp_6718=amp_6718_2,
                                                      amp_6733=amp_6733_2))

    def double_gauss_simple_two_vel_doublet_ratio(self, x_values, mu_balmer_1, sigma_balmer_1,
                                                  mu_balmer_2, sigma_balmer_2,
                                                  mu_forbidden_1, sigma_forbidden_1, mu_forbidden_2, sigma_forbidden_2,
                                                  amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                  amp_6565_1, amp_6565_2, amp_6585_1, amp_6585_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_1, sigma=sigma_balmer_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_2, sigma=sigma_balmer_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_forbidden_1, sigma=sigma_forbidden_1,
                                                     amp_5008=amp_5008_1,
                                                     amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585_1) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_forbidden_2, sigma=sigma_forbidden_2,
                                                     amp_5008=amp_5008_2,
                                                     amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585_2))

    def double_gauss_complex_two_vel_doublet_ratio(self, x_values, mu_balmer_1, sigma_balmer_1,
                                                   mu_balmer_2, sigma_balmer_2,
                                                   mu_forbidden_1, sigma_forbidden_1, mu_forbidden_2, sigma_forbidden_2,
                                                   amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                   amp_6302_1, amp_6302_2, amp_6565_1, amp_6565_2,
                                                   amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2,
                                                   amp_6733_1, amp_6733_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_1, sigma=sigma_balmer_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_2, sigma=sigma_balmer_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_1, sigma=sigma_forbidden_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_2, sigma=sigma_forbidden_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2,  amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2))

    def triple_gauss_complex_two_vel_doublet_ratio(self, x_values, mu_balmer_1, sigma_balmer_1,
                                                   mu_balmer_2, sigma_balmer_2,  mu_balmer_3, sigma_balmer_3,
                                                   mu_forbidden_1, sigma_forbidden_1, mu_forbidden_2, sigma_forbidden_2,
                                                   mu_forbidden_3, sigma_forbidden_3,
                                                   amp_4863_1, amp_4863_2, amp_4863_3,
                                                   amp_5008_1, amp_5008_2, amp_5008_3,
                                                   amp_6302_1, amp_6302_2, amp_6302_3,
                                                   amp_6565_1, amp_6565_2, amp_6565_3,
                                                   amp_6585_1, amp_6585_2, amp_6585_3,
                                                   amp_6718_1, amp_6718_2, amp_6718_3,
                                                   amp_6733_1, amp_6733_2, amp_6733_3):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_1, sigma=sigma_balmer_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_2, sigma=sigma_balmer_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_balmer_3, sigma=sigma_balmer_3,
                                           amp_4863=amp_4863_3, amp_6565=amp_6565_3) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_1, sigma=sigma_forbidden_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_2, sigma=sigma_forbidden_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2,  amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_forbidden_3, sigma=sigma_forbidden_3,
                                                      amp_4960=amp_5008_3 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_3,  amp_6302=amp_6302_3,
                                                      amp_6550=amp_6585_3 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_3, amp_6718=amp_6718_3, amp_6733=amp_6733_3))

    def double_gauss_co10(self, x_values, mu_1, sigma_1, mu_2, sigma_2, amp_co10_1, amp_co10_2):
        return (self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2))

    def double_gauss_co21(self, x_values, mu_1, sigma_1, mu_2, sigma_2, amp_co21_1, amp_co21_2):
        return (self.gaussian_co21_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co21=amp_co21_1) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co21=amp_co21_2))

    def double_gauss_co10_co21(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                               amp_co10_1, amp_co10_2, amp_co21_1, amp_co21_2):
        return (self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co21=amp_co21_1) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co21=amp_co21_2))

    def double_gauss_complex_co10_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                        amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                        amp_6302_1, amp_6302_2, amp_6565_1, amp_6565_2,
                                                        amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2,
                                                        amp_6733_1, amp_6733_2, amp_co10_1, amp_co10_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2))

    def double_gauss_simple_co10_co21_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                            amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                            amp_6565_1, amp_6565_2, amp_6585_1, amp_6585_2,
                                                            amp_co10_1, amp_co10_2, amp_co21_1, amp_co21_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_5008=amp_5008_1,
                                                     amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585_1) +
                self.gaussian_forbidden_lines_simple(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_5008=amp_5008_2,
                                                     amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                     amp_6585=amp_6585_2) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co21=amp_co21_1) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co21=amp_co21_2))

    def double_gauss_complex_co10_co21_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                             amp_4863_1, amp_4863_2, amp_5008_1, amp_5008_2,
                                                             amp_6302_1, amp_6302_2, amp_6565_1, amp_6565_2,
                                                             amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2,
                                                             amp_6733_1, amp_6733_2,
                                                             amp_co10_1, amp_co10_2, amp_co21_1, amp_co21_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                      amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                      amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_complex(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                      amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                      amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                      amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                      amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co21=amp_co21_1) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co21=amp_co21_2))

    def double_gauss_single_broad_muse_weak_one_vel_doublet_ratio(self, x_values, mu_1, sigma_1, mu_2, sigma_2,
                                                                  mu_broad, sigma_broad,
                                                                  amp_4863_1, amp_4863_2, amp_4863_broad,
                                                                  amp_5008_1, amp_5008_2,
                                                                  amp_6302_1, amp_6302_2, amp_6314_1, amp_6314_2,
                                                                  amp_6349_1, amp_6349_2,
                                                                  amp_6565_1, amp_6565_2, amp_6565_broad,
                                                                  amp_6585_1, amp_6585_2, amp_6718_1, amp_6718_2,
                                                                  amp_6733_1, amp_6733_2):
        return (self.gaussian_balmer_lines(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                           amp_4863=amp_4863_1, amp_6565=amp_6565_1) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                           amp_4863=amp_4863_2, amp_6565=amp_6565_2) +
                self.gaussian_balmer_lines(x_values=x_values, mu=mu_broad, sigma=sigma_broad,
                                           amp_4863=amp_4863_broad, amp_6565=amp_6565_broad) +
                self.gaussian_forbidden_lines_muse_weak(x_values=x_values, mu=mu_1, sigma=sigma_1,
                                                        amp_4960=amp_5008_1 / self.em_ratios['5008/4960'],
                                                        amp_5008=amp_5008_1, amp_6302=amp_6302_1,
                                                        amp_6314=amp_6314_1, amp_6349=amp_6349_1,
                                                        amp_6366=amp_6302_1 / self.em_ratios['6302/6366'],
                                                        amp_6550=amp_6585_1 / self.em_ratios['6585/6550'],
                                                        amp_6585=amp_6585_1, amp_6718=amp_6718_1, amp_6733=amp_6733_1) +
                self.gaussian_forbidden_lines_muse_weak(x_values=x_values, mu=mu_2, sigma=sigma_2,
                                                        amp_4960=amp_5008_2 / self.em_ratios['5008/4960'],
                                                        amp_5008=amp_5008_2, amp_6302=amp_6302_2,
                                                        amp_6314=amp_6314_2, amp_6349=amp_6349_2,
                                                        amp_6366=amp_6302_2 / self.em_ratios['6302/6366'],
                                                        amp_6550=amp_6585_2 / self.em_ratios['6585/6550'],
                                                        amp_6585=amp_6585_2, amp_6718=amp_6718_2, amp_6733=amp_6733_2))

    def triple_gauss_co10(self, x_values, mu_1, sigma_1, amp_co10_1, mu_2, sigma_2, amp_co10_2, mu_3, sigma_3,
                          amp_co10_3):
        return (self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_3, sigma=sigma_3, amp_co10=amp_co10_3))

    def triple_gauss_co21(self, x_values, mu_1, sigma_1, amp_co21_1, mu_2, sigma_2, amp_co21_2, mu_3, sigma_3,
                          amp_co21_3):
        return (self.gaussian_co21_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co21=amp_co21_1) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co21=amp_co21_2) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_3, sigma=sigma_3, amp_co21=amp_co21_3))

    def triple_gauss_co10_co21(self, x_values, mu_1, sigma_1, amp_co10_1, amp_co21_1,
                               mu_2, sigma_2, amp_co10_2, amp_co21_2,
                               mu_3, sigma_3, amp_co10_3, amp_co21_3):
        return (self.gaussian_co10_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co10=amp_co10_1) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co10=amp_co10_2) +
                self.gaussian_co10_line(x_values=x_values, mu=mu_3, sigma=sigma_3, amp_co10=amp_co10_3) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_co21=amp_co21_1) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_co21=amp_co21_2) +
                self.gaussian_co21_line(x_values=x_values, mu=mu_3, sigma=sigma_3, amp_co21=amp_co21_3))

    def single_gauss_hi(self, x_values, mu, sigma, amp_hi):
        return self.gaussian_hi_line(x_values=x_values, mu=mu, sigma=sigma, amp_hi=amp_hi)

    def double_gauss_hi(self, x_values, mu_1, sigma_1, amp_hi_1, mu_2, sigma_2, amp_hi_2):
        return (self.gaussian_hi_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_hi=amp_hi_1) +
                self.gaussian_hi_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_hi=amp_hi_2))

    def triple_gauss_hi(self, x_values, mu_1, sigma_1, amp_hi_1, mu_2, sigma_2, amp_hi_2, mu_3, sigma_3, amp_hi_3):
        return (self.gaussian_hi_line(x_values=x_values, mu=mu_1, sigma=sigma_1, amp_hi=amp_hi_1) +
                self.gaussian_hi_line(x_values=x_values, mu=mu_2, sigma=sigma_2, amp_hi=amp_hi_2) +
                self.gaussian_hi_line(x_values=x_values, mu=mu_3, sigma=sigma_3, amp_hi=amp_hi_3))

    def setup_fit_configuration(self, n_gauss=1, n_broad=0, fit_mode='simple', two_vel=False, doublet_ratio=True):
        """ configures fitting procedure with all keywords

        Parameters
        ----------

        n_gauss: int (default: 1)
          number of gaussian components fitted to the emission lines
        n_broad: int (default: 0)
          number of gaussian broad line balmer components fitted to the emission lines
        fit_mode : str (default: 'simple')
          'simple' for fitted lines: [4863, 5008, 6565, 6585]
          'complex' for fitted lines: [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]
          'muse_weak' for fitted lines: [4863, 4960, 5008, 6302, 6314, 6349, 6366, 6550, 6565, 6585, 6718, 6733]
        two_vel : bool (default: False)
          boolean flag if balmer and forbidden lines should be considered as independent components
        doublet_ratio: bool (default: True)
          flag empirical doublet emission line ratios for the [NII] doublet and [OIII] doublet should be used

        Returns
        -------
        None

        """

        self.n_gauss = n_gauss
        self.n_broad = n_broad
        self.fit_mode = fit_mode
        self.two_vel = two_vel
        self.doublet_ratio = doublet_ratio

    def select_fit_model(self):
        """ selection of fitting function from choice of parameters

        Returns
        -------
        func

        """

        function_string = ''

        if self.n_gauss == 1:
            function_string += 'single_gauss'
        elif self.n_gauss == 2:
            function_string += 'double_gauss'
        elif self.n_gauss == 3:
            function_string += 'triple_gauss'
        else:
            raise KeyError('you can only have 1, 2 or 3 Gaussian components')

        if self.n_broad == 1:
            function_string += '_single_broad'

        if 'simple' in self.fit_mode:
            function_string += '_simple'
        elif 'complex' in self.fit_mode:
            function_string += '_complex'
        elif 'muse_weak' in self.fit_mode:
            function_string += '_muse_weak'

        if 'co10' in self.fit_mode:
            function_string += '_co10'
        if 'co21' in self.fit_mode:
            function_string += '_co21'

        if 'hi' in self.fit_mode:
            function_string += '_hi'

        if self.two_vel is not None:
            if self.two_vel:
                function_string += '_two_vel'
            else:
                function_string += '_one_vel'

        if self.doublet_ratio:
            function_string += '_doublet_ratio'

        return getattr(self, function_string)

    def get_indep_line_list(self, line_type=None):
        """ returns a list of all emission lines according to fit configuration

        Returns
        -------
        list

        """
        line_list = []

        if line_type is None:
            if 'simple' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [4863, 5008, 6565, 6585]:
                        line_list.append(line)
                else:
                    for line in [4863, 5008, 6550, 6565, 6585]:
                        line_list.append(line)
            elif 'complex' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [4863, 5008, 6302, 6565, 6585, 6718, 6733]:
                        line_list.append(line)
                else:
                    for line in [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]:
                        line_list.append(line)
            elif 'muse_weak' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [4863, 5008, 6302, 6314, 6349, 6565, 6585, 6718, 6733]:
                        line_list.append(line)
                else:
                    for line in [4863, 4960, 5008, 6302, 6314, 6349, 6366, 6550, 6565, 6585, 6718, 6733]:
                        line_list.append(line)
            if 'co10' in self.fit_mode:
                line_list.append('co10')
            if 'co21' in self.fit_mode:
                line_list.append('co21')
            if 'hi' in self.fit_mode:
                line_list.append('hi')
            return line_list
        elif line_type == 'balmer':
            if 'simple' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [4863, 6565]:
                        line_list.append(line)
                else:
                    for line in [4863, 6565]:
                        line_list.append(line)
            elif 'complex' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [4863, 6565]:
                        line_list.append(line)
                else:
                    for line in [4863, 6565]:
                        line_list.append(line)
            return line_list

        elif line_type == 'forbidden':
            if 'simple' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [5008, 6585]:
                        line_list.append(line)
                else:
                    for line in [5008, 6550, 6585]:
                        line_list.append(line)
            elif 'complex' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [5008, 6302, 6585, 6718, 6733]:
                        line_list.append(line)
                else:
                    for line in [4960, 5008, 6302, 6550, 6585, 6718, 6733]:
                        line_list.append(line)
            elif 'muse_weak' in self.fit_mode:
                if self.doublet_ratio:
                    for line in [5008, 6302, 6314, 6349, 6585, 6718, 6733]:
                        line_list.append(line)
                else:
                    for line in [4960, 5008, 6302, 6314, 6349, 6366, 6550, 6585, 6718, 6733]:
                        line_list.append(line)
            return line_list
        elif line_type == 'co':
            if 'co10' in self.fit_mode:
                line_list.append('co10')
            if 'co21' in self.fit_mode:
                line_list.append('co21')
            return line_list
        elif line_type == 'hi':
            if 'hi' in self.fit_mode:
                line_list.append('hi')
            return line_list
        else:
            raise KeyError('line type must be None, balmer, forbidden or co')

    def get_full_line_list(self, line_type=None):
        """ returns a list of all emission lines according to fit configuration

        Returns
        -------
        list

        """
        line_list = []
        if line_type is None:
            if self.fit_mode is not None:
                if 'simple' in self.fit_mode:
                    line_list = [4863, 5008, 6550, 6565, 6585]
                elif 'complex' in self.fit_mode:
                    line_list = [4863, 4960, 5008, 6302, 6550, 6565, 6585, 6718, 6733]
                elif 'muse_weak' in self.fit_mode:
                    line_list = [4863, 4960, 5008, 6302, 6314, 6349, 6366, 6550, 6565, 6585, 6718, 6733]
            if 'co10' in self.fit_mode:
                line_list.append('co10')
            if 'co21' in self.fit_mode:
                line_list.append('co21')
            if 'hi' in self.fit_mode:
                line_list.append('hi')
            return line_list
        elif line_type == 'balmer':
            if self.fit_mode is not None:
                if 'simple' in self.fit_mode:
                    line_list = [4863, 6565]
                elif 'complex' in self.fit_mode:
                    line_list = [4863, 6565]
            return line_list

        elif line_type == 'forbidden':
            if self.fit_mode is not None:
                if 'simple' in self.fit_mode:
                    line_list = [5008, 6550, 6585]
                elif 'complex' in self.fit_mode:
                    line_list = [4960, 5008, 6302, 6550, 6585, 6718, 6733]
                elif 'muse_weak' in self.fit_mode:
                    line_list = [4960, 5008, 6302, 6314, 6349, 6366, 6550, 6585, 6718, 6733]
            return line_list

        elif line_type == 'co':
            if 'co10' in self.fit_mode:
                line_list.append('co10')
            if 'co21' in self.fit_mode:
                line_list.append('co21')
            return line_list

    def get_fit_parameter_list(self):
        """ returns a list of all parameters according to a fitting configuration

        Returns
        -------
        list

        """

        parameter_list = []

        # add mu and sigma
        if (self.two_vel is False) | (self.two_vel is None):
            if self.n_gauss == 1:
                parameter_list.append('mu')
                parameter_list.append('sigma')
            elif self.n_gauss == 2:
                parameter_list.append('mu_1')
                parameter_list.append('sigma_1')
                parameter_list.append('mu_2')
                parameter_list.append('sigma_2')
            elif self.n_gauss == 3:
                parameter_list.append('mu_1')
                parameter_list.append('sigma_1')
                parameter_list.append('mu_2')
                parameter_list.append('sigma_2')
                parameter_list.append('mu_3')
                parameter_list.append('sigma_3')
            else:
                raise KeyError('you can only have 1, 2 or 3 Gaussian components')
        else:
            if self.n_gauss == 1:
                parameter_list.append('mu_balmer')
                parameter_list.append('sigma_balmer')
                parameter_list.append('mu_forbidden')
                parameter_list.append('sigma_forbidden')
            elif self.n_gauss == 2:
                parameter_list.append('mu_balmer_1')
                parameter_list.append('sigma_balmer_1')
                parameter_list.append('mu_balmer_2')
                parameter_list.append('sigma_balmer_2')
                parameter_list.append('mu_forbidden_1')
                parameter_list.append('sigma_forbidden_1')
                parameter_list.append('mu_forbidden_2')
                parameter_list.append('sigma_forbidden_2')
            elif self.n_gauss == 3:
                parameter_list.append('mu_balmer_1')
                parameter_list.append('sigma_balmer_1')
                parameter_list.append('mu_balmer_2')
                parameter_list.append('sigma_balmer_2')
                parameter_list.append('mu_balmer_3')
                parameter_list.append('sigma_balmer_3')
                parameter_list.append('mu_forbidden_1')
                parameter_list.append('sigma_forbidden_1')
                parameter_list.append('mu_forbidden_2')
                parameter_list.append('sigma_forbidden_2')
                parameter_list.append('mu_forbidden_3')
                parameter_list.append('sigma_forbidden_3')
            else:
                raise KeyError('you can only have 1, 2 or 3 Gaussian components')
        if self.n_broad == 1:
            parameter_list.append('mu_broad')
            parameter_list.append('sigma_broad')

        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                parameter_list.append('amp_%s' % str(line))
            elif self.n_gauss == 2:
                parameter_list.append('amp_%s_1' % str(line))
                parameter_list.append('amp_%s_2' % str(line))
            elif self.n_gauss == 3:
                parameter_list.append('amp_%s_1' % str(line))
                parameter_list.append('amp_%s_2' % str(line))
                parameter_list.append('amp_%s_3' % str(line))
            else:
                raise KeyError('you can only have 1, 2 or 3 Gaussian components')
            if (self.n_broad == 1) & (line in self.em_wavelength.keys()):
                if self.em_wavelength[line]['transition'] == 'balmer':
                    parameter_list.append('amp_%s_broad' % str(line))

        return parameter_list

    def fill_fit_parameter_dict(self, migraded_fit):

        # fill mu and sigma
        if self.two_vel:
            if self.n_gauss == 1:
                if ('complex' in self.fit_mode) | ('simple' in self.fit_mode) | ('muse_weak' in self.fit_mode):
                    self.fit_param_dict.update({'mu_balmer': migraded_fit.values['mu_balmer']})
                    self.fit_param_dict.update({'mu_balmer_err': migraded_fit.errors['mu_balmer']})
                    self.fit_param_dict.update({'sigma_balmer': migraded_fit.values['sigma_balmer']})
                    self.fit_param_dict.update({'sigma_balmer_err': migraded_fit.errors['sigma_balmer']})

                    self.fit_param_dict.update({'mu_forbidden': migraded_fit.values['mu_forbidden']})
                    self.fit_param_dict.update({'mu_forbidden_err': migraded_fit.errors['mu_forbidden']})
                    self.fit_param_dict.update({'sigma_forbidden': migraded_fit.values['sigma_forbidden']})
                    self.fit_param_dict.update({'sigma_forbidden_err': migraded_fit.errors['sigma_forbidden']})

                if 'co10' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co10': migraded_fit.values['mu_balmer']})
                    self.fit_param_dict.update({'mu_co10_err': migraded_fit.errors['mu_balmer']})
                    self.fit_param_dict.update({'sigma_co10': migraded_fit.values['sigma_balmer']})
                    self.fit_param_dict.update({'sigma_co10_err': migraded_fit.errors['sigma_balmer']})
                if 'co21' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co21': migraded_fit.values['mu_balmer']})
                    self.fit_param_dict.update({'mu_co21_err': migraded_fit.errors['mu_balmer']})
                    self.fit_param_dict.update({'sigma_co21': migraded_fit.values['sigma_balmer']})
                    self.fit_param_dict.update({'sigma_co21_err': migraded_fit.errors['sigma_balmer']})

            elif self.n_gauss == 2:
                if ('complex' in self.fit_mode) | ('simple' in self.fit_mode) | ('muse_weak' in self.fit_mode):
                    self.fit_param_dict.update({'mu_balmer_1': migraded_fit.values['mu_balmer_1']})
                    self.fit_param_dict.update({'mu_balmer_1_err': migraded_fit.errors['mu_balmer_1']})
                    self.fit_param_dict.update({'sigma_balmer_1': migraded_fit.values['sigma_balmer_1']})
                    self.fit_param_dict.update({'sigma_balmer_1_err': migraded_fit.errors['sigma_balmer_1']})

                    self.fit_param_dict.update({'mu_balmer_2': migraded_fit.values['mu_balmer_2']})
                    self.fit_param_dict.update({'mu_balmer_2_err': migraded_fit.errors['mu_balmer_2']})
                    self.fit_param_dict.update({'sigma_balmer_2': migraded_fit.values['sigma_balmer_2']})
                    self.fit_param_dict.update({'sigma_balmer_2_err': migraded_fit.errors['sigma_balmer_2']})

                    self.fit_param_dict.update({'mu_forbidden_1': migraded_fit.values['mu_forbidden_1']})
                    self.fit_param_dict.update({'mu_forbidden_1_err': migraded_fit.errors['mu_forbidden_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1': migraded_fit.values['sigma_forbidden_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1_err': migraded_fit.errors['sigma_forbidden_1']})

                    self.fit_param_dict.update({'mu_forbidden_2': migraded_fit.values['mu_forbidden_2']})
                    self.fit_param_dict.update({'mu_forbidden_2_err': migraded_fit.errors['mu_forbidden_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2': migraded_fit.values['sigma_forbidden_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2_err': migraded_fit.errors['sigma_forbidden_2']})

                if 'co10' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co10_1': migraded_fit.values['mu_balmer_1']})
                    self.fit_param_dict.update({'mu_co10_1_err': migraded_fit.errors['mu_balmer_1']})
                    self.fit_param_dict.update({'sigma_co10_1': migraded_fit.values['sigma_balmer_1']})
                    self.fit_param_dict.update({'sigma_co10_1_err': migraded_fit.errors['sigma_balmer_1']})

                    self.fit_param_dict.update({'mu_co10_2': migraded_fit.values['mu_balmer_2']})
                    self.fit_param_dict.update({'mu_co10_2_err': migraded_fit.errors['mu_balmer_2']})
                    self.fit_param_dict.update({'sigma_co10_2': migraded_fit.values['sigma_balmer_2']})
                    self.fit_param_dict.update({'sigma_co10_2_err': migraded_fit.errors['sigma_balmer_2']})
                if 'co21' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co21_1': migraded_fit.values['mu_balmer_1']})
                    self.fit_param_dict.update({'mu_co21_1_err': migraded_fit.errors['mu_balmer_1']})
                    self.fit_param_dict.update({'sigma_co21_1': migraded_fit.values['sigma_balmer_1']})
                    self.fit_param_dict.update({'sigma_co21_1_err': migraded_fit.errors['sigma_balmer_1']})

                    self.fit_param_dict.update({'mu_co21_2': migraded_fit.values['mu_balmer_2']})
                    self.fit_param_dict.update({'mu_co21_2_err': migraded_fit.errors['mu_balmer_2']})
                    self.fit_param_dict.update({'sigma_co21_2': migraded_fit.values['sigma_balmer_2']})
                    self.fit_param_dict.update({'sigma_co21_2_err': migraded_fit.errors['sigma_balmer_2']})

            elif self.n_gauss == 3:
                if ('complex' in self.fit_mode) | ('simple' in self.fit_mode) | ('muse_weak' in self.fit_mode):
                    self.fit_param_dict.update({'mu_balmer_1': migraded_fit.values['mu_balmer_1']})
                    self.fit_param_dict.update({'mu_balmer_1_err': migraded_fit.errors['mu_balmer_1']})
                    self.fit_param_dict.update({'sigma_balmer_1': migraded_fit.values['sigma_balmer_1']})
                    self.fit_param_dict.update({'sigma_balmer_1_err': migraded_fit.errors['sigma_balmer_1']})

                    self.fit_param_dict.update({'mu_balmer_2': migraded_fit.values['mu_balmer_2']})
                    self.fit_param_dict.update({'mu_balmer_2_err': migraded_fit.errors['mu_balmer_2']})
                    self.fit_param_dict.update({'sigma_balmer_2': migraded_fit.values['sigma_balmer_2']})
                    self.fit_param_dict.update({'sigma_balmer_2_err': migraded_fit.errors['sigma_balmer_2']})

                    self.fit_param_dict.update({'mu_balmer_3': migraded_fit.values['mu_balmer_3']})
                    self.fit_param_dict.update({'mu_balmer_3_err': migraded_fit.errors['mu_balmer_3']})
                    self.fit_param_dict.update({'sigma_balmer_3': migraded_fit.values['sigma_balmer_3']})
                    self.fit_param_dict.update({'sigma_balmer_3_err': migraded_fit.errors['sigma_balmer_3']})

                    self.fit_param_dict.update({'mu_forbidden_1': migraded_fit.values['mu_forbidden_1']})
                    self.fit_param_dict.update({'mu_forbidden_1_err': migraded_fit.errors['mu_forbidden_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1': migraded_fit.values['sigma_forbidden_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1_err': migraded_fit.errors['sigma_forbidden_1']})

                    self.fit_param_dict.update({'mu_forbidden_2': migraded_fit.values['mu_forbidden_2']})
                    self.fit_param_dict.update({'mu_forbidden_2_err': migraded_fit.errors['mu_forbidden_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2': migraded_fit.values['sigma_forbidden_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2_err': migraded_fit.errors['sigma_forbidden_2']})

                    self.fit_param_dict.update({'mu_forbidden_3': migraded_fit.values['mu_forbidden_3']})
                    self.fit_param_dict.update({'mu_forbidden_3_err': migraded_fit.errors['mu_forbidden_3']})
                    self.fit_param_dict.update({'sigma_forbidden_3': migraded_fit.values['sigma_forbidden_3']})
                    self.fit_param_dict.update({'sigma_forbidden_3_err': migraded_fit.errors['sigma_forbidden_3']})

                if 'co10' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co10_1': migraded_fit.values['mu_balmer_1']})
                    self.fit_param_dict.update({'mu_co10_1_err': migraded_fit.errors['mu_balmer_1']})
                    self.fit_param_dict.update({'sigma_co10_1': migraded_fit.values['sigma_balmer_1']})
                    self.fit_param_dict.update({'sigma_co10_1_err': migraded_fit.errors['sigma_balmer_1']})

                    self.fit_param_dict.update({'mu_co10_2': migraded_fit.values['mu_balmer_2']})
                    self.fit_param_dict.update({'mu_co10_2_err': migraded_fit.errors['mu_balmer_2']})
                    self.fit_param_dict.update({'sigma_co10_2': migraded_fit.values['sigma_balmer_2']})
                    self.fit_param_dict.update({'sigma_co10_2_err': migraded_fit.errors['sigma_balmer_2']})

                    self.fit_param_dict.update({'mu_co10_3': migraded_fit.values['mu_balmer_3']})
                    self.fit_param_dict.update({'mu_co10_3_err': migraded_fit.errors['mu_balmer_3']})
                    self.fit_param_dict.update({'sigma_co10_3': migraded_fit.values['sigma_balmer_3']})
                    self.fit_param_dict.update({'sigma_co10_3_err': migraded_fit.errors['sigma_balmer_3']})
                if 'co21' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co21_1': migraded_fit.values['mu_balmer_1']})
                    self.fit_param_dict.update({'mu_co21_1_err': migraded_fit.errors['mu_balmer_1']})
                    self.fit_param_dict.update({'sigma_co21_1': migraded_fit.values['sigma_balmer_1']})
                    self.fit_param_dict.update({'sigma_co21_1_err': migraded_fit.errors['sigma_balmer_1']})

                    self.fit_param_dict.update({'mu_co21_2': migraded_fit.values['mu_balmer_2']})
                    self.fit_param_dict.update({'mu_co21_2_err': migraded_fit.errors['mu_balmer_2']})
                    self.fit_param_dict.update({'sigma_co21_2': migraded_fit.values['sigma_balmer_2']})
                    self.fit_param_dict.update({'sigma_co21_2_err': migraded_fit.errors['sigma_balmer_2']})

                    self.fit_param_dict.update({'mu_co21_3': migraded_fit.values['mu_balmer_3']})
                    self.fit_param_dict.update({'mu_co21_3_err': migraded_fit.errors['mu_balmer_3']})
                    self.fit_param_dict.update({'sigma_co21_3': migraded_fit.values['sigma_balmer_3']})
                    self.fit_param_dict.update({'sigma_co21_3_err': migraded_fit.errors['sigma_balmer_3']})
            else:
                raise KeyError('you can only have 1, 2 or 3 Gaussian components')
        else:
            if self.n_gauss == 1:
                if ('complex' in self.fit_mode) | ('simple' in self.fit_mode) | ('muse_weak' in self.fit_mode):
                    self.fit_param_dict.update({'mu_balmer': migraded_fit.values['mu']})
                    self.fit_param_dict.update({'mu_balmer_err': migraded_fit.errors['mu']})
                    self.fit_param_dict.update({'sigma_balmer': migraded_fit.values['sigma']})
                    self.fit_param_dict.update({'sigma_balmer_err': migraded_fit.errors['sigma']})

                    self.fit_param_dict.update({'mu_forbidden': migraded_fit.values['mu']})
                    self.fit_param_dict.update({'mu_forbidden_err': migraded_fit.errors['mu']})
                    self.fit_param_dict.update({'sigma_forbidden': migraded_fit.values['sigma']})
                    self.fit_param_dict.update({'sigma_forbidden_err': migraded_fit.errors['sigma']})

                if 'co10' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co10': migraded_fit.values['mu']})
                    self.fit_param_dict.update({'mu_co10_err': migraded_fit.errors['mu']})
                    self.fit_param_dict.update({'sigma_co10': migraded_fit.values['sigma']})
                    self.fit_param_dict.update({'sigma_co10_err': migraded_fit.errors['sigma']})

                if 'co21' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co21': migraded_fit.values['mu']})
                    self.fit_param_dict.update({'mu_co21_err': migraded_fit.errors['mu']})
                    self.fit_param_dict.update({'sigma_co21': migraded_fit.values['sigma']})
                    self.fit_param_dict.update({'sigma_co21_err': migraded_fit.errors['sigma']})

                if 'hi' in self.fit_mode:
                    self.fit_param_dict.update({'mu_hi': migraded_fit.values['mu']})
                    self.fit_param_dict.update({'mu_hi_err': migraded_fit.errors['mu']})
                    self.fit_param_dict.update({'sigma_hi': migraded_fit.values['sigma']})
                    self.fit_param_dict.update({'sigma_hi_err': migraded_fit.errors['sigma']})

            elif self.n_gauss == 2:
                if ('complex' in self.fit_mode) | ('simple' in self.fit_mode) | ('muse_weak' in self.fit_mode):
                    self.fit_param_dict.update({'mu_balmer_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_balmer_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_balmer_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_balmer_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_balmer_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_balmer_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_balmer_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_balmer_2_err': migraded_fit.errors['sigma_2']})

                    self.fit_param_dict.update({'mu_forbidden_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_forbidden_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_forbidden_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_forbidden_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2_err': migraded_fit.errors['sigma_2']})

                if 'co10' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co10_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_co10_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_co10_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_co10_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_co10_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_co10_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_co10_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_co10_2_err': migraded_fit.errors['sigma_2']})

                if 'co21' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co21_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_co21_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_co21_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_co21_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_co21_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_co21_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_co21_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_co21_2_err': migraded_fit.errors['sigma_2']})

                if 'hi' in self.fit_mode:
                    self.fit_param_dict.update({'mu_hi_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_hi_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_hi_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_hi_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_hi_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_hi_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_hi_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_hi_2_err': migraded_fit.errors['sigma_2']})

            elif self.n_gauss == 3:
                if ('complex' in self.fit_mode) | ('simple' in self.fit_mode) | ('muse_weak' in self.fit_mode):
                    self.fit_param_dict.update({'mu_balmer_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_balmer_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_balmer_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_balmer_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_balmer_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_balmer_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_balmer_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_balmer_2_err': migraded_fit.errors['sigma_2']})

                    self.fit_param_dict.update({'mu_balmer_3': migraded_fit.values['mu_3']})
                    self.fit_param_dict.update({'mu_balmer_3_err': migraded_fit.errors['mu_3']})
                    self.fit_param_dict.update({'sigma_balmer_3': migraded_fit.values['sigma_3']})
                    self.fit_param_dict.update({'sigma_balmer_3_err': migraded_fit.errors['sigma_3']})

                    self.fit_param_dict.update({'mu_forbidden_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_forbidden_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_forbidden_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_forbidden_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_forbidden_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_forbidden_2_err': migraded_fit.errors['sigma_2']})

                    self.fit_param_dict.update({'mu_forbidden_3': migraded_fit.values['mu_3']})
                    self.fit_param_dict.update({'mu_forbidden_3_err': migraded_fit.errors['mu_3']})
                    self.fit_param_dict.update({'sigma_forbidden_3': migraded_fit.values['sigma_3']})
                    self.fit_param_dict.update({'sigma_forbidden_3_err': migraded_fit.errors['sigma_3']})

                if 'co10' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co10_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_co10_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_co10_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_co10_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_co10_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_co10_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_co10_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_co10_2_err': migraded_fit.errors['sigma_2']})

                    self.fit_param_dict.update({'mu_co10_3': migraded_fit.values['mu_3']})
                    self.fit_param_dict.update({'mu_co10_3_err': migraded_fit.errors['mu_3']})
                    self.fit_param_dict.update({'sigma_co10_3': migraded_fit.values['sigma_3']})
                    self.fit_param_dict.update({'sigma_co10_3_err': migraded_fit.errors['sigma_3']})

                if 'co21' in self.fit_mode:
                    self.fit_param_dict.update({'mu_co21_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_co21_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_co21_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_co21_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_co21_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_co21_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_co21_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_co21_2_err': migraded_fit.errors['sigma_2']})

                    self.fit_param_dict.update({'mu_co21_3': migraded_fit.values['mu_3']})
                    self.fit_param_dict.update({'mu_co21_3_err': migraded_fit.errors['mu_3']})
                    self.fit_param_dict.update({'sigma_co21_3': migraded_fit.values['sigma_3']})
                    self.fit_param_dict.update({'sigma_co21_3_err': migraded_fit.errors['sigma_3']})

                if 'hi' in self.fit_mode:
                    self.fit_param_dict.update({'mu_hi_1': migraded_fit.values['mu_1']})
                    self.fit_param_dict.update({'mu_hi_1_err': migraded_fit.errors['mu_1']})
                    self.fit_param_dict.update({'sigma_hi_1': migraded_fit.values['sigma_1']})
                    self.fit_param_dict.update({'sigma_hi_1_err': migraded_fit.errors['sigma_1']})

                    self.fit_param_dict.update({'mu_hi_2': migraded_fit.values['mu_2']})
                    self.fit_param_dict.update({'mu_hi_2_err': migraded_fit.errors['mu_2']})
                    self.fit_param_dict.update({'sigma_hi_2': migraded_fit.values['sigma_2']})
                    self.fit_param_dict.update({'sigma_hi_2_err': migraded_fit.errors['sigma_2']})

                    self.fit_param_dict.update({'mu_hi_3': migraded_fit.values['mu_3']})
                    self.fit_param_dict.update({'mu_hi_3_err': migraded_fit.errors['mu_3']})
                    self.fit_param_dict.update({'sigma_hi_3': migraded_fit.values['sigma_3']})
                    self.fit_param_dict.update({'sigma_hi_3_err': migraded_fit.errors['sigma_3']})
            else:
                raise KeyError('you can only have 1, 2 or 3 Gaussian components')

        if self.n_broad == 1:
            self.fit_param_dict.update({'mu_broad': migraded_fit.values['mu_broad']})
            self.fit_param_dict.update({'mu_broad_err': migraded_fit.errors['mu_broad']})
            self.fit_param_dict.update({'sigma_broad': migraded_fit.values['sigma_broad']})
            self.fit_param_dict.update({'sigma_broad_err': migraded_fit.errors['sigma_broad']})

        # add all the independent emission line amplitudes

        for line in self.get_indep_line_list():
            if self.n_gauss == 1:
                self.fit_param_dict.update({'amp_%s' % str(line): migraded_fit.values['amp_%s' % str(line)]})
                self.fit_param_dict.update({'amp_%s_err' % str(line): migraded_fit.errors['amp_%s' % str(line)]})
            elif self.n_gauss == 2:
                self.fit_param_dict.update({'amp_%s_1' % str(line): migraded_fit.values['amp_%s_1' % str(line)]})
                self.fit_param_dict.update({'amp_%s_1_err' % str(line): migraded_fit.errors['amp_%s_1' % str(line)]})
                self.fit_param_dict.update({'amp_%s_2' % str(line): migraded_fit.values['amp_%s_2' % str(line)]})
                self.fit_param_dict.update({'amp_%s_2_err' % str(line): migraded_fit.errors['amp_%s_2' % str(line)]})
            elif self.n_gauss == 3:
                self.fit_param_dict.update({'amp_%s_1' % str(line): migraded_fit.values['amp_%s_1' % str(line)]})
                self.fit_param_dict.update({'amp_%s_1_err' % str(line): migraded_fit.errors['amp_%s_1' % str(line)]})
                self.fit_param_dict.update({'amp_%s_2' % str(line): migraded_fit.values['amp_%s_2' % str(line)]})
                self.fit_param_dict.update({'amp_%s_2_err' % str(line): migraded_fit.errors['amp_%s_2' % str(line)]})
                self.fit_param_dict.update({'amp_%s_3' % str(line): migraded_fit.values['amp_%s_3' % str(line)]})
                self.fit_param_dict.update({'amp_%s_3_err' % str(line): migraded_fit.errors['amp_%s_3' % str(line)]})
            else:
                raise KeyError('you can only have 1 or 2 Gaussian components')
            if (self.n_broad == 1) & (line in self.em_wavelength.keys()):
                if self.em_wavelength[line]['transition'] == 'balmer':
                    self.fit_param_dict.update({'amp_%s_broad' % str(line): migraded_fit.values['amp_%s_broad' % str(line)]})
                    self.fit_param_dict.update({'amp_%s_broad_err' % str(line): migraded_fit.errors['amp_%s_broad' % str(line)]})

        # now add fixed amplitudes
        if self.doublet_ratio:
            if self.n_gauss == 1:
                self.fit_param_dict.update({'amp_4960': migraded_fit.values['amp_5008'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_4960_err': migraded_fit.errors['amp_5008'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_6550': migraded_fit.values['amp_6585'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_6550_err': migraded_fit.errors['amp_6585'] / self.em_ratios['6585/6550']})
                if self.fit_mode == 'muse_weak':
                    self.fit_param_dict.update({'amp_6366': migraded_fit.values['amp_6302'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_err': migraded_fit.errors['amp_6302'] / self.em_ratios['6302/6366']})
            if self.n_gauss == 2:
                self.fit_param_dict.update({'amp_4960_1': migraded_fit.values['amp_5008_1'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_4960_1_err': migraded_fit.errors['amp_5008_1'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_6550_1': migraded_fit.values['amp_6585_1'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_6550_1_err': migraded_fit.errors['amp_6585_1'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_4960_2': migraded_fit.values['amp_5008_2'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_4960_2_err': migraded_fit.errors['amp_5008_2'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_6550_2': migraded_fit.values['amp_6585_2'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_6550_2_err': migraded_fit.errors['amp_6585_2'] / self.em_ratios['6585/6550']})
                if self.fit_mode == 'muse_weak':
                    self.fit_param_dict.update({'amp_6366_1': migraded_fit.values['amp_6302_1'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_1_err': migraded_fit.errors['amp_6302_1'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_2': migraded_fit.values['amp_6302_2'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_2_err': migraded_fit.errors['amp_6302_2'] / self.em_ratios['6302/6366']})
            if self.n_gauss == 3:
                self.fit_param_dict.update({'amp_4960_1': migraded_fit.values['amp_5008_1'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_4960_1_err': migraded_fit.errors['amp_5008_1'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_6550_1': migraded_fit.values['amp_6585_1'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_6550_1_err': migraded_fit.errors['amp_6585_1'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_4960_2': migraded_fit.values['amp_5008_2'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_4960_2_err': migraded_fit.errors['amp_5008_2'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_6550_2': migraded_fit.values['amp_6585_2'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_6550_2_err': migraded_fit.errors['amp_6585_2'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_4960_3': migraded_fit.values['amp_5008_3'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_4960_3_err': migraded_fit.errors['amp_5008_3'] / self.em_ratios['5008/4960']})
                self.fit_param_dict.update({'amp_6550_3': migraded_fit.values['amp_6585_3'] / self.em_ratios['6585/6550']})
                self.fit_param_dict.update({'amp_6550_3_err': migraded_fit.errors['amp_6585_3'] / self.em_ratios['6585/6550']})
                if self.fit_mode == 'muse_weak':
                    self.fit_param_dict.update({'amp_6366_1': migraded_fit.values['amp_6302_1'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_1_err': migraded_fit.errors['amp_6302_1'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_2': migraded_fit.values['amp_6302_2'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_2_err': migraded_fit.errors['amp_6302_2'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_3': migraded_fit.values['amp_6302_3'] / self.em_ratios['6302/6366']})
                    self.fit_param_dict.update({'amp_6366_3_err': migraded_fit.errors['amp_6302_3'] / self.em_ratios['6302/6366']})

    def get_em_flux(self):

        if self.line_flux_dict is None:
            self.line_flux_dict = {}

        for line in self.get_full_line_list():

            if self.n_gauss == 1:
                if isinstance(line, int):
                    inst_broadening = self.inst_broad[line]
                    sigma = self.fit_param_dict['sigma_%s' % self.em_wavelength[line]['transition']]
                    sigma_err = self.fit_param_dict['sigma_%s_err' % self.em_wavelength[line]['transition']]
                    observed_sigma = (np.sqrt(sigma ** 2 + inst_broadening ** 2) * self.em_wavelength[line]['vac_wave']
                                      / (speed_of_light * 1e-3))
                    observed_sigma_err = (sigma_err * self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                elif (line == 'co10') | (line == 'co21') | (line == 'hi'):
                    observed_sigma = self.fit_param_dict['sigma_%s' % line]
                    observed_sigma_err = self.fit_param_dict['sigma_%s_err' % line]
                else:
                    raise KeyError('line must be a ionized gas emission line or co line')
                line_amp = self.fit_param_dict['amp_%s' % str(line)]
                line_amp_err = self.fit_param_dict['amp_%s_err' % str(line)]
                # calculate flux
                self.line_flux_dict.update({'flux_%s' % str(line): line_amp * observed_sigma * np.sqrt(2 * np.pi)})
                # calculate flux error
                self.line_flux_dict.update({
                    'flux_%s_err' % str(line): np.sqrt(2 * np.pi) * np.sqrt((observed_sigma_err * line_amp) ** 2 +
                                                                            (line_amp_err * observed_sigma) ** 2)})
            elif self.n_gauss == 2:
                if isinstance(line, int):
                    inst_broadening = self.inst_broad[line]
                    sigma_1 = self.fit_param_dict['sigma_%s_1' % self.em_wavelength[line]['transition']]
                    sigma_1_err = self.fit_param_dict['sigma_%s_1_err' % self.em_wavelength[line]['transition']]
                    observed_sigma_1 = (np.sqrt(sigma_1 ** 2 + inst_broadening ** 2) *
                                        self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                    observed_sigma_1_err = (sigma_1_err * self.em_wavelength[line]['vac_wave'] /
                                            (speed_of_light * 1e-3))
                    sigma_2 = self.fit_param_dict['sigma_%s_2' % self.em_wavelength[line]['transition']]
                    sigma_2_err = self.fit_param_dict['sigma_%s_2_err' % self.em_wavelength[line]['transition']]
                    observed_sigma_2 = (np.sqrt(sigma_2 ** 2 + inst_broadening ** 2) *
                                        self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                    observed_sigma_2_err = (sigma_2_err * self.em_wavelength[line]['vac_wave'] /
                                            (speed_of_light * 1e-3))
                elif (line == 'co10') | (line == 'co21') | (line == 'hi'):
                    observed_sigma_1 = self.fit_param_dict['sigma_%s_1' % line]
                    observed_sigma_1_err = self.fit_param_dict['sigma_%s_1_err' % line]
                    observed_sigma_2 = self.fit_param_dict['sigma_%s_2' % line]
                    observed_sigma_2_err = self.fit_param_dict['sigma_%s_2_err' % line]
                else:
                    raise KeyError('line must be a ionized gas emission line or co line')
                line_amp_1 = self.fit_param_dict['amp_%s_1' % str(line)]
                line_amp_1_err = self.fit_param_dict['amp_%s_1_err' % str(line)]

                self.line_flux_dict.update({'flux_%s_1' % str(line): (line_amp_1 * observed_sigma_1 *
                                                                      np.sqrt(2 * np.pi))})
                self.line_flux_dict.update({
                    'flux_%s_1_err' % str(line): (np.sqrt(2 * np.pi) *
                                                  np.sqrt((observed_sigma_1_err * line_amp_1) ** 2 +
                                                          (line_amp_1_err * observed_sigma_1) ** 2))})

                line_amp_2 = self.fit_param_dict['amp_%s_2' % str(line)]
                line_amp_2_err = self.fit_param_dict['amp_%s_2_err' % str(line)]

                self.line_flux_dict.update({'flux_%s_2' % str(line): (line_amp_2 * observed_sigma_2 *
                                                                      np.sqrt(2 * np.pi))})
                self.line_flux_dict.update({
                    'flux_%s_2_err' % str(line): (np.sqrt(2 * np.pi) *
                                                  np.sqrt((observed_sigma_2_err * line_amp_2) ** 2 +
                                                          (line_amp_2_err * observed_sigma_2) ** 2))})

            elif self.n_gauss == 3:
                if isinstance(line, int):
                    inst_broadening = self.inst_broad[line]
                    sigma_1 = self.fit_param_dict['sigma_%s_1' % self.em_wavelength[line]['transition']]
                    sigma_1_err = self.fit_param_dict['sigma_%s_1_err' % self.em_wavelength[line]['transition']]
                    observed_sigma_1 = (np.sqrt(sigma_1 ** 2 + inst_broadening ** 2) *
                                        self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                    observed_sigma_1_err = (sigma_1_err * self.em_wavelength[line]['vac_wave'] /
                                            (speed_of_light * 1e-3))
                    sigma_2 = self.fit_param_dict['sigma_%s_2' % self.em_wavelength[line]['transition']]
                    sigma_2_err = self.fit_param_dict['sigma_%s_2_err' % self.em_wavelength[line]['transition']]
                    observed_sigma_2 = (np.sqrt(sigma_2 ** 2 + inst_broadening ** 2) *
                                        self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                    observed_sigma_2_err = (sigma_2_err * self.em_wavelength[line]['vac_wave'] /
                                            (speed_of_light * 1e-3))
                    sigma_3 = self.fit_param_dict['sigma_%s_3' % self.em_wavelength[line]['transition']]
                    sigma_3_err = self.fit_param_dict['sigma_%s_3_err' % self.em_wavelength[line]['transition']]
                    observed_sigma_3 = (np.sqrt(sigma_3 ** 2 + inst_broadening ** 2) *
                                        self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                    observed_sigma_3_err = (sigma_3_err * self.em_wavelength[line]['vac_wave'] /
                                            (speed_of_light * 1e-3))
                elif (line == 'co10') | (line == 'co21') | (line == 'hi'):
                    observed_sigma_1 = self.fit_param_dict['sigma_%s_1' % line]
                    observed_sigma_1_err = self.fit_param_dict['sigma_%s_1_err' % line]
                    observed_sigma_2 = self.fit_param_dict['sigma_%s_2' % line]
                    observed_sigma_2_err = self.fit_param_dict['sigma_%s_2_err' % line]
                    observed_sigma_3 = self.fit_param_dict['sigma_%s_3' % line]
                    observed_sigma_3_err = self.fit_param_dict['sigma_%s_3_err' % line]
                else:
                    raise KeyError('line must be a ionized gas emission line or co line')
                line_amp_1 = self.fit_param_dict['amp_%s_1' % str(line)]
                line_amp_1_err = self.fit_param_dict['amp_%s_1_err' % str(line)]
                self.line_flux_dict.update({'flux_%s_1' % str(line): (line_amp_1 * observed_sigma_1 *
                                                                      np.sqrt(2 * np.pi))})
                self.line_flux_dict.update({
                    'flux_%s_1_err' % str(line): (np.sqrt(2 * np.pi) *
                                                  np.sqrt((observed_sigma_1_err * line_amp_1) ** 2 +
                                                          (line_amp_1_err * observed_sigma_1) ** 2))})

                line_amp_2 = self.fit_param_dict['amp_%s_2' % str(line)]
                line_amp_2_err = self.fit_param_dict['amp_%s_2_err' % str(line)]

                self.line_flux_dict.update({'flux_%s_2' % str(line): (line_amp_2 * observed_sigma_2 *
                                                                      np.sqrt(2 * np.pi))})
                self.line_flux_dict.update({
                    'flux_%s_2_err' % str(line): (np.sqrt(2 * np.pi) *
                                                  np.sqrt((observed_sigma_2_err * line_amp_2) ** 2 +
                                                          (line_amp_2_err * observed_sigma_2) ** 2))})

                line_amp_3 = self.fit_param_dict['amp_%s_3' % str(line)]
                line_amp_3_err = self.fit_param_dict['amp_%s_3_err' % str(line)]

                self.line_flux_dict.update({'flux_%s_3' % str(line): (line_amp_3 * observed_sigma_3 *
                                                                      np.sqrt(2 * np.pi))})
                self.line_flux_dict.update({
                    'flux_%s_3_err' % str(line): (np.sqrt(2 * np.pi) *
                                                  np.sqrt((observed_sigma_3_err * line_amp_3) ** 2 +
                                                          (line_amp_3_err * observed_sigma_3) ** 2))})

            if (self.n_broad == 1) & (line in self.em_wavelength.keys()):
                if self.em_wavelength[line]['transition'] == 'balmer':
                    inst_broadening = self.inst_broad[line]
                    sigma = self.fit_param_dict['sigma_broad']
                    sigma_err = self.fit_param_dict['sigma_broad_err']
                    observed_sigma = (np.sqrt(sigma ** 2 + inst_broadening ** 2) * self.em_wavelength[line]['vac_wave']
                                      / (speed_of_light * 1e-3))
                    observed_sigma_err = (sigma_err * self.em_wavelength[line]['vac_wave'] / (speed_of_light * 1e-3))
                    line_amp = self.fit_param_dict['amp_%s_broad' % str(line)]
                    line_amp_err = self.fit_param_dict['amp_%s_broad_err' % str(line)]
                    # calculate flux
                    self.line_flux_dict.update({'flux_%s_broad' % str(line): line_amp * observed_sigma * np.sqrt(2 * np.pi)})
                    # calculate flux error
                    self.line_flux_dict.update({
                        'flux_%s_broad_err' % str(line): np.sqrt(2 * np.pi) * np.sqrt((observed_sigma_err * line_amp) ** 2 +
                                                                                      (line_amp_err * observed_sigma) ** 2)})

    def sort_fit_parameter_dict(self):

        if self.fit_mode in ['simple', 'complex', 'muse_weak']:
            if self.fit_param_dict['mu_balmer_1'] > self.fit_param_dict['mu_balmer_2']:
                for line in self.get_full_line_list(line_type='balmer'):
                    self.fit_param_dict['amp_%s_1' % str(line)], self.fit_param_dict['amp_%s_2' % str(line)] = \
                        self.fit_param_dict['amp_%s_2' % str(line)], self.fit_param_dict['amp_%s_1' % str(line)]
                    self.fit_param_dict['amp_%s_1_err' % str(line)], self.fit_param_dict['amp_%s_2_err' % str(line)] = \
                        self.fit_param_dict['amp_%s_2_err' % str(line)], self.fit_param_dict['amp_%s_1_err' % str(line)]
                    self.line_flux_dict['flux_%s_1' % str(line)], self.line_flux_dict['flux_%s_2' % str(line)] = \
                        self.line_flux_dict['flux_%s_2' % str(line)], self.line_flux_dict['flux_%s_1' % str(line)]
                self.fit_param_dict['mu_balmer_1'], self.fit_param_dict['mu_balmer_2'] = \
                    self.fit_param_dict['mu_balmer_2'], self.fit_param_dict['mu_balmer_1']
                self.fit_param_dict['sigma_balmer_1'], self.fit_param_dict['sigma_balmer_2'] = \
                    self.fit_param_dict['sigma_balmer_2'], self.fit_param_dict['sigma_balmer_1']

            if self.fit_param_dict['mu_forbidden_1'] > self.fit_param_dict['mu_forbidden_2']:
                for line in self.get_full_line_list(line_type='forbidden'):
                    self.fit_param_dict['amp_%s_1' % str(line)], self.fit_param_dict['amp_%s_2' % str(line)] = \
                        self.fit_param_dict['amp_%s_2' % str(line)], self.fit_param_dict['amp_%s_1' % str(line)]
                    self.fit_param_dict['amp_%s_1_err' % str(line)], self.fit_param_dict['amp_%s_2_err' % str(line)] = \
                        self.fit_param_dict['amp_%s_2_err' % str(line)], self.fit_param_dict['amp_%s_1_err' % str(line)]
                    self.line_flux_dict['flux_%s_1' % str(line)], self.line_flux_dict['flux_%s_2' % str(line)] = \
                        self.line_flux_dict['flux_%s_2' % str(line)], self.line_flux_dict['flux_%s_1' % str(line)]
                self.fit_param_dict['mu_forbidden_1'], self.fit_param_dict['mu_forbidden_2'] = \
                    self.fit_param_dict['mu_forbidden_2'], self.fit_param_dict['mu_forbidden_1']
                self.fit_param_dict['sigma_forbidden_1'], self.fit_param_dict['sigma_forbidden_2'] = \
                    self.fit_param_dict['sigma_forbidden_2'], self.fit_param_dict['sigma_forbidden_1']
        if 'co10' in self.fit_mode:
            if self.n_gauss == 2:
                if self.fit_param_dict['mu_co10_1'] > self.fit_param_dict['mu_co10_2']:
                    self.fit_param_dict['amp_co10_1'], self.fit_param_dict['amp_co10_2'] = \
                        self.fit_param_dict['amp_co10_2'], self.fit_param_dict['amp_co10_1']
                    self.fit_param_dict['amp_co10_1_err'], self.fit_param_dict['amp_co10_2_err'] = \
                        self.fit_param_dict['amp_co10_2_err'], self.fit_param_dict['amp_co10_1_err']
                    self.line_flux_dict['flux_co10_1'], self.line_flux_dict['flux_co10_2'] = \
                        self.line_flux_dict['flux_co10_2'], self.line_flux_dict['flux_co10_1']
                    self.fit_param_dict['mu_co10_1'], self.fit_param_dict['mu_co10_2'] = \
                        self.fit_param_dict['mu_co10_2'], self.fit_param_dict['mu_co10_1']
                    self.fit_param_dict['sigma_co10_1'], self.fit_param_dict['sigma_co10_2'] = \
                        self.fit_param_dict['sigma_co10_2'], self.fit_param_dict['sigma_co10_1']
            elif self.n_gauss == 3:
                # make sure _1 is the smallest
                if self.fit_param_dict['mu_co10_1'] > self.fit_param_dict['mu_co10_2']:  # smallest is larger than second
                    if self.fit_param_dict['mu_co10_1'] > self.fit_param_dict['mu_co10_3']:  # smallest is at third place !
                        self.fit_param_dict['amp_co10_1'], self.fit_param_dict['amp_co10_3'] = \
                            self.fit_param_dict['amp_co10_3'], self.fit_param_dict['amp_co10_1']
                        self.fit_param_dict['amp_co10_1_err'], self.fit_param_dict['amp_co10_3_err'] = \
                            self.fit_param_dict['amp_co10_3_err'], self.fit_param_dict['amp_co10_1_err']
                        self.line_flux_dict['flux_co10_1'], self.line_flux_dict['flux_co10_3'] = \
                            self.line_flux_dict['flux_co10_3'], self.line_flux_dict['flux_co10_1']
                        self.fit_param_dict['mu_co10_1'], self.fit_param_dict['mu_co10_3'] = \
                            self.fit_param_dict['mu_co10_3'], self.fit_param_dict['mu_co10_1']
                        self.fit_param_dict['sigma_co10_1'], self.fit_param_dict['sigma_co10_3'] = \
                            self.fit_param_dict['sigma_co10_3'], self.fit_param_dict['sigma_co10_1']
                    else:  # smallest is at second place
                        self.fit_param_dict['amp_co10_1'], self.fit_param_dict['amp_co10_2'] = \
                            self.fit_param_dict['amp_co10_2'], self.fit_param_dict['amp_co10_1']
                        self.fit_param_dict['amp_co10_1_err'], self.fit_param_dict['amp_co10_2_err'] = \
                            self.fit_param_dict['amp_co10_2_err'], self.fit_param_dict['amp_co10_1_err']
                        self.line_flux_dict['flux_co10_1'], self.line_flux_dict['flux_co10_2'] = \
                            self.line_flux_dict['flux_co10_2'], self.line_flux_dict['flux_co10_1']
                        self.fit_param_dict['mu_co10_1'], self.fit_param_dict['mu_co10_2'] = \
                            self.fit_param_dict['mu_co10_2'], self.fit_param_dict['mu_co10_1']
                        self.fit_param_dict['sigma_co10_1'], self.fit_param_dict['sigma_co10_2'] = \
                            self.fit_param_dict['sigma_co10_2'], self.fit_param_dict['sigma_co10_1']
                # now the smallest is at place _1 make now shure that _2 _3 is in increasing order
                if self.fit_param_dict['mu_co10_2'] > self.fit_param_dict['mu_co10_3']:  # second is bigger than thierd
                    self.fit_param_dict['amp_co10_2'], self.fit_param_dict['amp_co10_3'] = \
                        self.fit_param_dict['amp_co10_3'], self.fit_param_dict['amp_co10_2']
                    self.fit_param_dict['amp_co10_2_err'], self.fit_param_dict['amp_co10_3_err'] = \
                        self.fit_param_dict['amp_co10_3_err'], self.fit_param_dict['amp_co10_2_err']
                    self.line_flux_dict['flux_co10_2'], self.line_flux_dict['flux_co10_3'] = \
                        self.line_flux_dict['flux_co10_3'], self.line_flux_dict['flux_co10_2']
                    self.fit_param_dict['mu_co10_2'], self.fit_param_dict['mu_co10_3'] = \
                        self.fit_param_dict['mu_co10_3'], self.fit_param_dict['mu_co10_2']
                    self.fit_param_dict['sigma_co10_2'], self.fit_param_dict['sigma_co10_3'] = \
                        self.fit_param_dict['sigma_co10_3'], self.fit_param_dict['sigma_co10_2']

        if 'co21' in self.fit_mode:
            if self.n_gauss == 2:
                if self.fit_param_dict['mu_co21_1'] > self.fit_param_dict['mu_co21_2']:
                    self.fit_param_dict['amp_co21_1'], self.fit_param_dict['amp_co21_2'] = \
                        self.fit_param_dict['amp_co21_2'], self.fit_param_dict['amp_co21_1']
                    self.fit_param_dict['amp_co21_1_err'], self.fit_param_dict['amp_co21_2_err'] = \
                        self.fit_param_dict['amp_co21_2_err'], self.fit_param_dict['amp_co21_1_err']
                    self.line_flux_dict['flux_co21_1'], self.line_flux_dict['flux_co21_2'] = \
                        self.line_flux_dict['flux_co21_2'], self.line_flux_dict['flux_co21_1']
                    self.fit_param_dict['mu_co21_1'], self.fit_param_dict['mu_co21_2'] = \
                        self.fit_param_dict['mu_co21_2'], self.fit_param_dict['mu_co21_1']
                    self.fit_param_dict['sigma_co21_1'], self.fit_param_dict['sigma_co21_2'] = \
                        self.fit_param_dict['sigma_co21_2'], self.fit_param_dict['sigma_co21_1']
            elif self.n_gauss == 3:
                # make sure _1 is the smallest
                if self.fit_param_dict['mu_co21_1'] > self.fit_param_dict['mu_co21_2']:  # smallest is larger than second
                    if self.fit_param_dict['mu_co21_1'] > self.fit_param_dict['mu_co21_3']:  # smallest is at third place !
                        self.fit_param_dict['amp_co21_1'], self.fit_param_dict['amp_co21_3'] = \
                            self.fit_param_dict['amp_co21_3'], self.fit_param_dict['amp_co21_1']
                        self.fit_param_dict['amp_co21_1_err'], self.fit_param_dict['amp_co21_3_err'] = \
                            self.fit_param_dict['amp_co21_3_err'], self.fit_param_dict['amp_co21_1_err']
                        self.line_flux_dict['flux_co21_1'], self.line_flux_dict['flux_co21_3'] = \
                            self.line_flux_dict['flux_co21_3'], self.line_flux_dict['flux_co21_1']
                        self.fit_param_dict['mu_co21_1'], self.fit_param_dict['mu_co21_3'] = \
                            self.fit_param_dict['mu_co21_3'], self.fit_param_dict['mu_co21_1']
                        self.fit_param_dict['sigma_co21_1'], self.fit_param_dict['sigma_co21_3'] = \
                            self.fit_param_dict['sigma_co21_3'], self.fit_param_dict['sigma_co21_1']
                    else:  # smallest is at second place
                        self.fit_param_dict['amp_co21_1'], self.fit_param_dict['amp_co21_2'] = \
                            self.fit_param_dict['amp_co21_2'], self.fit_param_dict['amp_co21_1']
                        self.fit_param_dict['amp_co21_1_err'], self.fit_param_dict['amp_co21_2_err'] = \
                            self.fit_param_dict['amp_co21_2_err'], self.fit_param_dict['amp_co21_1_err']
                        self.line_flux_dict['flux_co21_1'], self.line_flux_dict['flux_co21_2'] = \
                            self.line_flux_dict['flux_co21_2'], self.line_flux_dict['flux_co21_1']
                        self.fit_param_dict['mu_co21_1'], self.fit_param_dict['mu_co21_2'] = \
                            self.fit_param_dict['mu_co21_2'], self.fit_param_dict['mu_co21_1']
                        self.fit_param_dict['sigma_co21_1'], self.fit_param_dict['sigma_co21_2'] = \
                            self.fit_param_dict['sigma_co21_2'], self.fit_param_dict['sigma_co21_1']
                # now the smallest is at place _1 make now shure that _2 _3 is in increasing order
                if self.fit_param_dict['mu_co21_2'] > self.fit_param_dict['mu_co21_3']:  # second is bigger than thierd
                    self.fit_param_dict['amp_co21_2'], self.fit_param_dict['amp_co21_3'] = \
                        self.fit_param_dict['amp_co21_3'], self.fit_param_dict['amp_co21_2']
                    self.fit_param_dict['amp_co21_2_err'], self.fit_param_dict['amp_co21_3_err'] = \
                        self.fit_param_dict['amp_co21_3_err'], self.fit_param_dict['amp_co21_2_err']
                    self.line_flux_dict['flux_co21_2'], self.line_flux_dict['flux_co21_3'] = \
                        self.line_flux_dict['flux_co21_3'], self.line_flux_dict['flux_co21_2']
                    self.fit_param_dict['mu_co21_2'], self.fit_param_dict['mu_co21_3'] = \
                        self.fit_param_dict['mu_co21_3'], self.fit_param_dict['mu_co21_2']
                    self.fit_param_dict['sigma_co21_2'], self.fit_param_dict['sigma_co21_3'] = \
                        self.fit_param_dict['sigma_co21_3'], self.fit_param_dict['sigma_co21_2']
        if 'hi' in self.fit_mode:
            if self.n_gauss == 2:
                if self.fit_param_dict['mu_hi_1'] > self.fit_param_dict['mu_hi_2']:
                    self.fit_param_dict['amp_hi_1'], self.fit_param_dict['amp_hi_2'] = \
                        self.fit_param_dict['amp_hi_2'], self.fit_param_dict['amp_hi_1']
                    self.fit_param_dict['amp_hi_1_err'], self.fit_param_dict['amp_hi_2_err'] = \
                        self.fit_param_dict['amp_hi_2_err'], self.fit_param_dict['amp_hi_1_err']
                    self.line_flux_dict['flux_hi_1'], self.line_flux_dict['flux_hi_2'] = \
                        self.line_flux_dict['flux_hi_2'], self.line_flux_dict['flux_hi_1']
                    self.fit_param_dict['mu_hi_1'], self.fit_param_dict['mu_hi_2'] = \
                        self.fit_param_dict['mu_hi_2'], self.fit_param_dict['mu_hi_1']
                    self.fit_param_dict['sigma_hi_1'], self.fit_param_dict['sigma_hi_2'] = \
                        self.fit_param_dict['sigma_hi_2'], self.fit_param_dict['sigma_hi_1']
            elif self.n_gauss == 3:
                # make sure _1 is the smallest
                if self.fit_param_dict['mu_hi_1'] > self.fit_param_dict['mu_hi_2']:  # smallest is larger than second
                    if self.fit_param_dict['mu_hi_1'] > self.fit_param_dict['mu_hi_3']:  # smallest is at third place !
                        self.fit_param_dict['amp_hi_1'], self.fit_param_dict['amp_hi_3'] = \
                            self.fit_param_dict['amp_hi_3'], self.fit_param_dict['amp_hi_1']
                        self.fit_param_dict['amp_hi_1_err'], self.fit_param_dict['amp_hi_3_err'] = \
                            self.fit_param_dict['amp_hi_3_err'], self.fit_param_dict['amp_hi_1_err']
                        self.line_flux_dict['flux_hi_1'], self.line_flux_dict['flux_hi_3'] = \
                            self.line_flux_dict['flux_hi_3'], self.line_flux_dict['flux_hi_1']
                        self.fit_param_dict['mu_hi_1'], self.fit_param_dict['mu_hi_3'] = \
                            self.fit_param_dict['mu_hi_3'], self.fit_param_dict['mu_hi_1']
                        self.fit_param_dict['sigma_hi_1'], self.fit_param_dict['sigma_hi_3'] = \
                            self.fit_param_dict['sigma_hi_3'], self.fit_param_dict['sigma_hi_1']
                    else:  # smallest is at second place
                        self.fit_param_dict['amp_hi_1'], self.fit_param_dict['amp_hi_2'] = \
                            self.fit_param_dict['amp_hi_2'], self.fit_param_dict['amp_hi_1']
                        self.fit_param_dict['amp_hi_1_err'], self.fit_param_dict['amp_hi_2_err'] = \
                            self.fit_param_dict['amp_hi_2_err'], self.fit_param_dict['amp_hi_1_err']
                        self.line_flux_dict['flux_hi_1'], self.line_flux_dict['flux_hi_2'] = \
                            self.line_flux_dict['flux_hi_2'], self.line_flux_dict['flux_hi_1']
                        self.fit_param_dict['mu_hi_1'], self.fit_param_dict['mu_hi_2'] = \
                            self.fit_param_dict['mu_hi_2'], self.fit_param_dict['mu_hi_1']
                        self.fit_param_dict['sigma_hi_1'], self.fit_param_dict['sigma_hi_2'] = \
                            self.fit_param_dict['sigma_hi_2'], self.fit_param_dict['sigma_hi_1']
                # now the smallest is at place _1 make now shure that _2 _3 is in increasing order
                if self.fit_param_dict['mu_hi_2'] > self.fit_param_dict['mu_hi_3']:  # second is bigger than thierd
                    self.fit_param_dict['amp_hi_2'], self.fit_param_dict['amp_hi_3'] = \
                        self.fit_param_dict['amp_hi_3'], self.fit_param_dict['amp_hi_2']
                    self.fit_param_dict['amp_hi_2_err'], self.fit_param_dict['amp_hi_3_err'] = \
                        self.fit_param_dict['amp_hi_3_err'], self.fit_param_dict['amp_hi_2_err']
                    self.line_flux_dict['flux_hi_2'], self.line_flux_dict['flux_hi_3'] = \
                        self.line_flux_dict['flux_hi_3'], self.line_flux_dict['flux_hi_2']
                    self.fit_param_dict['mu_hi_2'], self.fit_param_dict['mu_hi_3'] = \
                        self.fit_param_dict['mu_hi_3'], self.fit_param_dict['mu_hi_2']
                    self.fit_param_dict['sigma_hi_2'], self.fit_param_dict['sigma_hi_3'] = \
                        self.fit_param_dict['sigma_hi_3'], self.fit_param_dict['sigma_hi_2']

    def fit_model2data(self, model, init_param_dict, lim_param_dict, x_data, y_data, y_data_err=None, sort_peaks=True):

        x2r = Chi2Regression(model, x=x_data, y=y_data, error=y_data_err)

        # initialize minimizer with initial parameters
        m = Minuit(x2r, **init_param_dict)

        # add limits
        for variabale_name in init_param_dict.keys():
            # print(lim_param_dict['limit_' + variabale_name])
            m.limits[variabale_name] = lim_param_dict['limit_' + variabale_name]
            # m.error[variabale_name] = fit_parameters_lim['error_' + variabale_name]
            # print(variabale_name)


        m.migrad()
        print(m)

        # # fit
        # fit_status, fit_param = m.migrad()

        # get fit parameter
        if self.fit_param_dict is None:
            self.fit_param_dict = {}

        # calculate chi2 and ndof
        self.fit_param_dict.update({'chi2_%s' % self.fit_mode:  m.fval,
                                    'ndf_%s' % self.fit_mode: (len(x_data) - len(describe(x2r)))})
        # get fit parameters and errors
        self.fill_fit_parameter_dict(migraded_fit=m)
        # compute emission line flux
        self.get_em_flux()

        if sort_peaks & (self.n_gauss > 1):
            self.sort_fit_parameter_dict()

